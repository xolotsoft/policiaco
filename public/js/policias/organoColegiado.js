$(document).ready(function () {
    table();
    $('#btnAction').on('click', function(e){
        if ($('#btnAction').hasClass('btn-success')) {
            $.ajax({
                url: path()+'/policias/colegiado/create',
                type: 'GET',
            })
            .done(function(response){
                $('#btnAction').removeClass('btn-success');
                $('#btnAction').addClass('btn-warning');
                $('#btnAction').html('Regresar');
                $('#contenido').html(response);
                form();
                $('form').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $('#submit').prop('disabled', true);
                    $.ajax({
                        url: path()+'/policias/colegiado',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        toastr.success('Se guardo correctamente la información');
                        $('#submit').prop('disabled', false);
                        guardando(0);
                        $('#btnAction').removeClass('btn-warning');
                        $('#btnAction').addClass('btn-success');
                        $('#btnAction').html('Nuevo');
                        $('#contenido').html(response);
                        table();
                    })
                    .fail(function(response) {
                        errores(response);
                        $('#submit').prop('disabled', false);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#contenido').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        } else {
            $.ajax({
                url: path()+'/policias/colegiado',
                type: 'GET',
            })
            .done(function(response){
                $('#btnAction').removeClass('btn-warning');
                $('#btnAction').addClass('btn-success');
                $('#btnAction').html('Nuevo');
                $('#contenido').html(response);
                table();
            })
            .fail(function() {
                $('#contenido').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        }
    });
});

var edit = function(id) {
    $.ajax({
        url: path()+'/policias/colegiado/'+id+'/edit',
        type: 'GET',
    })
    .done(function(response){
        $('#btnAction').removeClass('btn-success');
        $('#btnAction').addClass('btn-warning');
        $('#btnAction').html('Regresar');
        $('#contenido').html(response);
        form();
        $('#editar').on('submit', function(event) {
            event.preventDefault();
            cleanErrores();
            guardando(1);
            $('#submit').prop('disabled', true);
            $.ajax({
                url: path()+'/policias/colegiado/'+id,
                type: 'POST',
                contentType: false,
                processData: false,
                data: new FormData(this)
            })
            .done(function(response) {
                toastr.success('Se guardo correctamente la información');
                $('#submit').prop('disabled', false);
                guardando(0);
                $('#btnAction').removeClass('btn-warning');
                $('#btnAction').addClass('btn-success');
                $('#btnAction').html('Nuevo');
                $('#contenido').html(response);
                table();
            })
            .fail(function(response) {
                errores(response);
                $('#submit').prop('disabled', false);
                guardando(0);
            });
        })
    })
    .fail(function() {
        $('#contenido').html("Error de conexión. Revise su conexíon a internet y recargue la página");
    });
}

var show = function(id) {
    $.ajax({
        url: path()+'/policias/colegiado/'+id,
        type: 'GET',
    })
    .done(function(response){
        $('#btnAction').removeClass('btn-success');
        $('#btnAction').addClass('btn-warning');
        $('#btnAction').html('Regresar');
        $('#contenido').html(response);
        form();
    })
    .fail(function() {
        $('#contenido').html("Error de conexión. Revise su conexíon a internet y recargue la página");
    });
}
