$(document).ready(function () {
    table();
    $(document.body).on("change","#estado",function(){
        if(this.value !== '') {
            $('#municipio').html('<option value=""><span class="sr-only">Cargando...</span></option>')
            // $('#municipio').html('<option value="">Selecciona una opción</option>')
            $('#colonia').html('<option value="">Selecciona una opción</option>')
            $('#municipio').prop('disabled', true)
            $('#colonia').prop('disabled', true)
            $.ajax({
                url: path()+'/listados/municipios/'+this.value,
                type: 'GET',
            })
            .done(function(response){
                var res = JSON.parse(response);
                $('#municipio').html('<option value="">Selecciona una opción</option>')
                res.map(function(e, i){
                    $('#municipio').append('<option value="'+e.ID+'">'+e.Name+'</option>')
                })
                $('#municipio').prop('disabled', false)
            })
        } else {
            $('#municipio').html('<option value="" disabled>Selecciona una opción</option>');
            $('#colonia').html('<option value="" disabled>Selecciona una opción</option>');
        }
    });
    $(document.body).on("change","#municipio",function(){
        if(this.value !== '') {
            $('#colonia').html('<option value=""><span class="sr-only">Cargando...</span></option>')
            $('#colonia').prop('disabled', true)
            $.ajax({
                url: path()+'/listados/colonias/'+this.value,
                type: 'GET',
            })
            .done(function(response){
                var res = JSON.parse(response);
                $('#colonia').html('<option value="">Selecciona una opción</option>')
                res.map(function(e, i){
                    $('#colonia').append('<option value="'+e.ID+'">'+e.Name+'</option>')
                })
                $('#colonia').append('<option value="NEW">Otro</option>');
                $('#colonia').prop('disabled', false)
            })
        }
    });
    $(document.body).on("change","#separacion",function(){
        if(this.value !== '') {
            $('#tipoSeparacion').html('<option value=""><span class="sr-only">Cargando...</span></option>')
            $('#tipoSeparacion').prop('disabled', true)
            $.ajax({
                url: path()+'/listados/tipoSeparacion/'+this.value,
                type: 'GET',
            })
            .done(function(response){
                var res = JSON.parse(response);
                $('#tipoSeparacion').html('<option value="">Selecciona una opción</option>')
                res.map(function(e, i){
                    $('#tipoSeparacion').append('<option value="'+e.id+'">'+e.nombre+'</option>')
                })
                $('#tipoSeparacion').prop('disabled', false)
            })
        }
    });
});

function edit(id) {
	loader();
	$.ajax({
		url: path()+'/policias/nuevo/'+id,
		type: 'GET'
	})
	.done(function(response) {
        $.ajax({
            url: path()+'/aspirantes/fotografia',
            type: 'GET',
        })
        .done(function(response){
            $(".fotografia").attr("src", response);
        })
        .fail(function() {
            $(".fotografia").attr("src", path()+"/img/persona.png");
        });
        cambio(response);
        $(".nav.nav-tabs a[data-toggle=tab]").parent().removeClass("disabled");
        form();
        $('#wizard').on('submit', function(event) {
            event.preventDefault();
            cleanErrores();
            guardando(1);
            $.ajax({
                url: path()+'/policias/personales',
                type: 'POST',
                contentType: false,
                processData: false,
                data: new FormData(this)
            })
            .done(function(response) {
                guardando(0);
                toastr.success('Se guardo correctamente la información');
                $('#home-tab').click();
            })
            .fail(function(response) {
                errores(response);
                guardando(0);
            });
        })

// Rutas para los tabs editar aspirante
        $('#home-tab').on('click', function() {
            cleanTabs();
            loader('#tab_content1');
            $.ajax({
                url: path()+'/policias/personales',
                type: 'GET',
            })
            .done(function(response){
                $('#tab_content1').html(response);
                form();
                $('#wizard').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $.ajax({
                        url: path()+'/policias/personales',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        guardando(0);
                        toastr.success('Se guardo correctamente la información');
                        $('#home-tab').click();
                    })
                    .fail(function(response) {
                        errores(response);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#tab_content1').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        })

        $('#profile-tab2').on('click', function() {
            cleanTabs();
            loader('#tab_content2');
            $.ajax({
                url: path()+'/policias/domicilio',
                type: 'GET',
            })
            .done(function(response){
                $('#tab_content2').html(response);
                form();
                $('#domicilio').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $.ajax({
                        url: path()+'/policias/domicilio',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        guardando(0);
                        toastr.success('Se guardo correctamente la información');
                        $('#profile-tab2').click();
                    })
                    .fail(function(response) {
                        errores(response);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#tab_content2').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        })

        $('#profile-tab3').on('click', function() {
            cleanTabs();
            loader('#tab_content3');
            $.ajax({
                url: path()+'/policias/familiares',
                type: 'GET',
            })
            .done(function(response){
                $('#tab_content3').html(response);
                form();
                $('#familiares').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $.ajax({
                        url: path()+'/policias/familiares',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        guardando(0);
                        toastr.success('Se guardo correctamente la información');
                        $('#profile-tab3').click();
                    })
                    .fail(function(response) {
                        errores(response);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#tab_content3').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        })

        $('#profile-tab4').on('click', function() {
            cleanTabs();
            loader('#tab_content4');
            $.ajax({
                url: path()+'/policias/datosActividad',
                type: 'GET',
            })
            .done(function(response){
                $('#tab_content4').html(response);
                form();
                $(document.body).on("change","#cargo",function(){
                    if(this.value !== '') {
                        $('#gradoPolicial').html('<option value=""><span class="sr-only">Cargando...</span></option>')
                        $('#gradoPolicial').prop('disabled', true)
                        $.ajax({
                            url: path()+'/listados/gradoPolicial/'+this.value,
                            type: 'GET',
                        })
                        .done(function(response){
                            var res = JSON.parse(response);
                            $('#gradoPolicial').html('<option value="">Selecciona una opción</option>')
                            res.map(function(e, i){
                                $('#gradoPolicial').append('<option value="'+e.id+'">'+e.Rank+'</option>')
                            })
                            $('#gradoPolicial').prop('disabled', false)
                        })
                    } else {
                        $('#gradoPolicial').html('<option value="" disabled>Selecciona una opción</option>');
                    }
                });
                $('#datosActividad').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $.ajax({
                        url: path()+'/policias/datosActividad',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        guardando(0);
                        toastr.success('Se guardo correctamente la información');
                        $('#profile-tab4').click();
                    })
                    .fail(function(response) {
                        errores(response);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#tab_content4').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        })

        $('#profile-tab5').on('click', function() {
            cleanTabs();
            loader('#tab_content5');
            $.ajax({
                url: path()+'/policias/promociones',
                type: 'GET',
            })
            .done(function(response){
                $('#tab_content5').html(response);
                form();
                $(document.body).on("change","#cargo",function(){
                    if(this.value !== '') {
                        $('#rango').html('<option value=""><span class="sr-only">Cargando...</span></option>')
                        $('#rango').prop('disabled', true)
                        $.ajax({
                            url: path()+'/listados/gradoPolicial/'+this.value,
                            type: 'GET',
                        })
                        .done(function(response){
                            var res = JSON.parse(response);
                            $('#rango').html('<option value="">Selecciona una opción</option>')
                            res.map(function(e, i){
                                $('#rango').append('<option value="'+e.id+'">'+e.Rank+'</option>')
                            })
                            $('#rango').prop('disabled', false)
                        })
                    } else {
                        $('#rango').html('<option value="" disabled>Selecciona una opción</option>');
                    }
                });
                $('#promociones').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $.ajax({
                        url: path()+'/policias/promociones',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        guardando(0);
                        toastr.success('Se guardo correctamente la información');
                        $('#profile-tab5').click();
                    })
                    .fail(function(response) {
                        errores(response);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#tab_content5').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        })

        $('#profile-tab6').on('click', function() {
            cleanTabs();
            loader('#tab_content6');
            $.ajax({
                url: path()+'/policias/herramientas',
                type: 'GET',
            })
            .done(function(response){
                $('#tab_content6').html(response);
                form();
                $('#herramientas').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $.ajax({
                        url: path()+'/policias/herramientas',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        guardando(0);
                        toastr.success('Se guardo correctamente la información');
                        $('#profile-tab6').click();
                    })
                    .fail(function(response) {
                        errores(response);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#tab_content6').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        })

        $('#profile-tab7').on('click', function() {
            cleanTabs();
            loader('#tab_content7');
            $.ajax({
                url: path()+'/policias/sanciones',
                type: 'GET',
            })
            .done(function(response){
                $('#tab_content7').html(response);
                form();
                $('#tipoSanciones').on('change', function(e) {
                    if ($(this).val() == 35) {
                        $('#fechasSanciones').css('display', 'block')
                    } else {
                        $('#fechasSanciones').css('display', 'none')
                    }
                });
                $('#sanciones').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $.ajax({
                        url: path()+'/policias/sanciones',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        guardando(0);
                        toastr.success('Se guardo correctamente la información');
                        $('#profile-tab7').click();
                    })
                    .fail(function(response) {
                        errores(response);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#tab_content7').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        })

        $('#profile-tab8').on('click', function() {
            cleanTabs();
            loader('#tab_content8');
            $.ajax({
                url: path()+'/policias/adscripcion',
                type: 'GET',
            })
            .done(function(response){
                $('#tab_content8').html(response);
                form();
                $('#adscripcion').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $.ajax({
                        url: path()+'/policias/adscripcion',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        guardando(0);
                        toastr.success('Se guardo correctamente la información');
                        $('#profile-tab8').click();
                    })
                    .fail(function(response) {
                        errores(response);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#tab_content8').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        })

        $('#profile-tab9').on('click', function() {
            cleanTabs();
            loader('#tab_content9');
            $.ajax({
                url: path()+'/policias/estimulos',
                type: 'GET',
            })
            .done(function(response){
                $('#tab_content9').html(response);
                form();
                $('#estimulos').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $.ajax({
                        url: path()+'/policias/estimulos',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        guardando(0);
                        toastr.success('Se guardo correctamente la información');
                        $('#profile-tab9').click();
                    })
                    .fail(function(response) {
                        errores(response);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#tab_content9').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        })

        $('#profile-tab10').on('click', function() {
            cleanTabs();
            loader('#tab_content10');
            $.ajax({
                url: path()+'/policias/planIndividual',
                type: 'GET',
            })
            .done(function(response){
                $('#tab_content10').html(response);
                form();
                $('#planIndividual').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $.ajax({
                        url: path()+'/policias/planIndividual',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        guardando(0);
                        toastr.success('Se guardo correctamente la información');
                        $('#profile-tab10').click();
                    })
                    .fail(function(response) {
                        errores(response);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#tab_content10').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        })

        $('#profile-tab11').on('click', function() {
            cleanTabs();
            loader('#tab_content11');
            $.ajax({
                url: path()+'/policias/administracion',
                type: 'GET',
            })
            .done(function(response){
                $('#tab_content11').html(response);
                form();
                $('#administracion').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $.ajax({
                        url: path()+'/policias/administracion',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        guardando(0);
                        toastr.success('Se guardo correctamente la información');
                        $('#profile-tab11').click();
                    })
                    .fail(function(response) {
                        errores(response);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#tab_content11').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        })

        $('#profile-tab12').on('click', function() {
            cleanTabs();
            loader('#tab_content12');
            $.ajax({
                url: path()+'/policias/separacion',
                type: 'GET',
            })
            .done(function(response){
                $('#tab_content12').html(response);
                form();
                $('#separacionform').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $.ajax({
                        url: path()+'/policias/separacion',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        guardando(0);
                        toastr.success('Se guardo correctamente la información');
                        $('#profile-tab12').click();
                    })
                    .fail(function(response) {
                        errores(response);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#tab_content12').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        })

        $('#profile-tab13').on('click', function() {
            cleanTabs();
            loader('#tab_content13');
            $.ajax({
                url: path()+'/policias/evaluaciones',
                type: 'GET',
            })
            .done(function(response){
                $('#tab_content13').html(response);
                form();
                $('#evaluaciones').on('submit', function(event) {
                    event.preventDefault();
                    cleanErrores();
                    guardando(1);
                    $.ajax({
                        url: path()+'/policias/evaluaciones',
                        type: 'POST',
                        contentType: false,
                        processData: false,
                        data: new FormData(this)
                    })
                    .done(function(response) {
                        guardando(0);
                        toastr.success('Se guardo correctamente la información');
                        $('#profile-tab13').click();
                    })
                    .fail(function(response) {
                        errores(response);
                        guardando(0);
                    });
                })
            })
            .fail(function() {
                $('#tab_content13').html("Error de conexión. Revise su conexíon a internet y recargue la página");
            });
        })

// FIN de las rutas de tabs
	})
	.fail(function() {
		console.log("error");
	});
}
// Acciones para familiares
var editFamiliar = function(id) {
    $.ajax({
        url: path()+'/familiar/editar/'+id,
		type: 'GET',
        success: function (response) {
            $('#formFamiliares').html(response);
            updateFamiliar();
        }
    });
}

var updateFamiliar = function() {
    $('#editarFamiliares').on('submit', function(e){
        e.preventDefault();
        cleanErrores();
        guardando(1);
        $.ajax({
            url: path()+'/familiar/actualizar',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this)
        })
        .done(function(response) {
            guardando(0);
            toastr.success(response);
            $('#profile-tab3').click();
        })
        .fail(function(response) {
            errores(response);
            guardando(0);
        });
    });
}

var destroyFamiliar = function(id) {
    swal({
        title: '¿Estas Seguro?',
        text: "Tu no podras recuperarlo!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, borrarlo!',
        cancelButtonText: 'Cancelar'
      }).then(function () {
        $.ajax({
                url: path()+'/familiar/eliminar/'+id,
                type: 'DELETE'
            })
            .done(function(response) {
                toastr.success(response);
                $('#profile-tab3').click();
            })
            .fail(function() {
                console.log("error");
            });
      })
}
// FIN Acciones para familiares

//region Familiar
var editFamiliar = function(id) {
    $.ajax({
        url: path()+'/familiar/editar/'+id,
		type: 'GET',
        success: function (response) {
            $('#formFamiliares').html(response);
            updateFamiliar();
        }
    });
}
var updateFamiliar = function() {
    $('#editarFamiliares').on('submit', function(e){
        e.preventDefault();
        cleanErrores();
        guardando(1);
        $.ajax({
            url: path()+'/familiar/actualizar',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this)
        })
        .done(function(response) {
            guardando(0);
            toastr.success(response);
            $('#profile-tab3').click();
        })
        .fail(function(response) {
            errores(response);
            guardando(0);
        });
    });
}
var destroyFamiliar = function(id) {
    swal({
        title: '¿Estas Seguro?',
        text: "Tu no podras recuperarlo!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, borrarlo!',
        cancelButtonText: 'Cancelar'
      }).then(function () {
        $.ajax({
                url: path()+'/familiar/eliminar/'+id,
                type: 'DELETE'
            })
            .done(function(response) {
                toastr.success(response);
                $('#profile-tab3').click();
            })
            .fail(function() {
                console.log("error");
            });
      })
}
//endregion
//region Plan
var editplan = function(id) {
    $.ajax({
        url: path()+'/plan/editar/'+id,
		type: 'GET',
        success: function (response) {
            $('#formplanes').html(response);
            form();
            updateplan();
        }
    });
}
var updateplan = function() {
    $('#editarplanes').on('submit', function(e){
        e.preventDefault();
        cleanErrores();
        guardando(1);
        $.ajax({
            url: path()+'/plan/actualizar',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this)
        })
        .done(function(response) {
            guardando(0);
            toastr.success(response);
            $('#profile-tab10').click();
        })
        .fail(function(response) {
            errores(response);
            guardando(0);
        });
    });
}
var destroyplan = function(id) {
    swal({
        title: '¿Estas Seguro?',
        text: "Tu no podras recuperarlo!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, borrarlo!',
        cancelButtonText: 'Cancelar'
      }).then(function () {
        $.ajax({
                url: path()+'/plan/eliminar/'+id,
                type: 'DELETE'
            })
            .done(function(response) {
                toastr.success(response);
                $('#profile-tab10').click();
            })
            .fail(function() {
                console.log("error");
            });
      })
}
//endregion
//region Herramientas
var editherramienta = function(id) {
    $.ajax({
        url: path()+'/herramienta/editar/'+id,
		type: 'GET',
        success: function (response) {
            $('#formherramientas').html(response);
            form();
            updateherramienta();
        }
    });
}
var updateherramienta = function() {
    $('#editarherramientas').on('submit', function(e){
        e.preventDefault();
        cleanErrores();
        guardando(1);
        $.ajax({
            url: path()+'/herramienta/actualizar',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this)
        })
        .done(function(response) {
            guardando(0);
            toastr.success(response);
            $('#profile-tab6').click();
        })
        .fail(function(response) {
            errores(response);
            guardando(0);
        });
    });
}
var destroyherramienta = function(id) {
    swal({
        title: '¿Estas Seguro?',
        text: "Tu no podras recuperarlo!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, borrarlo!',
        cancelButtonText: 'Cancelar'
      }).then(function () {
        $.ajax({
                url: path()+'/herramienta/eliminar/'+id,
                type: 'DELETE'
            })
            .done(function(response) {
                toastr.success(response);
                $('#profile-tab6').click();
            })
            .fail(function() {
                console.log("error");
            });
      })
}
//endregion
//region Promocion
var editpromocion = function(id) {
    $.ajax({
        url: path()+'/promocion/editar/'+id,
		type: 'GET',
        success: function (response) {
            $('#formpromociones').html(response);
            form();
            updatepromocion();
        }
    });
}
var updatepromocion = function() {
    $('#editarpromociones').on('submit', function(e){
        e.preventDefault();
        cleanErrores();
        guardando(1);
        $.ajax({
            url: path()+'/promocion/actualizar',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this)
        })
        .done(function(response) {
            guardando(0);
            toastr.success(response);
            $('#profile-tab5').click();
        })
        .fail(function(response) {
            errores(response);
            guardando(0);
        });
    });
}
var destroypromocion = function(id) {
    swal({
        title: '¿Estas Seguro?',
        text: "Tu no podras recuperarlo!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, borrarlo!',
        cancelButtonText: 'Cancelar'
      }).then(function () {
        $.ajax({
                url: path()+'/promocion/eliminar/'+id,
                type: 'DELETE'
            })
            .done(function(response) {
                toastr.success(response);
                $('#profile-tab5').click();
            })
            .fail(function() {
                console.log("error");
            });
      })
}
//endregion
//region Sancion
var editsancion = function(id) {
    $.ajax({
        url: path()+'/sancion/editar/'+id,
		type: 'GET',
        success: function (response) {
            $('#formsanciones').html(response);
            form();
            if ($('#tipoSanciones').val() == 35) {
                $('#fechasSanciones').css('display', 'block')
            } else {
                $('#fechasSanciones').css('display', 'none')
            }
            $('#tipoSanciones').on('change', function(e) {
                if ($(this).val() == 35) {
                    $('#fechasSanciones').css('display', 'block')
                } else {
                    $('#fechasSanciones').css('display', 'none')
                }
            });
            updatesancion();
        }
    });
}
var updatesancion = function() {
    $('#editarsanciones').on('submit', function(e){
        e.preventDefault();
        cleanErrores();
        guardando(1);
        $.ajax({
            url: path()+'/sancion/actualizar',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this)
        })
        .done(function(response) {
            guardando(0);
            toastr.success(response);
            $('#profile-tab7').click();
        })
        .fail(function(response) {
            errores(response);
            guardando(0);
        });
    });
}
var destroysancion = function(id) {
    swal({
        title: '¿Estas Seguro?',
        text: "Tu no podras recuperarlo!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, borrarlo!',
        cancelButtonText: 'Cancelar'
      }).then(function () {
        $.ajax({
                url: path()+'/sancion/eliminar/'+id,
                type: 'DELETE'
            })
            .done(function(response) {
                toastr.success(response);
                $('#profile-tab7').click();
            })
            .fail(function() {
                console.log("error");
            });
      })
}
//endregion
//region Adscripcion
var editadscripcion = function(id) {
    $.ajax({
        url: path()+'/adscripcion/editar/'+id,
		type: 'GET',
        success: function (response) {
            $('#formadscripcion').html(response);
            form();
            updateadscripcion();
        }
    });
}
var updateadscripcion = function() {
    $('#editaradscripcion').on('submit', function(e){
        e.preventDefault();
        cleanErrores();
        guardando(1);
        $.ajax({
            url: path()+'/adscripcion/actualizar',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this)
        })
        .done(function(response) {
            guardando(0);
            toastr.success(response);
            $('#profile-tab8').click();
        })
        .fail(function(response) {
            errores(response);
            guardando(0);
        });
    });
}
var destroyadscripcion = function(id) {
    swal({
        title: '¿Estas Seguro?',
        text: "Tu no podras recuperarlo!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, borrarlo!',
        cancelButtonText: 'Cancelar'
      }).then(function () {
        $.ajax({
                url: path()+'/adscripcion/eliminar/'+id,
                type: 'DELETE'
            })
            .done(function(response) {
                toastr.success(response);
                $('#profile-tab8').click();
            })
            .fail(function() {
                console.log("error");
            });
      })
}
//endregion
//region Estimulo
var editestimulo = function(id) {
    $.ajax({
        url: path()+'/estimulo/editar/'+id,
		type: 'GET',
        success: function (response) {
            $('#formestimulos').html(response);
            form();
            updateestimulo();
        }
    });
}
var updateestimulo = function() {
    $('#editarestimulos').on('submit', function(e){
        e.preventDefault();
        cleanErrores();
        guardando(1);
        $.ajax({
            url: path()+'/estimulo/actualizar',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this)
        })
        .done(function(response) {
            guardando(0);
            toastr.success(response);
            $('#profile-tab9').click();
        })
        .fail(function(response) {
            errores(response);
            guardando(0);
        });
    });
}
var destroyestimulo = function(id) {
    swal({
        title: '¿Estas Seguro?',
        text: "Tu no podras recuperarlo!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, borrarlo!',
        cancelButtonText: 'Cancelar'
      }).then(function () {
        $.ajax({
                url: path()+'/estimulo/eliminar/'+id,
                type: 'DELETE'
            })
            .done(function(response) {
                toastr.success(response);
                $('#profile-tab9').click();
            })
            .fail(function() {
                console.log("error");
            });
      })
}
//endregion
//region Administrativa
var editadministrativa = function(id) {
    $.ajax({
        url: path()+'/administrativa/editar/'+id,
		type: 'GET',
        success: function (response) {
            $('#formadministrativa').html(response);
            form();
            updateadministrativa();
        }
    });
}
var updateadministrativa = function() {
    $('#editaradministrativa').on('submit', function(e){
        e.preventDefault();
        cleanErrores();
        guardando(1);
        $.ajax({
            url: path()+'/administrativa/actualizar',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this)
        })
        .done(function(response) {
            guardando(0);
            toastr.success(response);
            $('#profile-tab11').click();
        })
        .fail(function(response) {
            errores(response);
            guardando(0);
        });
    });
}
var destroyadministrativa = function(id) {
    swal({
        title: '¿Estas Seguro?',
        text: "Tu no podras recuperarlo!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, borrarlo!',
        cancelButtonText: 'Cancelar'
      }).then(function () {
        $.ajax({
                url: path()+'/administrativa/eliminar/'+id,
                type: 'DELETE'
            })
            .done(function(response) {
                toastr.success(response);
                $('#profile-tab11').click();
            })
            .fail(function() {
                console.log("error");
            });
      })
}
//endregion
//region Evaluacion
