var nombramiento = function(id, folio) {
    $('input[name="idAspirante"]').val(id);
    $('#folioNombramiento').html(folio);
    $("#apto-id").modal();
}
var cerrarModal = function() {
    cleanErrores();
    guardando(0);
    $("#apto-id").modal("hide");
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
    $('input[name="idAspirante"]').val("");
    $('#folioNombramiento').html("");
}
$(document).ready(function () {
	table();
});
$('#nombrar').on('submit', function(event) {
    event.preventDefault();
    cleanErrores();
    guardando(1);
    $.ajax({
        url: path()+'/aspirantes/incorporar',
        type: 'POST',
        contentType: false,
        processData: false,
        data: new FormData(this)
    })
    .done(function(response) {
        guardando(0);
        $("#apto-id").modal("hide");
        $('input[name="idAspirante"]').val("");
        $('#folioNombramiento').html("");
        toastr.success('Se guardo correctamente la información');
        $('#contenido').html(response);
        table();
    })
    .fail(function(response) {
        errores(response);
        guardando(0);
    });
})
