$(document).ready(function () {
    $("#exportPDF").hide();
    $("#regresar").hide();
    table();
});
var show = function(id){
    $.ajax({
        url: path()+'/reportes/aspirantes/detalleKardex/'+id,
        type: 'GET'
    })
    .done(function(response) {
        $('#contenido').html(response);
        $("#exportPDF").show();
        $("#exportPDF").attr("data-section", "aspirantesKardex/"+id);
        $("#regresar").show().on('click', function() {
            $.ajax({
                url: path()+'/reportes/aspirantes/listadoKardex',
                type: 'GET'
            })
            .done(function(response) {
                $('#contenido').html(response);
                table();
                $("#exportPDF").hide();
                $("#regresar").hide();
            })
            .fail(function() {
              console.log("error");
            });
        });
        exportar();
    })
    .fail(function() {
      console.log("error");
    });
}
