$(document).ready(function () {
    $("#exportPDF").hide();
    $("#regresar").hide();
    table();
});
var show = function(id){
    $.ajax({
        url: path()+'/reportes/aspirantes/detalleControl/'+id,
        type: 'GET'
    })
    .done(function(response) {
        $('#contenido').html(response);
        table();
        $("#exportPDF").show();
        $("#exportPDF").attr("data-section", "aspirantesControl/"+id);
        $("#regresar").show().on('click', function() {
            $.ajax({
                url: path()+'/reportes/aspirantes/listadoControl',
                type: 'GET'
            })
            .done(function(response) {
                $('#contenido').html(response);
                table();
                $("#exportPDF").hide();
                $("#regresar").hide();
            })
            .fail(function() {
              console.log("error");
            });
        });
        exportar();
    })
    .fail(function() {
      console.log("error");
    });
}
