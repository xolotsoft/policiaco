$(document).ready(function () {
    form();
    $('#contraerFiltros').on('click', function() {
        if ($(this).find('span i.fa-inverse').hasClass('fa-rotate-180')) {
            $(this).find('span i.fa-inverse').removeClass('fa-rotate-180');
        } else {
            $(this).find('span i.fa-inverse').addClass('fa-rotate-180');
        }
        $('a.collapse-link').click();
    })
    $('#filtrar').on('click', function() {
        var edad = $('#edad').val();
        var estadoCivil = $('#estadoCivil').val();
        var lugarNacimiento = $('#lugarNacimiento').val();
        var cargoActividad = $('#cargoActividad').val();
        var gradoPolicialActividad = $('#gradoPolicialActividad').val();
        var puestoActividad = $('#puestoActividad').val();
        var areaAdscripcionActividad = $('#areaAdscripcionActividad').val();
        var areaProcedencia = $('#areaProcedencia').val();
        var areaDesignacion = $('#areaDesignacion').val();
        var fechaFinAdscripcion = $('#fechaFinAdscripcion').val();
        var fechaInicioAdscripcion = $('#fechaInicioAdscripcion').val();
        var examenControl = $('#examenControl').val();
        var fechaResultadosControl = $('#fechaResultadosControl').val();
        var resultadoControl = $('#resultadoControl').val();
        var tipoRecompensa = $('#tipoRecompensa').val();
        var motivoEstimulo = $('#motivoEstimulo').val();
        var premioEstimulo = $('#premioEstimulo').val();
        var tipoHerramienta = $('#tipoHerramienta').val();
        var fechaRecepcionHerramienta = $('#fechaRecepcionHerramienta').val();
        var fechaDevolucionHerramienta = $('#fechaDevolucionHerramienta').val();
        var nombrePlan = $('#nombrePlan').val();
        var caracteristicasPlan = $('#caracteristicasPlan').val();
        var instanciaPlan = $('#instanciaPlan').val();
        var gradoImportanciaPlan = $('#gradoImportanciaPlan').val();
        var resultadoPlan = $('#resultadoPlan').val();
        var cargoPromocion = $('#cargoPromocion').val();
        var gradoPolicialPromocion = $('#gradoPolicialPromocion').val();
        var tipoSanciones = $('#tipoSanciones').val();
        var causaSanciones = $('#causaSanciones').val();
        var fechaSanciones = $('#fechaSanciones').val();
        var tipoSeparacion = $('#tipoSeparacion').val();
        var fechaSeparacion = $('#fechaSeparacion').val();
        var motivoSeparacion = $('#motivoSeparacion').val();
        $.ajax({
            url: path()+'/reportes/graficados/activos',
            type: 'POST',
            data: {
                edad: edad,
                estadoCivil: estadoCivil,
                lugarNacimiento: lugarNacimiento,
                cargoActividad: cargoActividad,
                gradoPolicialActividad: gradoPolicialActividad,
                puestoActividad: puestoActividad,
                areaAdscripcionActividad: areaAdscripcionActividad,
                areaProcedencia: areaProcedencia,
                areaDesignacion: areaDesignacion,
                fechaFinAdscripcion: fechaFinAdscripcion,
                fechaInicioAdscripcion: fechaInicioAdscripcion,
                examenControl: examenControl,
                fechaResultadosControl: fechaResultadosControl,
                resultadoControl: resultadoControl,
                tipoRecompensa: tipoRecompensa,
                motivoEstimulo: motivoEstimulo,
                premioEstimulo: premioEstimulo,
                tipoHerramienta: tipoHerramienta,
                fechaRecepcionHerramienta: fechaRecepcionHerramienta,
                fechaDevolucionHerramienta: fechaDevolucionHerramienta,
                nombrePlan: nombrePlan,
                caracteristicasPlan: caracteristicasPlan,
                instanciaPlan: instanciaPlan,
                gradoImportanciaPlan: gradoImportanciaPlan,
                resultadoPlan: resultadoPlan,
                cargoPromocion: cargoPromocion,
                gradoPolicialPromocion: gradoPolicialPromocion,
                tipoSanciones: tipoSanciones,
                causaSanciones: causaSanciones,
                fechaSanciones: fechaSanciones,
                tipoSeparacion: tipoSeparacion,
                fechaSeparacion: fechaSeparacion,
                motivoSeparacion: motivoSeparacion
            }
        })
        .done(function(response) {
            console.log(response.resultado)
            var datos = [response.total,response.resultado]
            myDoughnutChart.data.datasets.forEach((dataset) => {
                dataset.data = datos;
            });
            myDoughnutChart.update();
        })
        .fail(function(response) {
            //
        });
    })
    var ctx = document.getElementById("myChart").getContext('2d');
    var myDoughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
          labels: ["Resto", "Filtrados"],
          datasets: [
            {
              label: "Policias (porcentaje)",
              backgroundColor: ["#8e5ea2","#3cba9f"],
              data: [1,0]
            }
          ]
        },
        options: {
            responsive: true,
          title: {
            display: true,
            text: 'S P C P'
          }
        }
    });
});
