$(document).ready(function () {
    table();
});

function index() {
    loader();
    $.ajax({
        url: path()+'/catalogos/evaluacion',
        type: 'GET'
    })
    .done(function(response) {
        cambio(response);
        table();
    })
    .fail(function() {
        cambio("Error de conexión. Revise su conexíon a internet y recargue la página");
    });
}

function create(){
    loader();
    $.ajax({
        url: path()+'/catalogos/evaluacion/create',
        type: 'GET'
    })
    .done(function(response) {
        cambio(response);
        form();
        $('form').on('submit', function(event) {
            event.preventDefault();
            cleanErrores();
            guardando(1);
            $('#submit').prop('disabled', true);
            $.ajax({
                url: path()+'/catalogos/evaluacion',
                type: 'POST',
                contentType: false,
                processData: false,
                data: new FormData(this)
            })
            .done(function(response) {
                toastr.success('Se guardo correctamente la información');
                $('#submit').prop('disabled', false);
                guardando(0);
                cambio(response);
                table();
            })
            .fail(function(response) {
                errores(response);
                $('#submit').prop('disabled', false);
                guardando(0);
            });
        })
    })
    .fail(function() {
        cambio("Error de conexión. Revise su conexíon a internet y recargue la página");
    });
}

function edit(id) {
    loader();
    $.ajax({
        url: path()+'/catalogos/evaluacion/'+id+'/edit',
        type: 'GET'
    })
    .done(function(response) {
        cambio(response);
        form();
        $('#editar').on('submit', function(event) {
            event.preventDefault();
            $('#submit').prop('disabled', true);
            cleanErrores();
            guardando(1);
            $.ajax({
                url: path()+'/catalogos/evaluacion/'+id,
                type: 'POST',
                contentType: false,
                processData: false,
                data: new FormData(this)
            })
            .done(function(response) {
                toastr.success('Se guardo correctamente la información');
                $('#submit').prop('disabled', false);
                guardando(0, 'Editar');
                cambio(response);
                table();
            })
            .fail(function(response) {
                errores(response);
                $('#submit').prop('disabled', false);
                guardando(0, 'Editar');
            });
        })
    })
    .fail(function() {
        cambio("Error de conexión. Revise su conexíon a internet y recargue la página");
    });
}

function destroy(id) {
	swal({
	  title: '¿Estas Seguro?',
	  text: "¡No podras recuperarlo!",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: '¡Si, borrarlo!',
      cancelButtonText: 'Cancelar',
      showLoaderOnConfirm: true,
      allowOutsideClick: false,
      preConfirm: function () {
        return new Promise(function (resolve, reject) {
            $('.swal2-cancel.swal2-styled').hide();
            $.ajax({
                url: path()+'/catalogos/evaluacion/'+id,
                type: 'DELETE',
                success: function(response) {
                    resolve(response)
                },
                error: function(a, b, c){
                    $('.swal2-cancel.swal2-styled').show();
                    reject("Problema de conexión")
                }
            })
        })
      }
	}).then(function (response) {
        swal({
            type: 'success',
            title: '¡Eliminado!',
            html: 'Se ha eliminado correctamente!'
        }).then(function () {
            $('#contenido').html(response);
            table();
        })
    })
}

function show(id) {
    loader();
    $.ajax({
        url: path()+'/catalogos/evaluacion/'+id,
        type: 'GET'
    })
    .done(function(response) {
        cambio(response);
    })
    .fail(function() {
        cambio("Error de conexión. Revise su conexíon a internet y recargue la página");
    });
}
