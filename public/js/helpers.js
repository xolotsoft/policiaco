function form() {
    $('input[type = "checkbox"]').iCheck({
        checkboxClass: 'icheckbox_flat-green'
    });
    $("select").select2({
        minimumResultsForSearch: 6,
        width: '100%'
    });
    $("select#colonia").select2({
        minimumResultsForSearch: 6,
        width: '100%'
    }).on('select2:close', function() {
        var el = $(this);
        if(el.val()==="NEW") {
            swal({
                title: '¿Agregar Opción?',
                text: "¡que valor deseas agregar!",
                input: 'text',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Agregar!',
                cancelButtonText: 'Cancelar',
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                preConfirm: function (colonia) {
                  return new Promise(function (resolve, reject) {
                      $('.swal2-cancel.swal2-styled').hide();
                      if (colonia === '') {
                        reject('Escribe el nombre de la colonia')
                        $('.swal2-cancel.swal2-styled').show();
                      } else {
                        $.ajax({
                            url: path()+'/catalogos/coloniaNueva/'+colonia+'/'+$('#municipio').val(),
                            type: 'GET',
                            success: function(response) {
                                resolve(response)
                            },
                            error: function(a, b, c){
                                $('.swal2-cancel.swal2-styled').show();
                                console.log(a);
                                if (a.status = 510) {
                                    reject(a.responseJSON);
                                  } else {
                                      reject("Problema de conexión")
                                }
                            }
                        })
                      }
                  })
                }
            }).then(function (response) {
                swal({
                    type: 'success',
                    title: '¡Agregado!',
                    html: 'Se ha agregado correctamente!'
                }).then(function () {
                    console.log(response);
                    el.append('<option value="'+response.id+'">'+response.nombre+'</option>').val(response.id);
                })
            })
        }
    });
    $("select#serviciosMedicos").select2({
        minimumResultsForSearch: 6,
        width: '100%'
    }).on('select2:close', function() {
        var el = $(this);
        if(el.val()==="NEW") {
            swal({
                title: '¿Agregar Opción?',
                text: "¡que valor deseas agregar!",
                input: 'text',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Agregar!',
                cancelButtonText: 'Cancelar',
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                preConfirm: function (valor) {
                  return new Promise(function (resolve, reject) {
                      $('.swal2-cancel.swal2-styled').hide();
                      if (valor === '') {
                        reject('Escribe el nombre del Servicio Médico')
                        $('.swal2-cancel.swal2-styled').show();
                      } else {
                        $.ajax({
                            url: path()+'/catalogos/serviciosMedicos/'+valor+'/'+14,
                            type: 'GET',
                            success: function(response) {
                                resolve(response)
                            },
                            error: function(a, b, c){
                                $('.swal2-cancel.swal2-styled').show();
                                console.log(a);
                                if (a.status = 510) {
                                    reject(a.responseJSON);
                                  } else {
                                      reject("Problema de conexión")
                                }
                            }
                        })
                      }
                  })
                }
            }).then(function (response) {
                swal({
                    type: 'success',
                    title: '¡Agregado!',
                    html: 'Se ha agregado correctamente!'
                }).then(function () {
                    console.log(response);
                    el.append('<option value="'+response.id+'">'+response.nombre+'</option>').val(response.id);
                })
            })
        }
    });
    $("select#posgrado").select2({
        minimumResultsForSearch: 6,
        width: '100%'
    }).on('select2:close', function() {
        var el = $(this);
        if(el.val()==="NEW") {
            swal({
                title: '¿Agregar Opción?',
                text: "¡que valor deseas agregar!",
                input: 'text',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Agregar!',
                cancelButtonText: 'Cancelar',
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                preConfirm: function (posgrado) {
                  return new Promise(function (resolve, reject) {
                      $('.swal2-cancel.swal2-styled').hide();
                      if (posgrado === '') {
                        reject('Escribe el nombre del posgrado')
                        $('.swal2-cancel.swal2-styled').show();
                      } else {
                        $.ajax({
                            url: path()+'/catalogos/posgrado/'+posgrado+'/'+16,
                            type: 'GET',
                            success: function(response) {
                                resolve(response)
                            },
                            error: function(a, b, c){
                                $('.swal2-cancel.swal2-styled').show();
                                console.log(a);
                                if (a.status == 510) {
                                    reject(a.responseJSON);
                                  } else {
                                    reject("Problema de conexión")
                                }
                            }
                        })
                      }
                  })
                }
            }).then(function (response) {
                swal({
                    type: 'success',
                    title: '¡Agregado!',
                    html: 'Se ha agregado correctamente!'
                }).then(function () {
                    console.log(response);
                    el.append('<option value="'+response.id+'">'+response.nombre+'</option>').val(response.id);
                })
            })
        }
    });
    $("select#comision").select2({
        minimumResultsForSearch: 6,
        width: '100%'
    }).on('select2:close', function() {
        var el = $(this);
        if(el.val()==="NEW") {
            swal({
                title: '¿Agregar Opción?',
                text: "¡que valor deseas agregar!",
                input: 'text',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Agregar!',
                cancelButtonText: 'Cancelar',
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                preConfirm: function (comision) {
                  return new Promise(function (resolve, reject) {
                      $('.swal2-cancel.swal2-styled').hide();
                      if (comision === '') {
                        reject('Escribe el nombre de la comision')
                        $('.swal2-cancel.swal2-styled').show();
                      } else {
                        $.ajax({
                            url: path()+'/catalogos/comisionNueva/'+comision+'/25',
                            type: 'GET',
                            success: function(response) {
                                resolve(response)
                            },
                            error: function(a, b, c){
                                $('.swal2-cancel.swal2-styled').show();
                                console.log(a);
                                if (a.status = 510) {
                                    reject(a.responseJSON);
                                  } else {
                                      reject("Problema de conexión")
                                }
                            }
                        })
                      }
                  })
                }
            }).then(function (response) {
                swal({
                    type: 'success',
                    title: '¡Agregado!',
                    html: 'Se ha agregado correctamente!'
                }).then(function () {
                    console.log(response);
                    el.append('<option value="'+response.id+'">'+response.nombre+'</option>').val(response.id);
                })
            })
        }
    });
    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY',
        widgetPositioning:{
            horizontal: 'auto',
            vertical: 'bottom'
        }
    });
    $('.tooltipo').tooltipster({
        theme: 'tooltipster-noir',
        trigger: 'custom',
        side: 'right',
        delay: 1000
    });
    $('.tooltipo')
    .focus(function(){
        $(this).tooltipster('open');
    })
    .blur(function(){
        $(this).tooltipster('close');
    });
    $('#wizard').smartWizard({
        keyNavigation: true,
        enableAllSteps: false,
        transitionEffect: 'fade',
        contentCache:true, // cache step contents, if false content is fetched always from ajax url
        cycleSteps: false, // cycle step navigation
                        enableFinishButton: false, // makes finish button enabled always
                        hideFinishButton: false,
        hideButtonsOnDisabled: true, // when the previous/next/finish buttons are disabled, hide them instead
        labelNext:'Siguente', // label for Next button
        labelPrevious:'Anterior', // label for Previous button
        labelFinish:'Guardar',  // label for Finish button
        noForwardJumping: false,
        buttonOrder: ['next', 'prev']  // button order, to hide a button remove it from the list
    });
    $('#wizard_verticle').smartWizard({
        transitionEffect: 'slide'
    });
    $('.buttonNext').addClass('btn btn-primary');
    $('.buttonPrevious').addClass('btn btn-info pull-left');
    $('.buttonFinish').addClass('btn btn-success hide');
}

function table() {
    $('.datatable').DataTable({
        'searching':false,
        'lengthChange':false,
        'language':{
            'url':'//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json'
        }
    });
}

function cambio(response) {
    $('#Regresar').toggle();
    $('#contenido').html(response);
    $('#Nuevo').toggle();
}

function loader(elemento = '#contenido') {
    $(elemento).html('<div style="text-align:center;"><span class="fa fa-spinner fa-pulse fa-fw" style="font-size:150px;"></span></div>');
}

function errores(response) {
    if (response.status >= 500) {
        toastr.error("Error",{"closeButton": true});
    } else {
        if (response.responseJSON != null) {
            Object.keys(response.responseJSON).forEach(function(index) {
                for (i=0; i<response.responseJSON[index].length; i++){
                    toastr.warning(response.responseJSON[index][i],{"closeButton": true});
                    $('#'+index).addClass('error');
                    var ids2 = $('#select2-'+index+'-container');
                    ids2.parent().addClass('error');
                }
            });
        } else {
            toastr.error("Error",{"closeButton": true});
        }
    }
}
function cleanErrores() {
    $('.form-control').removeClass('error');
    $('.select2-selection.select2-selection--single').removeClass('error');
}
function cleanTabs() {
    $('#tab_content1').html('');
    $('#tab_content2').html('');
    $('#tab_content3').html('');
    $('#tab_content4').html('');
    $('#tab_content5').html('');
    $('#tab_content6').html('');
    $('#tab_content7').html('');
    $('#tab_content8').html('');
}
function path() {
    var ruta = '';
    if (document.location.hostname == 'localhost') {
        ruta = 'http://localhost/policiaco/public';
    } else {
        ruta = location.protocol;
    }
    return ruta;
}
var guardando = function(val, txt = 'Guardar') {
    if (val == 1) {
        $('button[type="submit"]').prop('disabled', true).html('guardando... <i class="fa fa-spinner fa-pulse fa-fw"></i>');
    } else {
        $('button[type="submit"]').prop('disabled', false).html(txt + ' <span class="fa fa-save"></span>');
    }
}

var exportar = function(){
  $('#exportPDF').on('click', function() {
      var section = $(this).attr('data-section');
      window.open(path()+'/export/'+section+'/pdf', '_blank');
  });
  $('#exportEXCEL').on('click', function() {
      var section = $(this).attr('data-section');
      window.open(path()+'/export/'+section+'/xlsx', '_blank');
  });
}
