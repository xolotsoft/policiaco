var mostrarMensaje = function(id) {
    $.ajax({
        url: path()+'/mensajes/mostrar/'+id,
        type: 'GET'
    })
    .done(function(response) {
        $('#contenidoMensajes').html(response);
        listado();
        countm();
    })
    .fail(function(response) {
        errores(response);
    });
}
var eliminarMensaje = function(id) {
    $.ajax({
        url: path()+'/mensajes/eliminar/'+id,
        type: 'GET'
    })
    .done(function(response) {
        toastr.success(response);
        $('#contenidoMensajes').html('<div class="col-sm-9 mail_view" style="text-align:center;"><h2>SELECCIONA UN MENSAJE</h2></div>');
        listado();
    })
    .fail(function(response) {
        errores(response);
    });
}
var responder = function(id) {
    $('#destinatario').val(id);
    $('.compose').slideToggle();
    $('#asunto').focus();
}
var listado = function() {
    $.ajax({
        url: path()+'/mensajes/listado',
        type: 'GET'
    })
    .done(function(response) {
        $('#listadoMensajes').html(response);
    })
    .fail(function(response) {
        errores(response);
    });
}
var countm = function() {
    $.ajax({
        url: path()+'/mensajes/countm',
        type: 'GET'
    })
    .done(function(response) {
        $('#countm').text(response);
    })
    .fail(function(response) {
        errores(response);
    });
}
$(document).ready(function() {
    $('#compose, .compose-close').click(function(){
        $('#destinatario').val('');
        $('.compose').slideToggle();
    });
    setInterval(function(){
        listado();
    },300000);
    $('#nuevo-mensaje').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: path()+'/mensajes/guardar',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this)
        })
        .done(function(response) {
            toastr.success(response);
            $('.compose').slideToggle();
        })
        .fail(function(response) {
            errores(response);
        });
    });
});
