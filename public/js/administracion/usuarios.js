$(document).ready(function () {
    table();
});

function index() {
    $.ajax({
        url: 'http://localhost/policiaco/public/administracion/usuarios',
        type: 'GET'
    })
    .done(function(response) {
        cambio(response);
        table();
    })
    .fail(function() {
        console.log("error");
    });
}

function create(){
    $.ajax({
        url: 'http://localhost/policiaco/public/administracion/usuarios/create',
        type: 'GET'
    })
    .done(function(response) {
        cambio(response);
        form();
        $('form').on('submit', function(event) {
            event.preventDefault();
            $('#submit').prop('disabled', true);
            $.ajax({
                url: 'http://localhost/policiaco/public/administracion/usuarios',
                type: 'POST',
                contentType: false,
                processData: false,
                data: new FormData(this)
            })
            .done(function(response) {
                toastr.success('Se guardo correctamente la información');
                $('#submit').prop('disabled', false);
                cambio(response);
            })
            .fail(function(response) {
                errores(response);
                $('#submit').prop('disabled', false);
            });
        })
    })
    .fail(function() {
        console.log("error");
    });
}

function edit(id) {
    $.ajax({
        url: 'http://localhost/policiaco/public/administracion/usuarios/'+id+'/edit',
        type: 'GET'
    })
    .done(function(response) {
        cambio(response);
        form();
        $('#editar').on('submit', function(event) {
            event.preventDefault();
            $('#submit').prop('disabled', true);
            $.ajax({
                url: 'http://localhost/policiaco/public/administracion/usuarios/'+id,
                type: 'POST',
                contentType: false,
                processData: false,
                data: new FormData(this)
            })
            .done(function(response) {
                toastr.success('Se guardo correctamente la información');
                $('#submit').prop('disabled', false);
                cambio(response);
            })
            .fail(function(response) {
                errores(response);
                $('#submit').prop('disabled', false);
            });
        })
    })
    .fail(function() {
        console.log("error");
    });
}

function destroy(id) {
    swal({
      title: '¿Estas Seguro?',
      text: "Tu no podras recuperarlo!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrarlo!',
      cancelButtonText: 'Cancelar'
    }).then(function () {
      $.ajax({
            url: 'http://localhost/policiaco/public/administracion/usuarios/'+id,
            type: 'DELETE'
        })
        .done(function(response) {
            $('#contenido').html(response);
            toastr.success('Se elimino correctamente el registro');
            table();
        })
        .fail(function() {
            console.log("error");
        });
    })
}

function show(id) {
    $.ajax({
        url: 'http://localhost/policiaco/public/administracion/usuarios/'+id,
        type: 'GET'
    })
    .done(function(response) {
        cambio(response);
    })
    .fail(function() {
        console.log("error");
    });
}
