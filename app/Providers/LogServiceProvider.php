<?php

namespace App\Providers;

use DB;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;

class LogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        dd(Auth::user());
        if (Auth::check()) {
            DB::listen(function ($query) {
                $insert = DB::connection('scpmex')->table('log')->insert([
                    'usuario' => Auth::user()->id,
                    'modulo' => 'controlador',
                    'seccion' => 'metodo',
                    'query' => query($query->sql, $query->bindings),
                    'fecha' => Carbon::now()
                ]);
            });
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
