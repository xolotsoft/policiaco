<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Module;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app('view')->composer('page', function ($view) {
            $modules = Module::all();
            $action = app('request')->route()->getAction();
            $controller = class_basename($action['controller']);
            list($controller, $action) = explode('Controller@', $controller);
            $view
            ->with(compact('controller', 'action'))
            ->with('modules', $modules);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
