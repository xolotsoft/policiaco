<?php

namespace App;

use \Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $connection = 'scpmex';
    protected $dates = ['expired_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function municipio()
    {
        return $this->hasOne('App\Municipio', 'id', 'municipio_id');
    }

    public function perfil()
    {
        return $this->hasOne('App\Perfil', 'id', 'perfil_id');
    }

    public function getExpiredAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function scopeListado($query)
    {
        return $query
        ->where('id', '>', '1')
        ->orderBy('municipio_id', 'ASC')
        ->get();
    }
}
