<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $connection = 'scpmex';
    protected $table = "log";
    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'usuario');
    }
}
