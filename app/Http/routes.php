<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middlewareGroups' => ['web']], function () {
/*------------------------------------- ARCHIVOS ------------------------------------- */
    Route::group(['prefix' => 'archivos'], function () {
        Route::get('ver/{id}', 'ArchivosController@ver');
        Route::get('descargar/{id}', 'ArchivosController@descargar');
    });
/*-------------------------------------- LOGUEO -------------------------------------- */
    Route::get('/', 'LogController@log');
    Route::post('login', 'LogController@login');
    Route::get('/logout', 'LogController@logout');
    Route::group(['middleware' => ['auth']], function () {
/*------------------------------------- MENSAJES ------------------------------------- */
        Route::group(['prefix' => 'mensajes'], function () {
            Route::get('principal', 'MensajesController@principal');
            Route::post('guardar', 'MensajesController@guardar');
            Route::get('mostrar/{id}', 'MensajesController@mostrar');
            Route::get('listado', 'MensajesController@listado');
            Route::get('eliminar/{id}', 'MensajesController@eliminar');
            Route::get('countm', 'MensajesController@countm');
        });
/*------------------------------------- LISTADOS ------------------------------------- */
        Route::group(['prefix' => 'listados'], function () {
            Route::get('estados/{id}', 'ListadosController@estados');
            Route::get('municipios/{id}', 'ListadosController@municipios');
            Route::get('colonias/{id}', 'ListadosController@colonias');
            Route::get('gradoPolicial/{id}', 'ListadosController@gradoPolicial');
            Route::get('tipoSeparacion/{id}', 'ListadosController@tipoSeparacion');
            Route::get('caracteristicas/{id}', 'ListadosController@caracteristicas');
        });
/*------------------------------------ ASPIRANTES ------------------------------------ */
        Route::group(['prefix' => 'aspirantes'], function () {
            // Nuevo
            Route::get('nuevo/{id?}', 'AspirantesController@nuevo');
            Route::get('listado', 'AspirantesController@listado');
            Route::match(['get', 'post'], 'personales', 'AspirantesController@personales');
            Route::match(['get', 'post'], 'domicilio', 'AspirantesController@domicilio');
            Route::match(['get', 'post'], 'familiares', 'AspirantesController@familiares');
            Route::match(['get', 'post'], 'reclutamiento', 'AspirantesController@reclutamiento');
            Route::match(['get', 'post'], 'seleccion', 'AspirantesController@seleccion');
            Route::match(['get', 'post'], 'formacion', 'AspirantesController@formacion');
            Route::match(['get', 'post'], 'certificacion', 'AspirantesController@certificacion');
            Route::match(['get', 'post'], 'reingresos', 'AspirantesController@reingresos');
            Route::match(['get', 'post'], 'separacion', 'AspirantesController@separacion');
            Route::match(['get', 'post'], 'evaluaciones', 'AspirantesController@evaluaciones');
            Route::post('incorporar', 'AspirantesController@incorporar');
            Route::get('detalleConvocatoria/{id}', 'AspirantesController@detalleConvocatoria');
            Route::get('aptosParaNombramiento', 'AspirantesController@aptosParaNombramiento');
            Route::delete('destroy/{id}', 'AspirantesController@destroy');
            Route::get('fotografia', 'AspirantesController@fotografia');
        });
/*------------------------------------- POLICIAS ------------------------------------- */
        Route::group(['prefix' => 'policias'], function () {
            Route::get('nuevo/{id?}', 'PoliciasController@nuevo');
            Route::get('listado', 'PoliciasController@listado');
            Route::get('activos', 'PoliciasController@activos');
            Route::get('inactivos', 'PoliciasController@inactivos');
            Route::get('organoColegiado', 'PoliciasController@organoColegiado');
            Route::resource('colegiado', 'ColegiadoController', ['except' => ['update']]);
            Route::post('colegiado/{id}', 'ColegiadoController@update');
            Route::match(['get', 'post'], 'personales', 'PoliciasController@personales');
            Route::match(['get', 'post'], 'domicilio', 'PoliciasController@domicilio');
            Route::match(['get', 'post'], 'familiares', 'PoliciasController@familiares');
            Route::match(['get', 'post'], 'datosActividad', 'PoliciasController@datosActividad');
            Route::match(['get', 'post'], 'promociones', 'PoliciasController@promociones');
            Route::match(['get', 'post'], 'herramientas', 'PoliciasController@herramientas');
            Route::match(['get', 'post'], 'sanciones', 'PoliciasController@sanciones');
            Route::match(['get', 'post'], 'adscripcion', 'PoliciasController@adscripcion');
            Route::match(['get', 'post'], 'estimulos', 'PoliciasController@estimulos');
            Route::match(['get', 'post'], 'planIndividual', 'PoliciasController@planIndividual');
            Route::match(['get', 'post'], 'administracion', 'PoliciasController@administracion');
            Route::match(['get', 'post'], 'separacion', 'PoliciasController@separacion');
            Route::match(['get', 'post'], 'evaluaciones', 'PoliciasController@evaluaciones');
            Route::delete('destroy/{id}', 'PoliciasController@destroy');
        });
        Route::group(['prefix' => 'familiar'], function () {
            Route::get('ver/{id}', 'FamiliaresController@ver');
            Route::get('editar/{id}', 'FamiliaresController@editar');
            Route::post('actualizar', 'FamiliaresController@actualizar');
            Route::delete('eliminar/{id}', 'FamiliaresController@eliminar');
        });
        Route::group(['prefix' => 'promocion'], function () {
            Route::get('ver/{id}', 'PromocionController@ver');
            Route::get('editar/{id}', 'PromocionController@editar');
            Route::post('actualizar', 'PromocionController@actualizar');
            Route::delete('eliminar/{id}', 'PromocionController@eliminar');
        });
        Route::group(['prefix' => 'herramienta'], function () {
            Route::get('ver/{id}', 'HerramientaController@ver');
            Route::get('editar/{id}', 'HerramientaController@editar');
            Route::post('actualizar', 'HerramientaController@actualizar');
            Route::delete('eliminar/{id}', 'HerramientaController@eliminar');
        });
        Route::group(['prefix' => 'sancion'], function () {
            Route::get('ver/{id}', 'SancionController@ver');
            Route::get('editar/{id}', 'SancionController@editar');
            Route::post('actualizar', 'SancionController@actualizar');
            Route::delete('eliminar/{id}', 'SancionController@eliminar');
        });
        Route::group(['prefix' => 'adscripcion'], function () {
            Route::get('ver/{id}', 'AdscripcionController@ver');
            Route::get('editar/{id}', 'AdscripcionController@editar');
            Route::post('actualizar', 'AdscripcionController@actualizar');
            Route::delete('eliminar/{id}', 'AdscripcionController@eliminar');
        });
        Route::group(['prefix' => 'estimulo'], function () {
            Route::get('ver/{id}', 'EstimuloController@ver');
            Route::get('editar/{id}', 'EstimuloController@editar');
            Route::post('actualizar', 'EstimuloController@actualizar');
            Route::delete('eliminar/{id}', 'EstimuloController@eliminar');
        });
        Route::group(['prefix' => 'plan'], function () {
            Route::get('ver/{id}', 'PlanController@ver');
            Route::get('editar/{id}', 'PlanController@editar');
            Route::post('actualizar', 'PlanController@actualizar');
            Route::delete('eliminar/{id}', 'PlanController@eliminar');
        });
        Route::group(['prefix' => 'administrativa'], function () {
            Route::get('ver/{id}', 'AdministrativaController@ver');
            Route::get('editar/{id}', 'AdministrativaController@editar');
            Route::post('actualizar', 'AdministrativaController@actualizar');
            Route::delete('eliminar/{id}', 'AdministrativaController@eliminar');
        });
        Route::group(['prefix' => 'control'], function () {
            Route::get('ver/{id}', 'ControlController@ver');
            Route::get('editar/{id}', 'ControlController@editar');
            Route::post('actualizar', 'ControlController@actualizar');
            Route::delete('eliminar/{id}', 'ControlController@eliminar');
        });
/*------------------------------------- REPORTES ------------------------------------- */
        Route::group(['prefix' => 'reportes'], function () {
            Route::group(['prefix' => 'policias'], function () {
                Route::get('kardex', 'ReportesController@policiasKardex');
                Route::get('listadoKardex', 'ReportesController@policiaListadoKardex');
                Route::get('detalleKardex/{id}', 'ReportesController@policiaDetalleKardex');
                Route::get('activos', 'ReportesController@policiasActivos');
                Route::get('inactivos', 'ReportesController@policiasInactivos');
                Route::get('separacion', 'ReportesController@policiasSeparacionDelServicio');
                Route::get('confianza', 'ReportesController@policiasControlDeConfianza');
                Route::get('detalleControl/{id}', 'ReportesController@policiaDetalleControl');
                Route::get('listadoControl', 'ReportesController@policiasListadoControl');
            });
            Route::group(['prefix' => 'aspirantes'], function () {
                Route::get('Kardex', 'ReportesController@aspirantesKardex');
                Route::get('listadoKardex', 'ReportesController@listadoKardex');
                Route::get('detalleKardex/{id}', 'ReportesController@aspiranteDetalleKardex');
                Route::get('aprobados', 'ReportesController@aspirantesAprobados');
                Route::get('noaprobados', 'ReportesController@aspirantesNoAprobados');
                Route::get('bajas', 'ReportesController@aspirantesBajas');
                Route::get('confianza', 'ReportesController@aspirantesControlDeConfianza');
                Route::get('detalleControl/{id}', 'ReportesController@aspiranteDetalleControl');
                Route::get('listadoControl', 'ReportesController@listadoControl');
            });
            Route::group(['prefix' => 'graficados'], function () {
                Route::get('activos', 'ReportesController@graficadosActivos');
                Route::post('activos', 'ReportesController@graficadosActivosDatos');
                Route::get('inactivos', 'ReportesController@graficadosInactivos');
            });
            Route::get('elementos', 'ReportesController@elementos');
            Route::get('costos', 'ReportesController@costos');
            Route::get('bitacora', 'ReportesController@bitacora');
            Route::get('certificaciones', 'ReportesController@certificaciones');
            Route::get('edades', 'ReportesController@edades');
        });
        Route::group(['prefix' => 'export'], function () {
            Route::get('aspirantesAprobados/{tipo}', 'ExportController@aspirantesAprobados');
            Route::get('aspirantesNoAprobados/{tipo}', 'ExportController@aspirantesNoAprobados');
            Route::get('aspirantesBajas/{tipo}', 'ExportController@aspirantesBajas');
            Route::get('aspirantesKardex/{id}/{tipo}', 'ExportController@aspirantesKardex');
            Route::get('aspirantesControl/{id}/{tipo}', 'ExportController@aspirantesControl');
            Route::get('policiasKardex/{id}/{tipo}', 'ExportController@policiasKardex');
            Route::get('policiasActivos/{tipo}', 'ExportController@policiasActivos');
            Route::get('policiasInactivos/{tipo}', 'ExportController@policiasInactivos');
            Route::get('policiasSeparacionDelServicio/{tipo}', 'ExportController@policiasSeparacionDelServicio');
            Route::get('policiasControl/{id}/{tipo}', 'ExportController@policiasControl');
            Route::get('costos/{tipo}', 'ExportController@costos');
            Route::get('bitacora/{tipo}', 'ExportController@bitacora');
            Route::get('certificaciones/{tipo}', 'ExportController@certificaciones');
        });
        Route::get('/login', function () {
            return view('login.login');
        });

        Route::group(['prefix' => 'catalogos'], function () {
            // Convocatorias
            Route::get('convocatorias', 'CatalogosController@convocatorias');
            Route::resource('convocatoria', 'ConvocatoriaController', ['except' => ['update']]);
            Route::post('convocatoria/{id}', 'ConvocatoriaController@update');
            // Evaluaciones
            Route::get('evaluaciones', 'CatalogosController@evaluaciones');
            Route::resource('evaluacion', 'EvaluacionController', ['except' => ['update']]);
            Route::post('evaluacion/{id}', 'EvaluacionController@update');
            // Rangos
            Route::get('rangos', 'CatalogosController@rangos');
            Route::resource('rango', 'RangoController', ['except' => ['update']]);
            Route::post('rango/{id}', 'RangoController@update');
            // Instancias
            Route::get('instanciasCapacitadoras', 'CatalogosController@instanciasCapacitadoras');
            Route::resource('instancia', 'InstanciaController', ['except' => ['update']]);
            Route::post('instancia/{id}', 'InstanciaController@update');

            Route::get('coloniaNueva/{colonia}/{municipio}', 'CatalogosController@coloniaNueva');
            Route::get('serviciosMedicos/{valor}/{meta}', 'CatalogosController@serviciosMedicos');
            Route::get('posgrado/{valor}/{meta}', 'CatalogosController@posgrado');
            Route::get('comisionNueva/{valor}/{meta}', 'CatalogosController@comisionNueva');
        });
        Route::group(['prefix' => 'administracion'], function () {
            // Usuarios
            Route::get('usuarios', 'AdministracionController@usuarios');
            // Route::resource('usuario', 'UsuarioController', ['except' => ['update']]);
            // Route::post('usuario/{id}', 'UsuarioController@update');
            // Perfiles
            Route::get('perfiles', 'AdministracionController@perfiles');
            // Route::resource('convocatoria', 'ConvocatoriaController', ['except' => ['update']]);
            // Route::post('convocatoria/{id}', 'ConvocatoriaController@update');
            // Municipios
            Route::get('municipios', 'AdministracionController@municipios');
            // Route::resource('evaluacion', 'EvaluacionController', ['except' => ['update']]);
            // Route::post('evaluacion/{id}', 'EvaluacionController@update');
        });
    });
});
/*-------------------------------------- TOOLS --------------------------------------- */
    //Clear Cache facade value:
    Route::get('/clear-cache', function () {
        $exitCode = Artisan::call('cache:clear');
        return '<h1>Cache facade value cleared</h1>';
    });
    //Reoptimized class loader:
    Route::get('/optimize', function () {
        $exitCode = Artisan::call('optimize');
        return '<h1>Reoptimized class loader</h1>';
    });
    //Route cache:
    Route::get('/route-cache', function () {
        $exitCode = Artisan::call('route:cache');
        return '<h1>Routes cached</h1>';
    });
    //Clear Route cache:
    Route::get('/route-clear', function () {
        $exitCode = Artisan::call('route:clear');
        return '<h1>Route cache cleared</h1>';
    });
    //Clear View cache:
    Route::get('/view-clear', function () {
        $exitCode = Artisan::call('view:clear');
        return '<h1>View cache cleared</h1>';
    });
    //Clear Config cache:
    Route::get('/config-cache', function () {
        $exitCode = Artisan::call('config:cache');
        return '<h1>Clear Config cleared</h1>';
    });
