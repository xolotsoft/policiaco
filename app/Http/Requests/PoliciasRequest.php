<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Session;
use App\Http\Requests\Request;
use Carbon\Carbon;

class PoliciasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $under = Carbon::parse(Carbon::now()->subYears(18))->format('d-m-Y');
        $regex = 'regex:/^[a-zA-ZÑñáéíóúÁÉÍÓÚ]+(\s*[a-zA-ZÑñáéíóúÁÉÍÓÚ]*)*[a-zA-ZÑñáéíóúÁÉÍÓÚ]+\s*$/';
        $materno = 'regex:/^[a-zA-ZÑñáéíóúÁÉÍÓÚ]+(\s*[a-zA-ZÑñáéíóúÁÉÍÓÚ]*)*[a-zA-ZÑñáéíóúÁÉÍÓÚ]+\s*$/';
        $curp = 'regex:/\A[A-Z][AEIOUX][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][MH][A-Z][BCDFGHJKLMNÑPQRSTVWXYZ]{4}[0-9A-Z][0-9]\z/i';
        $rfc = 'regex:/\A[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9]([A-Z0-9]{3})?\z/i';
        $telefono = 'regex:/^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/';
        $requerida = ($this->editable != '')?'':'required';
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'curp'            => $curp.'|unique:persona,curp,'.Session::get('idPersona').'|'.$requerida,
                    'rfc'             => 'unique:persona,rfc,'.Session::get('idPersona').'|'.$rfc.'|'.$requerida,
                    'nombre'          => $regex.'|'.$requerida,
                    'aPaterno'        => $regex.'|'.$requerida,
                    'aMaterno'        => $materno,
                    'fechaNacimiento' => 'date|date_format:d-m-Y|before:'.$under.'|'.$requerida,
                    'lugarNacimiento' => 'required',
                    'genero'          => 'numeric|'.$requerida,
                    'estadoCivil'     => 'numeric|'.$requerida,
                    'estatura'        => 'numeric|'.$requerida,
                    'peso'            => 'numeric|'.$requerida,
                    'telefono'        => $telefono.'|'.$requerida,
                    'movil'           => $telefono,
                    'email'           => 'email',
                    'huella'          => 'mimes:jpeg,bmp,png|'.$requerida,
                    'observaciones'   => ''
                ];
                break;
            default:
                return [];
                break;
        }
    }
    public function attributes()
    {
        return [
            'curp'                => 'CURP',
            'rfc'                 => 'RFC',
            'nombre'              => 'Nombre',
            'aPaterno'            => 'Apellido Paterno',
            'aMaterno'            => 'Apellido Materno',
            'fechaNacimiento'     => 'Fecha de Nacimiento',
            'lugarNacimiento'     => 'Lugar de Nacimiento',
            'genero'              => 'Genero',
            'estadoCivil'         => 'Estado Civil',
            'estatura'            => 'Estatura',
            'peso'                => 'Peso',
            'telefono'            => 'Telefono',
            'movil'               => 'Movil',
            'email'               => 'eMail',
            'huella'              => 'Huellas dactilares',
            'observaciones'       => 'Observaciones'
        ];
    }
    public function messages()
    {
        return [
            'fechaNacimiento.before' => 'Tienes que ser mayor de 18'
        ];
    }
}
