<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PlanRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                if ($this->id != '') {
                    $req = '';
                } else {
                    $req = 'required|mimes:jpeg,bmp,png';
                }

                return [
                    'idPersona'       => 'numeric',
                    'nombre'          => 'required|numeric',
                    'caracteristicas' => 'required|numeric',
                    'instancia'       => 'numeric',
                    'duracion'        => 'required|numeric',
                    'obligatorio'     => 'boolean',
                    'fechaInicio'     => 'required|date|date_format:d-m-Y',
                    'fechaTermino'    => 'required|date|date_format:d-m-Y|after:'.$this->fechaInicio,
                    'documento'       => $req,
                    'resultado'       => 'required|numeric',
                    'comentarios'     => 'string',
                ];
                break;
            default:
                return [];
            break;
        }
    }
    public function attributes()
    {
        return [
            'nombre'          => 'Nombre',
            'caracteristicas' => 'Tipo de capacitación',
            'instancia'       => 'Instancia capacitadora',
            'duracion'        => 'Duración',
            'obligatorio'     => 'Obligatorio',
            'fechaInicio'     => 'Fecha de Inicio',
            'fechaTermino'    => 'Fecha de Terminoo',
            'documento'       => 'Documento',
            'resultado'       => 'Resultado',
            'comentarios'     => 'Comentarios',
        ];
    }
    public function messages()
    {
        return [
            'idPersona.numeric' => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
