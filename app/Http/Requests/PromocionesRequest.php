<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PromocionesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'rango'                    => 'required|numeric',
                    'fechaPromocion'           => 'required|date|date_format:d-m-Y',
                    'observacionesPromociones' => 'string',
                    'idPersona'                => 'numeric',
                ];
                break;
            default:
                return [];
            break;
        }
    }
    public function attributes()
    {
        return [
            'rango'                    => 'Nuevo rango',
            'fechaPromocion'           => 'Fecha de promoción',
            'observacionesPromociones' => 'Observaciones',
        ];
    }
    public function messages()
    {
        return [
            'idPersona.numeric'     => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
