<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ColegiadoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->editable == '1') {
            $req = '';
        } else {
            $req = 'required';
        }
        return [
            'comision'     => 'required',
            'integrante'   => 'required',
            'sesion'       => 'required',
            'fechasesion'  => 'required|date|date_format:d-m-Y',
            'temas'        => 'required',
            'acuerdos'     => $req,
            'resoluciones' => $req,
            'actaSesion'   => $req
        ];
    }

    public function attributes()
    {
        return [
            'comision'     => 'Comisión',
            'integrante'   => 'Integrante',
            'sesion'       => 'Sesión',
            'fechasesion'  => 'Fecha de sesión',
            'temas'        => 'Temas',
            'acuerdos'     => 'Acuerdos',
            'resoluciones' => 'Resoluciones',
            'actaSesion'   => 'Acta de sesión'
        ];
    }
}
