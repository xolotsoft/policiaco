<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FamiliaresRequest extends Request
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'categoria'       => 'required|numeric|min:1',
                    'nombre'          => 'required|regex:/^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+\s*$/',
                    'aPaterno'        => 'required|regex:/^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+\s*$/',
                    'aMaterno'        => 'required|regex:/^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+\s*$/',
                    'fechaNacimiento' => 'required|date|date_format:d-m-Y',
                    'genero'          => 'required|numeric|min:1',
                    'direccion'       => 'required|max:255|min:1',
                    'telefono'        => 'required|regex:/^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/',
                    'observacionesFamiliares'   => 'max:255'
                ];
                break;
            default:
                return [];
                break;
        }
    }

    public function attributes()
    {
        return [
            'idPersona'                 => 'Aspirante',
            'categoria'       => 'Categoría',
            'nombre'          => 'Nombre',
            'aPaterno'        => 'Apellido Paterno',
            'aMaterno'        => 'Apellido Materno',
            'fechaNacimiento' => 'Fecha de nacimiento',
            'genero'          => 'Seleciona una opción',
            'direccion'       => 'Dirección',
            'telefono'        => 'Teléfono',
            'observacionesFamiliares'   => 'Observaciones'
        ];
    }

    public function messages()
    {
        return [
            'idPersona.required' => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
