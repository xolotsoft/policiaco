<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Carbon\Carbon;

class ActividadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $under = Carbon::parse(Carbon::now()->subYears(18))->format('d-m-Y');
        $after = Carbon::parse(Carbon::now()->subYears(60))->format('d-m-Y');
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'certificadoNombramiento' => '',
                    'constanciaGrado'         => '',
                    'fotografiaUniforme'      => '',
                    'credencialPorteArma'     => '',
                    'idPersona'               => 'numeric',
                    'gradoPolicial'           => 'numeric',
                    'fechaNombramiento'       => 'date|date_format:d-m-Y|after:'.$after,
                    'areaAdscripcion'         => 'string',
                    'numeroEmpleado'          => 'numeric',
                    'cuip'                    => '',
                    'observacionesActividad'  => 'string',
                    'observacionesPromocion'  => 'string',
                ];
                break;
            default:
                return [];
            break;
        }
    }
    public function attributes()
    {
        return [
            'certificadoNombramiento' => 'Certificado de nombramiento',
            'constanciaGrado'         => 'Constancia de grado',
            'fotografiaUniforme'      => 'Fotografía con uniforme',
            'credencialPorteArma'     => 'Credencial de porte de arma',
            'gradoPolicial'           => 'Grado Policial',
            'fechaNombramiento'       => 'Fecha de nombramiento',
            'areaAdscripcion'         => 'Area de adscripción',
            'numeroEmpleado'          => 'Número de empleado',
            'cuip'                    => 'CUIP',
            'observacionesActividad'  => 'Observaciones',
            'observacionesPromocion'  => 'Observaciones de promoción',
        ];
    }
    public function messages()
    {
        return [
            'idPersona.numeric'     => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
