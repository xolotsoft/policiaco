<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EvaluacionesPoliciasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'idPersona'       => 'numeric',
                    'examen'          => 'required|numeric',
                    'resultado'       => 'required|string',
                    'fecha'           => 'required|date|date_format:d-m-Y',
                    'fechaResultados' => 'required|date|date_format:d-m-Y|after:'.$this->fecha,
                ];
                break;
            default:
                return [];
            break;
        }
    }
    public function attributes()
    {
        return [
            'examen'          => 'Examen',
            'resultado'       => 'Resultado',
            'fecha'           => 'Fecha de presentación del examen',
            'fechaResultados' => 'Fecha de entrega de resultados del examen',
        ];
    }
    public function messages()
    {
        return [
            'idPersona.numeric' => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
