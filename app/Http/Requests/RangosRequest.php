<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Config;

class RangosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
        public function rules()
    {
        $float = Config::get('constans.regex.float');
        $int = Config::get('constans.regex.int');
        if ($this->route('id') === null) {
            return [
                'Category'                     => 'string',
                'Rank'                         => 'string|required',
                'CommandLevel'                 => 'string',
                'Special'                      => 'boolean',
                'Normal_Salary'                => 'numeric|regex:'.$float,
                'Special_Salary'               => 'numeric|regex:'.$float,
                'OnlyMexican'                  => 'boolean',
                'HeightMan'                    => 'numeric|regex:'.$float,
                'HeightWoman'                  => 'numeric|regex:'.$float,
                'MinimumSchooling'             => 'string',
                'Age'                          => 'numeric|regex:'.$int,
                'Old'                          => 'numeric|regex:'.$int,
                'Courses_ContinuedFormation'   => 'numeric|regex:'.$int,
                'Courses_SpecializedFormation' => 'numeric|regex:'.$int,
                'Active_ContinuedFormation'    => 'numeric|regex:'.$int,
                'Active_SpecializedFormation'  => 'numeric|regex:'.$int,
                'ApproveCertificate'           => 'numeric|regex:'.$int,
                'ApproveCertificateFileName'   => 'numeric|regex:'.$int,
            ];
        } else {
            return [
                'Category'                     => 'string',
                'Rank'                         => 'string|required',
                'CommandLevel'                 => 'string',
                'Special'                      => 'boolean',
                'Normal_Salary'                => 'numeric|regex:'.$float,
                'Special_Salary'               => 'numeric|regex:'.$float,
                'OnlyMexican'                  => 'boolean',
                'HeightMan'                    => 'numeric|regex:'.$float,
                'HeightWoman'                  => 'numeric|regex:'.$float,
                'MinimumSchooling'             => 'string',
                'Age'                          => 'numeric|regex:'.$int,
                'Old'                          => 'numeric|regex:'.$int,
                'Courses_ContinuedFormation'   => 'numeric|regex:'.$int,
                'Courses_SpecializedFormation' => 'numeric|regex:'.$int,
                'Active_ContinuedFormation'    => 'numeric|regex:'.$int,
                'Active_SpecializedFormation'  => 'numeric|regex:'.$int,
                'ApproveCertificate'           => 'numeric|regex:'.$int,
                'ApproveCertificateFileName'   => 'numeric|regex:'.$int,
            ];
        }
        
    }
    public function attributes()
    {
        return [
            'Category'                     => 'Categoria',
            'Rank'                         => 'Grado',
            'CommandLevel'                 => 'Nivel de Mando',
            'Special'                      => 'Especial',
            'Normal_Salary'                => 'Salario Normal',
            'Special_Salary'               => 'Salario Especial',
            'OnlyMexican'                  => 'Solo Mexicano',
            'HeightMan'                    => 'Estatura Hombre',
            'HeightWoman'                  => 'Estatura Mujer',
            'MinimumSchooling'             => 'Escolaridad Minima',
            'Age'                          => 'Edad',
            'Old'                          => 'Antiguedad',
            'Courses_ContinuedFormation'   => 'Cursos de Formación',
            'Courses_SpecializedFormation' => 'Cursos de Especialización',
            'Active_ContinuedFormation'    => 'Formación Activa',
            'Active_SpecializedFormation'  => 'Especialización Activa',
            'ApproveCertificate'           => 'Certificado de Aprovación',
            'ApproveCertificateFileName'   => 'Archivo de Certificade de Aprovación'
        ];
    }
}
