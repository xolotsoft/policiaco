<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Carbon\Carbon;

class HerramientasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'idPersona'                 => 'numeric',
                    'descripcion'               => 'required|string',
                    'tipo'                      => 'required|numeric',
                    'cantidad'                  => 'required|numeric',
                    'fechaRecepcion'            => 'date|date_format:d-m-Y',
                    'fechaDevolucion'           => 'date|date_format:d-m-Y|after:'.$this->fechaRecepcion,
                    'areaResguardo'             => 'string',
                    'observacionesHerramientas' => 'string',
                ];
                break;
            default:
                return [];
            break;
        }
    }
    public function attributes()
    {
        return [
            'descripcion'               => 'Descripción',
            'tipo'                      => 'Tipo',
            'cantidad'                  => 'Cantidad',
            'fechaRecepcion'            => 'Fecha de recepción',
            'fechaDevolucion'           => 'Fecha devolución',
            'areaResguardo'             => 'Área de Resguardo',
            'observacionesHerramientas' => 'Observaciones',
        ];
    }
    public function messages()
    {
        return [
            'idPersona.numeric'     => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
