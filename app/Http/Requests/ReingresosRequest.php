<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ReingresosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'institucion'       => '',
                    'grado'             => '',
                    'antiguedad'        => '',
                    'cuip'              => '',
                    'fechaPresentacion' => 'date|date_format:d-m-Y',
                    'observaciones'     => ''
                ];
                break;
            default:
                return [];
                break;
        }
    }
    public function attributes()
    {
        return [
            'institucion'            => 'Institución',
            'grado'                  => 'Grado',
            'antiguedad'             => 'Antigüedad',
            'cuip'                   => 'CUIP',
            'fechaPresentacion'      => 'Fecha presentación de certificados de control de confianza',
            'observacionesReingreso' => 'Observaciones'
        ];
    }
    public function messages()
    {
        return [
            'idPersona.required'     => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
