<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DomicilioRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'calle'                  => 'required|regex:/^[a-zA-Z0-9]+(\s*[a-zA-Z0-9]*)*[a-zA-Z0-9]+\s*$/',
                    'exterior'               => 'required',
                    'interior'               => '',
                    'cp'                     => 'required|numeric|regex:/^\d{5}$/',
                    'estado'                 => 'required|numeric|min:1',
                    'municipio'              => 'required|numeric|min:1',
                    'colonia'                => 'required|numeric|min:1',
                    'pais'                   => 'required|numeric|min:1',
                    'observacionesDomicilio' => ''
                ];
                break;
            default:
                return [];
                break;
        }
    }
    public function attributes()
    {
        return [
            'calle'                  => 'Calle',
            'exterior'               => 'Número Exterior',
            'interior'               => 'Número Interior',
            'cp'                     => 'Código Postal',
            'estado'                 => 'Estado',
            'municipio'              => 'Municipio',
            'colonia'                => 'Colonia',
            'pais'                   => 'País',
            'observacionesDomicilio' => 'Observaciones'
        ];
    }
    public function messages()
    {
        return [
            'idPersona.required'     => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
