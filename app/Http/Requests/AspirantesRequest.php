<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Session;
use App\Http\Requests\Request;
use App\Models\Convocatoria;
use App\Models\Rango;
use Carbon\Carbon;
use Config;

class AspirantesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = '/^[a-zA-ZÑñáéíóúÁÉÍÓÚ]+(\s*[a-zA-ZÑñáéíóúÁÉÍÓÚ]*)*[a-zA-ZÑñáéíóúÁÉÍÓÚ]+\s*$/';
        $curp = '/\A[A-Z][AEIOUX][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][MH][A-Z][BCDFGHJKLMNÑPQRSTVWXYZ]{4}[0-9A-Z][0-9]\z/i';
        $rfc = '/\A[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9]([A-Z0-9]{3})?\z/i';
        $telefono = '/^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/';
        $min = 1.60;
        $under = Carbon::parse(Carbon::now()->subYears(18))->format('d-m-Y');
        $after = Carbon::parse(Carbon::now()->subYears(60))->format('d-m-Y');
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                if ($this->idConvocatoria != '') {
                    $convocatoria = Convocatoria::find($this->idConvocatoria);
                    $under = Carbon::parse(Carbon::now()->subYears($convocatoria->edadMinima))->format('d-m-Y');
                    $after = Carbon::parse(Carbon::now()->subYears($convocatoria->edadMaxima))->format('d-m-Y');
                }
                if ($this->puesto != '' && $this->genero != '') {
                    $rango = Rango::find($this->puesto);
                    if ($this->genero == 13) {
                        $min = $rango->HeightMan;
                    } else {
                        $min = $rango->HeightWoman;
                    }
                }
                $documentoCup = ($this->editable != '')?'':'required';
                $huella = ($this->editable != '')?'':'required|';
                return [
                    'curp'                => 'required|unique:persona,curp,'.Session::get('idPersona').'|regex:'.$curp,
                    'rfc'                 => 'required|unique:persona,rfc,'.Session::get('idPersona').'|regex:'.$rfc,
                    'nombre'              => 'required|regex:'.$regex,
                    'aPaterno'            => 'required|regex:'.$regex,
                    'aMaterno'            => 'regex:'.$regex,
                    'fechaRegistro'       => 'required|date|date_format:d-m-Y',
                    'idConvocatoria'      => 'required|numeric',
                    'detalleConvocatoria' => '',
                    'puesto'              => 'required|numeric',
                    'folio'               => 'numeric|max:6|min:6|regex:/[0-9A-Z]/',
                    'fechaNacimiento'     => 'required|date|date_format:d-m-Y|before:'.$under.'|after:'.$after,
                    'lugarNacimiento'     => 'required',
                    'genero'              => 'required|numeric',
                    'estadoCivil'         => 'required|numeric',
                    'estatura'            => 'required|numeric|min:'.$min,
                    'peso'                => 'required|numeric',
                    'telefono'            => 'required|regex:'.$telefono,
                    'movil'               => 'regex:'.$telefono,
                    'email'               => 'email',
                    'huella'              => $huella.'mimes:jpeg,bmp,png,pdf',
                    'observaciones'       => 'max:255',
                    'cup'                 => '',
                    'documentoCup'        => $documentoCup
                ];
                break;
            default:
                return [];
                break;
        }
    }
    public function attributes()
    {
        return [
            'curp'                => 'CURP',
            'rfc'                 => 'RFC',
            'nombre'              => 'Nombre',
            'aPaterno'            => 'Apellido Paterno',
            'aMaterno'            => 'Apellido Materno',
            'idConvocatoria'      => 'Numero Convocatoria',
            'detalleConvocatoria' => 'Detalle Convocatoria',
            'puesto'              => 'Nombre Puesto',
            'folio'               => 'No. de Folio',
            'fechaNacimiento'     => 'Fecha de Nacimiento',
            'lugarNacimiento'     => 'Lugar de Nacimiento',
            'fechaRegistro'       => 'Fecha de Registro',
            'genero'              => 'Genero',
            'estadoCivil'         => 'Estado Civil',
            'estatura'            => 'Estatura',
            'peso'                => 'Peso',
            'telefono'            => 'Telefono',
            'movil'               => 'Movil',
            'email'               => 'eMail',
            'huella'              => 'Huellas dactilares',
            'observaciones'       => 'Observaciones',
            'cup'                 => 'CUP',
            'documentoCup'        => 'Documento CUP'
        ];
    }
    public function messages()
    {
        if ($this->method() == 'POST') {
            $minima = 18;
            $maxima = 60;
            $min = 1.60;
            if ($this->idConvocatoria != '') {
                $convocatoria = Convocatoria::find($this->idConvocatoria);
                $minima = $convocatoria->edadMinima;
                $maxima = $convocatoria->edadMaxima;
            }
            if ($this->puesto != '' && $this->genero != '') {
                $rango = Rango::find($this->puesto);
                if ($this->genero == 13) {
                    $min = $rango->HeightMan;
                } else {
                    $min = $rango->HeightWoman;
                }
            }
            return [
                'fechaNacimiento.before' => 'Tienes que ser mayor de '.$minima.' años',
                'fechaNacimiento.after' => 'Tienes que ser menor de '.$maxima.' años',
                'estatura.min' => 'Debes de medir al menos '.$min.' mts.'
            ];
        } else {
            return [];
        }
    }
}
