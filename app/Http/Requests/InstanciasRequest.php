<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InstanciasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
        public function rules()
    {
        if ($this->route('id') === null) {
            return [
                'Name'         => 'required',
                'Observations' => 'required'
            ];
        } else {
            return [
                'Name'         => 'required',
                'Observations' => 'required'
            ];
        }
    }
    public function attributes()
    {
        return [
            'Name'         => 'Descripción',
            'Observations' => 'Observaciones'
        ];
    }
}
