<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EstimulosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'idPersona'              => 'numeric',
                    'motivo'                 => 'required|string',
                    'premio'                 => 'required|string',
                    'tipoRecompensa'         => 'required|numeric',
                    'fechaEmision'           => 'required|date|date_format:d-m-Y',
                    'observacionesEstimulos' => 'string',
                ];
                break;
            default:
                return [];
            break;
        }
    }
    public function attributes()
    {
        return [
            'tipoRecompensa'         => 'Tipo',
            'motivo'                 => 'Motivo',
            'premio'                 => 'Premio',
            'fechaEmision'           => 'Fecha de emisión',
            'observacionesEstimulos' => 'Observaciones',
        ];
    }
    public function messages()
    {
        return [
            'idPersona.numeric' => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
