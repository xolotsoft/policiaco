<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdscripcionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'idPersona'                => 'numeric',
                    'areaProcedencia'          => 'required|string',
                    'areaDesignacion'          => 'required|string',
                    'servicioRealizaba'        => 'required|string',
                    'servicioRealizar'         => 'required|string',
                    'fechaFin'                 => 'required|date|date_format:d-m-Y',
                    'fechaInicio'              => 'required|date|date_format:d-m-Y|after:'.$this->fechaFin,
                    'razon'                    => 'required|string',
                    'observacionesAdscripcion' => 'string',
                ];
                break;
            default:
                return [];
            break;
        }
    }
    public function attributes()
    {
        return [
            'areaProcedencia'          => 'Área de procedencia',
            'areaDesignacion'          => 'Área de designación',
            'servicioRealizaba'        => 'Servicio que realizaba',
            'servicioRealizar'         => 'Servicios que va a realizar',
            'fechaFin'                 => 'Fecha de fin de funciones que realizaba',
            'fechaInicio'              => 'Fecha de inicio de funciones  que va a realizar',
            'razon'                    => 'Razón del cambio',
            'observacionesAdscripcion' => 'Observaciones',
        ];
    }
    public function messages()
    {
        return [
            'idPersona.numeric'     => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
