<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FormacionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'material'     => '',
                    'fechaInicio'  => 'date|date_format:d-m-Y',
                    'fechaFin'     => 'date|date_format:d-m-Y|after:fechaInicio',
                    'duracion'     => 'numeric',
                    'certificado'  => '',
                    'constancia'   => '',
                    'cumplimiento' => '',
                    'instancia'    => '',
                    'observacionesFormacion' => '',
                ];
                break;
            default:
                return [];
                break;
        }
    }
    public function attributes()
    {
        return [
            'idPersona'              => 'Aspirante',
            'material'               => 'Materia',
            'fechaInicio'            => 'Fecha de inicio',
            'fechaFin'               => 'Fecha de finalización',
            'duracion'               => 'Duración',
            'certificado'            => 'Certificado',
            'constancia'             => 'Constancia',
            'cumplimiento'           => 'Cumplimiento',
            'observacionesFormacion' => 'Observaciones',
        ];
    }

    public function messages()
    {
        return [
            'idPersona.required' => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
