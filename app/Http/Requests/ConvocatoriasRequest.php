<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConvocatoriasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route('id') === null) {
            return [
                'nombre'      => 'required',
                'puestos'     => 'required|numeric|max:99|min:1',
                'fecha'       => 'required|date|date_format:d-m-Y',
                'edadMinima'  => 'required|numeric|max:30|min:18',
                'edadMaxima'  => 'required|numeric|max:40|min:30',
                'File'        => 'required|mimes:jpeg,bmp,png',
                'descripcion' => 'required|max:255|min:1'
            ];
        } else {
            return [
                'nombre'      => 'required',
                'puestos'     => 'required|numeric|max:99|min:1',
                'fecha'       => 'required|date|date_format:d-m-Y',
                'edadMinima'  => 'required|numeric|max:30|min:18',
                'edadMaxima'  => 'required|numeric|max:40|min:30',
                'File'        => 'mimes:jpg,jpeg',
                'descripcion' => 'required|max:255|min:1'
            ];
        }
    }

    public function attributes()
    {
        return [
            'nombre'      => 'Nombre',
            'puestos'     => 'Puestos a Concursar',
            'fecha'       => 'Fecha',
            'edadMinima'  => 'Edad mínima',
            'edadMaxima'  => 'Edad máxima',
            'File'        => 'Documento',
            'descripcion' => 'Descripción'
        ];
    }
}
