<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EvaluacionesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route('id') === null) {
            return [
                'EvaluationType'   => 'required',
                'EvaluationName'   => 'required',
                'Duration'         => 'required',
                'ImportanceDegree' => 'required',
                'Specialization'   => 'required',
                'Direction'        => 'required',
                'Compulsory'       => '',
                'Comments'         => 'required'
            ];
        } else {
            return [
                'EvaluationType'   => 'required',
                'EvaluationName'   => 'required',
                'Duration'         => 'required',
                'ImportanceDegree' => 'required',
                'Specialization'   => 'required',
                'Direction'        => 'required',
                'Compulsory'       => '',
                'Comments'         => 'required'
            ];
        }
    }
    public function attributes()
    {
        return [
            'EvaluationType'   =>'Tipo de evaluacion',
            'EvaluationName'   =>'Nombre',
            'Duration'         =>'Duración',
            'ImportanceDegree' =>'Castegoria',
            'Specialization'  =>'Especialización',
            'Direction'        =>'Alta de direccion',
            'Compulsory'       =>'Obligatorio',
            'Comments'         =>'Comentarios'
        ];
    }
}
