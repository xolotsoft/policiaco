<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdministracionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                if ($this->id != '') {
                    $req = '';
                } else {
                    $req = 'required|mimes:jpeg,bmp,png';
                }
                return [
                    'idPersona'      => 'numeric',
                    'descripcion'    => 'required|string',
                    'fechaInicio'    => 'required|date|date_format:d-m-Y',
                    'fechaTermino'   => 'required|date|date_format:d-m-Y|after:'.$this->fechaInicio,
                    'documento'      => $req,
                    'autorizante'    => 'required|string',
                    'observaciones'  => 'string',
                    'requerimientos' => 'string',
                    'asignado'       => 'boolean',
                    'realizado'      => 'boolean',
                    'comprobado'     => 'boolean',
                    'validado'       => 'boolean',
                ];
                break;
            default:
                return [];
            break;
        }
    }
    public function attributes()
    {
        return [
            'descripcion'    => 'Descripción',
            'fechaInicio'    => 'Fecha de inicio',
            'fechaTermino'   => 'Fecha de Termino',
            'documento'      => 'Documento',
            'autorizante'    => 'Autorizante',
            'observaciones'  => 'Observaciones',
            'requerimientos' => 'Requerimientos',
            'asignado'       => 'Asignado',
            'realizado'      => 'Realizado',
            'comprobado'     => 'Comprobado',
            'validado'       => 'Validado',
        ];
    }
    public function messages()
    {
        return [
            'idPersona.numeric' => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
