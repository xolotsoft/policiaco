<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SeparacionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $requerida = ($this->editable != '')?'':'required';
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'idPersona'       => 'numeric',
                    'tipoSeparacion'  => 'numeric|'.$requerida,
                    'separacion'      => 'numeric|'.$requerida,
                    'fechaSeparacion' => 'date|date_format:d-m-Y|'.$requerida,
                    'motivo'          => 'string|'.$requerida,
                    'certificado'     => 'mimes:jpeg,bmp,png,pdf|'.$requerida,
                    'aprobado'        => 'string|'.$requerida,
                ];
                break;
            default:
                return [];
            break;
        }
    }
    public function attributes()
    {
        return [
            'tipoSeparacion'  => 'Tipo de separación',
            'separacion'      => 'Separación',
            'fechaSeparacion' => 'Fecha de separación',
            'motivo'          => 'Motivo',
            'certificado'     => 'Certificado',
            'aprobado'        => 'Aprobado por',
        ];
    }
    public function messages()
    {
        return [
            'idPersona.numeric'     => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
