<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SeleccionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'resultadoConfianza'     => '',
                    'observacionesSeleccion' => ''
                ];
                break;
            default:
                return [];
                break;
        }
    }
    public function attributes()
    {
        return [
            'resultadoConfianza' => 'Resultado de control de confianza',
            'observaciones'      => 'Observaciones',
        ];
    }
    public function messages()
    {
        return [
            'idPersona.required' => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
