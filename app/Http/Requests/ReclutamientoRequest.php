<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ReclutamientoRequest extends Request
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                if ($this->editable != '') {
                    $req = '';
                } else {
                    $req = 'required';
                }
                return [
                    'idPersona'                  => '',
                    'perfilSolicitado'           => '',
                    // 'numConvocatoria'         => '',
                    // 'detConvocatoria'         => '',
                    // 'nombrePuesto'            => '',
                    'documento'                  => '',
                    'elector'                    => $req,
                    'nacimiento'                 => $req,
                    'cartilla'                   => $req,
                    'penales'                    => $req,
                    'bachillerato'               => $req,
                    'tecnico'                    => '',
                    'licenciatura'               => '',
                    'maestria'                   => '',
                    'bajaPolicial'               => '',
                    'fotoInfantil'               => $req,
                    'comprobanteDomicilio'       => $req,
                    'motivosIngreso'             => $req,
                    'refPersonales1'             => $req,
                    'refPersonales2'             => $req,
                    'nacimientoHijo'             => '',
                    'matrimonio'                 => '',
                    'sociedadConyugal'           => '',
                    'manejo'                     => $req,
                    'documentoRfc'               => $req,
                    'documentoCurp'              => $req,
                    'serviciosMedicos'           => $req,
                    'estadoCuenta'               => '',
                    'solicitud'                  => $req,
                    'curriculum'                 => '',
                    'antecedentesDisciplinarios' => '',
                    'observacionesReclutamiento' => ''
                ];
                break;
            default:
                return [];
                break;
        }
    }
    public function attributes()
    {
        return [
            'idPersona'                  => 'Aspirante',
            'perfilSolicitado'           => 'Perfil Solicitado',
            'documento'                  => 'Documento',
            'elector'                    => 'Credencial de elector',
            'nacimiento'                 => 'Acta de Nacimiento',
            'cartilla'                   => 'Cartilla de Servivio militar',
            'penales'                    => 'NO antecedentes penales',
            'bachillerato'               => 'Bachillerato',
            'tecnico'                    => 'Certificado Técnico',
            'licenciatura'               => 'Licenciatura',
            'maestria'                   => 'Certificado de maestría',
            'bajaPolicial'               => 'Certificado de baja policial',
            'fotoInfantil'               => 'Fotografía frente infantil',
            'comprobanteDomicilio'       => 'Comprobante de domicilio',
            'motivosIngreso'             => 'Carta con motivos de ingreso',
            'refPersonales1'             => 'Referencias personales 1',
            'refPersonales2'             => 'Referencias personales 2',
            'nacimientoHijo'             => 'Acta de nacimiento de hijos',
            'matrimonio'                 => 'Acta de matrimonio',
            'sociedadConyugal'           => 'Acta de sociedad conyugal',
            'manejo'                     => 'Licencia de manejo',
            'manejoDorso'                => 'Licencia de manejo (dorso)',
            'documentoCurp'              => 'CURP',
            'documentoRfc'               => 'RFC',
            'serviciosMedicos'           => 'Servicios médicos',
            'estadoCuenta'               => 'Estados de cuenta bancarios',
            'solicitud'                  => 'Solicitud elaborada',
            'curriculum'                 => 'Currículum',
            'antecedentesDisciplinarios' => 'Antecedentes disciplinarios',
            'observacionesReclutamiento' => 'Observaciones'
        ];
    }

    public function messages()
    {
        return [
            'idPersona.required' => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
