<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CertificacionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'cargo'        => '',
                    'puesto'       => '',
                    'fechaIngreso' => 'date|date_format:d-m-Y',
                    'adscripcion'  => '',
                    'calificacion' => 'numeric',
                    'nombramiento' => '',
                ];
                break;
            default:
                return [];
                break;
        }
    }
    public function attributes()
    {
        return [
            'cargo'        => 'Nombre del cargo',
            'puesto'       => 'Nombre del puesto',
            'fechaIngreso' => 'Fecha de ingreso',
            'adscripcion'  => 'Area o unidad de adscripción',
            'calificacion' => 'Calificación de curso de formación inicial',
            'nombramiento' => 'Nombramiento',
        ];
    }
    public function messages()
    {
        return [
            'idPersona.required' => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
