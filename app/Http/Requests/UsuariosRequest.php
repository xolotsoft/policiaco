<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UsuariosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
        public function rules()
    {
        return [
            'tipo'            =>'required',
            'evaluacion'      =>'required',
            'duracion'        =>'required',
            'categoria'       =>'required',
            'especializacion' =>'required',
            'alta'            =>'required',
            'estatus'         =>'',
            'comentarios'     =>'required'
            ];
    }
    public function attributes()
    {
        return [
            'tipo'            =>'Tipo de evaluacion',
            'evaluacion'      =>'Nombre',
            'duracion'        =>'Duración',
            'categoria'       =>'Castegoria',
            'especializacion' =>'Especialización',
            'alta'            =>'Alta de direccion',
            'estatus'         =>'Obligatorio',
            'comentarios'     =>'Comentarios'
        ];
    }
}
