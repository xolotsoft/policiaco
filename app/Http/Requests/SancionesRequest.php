<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SancionesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'POST':
                return [
                    'idPersona'              => 'numeric',
                    'tipoSanciones'          => 'required|numeric',
                    'duracion'               => 'required|numeric',
                    'causa'                  => 'required|string',
                    'fechaSanciones'         => 'date|date_format:d-m-Y',
                    'observacionesSanciones' => 'string',
                ];
                break;
            default:
                return [];
            break;
        }
    }
    public function attributes()
    {
        return [
            'tipoSanciones'          => 'Tipo',
            'duracion'               => 'Dutación',
            'causa'                  => 'Causa',
            'fechaSanciones'         => 'Fecha',
            'observacionesSanciones' => 'Observaciones',
        ];
    }
    public function messages()
    {
        return [
            'idPersona.numeric'     => 'No se identifica al Aspirante para asignarle la información'
        ];
    }
}
