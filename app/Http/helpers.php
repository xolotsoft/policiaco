<?php

function query($q, $b)
{
    foreach ($b as $value) {
        $q = preg_replace("#\?#", $value, $q, 1);
    }
    return $q;
}

function texto($p)
{
    $p = ucwords(mb_strtolower($p));
    return $p;
}

function titulo($p)
{
    $min = ['El', 'La', 'Para', 'Los', 'De', 'Del'];
    $p = snake_case($p);
    $p = str_replace('_', ' ', $p);
    $p = str_replace('ion ', 'ión ', $p);
    $p = str_replace('bitacora', 'bitácora', $p);
    $p = ucwords($p);
    $p = explode(' ', $p);
    foreach ($p as $k => $px) {
        if(in_array($px, $min)) {
            $p[$k] = strtolower($px);
        }
        if($px == 'Aspirantes') {
            $p[$k] = 'Aspirantes <i class="fa fa-caret-right" aria-hidden="true"></i>';
        }
        if($px == 'Policias') {
            $p[$k] = 'Policias <i class="fa fa-caret-right" aria-hidden="true"></i>';
        }
    }
    $p = implode(' ', $p);
    return $p;
}

function accion()
{
    $parametros = app('request')->route()->parameters();
    $action = app('request')->route()->getAction();
    $controller = class_basename($action['controller']);
    list($controller, $action) = explode('@', $controller);
    $datos = [
        'controlador' => substr($controller, 0, -10),
        'accion' => $action,
        'parametros' => $parametros
    ];
    return $datos;
}

function accionLog($query)
{
    $es = [
        'update' => 'Actualizar',
        'insert' => 'Nuevo',
        'delete' => 'Eliminar'
    ];
    $arr = explode(' ', trim($query));
    return $es[$arr[0]];
}
