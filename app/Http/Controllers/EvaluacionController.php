<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\EvaluacionesRequest;
use App\Models\Evaluacion;
use App\Models\EvaluationType;
use Carbon\Carbon;
use DB;

class EvaluacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $evaluaciones = Evaluacion::all();
        return view('catalogos.evaluaciones.index')
        ->with('evaluaciones', $evaluaciones)
        ->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos = EvaluationType::lists('Name', 'ID');
        return view('catalogos.evaluaciones.create')
        ->with('tipos', $tipos)
        ->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EvaluacionesRequest $request)
    {
        DB::beginTransaction();
        try {
            $evaluacion = new Evaluacion();
            $evaluacion->EvaluationName   = texto($request->EvaluationName);
            $evaluacion->Duration         = $request->Duration;
            $evaluacion->Compulsory       = $request->Compulsory;
            $evaluacion->Comments         = $request->Comments;
            $evaluacion->EvaluationType   = $request->EvaluationType;
            $evaluacion->ImportanceDegree = $request->ImportanceDegree;
            $evaluacion->Actualization    = $request->Actualization;
            $evaluacion->Specialization   = texto($request->Specialization);
            $evaluacion->Direction        = texto($request->Direction);
            $evaluacion->save();
            DB::commit();
            return $this->index();
        } catch (\Exception $e) {
            DB::rollback();
            return response('Hubo un problema al guardar la información', 510);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $evaluacion = Evaluacion::find($id);
        return view('catalogos.evaluaciones.show')
        ->with('evaluacion', $evaluacion)
        ->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipos = EvaluationType::lists('Name', 'ID');
        $evaluacion = Evaluacion::find($id);
        return view('catalogos.evaluaciones.edit')
        ->with('evaluacion', $evaluacion)
        ->with('tipos', $tipos)
        ->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EvaluacionesRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $evaluacion = Evaluacion::find($id);
            $evaluacion->EvaluationName   = $request->EvaluationName;
            $evaluacion->Duration         = $request->Duration;
            $evaluacion->Compulsory       = $request->Compulsory;
            $evaluacion->Comments         = $request->Comments;
            $evaluacion->EvaluationType   = $request->EvaluationType;
            $evaluacion->ImportanceDegree = $request->ImportanceDegree;
            $evaluacion->Actualization    = $request->Actualization;
            $evaluacion->Specialization   = $request->Specialization;
            $evaluacion->Direction        = $request->Direction;
            $evaluacion->save();
            DB::commit();
            return $this->index();
        } catch (\Exception $e) {
            DB::rollback();
            return response('Hubo un problema al editar la información', 510);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Evaluacion::find($id)->delete();
        return $this->index();
    }
}
