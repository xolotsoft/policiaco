<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests\AdministracionRequest;
use App\Http\Requests;
use App\Models\Administracion;
use App\Models\Catalogo;
use DB;

class AdministrativaController extends Controller
{
    public function ver($id)
    {
        $administracion = Administracion::find($id);
        return response('');
    }

    public function editar($id)
    {
        $administracion = Administracion::find($id);
        return view('policias.administracion.edit')
        ->with('administracion', $administracion)
        ->render();
    }

    public function actualizar(AdministracionRequest $request)
    {
        DB::beginTransaction();
        try {
            $administracion = Administracion::find($request->id);
            $old = $administracion->documento;
            if ($request->file('documento') !== null) {
                $archivo->name = $File->getClientOriginalName();
                $archivo->size = $File->getSize();
                $archivo->type = $File->getMimeType();
                $archivo->ext  = $File->getClientOriginalExtension();
                $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                $archivo->save();
                if ($archivo->save()) {
                    $administracion->documento = $archivo->id;
                    Archivo::find($old)->delete();
                }
            }
            $administracion->descripcion    = $request->descripcion;
            $administracion->fechaInicio    = $request->fechaInicio;
            $administracion->fechaTermino   = $request->fechaTermino;
            $administracion->autorizante    = $request->autorizante;
            $administracion->asignado       = $request->asignado;
            $administracion->realizado      = $request->realizado;
            $administracion->comprobado     = $request->comprobado;
            $administracion->validado       = $request->validado;
            $administracion->requerimientos = $request->requerimientos;
            $administracion->observaciones  = $request->observaciones;
            $administracion->idPersona      = Session::get('idPersona');
            $administracion->save();
            DB::commit();
            return response('Se guardo correctamente la información');
        } catch (\Exeption $e) {
            DB::rollback();
            return response('Hubo un problema al guardar en la base de datos', 510);
        }
    }

    public function eliminar($id)
    {
        $administracion = Administracion::find($id)->delete();
        return response('Se eliminó correctamente al administracion');
    }
}
