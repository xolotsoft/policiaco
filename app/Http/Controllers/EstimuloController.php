<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests\EstimulosRequest;
use App\Http\Requests;
use App\Models\Estimulo;
use App\Models\Catalogo;
use DB;

class EstimuloController extends Controller
{
    public function ver($id)
    {
        $estimulo = Estimulo::find($id);
        return response('');
    }

    public function editar($id)
    {
        $estimulo = Estimulo::find($id);
        $tipos = Catalogo::Estimulos();
        return view('policias.estimulos.edit')
        ->with('estimulo', $estimulo)
        ->with('tipos', $tipos)
        ->render();
    }

    public function actualizar(EstimulosRequest $request)
    {
        DB::beginTransaction();
        try {
            $estimulo = Estimulo::find($request->id);
            $estimulo->tipoRecompensa         = $request->tipoRecompensa;
            $estimulo->motivo                 = $request->motivo;
            $estimulo->premio                 = $request->premio;
            $estimulo->fechaEmision           = $request->fechaEmision;
            $estimulo->observacionesEstimulos = $request->observacionesEstimulos;
            $estimulo->idPersona              = Session::get('idPersona');
            $estimulo->save();
            DB::commit();
            return response('Se guardo correctamente la información');
        } catch (\Exeption $e) {
            DB::rollback();
            return response('Hubo un problema al guardar en la base de datos', 510);
        }
    }

    public function eliminar($id)
    {
        $estimulo = Estimulo::find($id)->delete();
        return response('Se eliminó correctamente al estimulo');
    }
}
