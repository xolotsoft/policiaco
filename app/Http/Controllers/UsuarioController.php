<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UsuariosRequest;
use App\Http\Requests\UsuariosUpdateRequest;
use App\Models\Usuario;
use Carbon\Carbon;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = Usuario::all();
        return view('catalogos.usuarios.index')
        ->with('usuarios', $usuarios)
        ->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('catalogos.usuarios.create')->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsuariosRequest $request)
    {
        $documento = $request->file('documento');
        $usuario = new Usuario();
        $usuario->usuario = $request->usuario;
        $usuario->puesto = $request->puesto;
        $usuario->fecha = Carbon::parse($request->fecha)->format('Y-m-d');
        $usuario->edadMinima = $request->edadMinima;
        $usuario->edadMaxima = $request->edadMaxima;
        $usuario->documento = base64_encode(file_get_contents($documento->getRealPath()));
        $usuario->descripcion = $request->descripcion;
        $usuario->save();
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = Usuario::find($id);
        return view('catalogos.usuarios.show')
        ->with('usuario', $usuario)
        ->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = Usuario::find($id);
        $usuario->fecha = Carbon::parse($usuario->fecha)->format('d-m-Y');
        return view('catalogos.usuarios.edit')
        ->with('usuario', $usuario)
        ->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsuariosUpdateRequest $request, $id)
    {
        $usuario = Usuario::find($id);
        $usuario->usuario = $request->usuario;
        $usuario->puesto = $request->puesto;
        $usuario->fecha = Carbon::parse($request->fecha)->format('Y-m-d');
        $usuario->edadMinima = $request->edadMinima;
        $usuario->edadMaxima = $request->edadMaxima;
        if ($request->file('documento') !== null) {
            $documento = $request->file('documento');
            $usuario->documento = base64_encode(file_get_contents($documento->getRealPath()));
        }
        $usuario->descripcion = $request->descripcion;
        $usuario->save();
        return $this->index();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Usuario::find($id)->delete();
        return $this->index();
    }
}
