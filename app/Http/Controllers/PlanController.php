<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests\PlanRequest;
use App\Http\Requests;
use App\Models\Plan;
use App\Models\Catalogo;
use App\Models\Instancia;
use App\Models\Archivo;
use App\Models\Evaluacion;
use App\Models\EvaluationType;
use DB;

class PlanController extends Controller
{
    public function ver($id)
    {
        $plan = Plan::find($id);
        return response('');
    }

    public function editar($id)
    {
        $plan = Plan::find($id);
        $nombres = EvaluationType::lists('Name', 'ID');
        $caracteristicas = Evaluacion::where('EvaluationType', '=', $plan->nombre)
        ->select(DB::raw('EvaluationName AS Name'), 'ID')->lists('Name', 'ID');
        $instancias = Instancia::lists('Name', 'ID');
        $apto = Catalogo::Apto();
        return view('policias.plan.edit')
        ->with('plan', $plan)
        ->with('nombres', $nombres)
        ->with('instancias', $instancias)
        ->with('caracteristicas', $caracteristicas)
        ->with('apto', $apto)
        ->with('editable', true)
        ->render();
    }

    public function actualizar(PlanRequest $request)
    {
        DB::beginTransaction();
        try {
            $plan = Plan::find($request->id);
            $old = $plan->documento;
            if ($request->file('documento') !== null) {
                $File = $request->file('documento');
                $archivo = new Archivo();
                $archivo->name = $File->getClientOriginalName();
                $archivo->size = $File->getSize();
                $archivo->type = $File->getMimeType();
                $archivo->ext  = $File->getClientOriginalExtension();
                $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                $archivo->save();
                if ($archivo->save()) {
                    $plan->documento = $archivo->id;
                    Archivo::find($old)->delete();
                }
            }
            $plan->nombre           = $request->nombre;
            $plan->caracteristicas  = $request->caracteristicas;
            $plan->instancia        = $request->instancia;
            $plan->gradoImportancia = $request->gradoImportancia;
            $plan->duracion         = $request->duracion;
            $plan->obligatorio      = $request->obligatorio;
            $plan->fechaInicio      = $request->fechaInicio;
            $plan->fechaTermino     = $request->fechaTermino;
            $plan->resultado        = $request->resultado;
            $plan->comentarios      = $request->comentarios;
            $plan->idPersona        = Session::get('idPersona');
            $plan->save();
            DB::commit();
            return response('Se guardo correctamente la información');
        } catch (\Exeption $e) {
            DB::rollback();
            return response('Hubo un problema al guardar en la base de datos', 510);
        }
    }

    public function eliminar($id)
    {
        $plan = Plan::find($id)->delete();
        return response('Se eliminó correctamente al plan');
    }
}
