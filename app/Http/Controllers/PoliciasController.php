<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Carbon\Carbon;
use \DB;
// Request
    use App\Http\Requests;
    use App\Http\Requests\PoliciasRequest;
    use App\Http\Requests\DomicilioRequest;
    use App\Http\Requests\FamiliaresRequest;
    use App\Http\Requests\ActividadRequest;
    use App\Http\Requests\PromocionesRequest;
    use App\Http\Requests\HerramientasRequest;
    use App\Http\Requests\SancionesRequest;
    use App\Http\Requests\AdscripcionRequest;
    use App\Http\Requests\EstimulosRequest;
    use App\Http\Requests\PlanRequest;
    use App\Http\Requests\AdministracionRequest;
    use App\Http\Requests\SeparacionRequest;
    use App\Http\Requests\EvaluacionesPoliciasRequest;
// Models
    use App\Models\Policia;
    use App\Models\Catalogo;
    use App\Models\Countries;
    use App\Models\Rango;
    use App\Models\State;
    use App\Models\Familia;
    use App\Models\Archivo;
    use App\Models\Reclutamiento;
    use App\Models\Convocatoria;
    use App\Models\Provinces;
    use App\Models\Areas;
    use App\Models\Seleccion;
    use App\Models\Persona;
    use App\Models\Domicilio;
    use App\Models\Actividad;
    use App\Models\Promocion;
    use App\Models\Herramienta;
    use App\Models\Sancion;
    use App\Models\Adscripcion;
    use App\Models\Estimulo;
    use App\Models\Plan;
    use App\Models\Administracion;
    use App\Models\Separacion;
    use App\Models\EvaluacionesPolicias;
    use App\Models\Mensaje;
    use App\Models\Instancia;
    use App\Models\Colegiado;
    use App\Models\EvaluationType;

class PoliciasController extends Controller
{
    public function nuevo($id = null)
    {
        Session::forget('idPersona');
        // Session::put('idPersona', 29);
        if ($id) {
            Session::put('idPersona', $id);
            $view = 'policias.nuevo.index';
        } else {
            $view = 'page';
        }
        try {
            $policia = Policia::join('persona', 'persona.id', '=', 'policias.idPersona')
            ->where('policias.idPersona', Session::get('idPersona'))
            ->firstOrFail();
            $persona = Persona::select('*')
            ->where('persona.id', Session::get('idPersona'))
            ->firstOrFail();
            $reclutamiento = Reclutamiento::select('*')
            ->where('idPersona', Session::get('idPersona'))
            ->firstOrFail();
            $editable = true;
        } catch (ModelNotFoundException $e) {
            $policia = [];
            $persona = [];
            $reclutamiento = [];
            $editable = false;
        }
        $countm = Mensaje::nuevos()->total;
        $genero = Catalogo::Genero();
        $civil = Catalogo::Civil();
        $paises = Countries::lists('Name', 'ID');
        $puestos = Rango::lists('Rank', 'ID');
        $estados = State::lists('Name', 'ID');
        return view($view)
        ->with('genero', $genero)
        ->with('civil', $civil)
        ->with('estados', $estados)
        ->with('puestos', $puestos)
        ->with('policia', $policia)
        ->with('editable', $editable)
        ->with('countm', $countm)
        ->with('persona', $persona)
        ->with('reclutamiento', $reclutamiento)
        ->render();
    }

    public function listado()
    {
        $policias = Policia::all();
        $countm = Mensaje::nuevos()->total;
        return view('page')
        ->with('countm', $countm)
        ->with('policias', $policias);
    }

    public function viewListado()
    {
        $policias = Policia::all();
        $countm = Mensaje::nuevos()->total;
        return view('policas.listado.index')
        ->with('countm', $countm)
        ->with('policias', $policias)
        ->render();
    }

    public function activos()
    {
        $inactivos = Policia::select('policias.idPersona')
        ->join('separacion', 'policias.idPersona', '=', 'separacion.idPersona')
        ->get();
        $policias = Policia::whereNotIn('idPersona', $inactivos)->get();
        $countm = Mensaje::nuevos()->total;
        return view('page')
        ->with('countm', $countm)
        ->with('policias', $policias);
    }

    public function inactivos()
    {
        $policias = Policia::select('policias.*')
        ->join('separacion', 'policias.idPersona', '=', 'separacion.idPersona')
        ->get();
        // dd($policias);
        $countm = Mensaje::nuevos()->total;
        return view('page')
        ->with('countm', $countm)
        ->with('policias', $policias);
    }

    public function organoColegiado()
    {
        $organos = Colegiado::all();
        $countm = Mensaje::nuevos()->total;
        return view('page')
        ->with('countm', $countm)
        ->with('organos', $organos);
    }

    public function personales(PoliciasRequest $request)
    {
        if ($request->getMethod() == "GET") {
            try {
                $policia = Policia::join('persona', 'persona.id', '=', 'policias.idPersona')
                ->where('policias.idPersona', Session::get('idPersona'))
                ->firstOrFail();
                $persona = Persona::select('*')
                ->where('persona.id', Session::get('idPersona'))
                ->firstOrFail();
                $reclutamiento = Reclutamiento::select('*')
                ->where('idPersona', Session::get('idPersona'))
                ->firstOrFail();
                $editable = true;
            } catch (ModelNotFoundException $e) {
                $policia = [];
                $persona = [];
                $reclutamiento = [];
                $editable = false;
            }
            $genero        = Catalogo::Genero();
            $civil         = Catalogo::Civil();
            $estados       = State::lists('Name', 'ID');
            $puestos       = Rango::lists('Rank', 'ID');
            return view('policias.personales.main')
            ->with('genero', $genero)
            ->with('civil', $civil)
            ->with('estados', $estados)
            ->with('puestos', $puestos)
            ->with('policia', $policia)
            ->with('persona', $persona)
            ->with('reclutamiento', $reclutamiento)
            ->with('editable', $editable)
            ->render();
        } else {
            try {
                $policia = Policia::select('*')
                ->where('policias.idPersona', Session::get('idPersona'))
                ->firstOrFail();
                $persona = Persona::select('*')
                ->where('persona.id', Session::get('idPersona'))
                ->firstOrFail();
                $reclutamiento = Reclutamiento::select('*')
                ->where('idPersona', Session::get('idPersona'))
                ->firstOrFail();
                $folio = $policia->folio;
            } catch (ModelNotFoundException $e) {
                $policia = new Policia();
                $persona = new Persona();
                $reclutamiento = new Reclutamiento();
            }
            DB::beginTransaction();
            $documentos = [
                'elector',
                'fotoInfantil',
                'comprobanteDomicilio',
                'documentoCurp',
                'documentoRfc'
            ];
            try {
                // Se guarda el archivo
                foreach ($documentos as $documento) {
                    if ($request->file($documento) !== null) {
                        $archivo = new Archivo();
                        $File = $request->file($documento);
                        $archivo->name = $File->getClientOriginalName();
                        $archivo->size = $File->getSize();
                        $archivo->type = $File->getMimeType();
                        $archivo->ext  = $File->getClientOriginalExtension();
                        $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                        $archivo->save();
                        $reclutamiento[$documento] = $archivo->id;
                        unset($File);
                    }
                }
                if ($request->file('huella') !== null) {
                    $File = $request->file('huella');
                    $archivo = new Archivo();
                    $archivo->name = $File->getClientOriginalName();
                    $archivo->size = $File->getSize();
                    $archivo->type = $File->getMimeType();
                    $archivo->ext  = $File->getClientOriginalExtension();
                    $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                    if ($archivo->save()) {
                        $persona->huella = $archivo->id;
                    }
                }
                // se guardan datos personales
                $persona->curp            = strtoupper($request->curp);
                $persona->rfc             = strtoupper($request->rfc);
                $persona->nombre          = texto($request->nombre);
                $persona->aPaterno        = texto($request->aPaterno);
                $persona->aMaterno        = texto($request->aMaterno);
                $persona->fechaNacimiento = $request->fechaNacimiento;
                $persona->lugarNacimiento = $request->lugarNacimiento;
                $persona->genero          = $request->genero;
                $persona->estadoCivil     = $request->estadoCivil;
                $persona->estatura        = $request->estatura;
                $persona->peso            = $request->peso;
                $persona->telefono        = $request->telefono;
                $persona->movil           = $request->movil;
                $persona->email           = $request->email;
                $persona->save();
                $policia->observaciones  = $request->observaciones;
                $policia->idPersona      = $persona->id;
                $policia->save();
                Session::put('idPersona', $persona->id);
                $reclutamiento->idPersona = $persona->id;
                $reclutamiento->save();
                DB::commit();
                return response('Se guardaron correctamente los datos');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function domicilio(DomicilioRequest $request)
    {
        $paises  = Countries::lists('Name', 'ID');
        $estados = State::lists('Name', 'ID');
        if ($request->getMethod() == "GET") {
            try {
                $domicilio = Domicilio::where('idPersona', Session::get('idPersona'))->firstOrFail();
                $municipios = Provinces::where('StateId', $domicilio->estado)->lists('Name', 'ID');
                $colonias = Areas::where('ProvinceId', $domicilio->municipio)->lists('Name', 'ID');
                $editable = true;
            } catch (ModelNotFoundException $e) {
                $domicilio  = [];
                $municipios = [];
                $colonias   = [];
                $editable = false;
            }
            return view('aspirantes.domicilio.create')
            ->with('paises', $paises)
            ->with('estados', $estados)
            ->with('municipios', $municipios)
            ->with('colonias', $colonias)
            ->with('domicilio', $domicilio)
            ->with('editable', $editable)
            ->render();
        } else {
            try {
                $domicilio = Domicilio::where('idPersona', Session::get('idPersona'))
                ->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $domicilio = new Domicilio();
            }
            DB::beginTransaction();
            try {
                $domicilio->calle                  = texto($request->calle);
                $domicilio->exterior               = $request->exterior;
                $domicilio->interior               = $request->interior;
                $domicilio->cp                     = $request->cp;
                $domicilio->estado                 = $request->estado;
                $domicilio->municipio              = $request->municipio;
                $domicilio->colonia                = $request->colonia;
                $domicilio->observacionesDomicilio = $request->observacionesDomicilio;
                $domicilio->idPersona              = Session::get('idPersona');
                $domicilio->save();
                DB::commit();
                return response('success');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function familiares(FamiliaresRequest $request)
    {
        $genero     = Catalogo::Genero();
        $parentesco = Catalogo::Parentesco();
        if ($request->getMethod() == "GET") {
            try {
                $familiares = Familia::where('idPersona', Session::get('idPersona'))->get();
            } catch (\Exception $e) {
                $familiares = [];
            }
            return view('policias.familiares.create')
            ->with('genero', $genero)
            ->with('parentesco', $parentesco)
            ->with('familiares', $familiares)
            ->render();
        } else {
            DB::beginTransaction();
            try {
                $familia = new Familia();
                $familia->categoria       = $request->categoria;
                $familia->nombre          = texto($request->nombre);
                $familia->aPaterno        = texto($request->aPaterno);
                $familia->aMaterno        = texto($request->aMaterno);
                $familia->fechaNacimiento = $request->fechaNacimiento;
                $familia->genero          = $request->genero;
                $familia->direccion       = $request->direccion;
                $familia->telefono        = $request->telefono;
                $familia->observacionesFamiliares   = $request->observacionesFamiliares;
                $familia->idPersona       = Session::get('idPersona');
                $familia->save();
                DB::commit();
                return response('Se guardo correctamente la información');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function datosActividad(ActividadRequest $request)
    {
        $documentos = [
            'certificadoNombramiento',
            'constanciaGrado',
            'fotografiaUniforme',
            'credencialPorteArma'
        ];
        if ($request->getMethod() == "GET") {
            $categoriaPolicial = Catalogo::categoriaPolicial();
            try {
                $actividad = Actividad::where('idPersona', Session::get('idPersona'))
                ->firstOrFail();
                $rangos = Rango::where('Category', '=', $actividad->cargo)->lists('Rank', 'id');
                $editable = true;
            } catch (ModelNotFoundException $e) {
                $actividad = [];
                $rangos = [];
                $editable = false;
            }
            return view('policias.actividad.create')
            ->with('rangos', $rangos)
            ->with('categoriaPolicial', $categoriaPolicial)
            ->with('editable', $editable)
            ->with('actividad', $actividad)
            ->render();
        } else {
            try {
                $actividad = Actividad::where('idPersona', Session::get('idPersona'))
                ->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $actividad = new Actividad();
            }
            DB::beginTransaction();
            try {
                // Se guardan el archivos
                foreach ($documentos as $documento) {
                    if ($request->file($documento) !== null) {
                        $archivo = new Archivo();
                        $File = $request->file($documento);
                        $archivo->name = $File->getClientOriginalName();
                        $archivo->size = $File->getSize();
                        $archivo->type = $File->getMimeType();
                        $archivo->ext  = $File->getClientOriginalExtension();
                        $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                        $archivo->save();
                        $actividad[$documento] = $archivo->id;
                        unset($File);
                    }
                }
                // se guardan datos
                $actividad->idPersona              = Session::get('idPersona');
                $actividad->cargo                  = $request->cargo;
                $actividad->gradoPolicial          = $request->gradoPolicial;
                $actividad->puesto                 = $request->puesto;
                $actividad->funciones              = $request->funciones;
                $actividad->fechaNombramiento      = $request->fechaNombramiento;
                $actividad->areaAdscripcion        = $request->areaAdscripcion;
                $actividad->numeroEmpleado         = $request->numeroEmpleado;
                $actividad->cuip                   = $request->cuip;
                $actividad->cup                    = $request->cup;
                $actividad->respondiente           = $request->respondiente;
                $actividad->observacionesActividad = $request->observacionesActividad;
                $actividad->save();
                DB::commit();
                return response('success');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function promociones(PromocionesRequest $request)
    {
        $puestos = Rango::lists('Rank', 'ID');
        if ($request->getMethod() == "GET") {
            $categoriaPolicial = Catalogo::categoriaPolicial();
            $puestos = [];
            $editable = false;
            try {
                $promociones = Promocion::select('promociones.*', 'catalogos.nombre', 'Rank', 'CommandLevel','Special')
                ->join('rangos', 'rangos.id', '=', 'promociones.rango')
                ->join('catalogos', 'catalogos.id', '=', 'rangos.Category')
                ->where('promociones.idPersona', Session::get('idPersona'))
                ->get();
            } catch (\Exception $e) {
                $promociones = [];
            }
            return view('policias.promociones.create')
            ->with('puestos', $puestos)
            ->with('categoriaPolicial', $categoriaPolicial)
            ->with('editable', $editable)
            ->with('promociones', $promociones)
            ->render();
        } else {
            DB::beginTransaction();
            try {
                $promociones = new Promocion();
                $promociones->idPersona                = Session::get('idPersona');
                $promociones->cargo                    = $request->cargo;
                $promociones->rango                    = $request->rango;
                $promociones->fechaPromocion           = $request->fechaPromocion;
                $promociones->observacionesPromociones = $request->observacionesPromociones;
                $promociones->save();
                DB::commit();
                return redirect('policias/promociones');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }
    public function herramientas(HerramientasRequest $request)
    {
        $tipo = Catalogo::Herramientas();
        if ($request->getMethod() == "GET") {
            try {
                $herramientas = Herramienta::where('idPersona', Session::get('idPersona'))->get();
            } catch (\Exception $e) {
                $herramientas = [];
            }
            return view('policias.herramientas.create')
            ->with('herramientas', $herramientas)
            ->with('tipo', $tipo)
            ->render();
        } else {
            DB::begintransaction();
            $herramientas = new Herramienta();
            try {
                $herramientas->idPersona                 = Session::get('idPersona');
                $herramientas->areaResguardo             = $request->areaResguardo;
                $herramientas->descripcion               = $request->descripcion;
                $herramientas->tipo                      = $request->tipo;
                $herramientas->cantidad                  = $request->cantidad;
                $herramientas->fechaRecepcion            = $request->fechaRecepcion;
                $herramientas->fechaDevolucion           = $request->fechaDevolucion;
                $herramientas->observacionesHerramientas = $request->observacionesHerramientas;
                $herramientas->save();
                DB::commit();
                return redirect('policias/herramientas');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function sanciones(SancionesRequest $request)
    {
        $tipos = Catalogo::Sancion();
        if ($request->getMethod() == "GET") {
            try {
                $sanciones = Sancion::where('sanciones.idPersona', Session::get('idPersona'))
                ->get();
            } catch (\Exception $e) {
                $sanciones = [];
            }
            return view('policias.sanciones.create')
            ->with('sanciones', $sanciones)
            ->with('tipos', $tipos)
            ->render();
        } else {
            DB::beginTransaction();
            $sanciones = new Sancion();
            try {
                $sanciones->idPersona              = Session::get('idPersona');
                $sanciones->tipoSanciones          = $request->tipoSanciones;
                $sanciones->fechaInicio            = $request->fechaInicio;
                $sanciones->fechaConclusion        = $request->fechaConclusion;
                $sanciones->duracion               = $request->duracion;
                $sanciones->causa                  = $request->causa;
                $sanciones->fechaSanciones         = $request->fechaSanciones;
                $sanciones->observacionesSanciones = $request->observacionesSanciones;
                $sanciones->save();
                DB::commit();
                return redirect('policias/sanciones');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function adscripcion(AdscripcionRequest $request)
    {
        if ($request->getMethod() == "GET") {
            try {
                $adscripciones = Adscripcion::where('idPersona', Session::get('idPersona'))->get();
            } catch (\Exception $e) {
                $adscripciones = [];
            }
            return view('policias.adscripcion.create')
            ->with('adscripciones', $adscripciones)
            ->render();
        } else {
            DB::beginTransaction();
            $adscripciones = new Adscripcion();
            try {
                $adscripciones->areaProcedencia          = $request->areaProcedencia;
                $adscripciones->areaDesignacion          = $request->areaDesignacion;
                $adscripciones->servicioRealizaba        = $request->servicioRealizaba;
                $adscripciones->servicioRealizar         = $request->servicioRealizar;
                $adscripciones->fechaFin                 = $request->fechaFin;
                $adscripciones->fechaInicio              = $request->fechaInicio;
                $adscripciones->razon                    = $request->razon;
                $adscripciones->observacionesAdscripcion = $request->observacionesAdscripcion;
                $adscripciones->idPersona                = Session::get('idPersona');
                $adscripciones->save();
                DB::commit();
                return redirect('policias/adscripcion');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function estimulos(EstimulosRequest $request)
    {
        $tipos = Catalogo::Estimulos();
        if ($request->getMethod() == "GET") {
            try {
                $estimulos = Estimulo::where('idPersona', Session::get('idPersona'))->get();
            } catch (\Exception $e) {
                $estimulos = [];
            }
            return view('policias.estimulos.create')
            ->with('estimulos', $estimulos)
            ->with('tipos', $tipos)
            ->render();
        } else {
            DB::beginTransaction();
            $estimulos = new Estimulo();
            try {
                $estimulos->tipoRecompensa         = $request->tipoRecompensa;
                $estimulos->motivo                 = $request->motivo;
                $estimulos->premio                 = $request->premio;
                $estimulos->fechaEmision           = $request->fechaEmision;
                $estimulos->observacionesEstimulos = $request->observacionesEstimulos;
                $estimulos->idPersona              = Session::get('idPersona');
                $estimulos->save();
                DB::commit();
                return redirect('policias/estimulos');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function planIndividual(PlanRequest $request)
    {
        $nombres = EvaluationType::lists('Name', 'ID');
        $apto = Catalogo::Apto();
        if ($request->getMethod() == "GET") {
            $instancias = Instancia::lists('Name', 'ID');
            try {
                $planes = Plan::where('idPersona', Session::get('idPersona'))->get();
            } catch (\Exception $e) {
                $planes = [];
            }
            return view('policias.plan.create')
            ->with('planes', $planes)
            ->with('nombres', $nombres)
            ->with('instancias', $instancias)
            ->with('caracteristicas', [])
            ->with('apto', $apto)
            ->with('editable', false)
            ->render();
        } else {
            DB::beginTransaction();
            $File = $request->file('documento');
            $planes  = new Plan();
            $archivo = new Archivo();
            try {
                $archivo->name = $File->getClientOriginalName();
                $archivo->size = $File->getSize();
                $archivo->type = $File->getMimeType();
                $archivo->ext  = $File->getClientOriginalExtension();
                $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                $archivo->save();
                $planes->nombre           = $request->nombre;
                $planes->caracteristicas  = $request->caracteristicas;
                $planes->instancia        = $request->instancia;
                $planes->gradoImportancia = $request->gradoImportancia;
                $planes->duracion         = $request->duracion;
                $planes->obligatorio      = $request->obligatorio;
                $planes->fechaInicio      = $request->fechaInicio;
                $planes->fechaTermino     = $request->fechaTermino;
                $planes->documento        = $archivo->id;
                $planes->resultado        = $request->resultado;
                $planes->comentarios      = $request->comentarios;
                $planes->idPersona        = Session::get('idPersona');
                $planes->save();
                DB::commit();
                return redirect('policias/planIndividual');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function administracion(AdministracionRequest $request)
    {
        $apto = Catalogo::Apto();
        if ($request->getMethod() == "GET") {
            try {
                $administraciones = Administracion::where('idPersona', Session::get('idPersona'))->get();
            } catch (\Exception $e) {
                $administraciones = [];
            }
            return view('policias.administracion.create')
            ->with('administraciones', $administraciones)
            ->render();
        } else {
            DB::beginTransaction();
            $File = $request->file('documento');
            $administraciones  = new Administracion();
            $archivo = new Archivo();
            try {
                $archivo->name = $File->getClientOriginalName();
                $archivo->size = $File->getSize();
                $archivo->type = $File->getMimeType();
                $archivo->ext  = $File->getClientOriginalExtension();
                $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                $archivo->save();
                $administraciones->descripcion    = $request->descripcion;
                $administraciones->fechaInicio    = $request->fechaInicio;
                $administraciones->fechaTermino   = $request->fechaTermino;
                $administraciones->documento      = $archivo->id;
                $administraciones->autorizante    = $request->autorizante;
                $administraciones->asignado       = $request->asignado;
                $administraciones->realizado      = $request->realizado;
                $administraciones->comprobado     = $request->comprobado;
                $administraciones->validado       = $request->validado;
                $administraciones->requerimientos = $request->requerimientos;
                $administraciones->observaciones  = $request->observaciones;
                $administraciones->idPersona      = Session::get('idPersona');
                $administraciones->save();
                DB::commit();
                return redirect('policias/administracion');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function separacion(SeparacionRequest $request)
    {
        $separacion = Catalogo::Separaciones();
        if ($request->getMethod() == "GET") {
            try {
                $separaciones = Separacion::where('idPersona', Session::get('idPersona'))->firstOrFail();
                $editable = true;
            } catch (ModelNotFoundException $e) {
                $separaciones = [];
                $editable = false;
            }
            return view('policias.separacion.create')
            ->with('separacion', $separacion)
            ->with('separaciones', $separaciones)
            ->with('editable', $editable)
            ->render();
        } else {
            try {
                $separaciones = Separacion::where('idPersona', Session::get('idPersona'))
                ->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $separaciones = new Separacion();
            }
            DB::beginTransaction();
            $File = $request->file('certificado');
            $archivo = new Archivo();
            try {
                if ($request->file('certificado') !== null) {
                    $archivo->name = $File->getClientOriginalName();
                    $archivo->size = $File->getSize();
                    $archivo->type = $File->getMimeType();
                    $archivo->ext  = $File->getClientOriginalExtension();
                    $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                    $archivo->save();
                    $separaciones->certificado = $archivo->id;
                }
                $separaciones->separacion      = $request->separacion;
                $separaciones->tipoSeparacion  = $request->tipoSeparacion;
                $separaciones->fechaSeparacion = $request->fechaSeparacion;
                $separaciones->motivo          = $request->motivo;
                $separaciones->aprobado        = $request->aprobado;
                $separaciones->idPersona       = Session::get('idPersona');
                $separaciones->save();
                DB::commit();
                return redirect('policias/separacion');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function evaluaciones(EvaluacionesPoliciasRequest $request)
    {
        $examenes = Catalogo::Examenes();
        $resultados = Catalogo::Cumplimiento();
        if ($request->getMethod() == "GET") {
            try {
                $evaluaciones = EvaluacionesPolicias::where('idPersona', Session::get('idPersona'))->get();
            } catch (\Exception $e) {
                $evaluaciones = [];
            }
            return view('policias.evaluaciones.create')
            ->with('evaluaciones', $evaluaciones)
            ->with('examenes', $examenes)
            ->with('resultados', $resultados)
            ->render();
        } else {
            DB::beginTransaction();
            try {
                $evaluaciones = new EvaluacionesPolicias();
                $evaluaciones->examen          = $request->examen;
                $evaluaciones->resultado       = $request->resultado;
                $evaluaciones->fechaResultados = $request->fechaResultados;
                $evaluaciones->fecha           = $request->fecha;
                $evaluaciones->observaciones   = $request->observaciones;
                $evaluaciones->idPersona       = Session::get('idPersona');
                $evaluaciones->save();
                DB::commit();
                return response('Se guardo correctamente la información');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function destroy($id)
    {
        Policia::find($id)->delete();
        return $this->viewListado();
    }
}
