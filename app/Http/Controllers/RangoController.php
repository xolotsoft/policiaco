<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RangosRequest;
use App\Models\Rango;
use App\Models\Catalogo;
use Carbon\Carbon;
use DB;

class RangoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rangos = Rango::all();
        return view('catalogos.rangos.index')
        ->with('rangos', $rangos)
        ->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoriaPolicial = Catalogo::categoriaPolicial();
        return view('catalogos.rangos.create')
        ->with('categoriaPolicial', $categoriaPolicial)
        ->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RangosRequest $request)
    {
        DB::beginTransaction();
        try {
            $rango = new Rango();
            $rango->Category                     = $request->Category;
            $rango->Rank                         = texto($request->Rank);
            $rango->CommandLevel                 = $request->CommandLevel;
            $rango->Special                      = $request->Special;
            $rango->Normal_Salary                = $request->Normal_Salary;
            $rango->Special_Salary               = $request->Special_Salary;
            $rango->OnlyMexican                  = $request->OnlyMexican;
            $rango->HeightMan                    = $request->HeightMan;
            $rango->HeightWoman                  = $request->HeightWoman;
            $rango->MinimumSchooling             = $request->MinimumSchooling;
            $rango->Age                          = $request->Age;
            $rango->Old                          = $request->Old;
            $rango->Courses_ContinuedFormation   = $request->Courses_ContinuedFormation;
            $rango->Courses_SpecializedFormation = $request->Courses_SpecializedFormation;
            $rango->Active_ContinuedFormation    = $request->Active_ContinuedFormation;
            $rango->Active_SpecializedFormation  = $request->Active_SpecializedFormation;
            $rango->ApproveCertificate           = $request->ApproveCertificate;
            $rango->ApproveCertificateFileName   = $request->ApproveCertificateFileName;
            $rango->save();
            DB::commit();
            return $this->index();
        } catch (\Exception $e) {
            DB::rollback();
            return response('Hubo un problema al guardar la información', 510);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rango = Rango::find($id);
        return view('catalogos.rangos.show')
        ->with('rango', $rango)
        ->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoriaPolicial = Catalogo::categoriaPolicial();
        $rango = Rango::find($id);
        return view('catalogos.rangos.edit')
        ->with('rango', $rango)
        ->with('categoriaPolicial', $categoriaPolicial)
        ->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RangosRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $rango = Rango::find($id);
            $rango->Category                     = $request->Category;
            $rango->Rank                         = texto($request->Rank);
            $rango->CommandLevel                 = $request->CommandLevel;
            $rango->Special                      = $request->Special;
            $rango->Normal_Salary                = $request->Normal_Salary;
            $rango->Special_Salary               = $request->Special_Salary;
            $rango->OnlyMexican                  = $request->OnlyMexican;
            $rango->HeightMan                    = $request->HeightMan;
            $rango->HeightWoman                  = $request->HeightWoman;
            $rango->MinimumSchooling             = $request->MinimumSchooling;
            $rango->Age                          = $request->Age;
            $rango->Old                          = $request->Old;
            $rango->Courses_ContinuedFormation   = $request->Courses_ContinuedFormation;
            $rango->Courses_SpecializedFormation = $request->Courses_SpecializedFormation;
            $rango->Active_ContinuedFormation    = $request->Active_ContinuedFormation;
            $rango->Active_SpecializedFormation  = $request->Active_SpecializedFormation;
            $rango->ApproveCertificate           = $request->ApproveCertificate;
            $rango->ApproveCertificateFileName   = $request->ApproveCertificateFileName;
            $rango->save();
            DB::commit();
            return $this->index();
        } catch (\Exception $e) {
            DB::rollback();
            return response('Hubo un problema al editar la información', 510);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Rango::find($id)->delete();
        return $this->index();
    }
}
