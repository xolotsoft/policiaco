<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests\AdscripcionRequest;
use App\Http\Requests;
use App\Models\Adscripcion;
use DB;

class AdscripcionController extends Controller
{
    public function ver($id)
    {
        $adscripcion = Adscripcion::find($id);
        return response('');
    }

    public function editar($id)
    {
        $adscripcion = Adscripcion::find($id);
        return view('policias.adscripcion.edit')
        ->with('adscripcion', $adscripcion)
        ->render();
    }

    public function actualizar(AdscripcionRequest $request)
    {
        DB::beginTransaction();
        try {
            $adscripcion = Adscripcion::find($request->id);
            $adscripcion->areaProcedencia          = $request->areaProcedencia;
            $adscripcion->areaDesignacion          = $request->areaDesignacion;
            $adscripcion->servicioRealizaba        = $request->servicioRealizaba;
            $adscripcion->servicioRealizar         = $request->servicioRealizar;
            $adscripcion->fechaFin                 = $request->fechaFin;
            $adscripcion->fechaInicio              = $request->fechaInicio;
            $adscripcion->razon                    = $request->razon;
            $adscripcion->observacionesAdscripcion = $request->observacionesAdscripcion;
            $adscripcion->idPersona                = Session::get('idPersona');
            $adscripcion->save();
            DB::commit();
            return response('Se guardo correctamente la información');
        } catch (\Exeption $e) {
            DB::rollback();
            return response('Hubo un problema al guardar en la base de datos', 510);
        }
    }

    public function eliminar($id)
    {
        $adscripcion = Adscripcion::find($id)->delete();
        return response('Se eliminó correctamente al adscripcion');
    }
}
