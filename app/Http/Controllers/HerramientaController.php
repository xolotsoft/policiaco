<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests\HerramientasRequest;
use App\Http\Requests;
use App\Models\Herramienta;
use App\Models\Catalogo;
use DB;

class HerramientaController extends Controller
{
    public function ver($id)
    {
        $herramienta = Herramienta::find($id);
        return response('');
    }

    public function editar($id)
    {
        $herramienta = Herramienta::find($id);
        $tipo = Catalogo::Herramientas();
        return view('policias.herramientas.edit')
        ->with('herramienta', $herramienta)
        ->with('tipo', $tipo)
        ->with('editable', true)
        ->render();
    }

    public function actualizar(HerramientasRequest $request)
    {
        DB::beginTransaction();
        try {
            $herramienta = Herramienta::find($request->id);
            $herramienta->idPersona                 = Session::get('idPersona');
            $herramienta->areaResguardo             = $request->areaResguardo;
            $herramienta->descripcion               = $request->descripcion;
            $herramienta->tipo                      = $request->tipo;
            $herramienta->cantidad                  = $request->cantidad;
            $herramienta->fechaRecepcion            = $request->fechaRecepcion;
            $herramienta->fechaDevolucion           = $request->fechaDevolucion;
            $herramienta->observacionesHerramientas = $request->observacionesHerramientas;
            $herramienta->save();
            DB::commit();
            return response('Se guardo correctamente la información');
        } catch (\Exeption $e) {
            DB::rollback();
            return response('Hubo un problema al guardar en la base de datos', 510);
        }
    }

    public function eliminar($id)
    {
        $herramienta = Herramienta::find($id)->delete();
        return response('Se eliminó correctamente al herramienta');
    }
}
