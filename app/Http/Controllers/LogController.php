<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests;
use App\Http\Requests\LoginRequest;
use App\Municipio;
use App\User;
use Carbon\Carbon;
use Session;
use Config;
use Auth;

class LogController extends Controller
{
    public function log()
    {
        if (Auth::check()) {
            return redirect('aspirantes/nuevo');
        } else {
            $municipios = Municipio::where('estado', 1)->get();
            return view('login.login')
            ->with('municipios', $municipios);
        }
    }

    public function login(Request $request)
    {
        if ($request->username == User::find(1)->username) {
            $aut = Auth::attempt([
                'username' => $request->username,
                'password' => $request->password
            ]);
        } else {
            $aut = Auth::attempt([
                'username' => $request->username,
                'password' => $request->password,
                'municipio_id' => $request->municipio
            ]);
        }
        if ($aut) {
            if (Auth::user()->perfil->id == 1) {
                $user = User::find(Auth::user()->id);
                $user->municipio_id = $request->municipio;
                $user->save();
                Auth::user()->fresh();
            } else {
                if (Auth::user()->municipio_id != $request->municipio) {
                    Auth::logout();
                    $request->session()->flash('error', 'Los datos son incorrectos');
                    return redirect('/');
                }
                if (Carbon::parse(Auth::user()->expired_at)->lt(Carbon::now())) {
                    Auth::logout();
                    $request->session()->flash('error', 'La licencia ah expirado');
                    return redirect('/');
                }
                if (Auth::user()->status != 1) {
                    Auth::logout();
                    $request->session()->flash('error', 'El usuario esta inactivo');
                    return redirect('/');
                }
            }
            return redirect('aspirantes/listado');
        } else {
            $request->session()->flash('error', 'Los datos son incorrectos');
            return redirect('/');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect(\URL::previous());
    }
}
