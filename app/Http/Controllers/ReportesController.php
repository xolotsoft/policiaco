<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
/*---------- MODELS ------------*/
use App\Models\EvaluacionesPolicias;
use App\Models\Archivo;
use App\Models\Aspirante;
use App\Models\Persona;
use App\Models\Domicilio;
use App\Models\Familia;
use App\Models\Reclutamiento;
use App\Models\Convocatoria;
use App\Models\State;
use App\Models\Countries;
use App\Models\Provinces;
use App\Models\Areas;
use App\Models\Catalogo;
use App\Models\Seleccion;
use App\Models\Rango;
use App\Models\Formations;
use App\Models\Certificacion;
use App\Models\Reingresos;
use App\Models\Policia;
use App\Models\Instancia;
use App\Models\Mensaje;
use App\Models\Separacion;
use App\Models\EvaluationType;
use App\Log;
use DB;

class ReportesController extends Controller
{
    public function aspirantesKardex()
    {
        $aspirantes = Aspirante::all();
        return view('page')
        ->with('aspirantes', $aspirantes);
    }

    public function listadoKardex()
    {
        $aspirantes = Aspirante::all();
        return view('reportes.aspirantesKardex')
        ->with('aspirantes', $aspirantes);
    }

    public function aspiranteDetalleKardex($id)
    {
        $persona = Persona::find($id);
        return view('reportes.aspirantesDetalleKardex')
        ->with('persona', $persona);
    }

    public function aspirantesAprobados()
    {
        $aspirantes = DB::table('evaluacionespolicias')
        ->select(DB::raw('count(*) AS Total, aspirantes.folio, persona.rfc, persona.nombre, persona.aMaterno, persona.aPaterno, persona.id'))
        ->join('aspirantes', 'aspirantes.idPersona', '=', 'evaluacionespolicias.idPersona')
        ->join('persona', 'persona.id', '=', 'evaluacionespolicias.idPersona')
        ->where('evaluacionespolicias.resultado', '=', '26')
        ->groupBy('evaluacionespolicias.idPersona')
        ->get();
        return view('page')
        ->with('aspirantes', $aspirantes);
    }

    public function aspirantesNoAprobados()
    {
        $aspirantes = DB::table('persona')
        ->select(DB::raw('count(*) AS Total, aspirantes.folio, persona.rfc, persona.nombre, persona.aMaterno, persona.aPaterno, persona.id'))
        ->join('aspirantes', 'aspirantes.idPersona', '=', 'persona.id')
        ->leftJoin('evaluacionespolicias', function($join) {
            $join->on('persona.id', '=', 'evaluacionespolicias.idPersona')
            ->where('evaluacionespolicias.resultado', '=', '26');
        })
        ->whereNull('persona.deleted_at')
        ->whereNull('aspirantes.deleted_at')
        ->groupBy('persona.id')
        ->get();
        return view('page')
        ->with('aspirantes', $aspirantes);
    }

    public function aspirantesBajas()
    {
        $eliminados = Aspirante::whereNotNull('deleted_at')
        ->pluck('idPersona')->toArray();
        $separacion = Aspirante::select('aspirantes.idPersona')->join('separacion', 'separacion.idPersona', '=', 'aspirantes.idPersona')
        ->pluck('aspirante.idPersona')->toArray();
        $arreglos = array_merge($eliminados, $separacion);
        $aspirantes = Aspirante::whereIn('idPersona', $arreglos)->get();
        return view('page')
        ->with('aspirantes', $aspirantes);
    }

    public function aspirantesControlDeConfianza()
    {
        $aspirantes = Aspirante::all();
        return view('page')
        ->with('aspirantes', $aspirantes);
    }

    public function listadoControl()
    {
        $aspirantes = Aspirante::all();
        return view('reportes.aspirantesControlDeConfianza')
        ->with('aspirantes', $aspirantes);
    }

    public function aspiranteDetalleControl($id)
    {
        $persona = Persona::find($id);
        return view('reportes.aspirantes.control.detalle')
        ->with('persona', $persona);
    }

    public function policiasKardex()
    {
        $policias = Policia::all();
        return view('page')
        ->with('policias', $policias);
    }

    public function policiaDetalleKardex($id)
    {
        $persona = Persona::find($id);
        return view('reportes.policiasDetalleKardex')
        ->with('persona', $persona);
    }

    public function policiaListadoKardex()
    {
        $policias = Policia::all();
        return view('reportes.policiasKardex')
        ->with('policias', $policias);
    }

    public function policiasActivos()
    {
        $inactivos = Policia::select('policias.idPersona')
        ->join('separacion', 'policias.idPersona', '=', 'separacion.idPersona')
        ->get();
        $policias = Policia::whereNotIn('idPersona', $inactivos)->get();
        return view('page')
        ->with('policias', $policias);
    }

    public function policiasInactivos()
    {
        $policias =  Policia::select('policias.*')
        ->join('separacion', 'policias.idPersona', '=', 'separacion.idPersona')
        ->get();
        return view('page')
        ->with('policias', $policias);
    }

    public function policiasSeparacionDelServicio()
    {
        $policias =  Policia::select('policias.*')
        ->join('separacion', 'policias.idPersona', '=', 'separacion.idPersona')
        ->get();
        return view('page')
        ->with('policias', $policias);
    }

    public function policiasControlDeConfianza()
    {
        $policias = Policia::all();
        return view('page')
        ->with('policias', $policias);
    }

    public function policiasListadoControl()
    {
        $policias = Policia::all();
        return view('reportes.policiasControlDeConfianza')
        ->with('policias', $policias);
    }

    public function policiaDetalleControl($id)
    {
        $persona = Persona::find($id);
        return view('reportes.policias.control.detalle')
        ->with('persona', $persona);
    }

    public function elementos()
    {
        return view('page');
    }

    public function costos()
    {
      $costos = Rango::costos();
      return view('page')
      ->with('costos', $costos);
    }

    public function bitacora()
    {
        $bitacora = Log::all();
        return view('page')
        ->with('bitacora', $bitacora);
    }

    public function certificaciones()
    {
        $certificados = Certificacion::all();
        return view('page')
        ->with('certificados', $certificados);
    }

    public function edades()
    {
        return view('page');
    }

    public function graficadosActivos()
    {
        $civil = Catalogo::Civil();
        $estados = State::lists('Name', 'ID');
        $categoriaPolicial = Catalogo::categoriaPolicial();
        $tipos = Catalogo::Sancion();
        $tipo = Catalogo::Herramientas();
        $estimulos = Catalogo::Estimulos();
        $nombres = EvaluationType::lists('Name', 'ID');
        $apto = Catalogo::Apto();
        $instancias = Instancia::lists('Name', 'ID');
        $examenes = Catalogo::Examenes();
        $resultados = Catalogo::Cumplimiento();
        $separacion = Catalogo::Separaciones();
        return view('page')
        ->with('civil', $civil)
        ->with('categoriaPolicial', $categoriaPolicial)
        ->with('rangos', [])
        ->with('tipo', $tipo)
        ->with('estimulos', $estimulos)
        ->with('tipos', $tipos)
        ->with('nombres', $nombres)
        ->with('instancias', $instancias)
        ->with('caracteristicas', [])
        ->with('apto', $apto)
        ->with('examenes', $examenes)
        ->with('resultados', $resultados)
        ->with('separacion', $separacion)
        ->with('estados', $estados);
    }

    public function graficadosActivosDatos(Request $request)
    {
        $total = Persona::join('policias', 'policias.idPersona', '=', 'persona.id')->get()->count();
        $resultado = Persona::graficadosActivos($request);
        if($resultado->isEmpty()) {
            $resultado = 0;
        } else {
            $resultado = $resultado->count();
        }
        return response()->json(['resultado' => $resultado, 'total' => $total-$resultado], 200);
    }

    public function graficadosInactivos()
    {
        $civil         = Catalogo::Civil();
        $estados       = State::lists('Name', 'ID');
        $categoriaPolicial = Catalogo::categoriaPolicial();
        $tipos = Catalogo::Sancion();
        $tipo = Catalogo::Herramientas();
        $estimulos = Catalogo::Estimulos();
        $nombres = EvaluationType::lists('Name', 'ID');
        $apto = Catalogo::Apto();
        $instancias = Instancia::lists('Name', 'ID');
        $examenes = Catalogo::Examenes();
        $resultados = Catalogo::Cumplimiento();
        $separacion = Catalogo::Separaciones();
        return view('page')
        ->with('civil', $civil)
        ->with('categoriaPolicial', $categoriaPolicial)
        ->with('rangos', [])
        ->with('tipo', $tipo)
        ->with('estimulos', $estimulos)
        ->with('tipos', $tipos)
        ->with('nombres', $nombres)
        ->with('instancias', $instancias)
        ->with('caracteristicas', [])
        ->with('apto', $apto)
        ->with('examenes', $examenes)
        ->with('resultados', $resultados)
        ->with('separacion', $separacion)
        ->with('estados', $estados);
    }
}
