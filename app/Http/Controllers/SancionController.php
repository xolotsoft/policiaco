<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests\SancionesRequest;
use App\Http\Requests;
use App\Models\Sancion;
use App\Models\Catalogo;
use DB;

class SancionController extends Controller
{
    public function ver($id)
    {
        $sancion = Sancion::find($id);
        return response('');
    }

    public function editar($id)
    {
        $sancion = Sancion::find($id);
        $tipos = Catalogo::Sancion();
        return view('policias.sanciones.edit')
        ->with('sancion', $sancion)
        ->with('tipos', $tipos)
        ->render();
    }

    public function actualizar(SancionesRequest $request)
    {
        DB::beginTransaction();
        try {
            $sancion = Sancion::find($request->id);
            $sancion->idPersona              = Session::get('idPersona');
            $sancion->tipoSanciones          = $request->tipoSanciones;
            $sancion->fechaInicio            = $request->fechaInicio;
            $sancion->fechaConclusion        = $request->fechaConclusion;
            $sancion->duracion               = $request->duracion;
            $sancion->causa                  = $request->causa;
            $sancion->fechaSanciones         = $request->fechaSanciones;
            $sancion->observacionesSanciones = $request->observacionesSanciones;
            $sancion->save();
            DB::commit();
            return response('Se guardo correctamente la información');
        } catch (\Exeption $e) {
            DB::rollback();
            return response('Hubo un problema al guardar en la base de datos', 510);
        }
    }

    public function eliminar($id)
    {
        $sancion = Sancion::find($id)->delete();
        return response('Se eliminó correctamente al sanción');
    }
}
