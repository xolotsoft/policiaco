<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ColegiadoRequest;
use App\Models\Colegiado;
use App\Models\Archivo;
use App\Models\Catalogo;
use DB;

class ColegiadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organos = Colegiado::all();
        return view('policias.organoColegiado.index')
        ->with('organos', $organos)
        ->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $comisiones = Catalogo::organosColegiados();
        return view('policias.organoColegiado.create')
        ->with('editable', false)
        ->with('comisiones', $comisiones)
        ->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ColegiadoRequest $request)
    {
        $documentos = [
            'acuerdos',
            'resoluciones',
            'actaSesion'
        ];
        DB::beginTransaction();
        try {
            $colegiado = new Colegiado();
            foreach ($documentos as $documento) {
                if ($request->file($documento) !== null) {
                    $archivo = new Archivo();
                    $File = $request->file($documento);
                    $archivo->name = $File->getClientOriginalName();
                    $archivo->size = $File->getSize();
                    $archivo->type = $File->getMimeType();
                    $archivo->ext  = $File->getClientOriginalExtension();
                    $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                    $archivo->save();
                    $colegiado[$documento] = $archivo->id;
                    unset($File);
                }
            }
            $colegiado->comision      = $request->comision;
            $colegiado->integrante    = $request->integrante;
            $colegiado->sesion        = $request->sesion;
            $colegiado->fechasesion   = $request->fechasesion;
            $colegiado->temas         = $request->temas;
            $colegiado->observaciones = $request->observaciones;
            $colegiado->save();
            DB::commit();
            return $this->index();
        } catch (\Exception $e) {
            DB::rollback();
            return response($e->getMessage(), 510);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $colegiado = Colegiado::find($id);
        return view('policias.organoColegiado.show')
        ->with('colegiado', $colegiado)
        ->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comisiones = Catalogo::organosColegiados();
        $colegiado = Colegiado::find($id);
        return view('policias.organoColegiado.edit')
        ->with('editable', true)
        ->with('comisiones', $comisiones)
        ->with('colegiado', $colegiado)
        ->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ColegiadoRequest $request, $id)
    {
        $documentos = [
            'acuerdos',
            'resoluciones',
            'actaSesion'
        ];
        DB::beginTransaction();
        try {
            $colegiado = Colegiado::find($id);
            foreach ($documentos as $documento) {
                if ($request->file($documento) !== null) {
                    $archivo = new Archivo();
                    $File = $request->file($documento);
                    $archivo->name = $File->getClientOriginalName();
                    $archivo->size = $File->getSize();
                    $archivo->type = $File->getMimeType();
                    $archivo->ext  = $File->getClientOriginalExtension();
                    $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                    $archivo->save();
                    $colegiado[$documento] = $archivo->id;
                    unset($File);
                }
            }
            $colegiado->comision      = $request->comision;
            $colegiado->integrante    = $request->integrante;
            $colegiado->sesion        = $request->sesion;
            $colegiado->fechasesion   = $request->fechasesion;
            $colegiado->temas         = $request->temas;
            $colegiado->observaciones = $request->observaciones;
            $colegiado->save();
            DB::commit();
            return $this->index();
        } catch (\Exception $e) {
            DB::rollback();
            return response($e->getMessage(), 510);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Colegiado::find($id)->delete();
        return $this->index();
    }
}
