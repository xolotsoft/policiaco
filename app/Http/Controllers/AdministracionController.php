<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Perfil;
use App\Municipio;
use App\Models\Mensaje;

class AdministracionController extends Controller
{
    public function usuarios()
    {
        $usuarios = User::listado();
        return view('page')
        ->with('usuarios', $usuarios);
    }

    public function perfiles()
    {
        $perfiles = Perfil::all();
        return view('page')
        ->with('perfiles', $perfiles);
    }

    public function municipios()
    {
        $municipios = Municipio::all();
        return view('page')
        ->with('municipios', $municipios);
    }
}
