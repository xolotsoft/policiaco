<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests\EvaluacionesPoliciasRequest;
use App\Http\Requests;
use App\Models\EvaluacionesPolicias;
use App\Models\Catalogo;
use DB;

class ControlController extends Controller
{
    public function ver($id)
    {
        $evaluacion = EvaluacionesPolicias::find($id);
        return response('');
    }

    public function editar($id)
    {
        $evaluacion = EvaluacionesPolicias::find($id);
        $examenes = Catalogo::Examenes();
        $resultados = Catalogo::Cumplimiento();
        return view('policias.evaluaciones.edit')
        ->with('evaluacion', $evaluacion)
        ->with('examenes', $examenes)
        ->with('resultados', $resultados)
        ->render();
    }

    public function actualizar(EvaluacionesPoliciasRequest $request)
    {
        DB::beginTransaction();
        try {
            $evaluacion = EvaluacionesPolicias::find($request->id);
            $evaluacion->examen          = $request->examen;
            $evaluacion->resultado       = $request->resultado;
            $evaluacion->fechaResultados = $request->fechaResultados;
            $evaluacion->fecha           = $request->fecha;
            $evaluacion->idPersona       = Session::get('idPersona');
            $evaluacion->save();
            DB::commit();
            return response('Se guardo correctamente la información');
        } catch (\Exeption $e) {
            DB::rollback();
            return response('Hubo un problema al guardar en la base de datos', 510);
        }
    }

    public function eliminar($id)
    {
        $evaluacion = EvaluacionesPolicias::find($id)->delete();
        return response('Se eliminó correctamente al evaluacion');
    }
}
