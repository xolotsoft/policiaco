<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Areas;
use App\Models\Provinces;
use App\Models\State;
use App\Models\Countries;
use App\Models\Catalogo;
use App\Models\Rango;
use App\Models\Evaluacion;
use DB;

class ListadosController extends Controller
{
    public function estados($id)
    {
        $estados = State::where('IdCountries', '=', $id)->select('ID', 'Name')->get()->toJson();
        return $estados;
    }

    public function municipios($id)
    {
        $municipios = Provinces::where('StateId', '=', $id)->select('ID', 'Name')->get()->toJson();
        return $municipios;
    }

    public function colonias($id)
    {
        $colonias = Areas::where('ProvinceId', '=', $id)->select('ID', 'Name')->get()->toJson();
        return $colonias;
    }

    public function gradoPolicial($id)
    {
        $colonias = Rango::where('Category', '=', $id)->select('id', 'Rank')->get()->toJson();
        return $colonias;
    }

    public function tipoSeparacion($id)
    {
        if ($id == '1') {
            $tipos = Catalogo::where('idMetacatalogo', 9)
            ->select('nombre', 'id')
            ->get()
            ->toJson();
        } else {
            $tipos = Catalogo::where('idMetacatalogo', 24)
            ->select('nombre', 'id')
            ->get()
            ->toJson();
        }

        return $tipos;
    }

    public function caracteristicas($id)
    {
        $caracteristicas = Evaluacion::where('EvaluationType', '=', $id)
        ->select('ID', DB::raw('EvaluationName AS Name'))
        ->get()
        ->toJson();
        return $caracteristicas;
    }
}
