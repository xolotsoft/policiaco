<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Archivo;

class ArchivosController extends Controller
{
    public function ver($id)
    {
        $archivo = Archivo::find($id);
        return view('archivos.index')
        ->with('archivo', $archivo);
    }

    public function descargar($id)
    {
        $archivo = Archivo::find($id);
        $file_contents = base64_decode($archivo->data);
        return response($file_contents)
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', $archivo->mime)
            ->header('Content-length', strlen($file_contents))
            ->header('Content-Disposition', 'attachment; filename=' . $archivo->name)
            ->header('Content-Transfer-Encoding', 'binary');
    }
}
