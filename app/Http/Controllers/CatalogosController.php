<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Models\Convocatoria;
use App\Models\Evaluacion;
use App\Models\Rango;
use App\Models\Instancia;
use App\Models\Areas;
use App\Models\Catalogo;

class CatalogosController extends Controller
{
    public function convocatorias()
    {
        $convocatorias = Convocatoria::all();
        return view('page')->with('convocatorias', $convocatorias);
    }

    public function evaluaciones()
    {
        $evaluaciones = Evaluacion::all();
        return view('page')->with('evaluaciones', $evaluaciones);
    }
    public function rangos()
    {
        $rangos = Rango::all();
        return view('page')->with('rangos', $rangos);
    }

    public function instanciasCapacitadoras()
    {
        $instancias = Instancia::all();
        return view('page')->with('instancias', $instancias);
    }

    public function coloniaNueva($colonia, $municipio)
    {
        $colonia = texto($colonia);
        try{
            Areas::where('Name', $colonia)->firstOrFail();
            return response()->json('Ya existe la colonia', 510);
        } catch (ModelNotFoundException $e) {
            try {
                $area = new Areas();
                $area->Name = $colonia;
                $area->ProvinceId = $municipio;
                $area->save();
                return response()->json(['id' => $area->ID, 'nombre' => $colonia]);
            } catch (\Exeption $e) {
                return response()->json('Error', 510);
            }
        }
    }
    public function serviciosMedicos($valor, $meta)
    {
        $valor = texto($valor);
        try{
            Catalogo::where('nombre', $valor)->firstOrFail();
            return response()->json('Ya existe la valor', 510);
        } catch (ModelNotFoundException $e) {
            try {
                $opcion = new Catalogo();
                $opcion->nombre = $valor;
                $opcion->idMetacatalogo = $meta;
                $opcion->save();
                return response()->json(['id' => $opcion->id, 'nombre' => $valor]);
            } catch (\Exeption $e) {
                return response()->json('Error', 510);
            }
        }
    }
    public function posgrado($valor, $meta)
    {
        $valor = texto($valor);
        try{
            Catalogo::where('nombre', $valor)->firstOrFail();
            return response()->json('Ya existe la valor', 510);
        } catch (ModelNotFoundException $e) {
            try {
                $opcion = new Catalogo();
                $opcion->nombre = $valor;
                $opcion->idMetacatalogo = $meta;
                $opcion->save();
                return response()->json(['id' => $opcion->id, 'nombre' => $valor]);
            } catch (\Exeption $e) {
                return response()->json('Error', 510);
            }
        }
    }
    public function comisionNueva($valor, $meta)
    {
        $valor = texto($valor);
        try{
            Catalogo::where('nombre', $valor)->firstOrFail();
            return response()->json('Ya existe la valor', 510);
        } catch (ModelNotFoundException $e) {
            try {
                $opcion = new Catalogo();
                $opcion->nombre = $valor;
                $opcion->idMetacatalogo = $meta;
                $opcion->save();
                return response()->json(['id' => $opcion->id, 'nombre' => $valor]);
            } catch (\Exeption $e) {
                return response()->json('Error', 510);
            }
        }
    }
}
