<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FamiliaresRequest;
use App\Http\Requests;
use App\Models\Familia;
use App\Models\Catalogo;
use DB;

class FamiliaresController extends Controller
{
    public function ver($id)
    {
        $familiar = Familia::find($id);
        return response('');
    }

    public function editar($id)
    {
        $genero     = Catalogo::Genero();
        $parentesco = Catalogo::Parentesco();
        $familiar = Familia::find($id);
        return view('aspirantes.familiares.edit')
        ->with('genero', $genero)
        ->with('parentesco', $parentesco)
        ->with('familiar', $familiar)
        ->render();
    }

    public function actualizar(FamiliaresRequest $request)
    {
        DB::beginTransaction();
        try {
            $familia = Familia::find($request->id);
            $familia->categoria       = $request->categoria;
            $familia->nombre          = texto($request->nombre);
            $familia->aPaterno        = texto($request->aPaterno);
            $familia->aMaterno        = texto($request->aMaterno);
            $familia->fechaNacimiento = $request->fechaNacimiento;
            $familia->genero          = $request->genero;
            $familia->direccion       = texto($request->direccion);
            $familia->telefono        = $request->telefono;
            $familia->observacionesFamiliares   = $request->observacionesFamiliares;
            $familia->save();
            DB::commit();
            return response('Se guardo correctamente la información');
        } catch (\Exeption $e) {
            DB::rollback();
            return response('Hubo un problema al guardar en la base de datos', 510);
        }
    }

    public function eliminar($id)
    {
        $familiar = Familia::find($id)->delete();
        return response('Se eliminó correctamente al familiar');
    }
}
