<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\InstanciasRequest;
use App\Models\Instancia;
use Carbon\Carbon;
use DB;

class InstanciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instancias = Instancia::all();
        return view('catalogos.instanciasCapacitadoras.index')
        ->with('instancias', $instancias)
        ->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('catalogos.instanciasCapacitadoras.create')->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InstanciasRequest $request)
    {
        DB::beginTransaction();
        try {
            $instancia = new Instancia();
            $instancia->Name         = $request->Name;
            $instancia->Observations = $request->Observations;
            $instancia->save();
            DB::commit();
            return $this->index();
        } catch (\Exception $e) {
            DB::rollback();
            return response('Hubo un problema al guardar la información', 510);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $instancia = Instancia::find($id);
        return view('catalogos.instanciasCapacitadoras.show')
        ->with('instancia', $instancia)
        ->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instancia = Instancia::find($id);
        return view('catalogos.instanciasCapacitadoras.edit')
        ->with('instancia', $instancia)
        ->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InstanciasRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $instancia = Instancia::find($id);
            $instancia->Name         = $request->Name;
            $instancia->Observations = $request->Observations;
            $instancia->save();
            DB::commit();
            return $this->index();
        } catch (\Exception $e) {
            DB::rollback();
            return response('Hubo un problema al editar la información', 510);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Instancia::find($id)->delete();
        return $this->index();
    }
}
