<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests\PromocionesRequest;
use App\Http\Requests;
use App\Models\Promocion;
use App\Models\Catalogo;
use App\Models\Rango;
use DB;

class PromocionController extends Controller
{
    public function ver($id)
    {
        $promocion = Promocion::find($id);
        return response('');
    }

    public function editar($id)
    {
        $promocion = Promocion::find($id);
        $categoriaPolicial = Catalogo::categoriaPolicial();
        $puestos = Rango::where('Category', 73)->lists('Rank', 'ID');
        return view('policias.promociones.edit')
        ->with('promocion', $promocion)
        ->with('puestos', $puestos)
        ->with('categoriaPolicial', $categoriaPolicial)
        ->with('editable', true)
        ->render();
    }

    public function actualizar(PromocionesRequest $request)
    {
        DB::beginTransaction();
        try {
            $promocion = Promocion::find($request->id);
            $promocion->idPersona                = Session::get('idPersona');
            $promocion->cargo                    = $request->cargo;
            $promocion->rango                    = $request->rango;
            $promocion->fechaPromocion           = $request->fechaPromocion;
            $promocion->observacionesPromociones = $request->observacionesPromociones;
            $promocion->save();
            DB::commit();
            return response('Se guardo correctamente la información');
        } catch (\Exeption $e) {
            DB::rollback();
            return response('Hubo un problema al guardar en la base de datos', 510);
        }
    }

    public function eliminar($id)
    {
        $promocion = Promocion::find($id)->delete();
        return response('Se eliminó correctamente al promocion');
    }
}
