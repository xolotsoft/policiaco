<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use App\Models\Aspirante;
use App\Models\Rango;
use App\Models\Persona;
use App\Models\Policia;
use App\Models\Certificacion;
use App\Log;
use Excel;
use PDF;
use DB;
use App;

class ExportController extends Controller
{
    public function aspirantesKardex($id, $tipo)
    {
        $persona = Persona::find($id);
        if ($tipo == 'pdf') {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('exports.aspirantes.kardex', compact('persona'));
            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(280, 765, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            return $pdf->stream('ReporteAspirantesKardex'.Carbon::now()->format('Ymdhis').'.'.$tipo);
        } else {
            Excel::create('ReporteAspirantesKardex'.Carbon::now()->format('Ymdhis'), function ($excel) {
                $excel->sheet('New sheet', function ($sheet) {
                    $sheet->loadView('exports.aspirantes.kardex');
                });
            })->download($tipo);
        }
    }

    public function aspirantesAprobados($tipo)
    {
        $aspirantes = DB::table('evaluacionespolicias')
        ->select(DB::raw('count(*) AS Total, aspirantes.folio, persona.rfc, persona.nombre, persona.aMaterno, persona.aPaterno, persona.id'))
        ->join('aspirantes', 'aspirantes.idPersona', '=', 'evaluacionespolicias.idPersona')
        ->join('persona', 'persona.id', '=', 'evaluacionespolicias.idPersona')
        ->where('evaluacionespolicias.resultado', '=', '26')
        ->groupBy('evaluacionespolicias.idPersona')
        ->get();
        if ($tipo == 'pdf') {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('exports.aspirantes.aprobados', compact('aspirantes'));
            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(280, 765, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            return $pdf->stream('ReporteAspirantesAprobados'.Carbon::now()->format('Ymdhis').'.'.$tipo);
        } else {
            Excel::create('ReporteAspirantesAprobados'.Carbon::now()->format('Ymdhis'), function ($excel) {
                $excel->sheet('New sheet', function ($sheet) {
                    $sheet->loadView('exports.aspirantes.aprobados');
                });
            })->download($tipo);
        }
    }

    public function aspirantesNoAprobados($tipo)
    {
        $aspirantes = DB::table('persona')
        ->select(DB::raw('count(*) AS Total, aspirantes.folio, persona.rfc, persona.nombre, persona.aMaterno, persona.aPaterno, persona.id'))
        ->join('aspirantes', 'aspirantes.idPersona', '=', 'persona.id')
        ->leftJoin('evaluacionespolicias', function($join) {
            $join->on('persona.id', '=', 'evaluacionespolicias.idPersona')
            ->where('evaluacionespolicias.resultado', '=', '26');
        })
        ->whereNull('persona.deleted_at')
        ->whereNull('aspirantes.deleted_at')
        ->groupBy('persona.id')
        ->get();
        if ($tipo == 'pdf') {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('exports.aspirantes.noAprobados', compact('aspirantes'));
            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(280, 765, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            return $pdf->stream('ReporteAspirantesNoAprobados'.Carbon::now()->format('Ymdhis').'.'.$tipo);
        } else {
            Excel::create('ReporteAspirantesAprobados'.Carbon::now()->format('Ymdhis'), function ($excel) {
                $excel->sheet('New sheet', function ($sheet) {
                    $sheet->loadView('exports.aspirantes.noAprobados');
                });
            })->download($tipo);
        }
    }

    public function aspirantesBajas($tipo)
    {
        $eliminados = Aspirante::whereNotNull('deleted_at')
        ->pluck('idPersona')->toArray();
        $separacion = Aspirante::select('aspirantes.idPersona')->join('separacion', 'separacion.idPersona', '=', 'aspirantes.idPersona')
        ->pluck('aspirante.idPersona')->toArray();
        $arreglos = array_merge($eliminados, $separacion);
        $aspirantes = Aspirante::whereIn('idPersona', $arreglos)->get();
        if ($tipo == 'pdf') {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('exports.aspirantes.bajas', compact('aspirantes'));
            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(280, 765, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            return $pdf->stream('ReporteAspirantesBajas'.Carbon::now()->format('Ymdhis').'.'.$tipo);
        } else {
            Excel::create('ReporteAspirantesAprobados'.Carbon::now()->format('Ymdhis'), function ($excel) {
                $excel->sheet('New sheet', function ($sheet) {
                    $sheet->loadView('exports.aspirantes.aprobados');
                });
            })->download($tipo);
        }
    }

    public function aspirantesControl($id, $tipo)
    {
        $persona = Persona::find($id);
        if ($tipo == 'pdf') {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('exports.aspirantes.control.detalle', compact('persona'));
            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(280, 765, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            return $pdf->stream('ReporteAspirantesControl'.Carbon::now()->format('Ymdhis').'.'.$tipo);
        } else {
            Excel::create('ReporteAspirantesKardex'.Carbon::now()->format('Ymdhis'), function ($excel) {
                $excel->sheet('New sheet', function ($sheet) {
                    $sheet->loadView('exports.aspirantes.kardex');
                });
            })->download($tipo);
        }
    }

    public function policiasKardex($id, $tipo)
    {
        $persona = Persona::find($id);
        if ($tipo == 'pdf') {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('exports.policias.kardex', compact('persona'));
            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(280, 765, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            return $pdf->stream('ReportePoliciasKardex'.Carbon::now()->format('Ymdhis').'.'.$tipo);
        } else {
            Excel::create('ReportePoliciasKardex'.Carbon::now()->format('Ymdhis'), function ($excel) {
                $excel->sheet('New sheet', function ($sheet) {
                    $sheet->loadView('exports.policias.kardex');
                });
            })->download($tipo);
        }
    }

    public function policiasActivos($tipo)
    {
        $inactivos = Policia::select('policias.idPersona')
        ->join('separacion', 'policias.idPersona', '=', 'separacion.idPersona')
        ->get();
        $policias = Policia::whereNotIn('idPersona', $inactivos)->get();
        if ($tipo == 'pdf') {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('exports.policias.policiasActivos', compact('policias'));
            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(280, 765, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            return $pdf->stream('ReporteAspirantesAprobados'.Carbon::now()->format('Ymdhis').'.'.$tipo);
        } else {
            Excel::create('ReporteAspirantesAprobados'.Carbon::now()->format('Ymdhis'), function ($excel) {
                $excel->sheet('New sheet', function ($sheet) {
                    $sheet->loadView('exports.policias.policiasActivos');
                });
            })->download($tipo);
        }
    }

    public function policiasInactivos($tipo)
    {
        $policias = Policia::select('policias.*')
        ->join('separacion', 'policias.idPersona', '=', 'separacion.idPersona')
        ->get();
        if ($tipo == 'pdf') {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('exports.policias.policiasInactivos', compact('policias'));
            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(280, 765, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            return $pdf->stream('ReporteAspirantesAprobados'.Carbon::now()->format('Ymdhis').'.'.$tipo);
        } else {
            Excel::create('ReporteAspirantesAprobados'.Carbon::now()->format('Ymdhis'), function ($excel) {
                $excel->sheet('New sheet', function ($sheet) {
                    $sheet->loadView('exports.policias.policiasInactivos');
                });
            })->download($tipo);
        }
    }

    public function policiasSeparacionDelServicio($tipo)
    {
        $policias = Policia::select('policias.*')
        ->join('separacion', 'policias.idPersona', '=', 'separacion.idPersona')
        ->get();
        if ($tipo == 'pdf') {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('exports.policias.policiasSeparacionDelServicio', compact('policias'));
            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(280, 765, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            return $pdf->stream('ReporteAspirantesAprobados'.Carbon::now()->format('Ymdhis').'.'.$tipo);
        } else {
            Excel::create('ReporteAspirantesAprobados'.Carbon::now()->format('Ymdhis'), function ($excel) {
                $excel->sheet('New sheet', function ($sheet) {
                    $sheet->loadView('exports.policias.policiasSeparacionDelServicio');
                });
            })->download($tipo);
        }
    }

    public function policiasControl($id, $tipo)
    {
        $persona = Persona::find($id);
        if ($tipo == 'pdf') {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('exports.policias.control.detalle', compact('persona'));
            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(280, 765, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            return $pdf->stream('ReportePoliciasControl'.Carbon::now()->format('Ymdhis').'.'.$tipo);
        } else {
            Excel::create('ReporteAspirantesAprobados'.Carbon::now()->format('Ymdhis'), function ($excel) {
                $excel->sheet('New sheet', function ($sheet) {
                    $sheet->loadView('exports.policias.policiasInactivos');
                });
            })->download($tipo);
        }
    }


    public function costos($tipo)
    {
        $costos = Rango::costos();
        if ($tipo == 'pdf') {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('exports.costos', compact('costos'));
            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(280, 765, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            return $pdf->stream('ReporteCostos'.Carbon::now()->format('Ymdhis').'.'.$tipo);
        } else {
            Excel::create('ReporteCostos'.Carbon::now()->format('Ymdhis'), function($excel){
                $excel->sheet('New sheet', function($sheet) {
                    $sheet->loadView('exports.costos');
                });
            })->download($tipo);
        }
    }

    public function bitacora($tipo)
    {
        $bitacora = Log::all();
        if ($tipo == 'pdf') {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('exports.bitacora', compact('bitacora'));
            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(280, 765, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            return $pdf->stream('Bitacora'.Carbon::now()->format('Ymdhis').'.'.$tipo);
        } else {
            Excel::create('ReporteCostos'.Carbon::now()->format('Ymdhis'), function($excel){
                $excel->sheet('New sheet', function($sheet) {
                    $sheet->loadView('exports.bitacora');
                });
            })->download($tipo);
        }
    }

    public function certificaciones($tipo)
    {
        $certificados = Certificacion::all();
        if ($tipo == 'pdf') {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('exports.certificados', compact('certificados'));
            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(280, 765, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            return $pdf->stream('ReporteCostos'.Carbon::now()->format('Ymdhis').'.'.$tipo);
        } else {
            Excel::create('ReporteCostos'.Carbon::now()->format('Ymdhis'), function($excel){
                $excel->sheet('New sheet', function($sheet) {
                    $sheet->loadView('exports.certificados');
                });
            })->download($tipo);
        }
    }
}
