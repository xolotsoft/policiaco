<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ConvocatoriasRequest;
use App\Models\Convocatoria;
use App\Models\Archivo;
use Carbon\Carbon;
use DB;

class ConvocatoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $convocatorias = Convocatoria::all();
        return view('catalogos.convocatorias.index')
        ->with('convocatorias', $convocatorias)
        ->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('catalogos.convocatorias.create')->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConvocatoriasRequest $request)
    {
        $File = $request->file('File');
        DB::beginTransaction();
        try {
            // Se guarda el archivo
            $archivo = new Archivo();
            $archivo->name = $File->getClientOriginalName();
            $archivo->size = $File->getSize();
            $archivo->type = $File->getMimeType();
            $archivo->ext  = $File->getClientOriginalExtension();
            $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
            $archivo->save();
            // Se guarda la convocatoria
            $convocatoria = new Convocatoria();
            $convocatoria->nombre      = $request->nombre;
            $convocatoria->puestos     = $request->puestos;
            $convocatoria->fecha       = $request->fecha;
            $convocatoria->edadMinima  = $request->edadMinima;
            $convocatoria->edadMaxima  = $request->edadMaxima;
            $convocatoria->idArchivo   = $archivo->id;
            $convocatoria->descripcion = $request->descripcion;
            $convocatoria->save();
            DB::commit();
            return $this->index();
        } catch (\Exception $e) {
            DB::rollback();
            return response('Hubo un problema al guardar la información', 510);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $convocatoria = Convocatoria::find($id);
        return view('catalogos.convocatorias.show')
        ->with('convocatoria', $convocatoria)
        ->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $convocatoria = Convocatoria::find($id);
        return view('catalogos.convocatorias.edit')
        ->with('convocatoria', $convocatoria)
        ->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ConvocatoriasRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            // Se guarda o se cambia el archivo si existe
            $convocatoria = Convocatoria::find($id);
            if ($request->file('File') !== null) {
                $File = $request->file('File');
                if ($convocatoria->idArchivo > 0) {
                    $archivo = Archivo::find($convocatoria->idArchivo);
                } else {
                    $archivo = new Archivo();
                }
                $archivo->name = $File->getClientOriginalName();
                $archivo->size = $File->getSize();
                $archivo->type = $File->getMimeType();
                $archivo->ext  = $File->getClientOriginalExtension();
                $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                $archivo->save();
                $convocatoria->idArchivo = $archivo->id;
            }
            // Se guarda la convocatoria
            $convocatoria->nombre      = texto($request->nombre);
            $convocatoria->puestos     = $request->puestos;
            $convocatoria->fecha       = $request->fecha;
            $convocatoria->edadMinima  = $request->edadMinima;
            $convocatoria->edadMaxima  = $request->edadMaxima;
            $convocatoria->descripcion = $request->descripcion;
            $convocatoria->save();
            DB::commit();
            return $this->index();
        } catch (\Exception $e) {
            DB::rollback();
            return response('Hubo un problema al editar la información', 510);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Convocatoria::find($id)->delete();
        return $this->index();
    }
}
