<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
/*---------- REQUESTS -----------*/
use Illuminate\Http\Request;
use App\Http\Requests\AspirantesRequest;
use App\Http\Requests\DomicilioRequest;
use App\Http\Requests\FamiliaresRequest;
use App\Http\Requests\ReclutamientoRequest;
use App\Http\Requests\SeleccionRequest;
use App\Http\Requests\FormacionRequest;
use App\Http\Requests\CertificacionRequest;
use App\Http\Requests\ReingresosRequest;
use App\Http\Requests\EvaluacionesPoliciasRequest;
use App\Http\Requests;
/*---------- MODELS ------------*/
use App\Models\EvaluacionesPolicias;
use App\Models\Archivo;
use App\Models\Aspirante;
use App\Models\Persona;
use App\Models\Domicilio;
use App\Models\Familia;
use App\Models\Reclutamiento;
use App\Models\Convocatoria;
use App\Models\State;
use App\Models\Countries;
use App\Models\Provinces;
use App\Models\Areas;
use App\Models\Catalogo;
use App\Models\Seleccion;
use App\Models\Rango;
use App\Models\Formations;
use App\Models\Certificacion;
use App\Models\Reingresos;
use App\Models\Policia;
use App\Models\Instancia;
use App\Models\Mensaje;
use App\Models\Separacion;
use Carbon\Carbon;
use \DB;

class AspirantesController extends Controller
{
    public function nuevo($id = null)
    {
        Session::forget('idPersona');
        if ($id) {
            Session::put('idPersona', $id);
            $view = 'aspirantes.nuevo.index';
        } else {
            $view = 'page';
        }
        try {
            $aspirante = Aspirante::join('persona', 'persona.id', '=', 'aspirantes.idPersona')
            ->where('aspirantes.idPersona', Session::get('idPersona'))
            ->firstOrFail();
            $editable = true;
        } catch (ModelNotFoundException $e) {
            $aspirante = [];
            $editable = false;
        }
        $countm = Mensaje::nuevos()->total;
        $folio         = 'A'.str_pad(Aspirante::count('*')+1, 5, '0', STR_PAD_LEFT);
        $genero        = Catalogo::Genero();
        $civil         = Catalogo::Civil();
        $convocatorias = Convocatoria::lists('nombre', 'id');
        $estados       = State::lists('Name', 'ID');
        $puestos       = Rango::lists('Rank', 'ID');
        return view($view)
        ->with('folio', $folio)
        ->with('genero', $genero)
        ->with('civil', $civil)
        ->with('convocatorias', $convocatorias)
        ->with('estados', $estados)
        ->with('puestos', $puestos)
        ->with('aspirante', $aspirante)
        ->with('editable', $editable)
        ->with('countm', $countm)
        ->render();
    }

    public function listado()
    {
        $countm = Mensaje::nuevos()->total;
        $aspirantes = $this->aspirantesListado();
        return view('page')
        ->with('aspirantes', $aspirantes)
        ->with('countm', $countm)
        ->render();
    }

    public function aptosParaNombramiento()
    {
        $countm = Mensaje::nuevos()->total;
        $aptos = Aspirante::select('aspirantes.*')
        ->join('persona', 'aspirantes.idPersona', '=', 'persona.id')
        ->join('reclutamiento', 'reclutamiento.idPersona', '=', 'persona.id')
        ->groupBy('persona.id')
        ->get();
        return view('page')
        ->with('aptos', $aptos)
        ->with('countm', $countm)
        ->render();
    }

    public function personales(AspirantesRequest $request)
    {
        if ($request->getMethod() == "GET") {
            try {
                $aspirante = Aspirante::join('persona', 'persona.id', '=', 'aspirantes.idPersona')
                ->where('aspirantes.idPersona', Session::get('idPersona'))
                ->firstOrFail();
                $editable = true;
            } catch (ModelNotFoundException $e) {
                $aspirante = [];
                $editable = false;
            }
            $folio         = 'A'.str_pad(Aspirante::count('*') + 1, 5, '0', STR_PAD_LEFT);
            $genero        = Catalogo::Genero();
            $civil         = Catalogo::Civil();
            $convocatorias = Convocatoria::lists('nombre', 'id');
            $estados       = State::lists('Name', 'ID');
            $puestos       = Rango::lists('Rank', 'ID');
            return view('aspirantes.personales.main')
            ->with('folio', $folio)
            ->with('genero', $genero)
            ->with('civil', $civil)
            ->with('convocatorias', $convocatorias)
            ->with('estados', $estados)
            ->with('puestos', $puestos)
            ->with('aspirante', $aspirante)
            ->with('editable', $editable)
            ->render();
        } else {
            try {
                $aspirante = Aspirante::select('*')
                ->where('aspirantes.idPersona', Session::get('idPersona'))
                ->firstOrFail();
                $persona = Persona::select('*')
                ->where('persona.id', Session::get('idPersona'))
                ->firstOrFail();
                $folio = $aspirante->folio;
            } catch (ModelNotFoundException $e) {
                $aspirante = new Aspirante();
                $persona   = new Persona();
                $folio = 'A'.str_pad(Aspirante::count('*') + 1, 5, '0', STR_PAD_LEFT);
            }
            DB::beginTransaction();
            $documentos = ['huella', 'documentoCup'];
            $archivo = new Archivo();
            try {
                // Se guarda el archivo
                foreach ($documentos as $documento) {
                    if ($request->file($documento) !== null) {
                        $archivo = new Archivo();
                        $File = $request->file($documento);
                        $archivo->name = $File->getClientOriginalName();
                        $archivo->size = $File->getSize();
                        $archivo->type = $File->getMimeType();
                        $archivo->ext  = $File->getClientOriginalExtension();
                        $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                        $archivo->save();
                        $persona[$documento] = $archivo->id;
                        unset($File);
                    }
                }
                // Datos personales
                $persona->curp            = strtoupper($request->curp);
                $persona->rfc             = strtoupper($request->rfc);
                $persona->nombre          = texto($request->nombre);
                $persona->aPaterno        = texto($request->aPaterno);
                $persona->aMaterno        = texto($request->aMaterno);
                $persona->fechaNacimiento = $request->fechaNacimiento;
                $persona->lugarNacimiento = $request->lugarNacimiento;
                $persona->genero          = $request->genero;
                $persona->estadoCivil     = $request->estadoCivil;
                $persona->estatura        = $request->estatura;
                $persona->peso            = $request->peso;
                $persona->telefono        = $request->telefono;
                $persona->movil           = $request->movil;
                $persona->email           = $request->email;
                $persona->cup             = $request->cup;
                //dd($persona->isDirty());
                $persona->save();
                // FIN Datos personales
                $aspirante->fechaRegistro  = $request->fechaRegistro;
                $aspirante->idConvocatoria = $request->idConvocatoria;
                $aspirante->puesto         = $request->puesto;
                $aspirante->folio          = $folio;
                $aspirante->observaciones  = $request->observaciones;
                $aspirante->estatus        = 'Aspirante';
                $aspirante->idPersona      = $persona->id;
                $aspirante->save();
                Session::put('idPersona', $persona->id);
                DB::commit();
                return response('Se guardo correctamente la información');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function domicilio(DomicilioRequest $request)
    {
        $paises  = Countries::lists('Name', 'ID');
        $estados = State::lists('Name', 'ID');
        if ($request->getMethod() == "GET") {
            try {
                $domicilio = Domicilio::where('idPersona', Session::get('idPersona'))->firstOrFail();
                $municipios = Provinces::where('StateId', $domicilio->estado)->lists('Name', 'ID');
                $colonias = Areas::where('ProvinceId', $domicilio->municipio)->lists('Name', 'ID')->toArray();
                $editable = true;
            } catch (ModelNotFoundException $e) {
                $domicilio  = [];
                $municipios = [];
                $colonias   = [];
                $editable = false;
            }
            return view('aspirantes.domicilio.create')
            ->with('paises', $paises)
            ->with('estados', $estados)
            ->with('municipios', $municipios)
            ->with('colonias', $colonias)
            ->with('domicilio', $domicilio)
            ->with('editable', $editable)
            ->render();
        } else {
            try {
                $domicilio = Domicilio::where('idPersona', Session::get('idPersona'))
                ->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $domicilio = new Domicilio();
            }
            DB::beginTransaction();
            try {
                $domicilio->calle                  = texto($request->calle);
                $domicilio->exterior               = $request->exterior;
                $domicilio->interior               = $request->interior;
                $domicilio->cp                     = $request->cp;
                $domicilio->estado                 = $request->estado;
                $domicilio->municipio              = $request->municipio;
                $domicilio->colonia                = $request->colonia;
                $domicilio->observacionesDomicilio = $request->observacionesDomicilio;
                $domicilio->idPersona              = Session::get('idPersona');
                $domicilio->save();
                DB::commit();
                return response('Se guardo correctamente la información');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function familiares(FamiliaresRequest $request)
    {
        $genero     = Catalogo::Genero();
        $parentesco = Catalogo::Parentesco();
        if ($request->getMethod() == "GET") {
            try {
                $familiares = Familia::where('idPersona', Session::get('idPersona'))->get();
            } catch (\Exception $e) {
                $familiares = [];
            }
            return view('aspirantes.familiares.create')
            ->with('genero', $genero)
            ->with('parentesco', $parentesco)
            ->with('familiares', $familiares)
            ->render();
        } else {
            DB::beginTransaction();
            try {
                $familia = new Familia();
                $familia->categoria       = $request->categoria;
                $familia->nombre          = texto($request->nombre);
                $familia->aPaterno        = texto($request->aPaterno);
                $familia->aMaterno        = texto($request->aMaterno);
                $familia->fechaNacimiento = $request->fechaNacimiento;
                $familia->genero          = $request->genero;
                $familia->direccion       = texto($request->direccion);
                $familia->telefono        = $request->telefono;
                $familia->observacionesFamiliares   = $request->observacionesFamiliares;
                $familia->idPersona       = Session::get('idPersona');
                $familia->save();
                DB::commit();
                return response('Se guardo correctamente la información');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function reclutamiento(ReclutamientoRequest $request)
    {
        if ($request->getMethod() == "GET") {
            $servicios = Catalogo::servicios();
            $asimilamiento = Catalogo::asimilamiento();
            $posgrados = Catalogo::posgrado();
            try {
                $reclutamiento = Reclutamiento::where('idPersona', Session::get('idPersona'))->firstOrFail();
                $editable = true;
            } catch (ModelNotFoundException $e) {
                $reclutamiento = [];
                $editable = false;
            }
            return view('aspirantes.reclutamiento.create')
            ->with('reclutamiento', $reclutamiento)
            ->with('asimilamiento', $asimilamiento)
            ->with('servicios', $servicios)
            ->with('posgrados', $posgrados)
            ->with('editable', $editable)
            ->render();
        } else {
            $documentos = [
                'documento',
                'elector',
                'nacimiento',
                'cartilla',
                'penales',
                'secundaria',
                'bachillerato',
                'tecnico',
                'licenciatura',
                'maestria',
                'bajaPolicial',
                'fotoInfantil',
                'comprobanteDomicilio',
                'motivosIngreso',
                'refPersonales1',
                'refPersonales2',
                'nacimientoHijo',
                'matrimonio',
                'sociedadConyugal',
                'manejo',
                'documentoCurp',
                'documentoRfc',
                'estadoCuenta',
                'solicitud',
                'curriculum',
                'perfilSolicitado'
            ];
            try {
                $reclutamiento = Reclutamiento::where('idPersona', Session::get('idPersona'))->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $reclutamiento = new Reclutamiento();
            }
            DB::beginTransaction();
            try {
                foreach ($documentos as $documento) {
                    if ($request->file($documento) !== null) {
                        $archivo = new Archivo();
                        $File = $request->file($documento);
                        $archivo->name = $File->getClientOriginalName();
                        $archivo->size = $File->getSize();
                        $archivo->type = $File->getMimeType();
                        $archivo->ext  = $File->getClientOriginalExtension();
                        $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                        $archivo->save();
                        $reclutamiento[$documento] = $archivo->id;
                        unset($File);
                    }
                }
                $reclutamiento->posgrado = $request->posgrado;
                $reclutamiento->asimilamientoGradoPolicial = $request->asimilamientoGradoPolicial;
                $reclutamiento->serviciosMedicos           = $request->serviciosMedicos;
                $reclutamiento->observacionesReclutamiento = $request->observacionesReclutamiento;
                $reclutamiento->idPersona                  = Session::get('idPersona');
                $reclutamiento->save();
                DB::commit();
                return response('Se guardo correctamente la información');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function seleccion(SeleccionRequest $request)
    {
        $apto = Catalogo::Apto();
        if ($request->getMethod() == "GET") {
            try {
                $seleccion = Seleccion::where('idPersona', Session::get('idPersona'))->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $seleccion  = [];
            }
            return view('aspirantes.seleccion.create')
                ->with('seleccion', $seleccion)
                ->with('cat', $apto)
                ->render();
        } else {
            try {
                $seleccion = Seleccion::where('idPersona', Session::get('idPersona'))
                ->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $seleccion = new Seleccion();
            }
            DB::beginTransaction();
            try {
                $seleccion->resultadoConfianza = $request->resultadoConfianza;
                $seleccion->observaciones      = $request->observaciones;
                $seleccion->idPersona          = Session::get('idPersona');
                $seleccion->save();
                DB::commit();
                return response('Se guardo correctamente la información');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function formacion(FormacionRequest $request)
    {
        $instancias = Instancia::lists('Name', 'ID');
        $cumplimiento = Catalogo::Cumplimiento();
        if ($request->getMethod() == "GET") {
            try {
                $formacion = Formations::where('idPersona', Session::get('idPersona'))->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $formacion = [];
            }
            return view('aspirantes.formacion.create')
            ->with('formacion', $formacion)
            ->with('instancias', $instancias)
            ->with('cumplimiento', $cumplimiento)
            ->render();
        } else {
            try {
                $formacion = Formations::where('idPersona', Session::get('idPersona'))->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $formacion = new Formations();
            }
            DB::beginTransaction();
            $documentos = ['certificado', 'constancia'];
            try {
                foreach ($documentos as $documento) {
                    if ($request->file($documento) !== null) {
                        $archivo = new Archivo();
                        $File = $request->file($documento);
                        $archivo->name = $File->getClientOriginalName();
                        $archivo->size = $File->getSize();
                        $archivo->type = $File->getMimeType();
                        $archivo->ext  = $File->getClientOriginalExtension();
                        $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                        $archivo->save();
                        $formacion[$documento] = $archivo->id;
                        unset($File);
                    }
                }
                $formacion->material      = $request->material;
                $formacion->fechaInicio   = $request->fechaInicio;
                $formacion->fechaFin      = $request->fechaFin;
                $formacion->duracion      = $request->duracion;
                $formacion->cumplimiento  = $request->cumplimiento;
                $formacion->instancia     = $request->instancia;
                $formacion->observaciones = $request->observaciones;
                $formacion->idPersona     = Session::get('idPersona');
                $formacion->save();
                DB::commit();
                return response('Se guardo correctamente la información');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function certificacion(CertificacionRequest $request)
    {
        if ($request->getMethod() == "GET") {
            $categoriaPolicial = Catalogo::categoriaPolicial();
            $formaciones = Catalogo::where('id', 90)->lists('nombre', 'id');;
            try {
                $certificacion = Certificacion::where('idPersona', Session::get('idPersona'))->firstOrFail();
                $grados = Rango::where('Category', '=', $certificacion->cargo)->lists('Rank', 'id');
                $editable = true;
            } catch (ModelNotFoundException $e) {
                $certificacion = [];
                $grados = [];
                $editable = false;
            }
            return view('aspirantes.certificacion.create')
            ->with('certificacion', $certificacion)
            ->with('categoriaPolicial', $categoriaPolicial)
            ->with('editable', $editable)
            ->with('formaciones', $formaciones)
            ->with('grados', $grados)
            ->render();
        } else {
            try {
                $certificacion = Certificacion::where('idPersona', Session::get('idPersona'))->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $certificacion = new Certificacion();
            }
            DB::beginTransaction();
            try {
                if ($request->file('nombramiento') !== null) {
                    $archivo = new Archivo();
                    $File = $request->file('nombramiento');
                    $archivo->name = $File->getClientOriginalName();
                    $archivo->size = $File->getSize();
                    $archivo->type = $File->getMimeType();
                    $archivo->ext  = $File->getClientOriginalExtension();
                    $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                    $archivo->save();
                    $certificacion->nombramiento = $archivo->id;
                }
                if ($request->file('documentoCurso') !== null) {
                    $archivo = new Archivo();
                    $File = $request->file('documentoCurso');
                    $archivo->name = $File->getClientOriginalName();
                    $archivo->size = $File->getSize();
                    $archivo->type = $File->getMimeType();
                    $archivo->ext  = $File->getClientOriginalExtension();
                    $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                    $archivo->save();
                    $certificacion->documentoCurso = $archivo->id;
                }
                $certificacion->cargo            = $request->cargo;
                $certificacion->puesto           = $request->puesto;
                $certificacion->fechaIngreso     = $request->fechaIngreso;
                $certificacion->adscripcion      = $request->adscripcion;
                $certificacion->calificacion     = $request->calificacion;
                $certificacion->observaciones    = $request->observaciones;
                $certificacion->nombreCurso      = $request->nombreCurso;
                $certificacion->formacionInicial = $request->formacionInicial;
                $certificacion->idPersona        = Session::get('idPersona');
                $certificacion->save();
                DB::commit();
                return response('Se guardo correctamente la información');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function reingresos(ReingresosRequest $request)
    {
        if ($request->getMethod() == "GET") {
            $puestos = Rango::lists('Rank', 'ID');
            $estados = State::lists('Name', 'ID');
            $instituciones = Catalogo::institucionesReingreso();
            try {
                $reingresos = Reingresos::where('idPersona', Session::get('idPersona'))->firstOrFail();
                $municipios = Provinces::where('StateId', $reingresos->estado)->lists('Name', 'ID');
                $editable = true;
            } catch (ModelNotFoundException $e) {
                $municipios = [];
                $reingresos = [];
                $editable = false;
            }
            return view('aspirantes.reingresos.create')
            ->with('puestos', $puestos)
            ->with('instituciones', $instituciones)
            ->with('estados', $estados)
            ->with('municipios', $municipios)
            ->with('reingresos', $reingresos)
            ->with('editable', $editable)
            ->render();
        } else {
            try {
                $reingresos = Reingresos::where('idPersona', Session::get('idPersona'))->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $reingresos = new Reingresos();
            }
            DB::beginTransaction();
            try {
                $reingresos->institucion       = $request->institucion;
                $reingresos->estado            = $request->estado;
                $reingresos->municipio         = $request->municipio;
                $reingresos->grado             = $request->grado;
                $reingresos->antiguedad        = $request->antiguedad;
                $reingresos->instanciaEvaluadora = $request->instanciaEvaluadora;
                $reingresos->nombreInstancia = $request->nombreInstancia;
                $reingresos->fechaPresentacion = $request->fechaPresentacion;
                $reingresos->observaciones     = $request->observaciones;
                $reingresos->idPersona         = Session::get('idPersona');
                $reingresos->save();
                DB::commit();
                return response('Se guardo correctamente la información');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function separacion(SeleccionRequest $request)
    {
        $data = [];
        $data['Ordinaria'] = Catalogo::Separaciones()->toArray();
        $data['Extraordinaria'] = Catalogo::separacionExtraordinaria()->toArray();
        if ($request->getMethod() == "GET") {
            try {
                $separaciones = Separacion::where('idPersona', Session::get('idPersona'))->firstOrFail();
                $editable = true;
            } catch (ModelNotFoundException $e) {
                $separaciones = [];
                $editable = false;
            }
            return view('aspirantes.separacion.create')
            ->with('separaciones', $separaciones)
            ->with('data', $data)
            ->with('editable', $editable)
            ->render();
        } else {
            try {
                $separaciones = Separacion::where('idPersona', Session::get('idPersona'))
                ->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $separaciones = new Separacion();
            }
            DB::beginTransaction();
            $File = $request->file('certificado');
            $archivo = new Archivo();
            try {
                if ($request->file('certificado') !== null) {
                    $archivo->name = $File->getClientOriginalName();
                    $archivo->size = $File->getSize();
                    $archivo->type = $File->getMimeType();
                    $archivo->ext  = $File->getClientOriginalExtension();
                    $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
                    $archivo->save();
                    $separaciones->certificado = $archivo->id;
                }
                $separaciones->tipoSeparacion  = $request->tipoSeparacion;
                $separaciones->fechaSeparacion = $request->fechaSeparacion;
                $separaciones->motivo          = $request->motivo;
                $separaciones->aprobado        = $request->aprobado;
                $separaciones->idPersona       = Session::get('idPersona');
                $separaciones->save();
                DB::commit();
                return response('Se guardo correctamente la información');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function evaluaciones(EvaluacionesPoliciasRequest $request)
    {
        $examenes = Catalogo::Examenes();
        $resultados = Catalogo::Cumplimiento();
        if ($request->getMethod() == "GET") {
            try {
                $evaluaciones = EvaluacionesPolicias::where('idPersona', Session::get('idPersona'))->get();
            } catch (\Exception $e) {
                $evaluaciones = [];
            }
            return view('aspirantes.evaluaciones.create')
            ->with('evaluaciones', $evaluaciones)
            ->with('examenes', $examenes)
            ->with('resultados', $resultados)
            ->render();
        } else {
            DB::beginTransaction();
            try {
                $evaluaciones = new EvaluacionesPolicias();
                $evaluaciones->examen          = $request->examen;
                $evaluaciones->resultado       = $request->resultado;
                $evaluaciones->fechaResultados = $request->fechaResultados;
                $evaluaciones->fecha           = $request->fecha;
                $evaluaciones->observaciones   = $request->observaciones;
                $evaluaciones->idPersona       = Session::get('idPersona');
                $evaluaciones->save();
                DB::commit();
                return response('Se guardo correctamente la información');
            } catch (\Exeption $e) {
                DB::rollback();
                return response('Hubo un problema al guardar en la base de datos', 510);
            }
        }
    }

    public function tablaFamiliares($id)
    {
        $familiares = Familia::where('idPersona', '=', $id)->get();
        return view('aspirantes.familiares.listado')
        ->with('familiares', $familiares)
        ->render();
    }

    public function detalleConvocatoria($id)
    {
        $detalle = Convocatoria::where('id', '=', $id)->first()->toJson();
        return $detalle;
    }

    public function destroy($id)
    {
        Aspirante::find($id)->delete();
        return $this->viewListado();
    }

    public function viewListado()
    {
        $aspirantes = $this->aspirantesListado();
        return view('aspirantes.listado.index')
        ->with('aspirantes', $aspirantes)
        ->render();
    }

    private function aspirantesListado()
    {
        $aspirantes = Aspirante::all();
        foreach ($aspirantes as $key => $value) {
            $etapa = 12.5;
            $domicilio = Domicilio::where('idPersona', '=', $value->idPersona)->first();
            $familiares = Familia::where('idPersona', '=', $value->idPersona)->first();
            $reclutamiento = Reclutamiento::where('idPersona', '=', $value->idPersona)->first();
            $seleccion = Seleccion::where('idPersona', '=', $value->idPersona)->first();
            $formacion = Formations::where('idPersona', '=', $value->idPersona)->first();
            $certificacion = Certificacion::where('idPersona', '=', $value->idPersona)->first();
            $reingresos = Reingresos::where('idPersona', '=', $value->idPersona)->first();
            $etapa = ($domicilio != null) ? $etapa + 12.5: $etapa;
            $etapa = ($familiares != null) ? $etapa + 12.5: $etapa;
            $etapa = ($reclutamiento != null) ? $etapa + 12.5: $etapa;
            $etapa = ($seleccion != null) ? $etapa + 12.5: $etapa;
            $etapa = ($formacion != null) ? $etapa + 12.5: $etapa;
            $etapa = ($certificacion != null) ? $etapa + 12.5: $etapa;
            $etapa = ($reingresos != null) ? $etapa + 12.5: $etapa;
            $value->etapa = $etapa;
        }
        return $aspirantes;
    }

    public function viewAptos()
    {
        $aptos = Aspirante::select('aspirantes.*')
        ->join('persona', 'aspirantes.idPersona', '=', 'persona.id')
        ->join('reclutamiento', 'reclutamiento.idPersona', '=', 'persona.id')
        ->groupBy('persona.id')
        ->get();
        return view('aspirantes.aptos.index')
        ->with('aptos', $aptos)
        ->render();
    }

    public function incorporar(Request $request)
    {
        $this->validate($request, [
                'idAspirante' => 'required',
                'nombramiento' => 'required',
        ]);
        DB::beginTransaction();
        $File = $request->file('nombramiento');
        try {
            // Se guarda el archivo
            $policia = new Policia();
            $archivo = new Archivo();
            $archivo->name = $File->getClientOriginalName();
            $archivo->size = $File->getSize();
            $archivo->type = $File->getMimeType();
            $archivo->ext  = $File->getClientOriginalExtension();
            $archivo->data = base64_encode(file_get_contents($File->getRealPath()));
            $archivo->save();
            // se guardan datos
            $policia->idPersona  = Aspirante::find($request->idAspirante)->persona->id;
            $policia->nombramiento = $archivo->id;
            $policia->save();
            Aspirante::find($request->idAspirante)->delete();
            DB::commit();
            return $this->viewAptos();
        } catch (\Exeption $e) {
            DB::rollback();
            return response('Hubo un problema al guardar en la base de datos', 510);
        }
    }

    public function fotografia()
    {
        try {
            $reclutamiento = Reclutamiento::where('idPersona', Session::get('idPersona'))->firstOrFail();
            $archivo = Archivo::find($reclutamiento->fotoInfantil);
            return response('data:'.$archivo->type.';base64,'.$archivo->data);
        } catch (ModelNotFoundException $e) {
            return response('Fail', 510);
        }
    }
}
