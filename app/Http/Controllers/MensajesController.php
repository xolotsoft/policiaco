<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Mensaje;
use App\User;
use \DB;

class MensajesController extends Controller
{
    public function principal()
    {
        $mensajes = Mensaje::where('destinatario', Auth::user()->id)->get();
        $countm = Mensaje::nuevos()->total;
        $destinatarios = User::where('municipio_id', Auth::user()->municipio_id)
        ->where('id', '!=', 1)
        ->where('id', '!=', Auth::user()->id)
        ->lists('name', 'id');
        return view('page')
        ->with('mensajes', $mensajes)
        ->with('countm', $countm)
        ->with('destinatarios', $destinatarios)
        ->render();
    }

    public function guardar(Request $request)
    {
        DB::beginTransaction();
        try {
            $mensaje = new Mensaje();
            $mensaje->asunto = $request->asunto;
            $mensaje->mensaje = $request->mensaje;
            $mensaje->destinatario = $request->destinatario;
            $mensaje->remitente = Auth::user()->id;
            $mensaje->save();
            DB::commit();
            return response('Mensaje enviado correctamente');
        } catch (\Exception $e) {
            DB::rollback();
            return response('Hubo un problema al guardar en la base de datos', 510);
        }
    }

    public function mostrar($id)
    {
        $mensaje = Mensaje::find($id);
        $mensaje->estado = 1;
        $mensaje->save();
        return view('mensajes.principal.contenido')
        ->with('mensaje', $mensaje)
        ->render();
    }

    public function listado()
    {
        $mensajes = Mensaje::where('destinatario', Auth::user()->id)->get();
        return view('mensajes.principal.listado')
        ->with('mensajes', $mensajes)
        ->render();
    }

    public function eliminar($id)
    {
        $mensaje = Mensaje::find($id)->delete();
        return response('Se eliminó correctamente el mensaje');
    }

    public function countm()
    {
        return Mensaje::nuevos()->total;
    }
}
