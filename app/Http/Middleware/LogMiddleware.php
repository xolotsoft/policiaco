<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Auth;
use DB;

class LogMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            DB::listen(function ($query) {
                $qrLog = "insert into `log`";
                $qrSelect = "select";
                if (strpos($query->sql, $qrLog) === false) {
                    if (strpos($query->sql, $qrSelect) === false) {
                        $ruta = accion();
                        $insert = DB::connection('scpmex')->table('log')->insert([
                            'usuario' => Auth::user()->id,
                            'modulo' => $ruta['controlador'],
                            'seccion' => ucwords($ruta['accion']),
                            'accion' => accionLog($query->sql),
                            'query' => query($query->sql, $query->bindings),
                            'fecha' => Carbon::now()
                        ]);
                    }
                }
            });
        }

        return $next($request);
    }
}
