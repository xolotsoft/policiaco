<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Actividad extends Model
{
    use SoftDeletes;
    protected $table = "datosactividad";
    protected $primaryKey = "id";
    protected $dates = ['deleted_at'];

    /**
     * Obtiene el registro de persona asociado con el aspirante.
     */
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'idPersona');
    }

    public function archivoCertificadoNombramiento()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'certificadoNombramiento');
    }

    public function archivoConstanciaGrado()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'constanciaGrado');
    }

    public function archivoFotografiaUniforme()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'fotografiaUniforme');
    }

    public function archivoCredencialPorteArma()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'credencialPorteArma');
    }

    public function getFechaNombramientoAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaNombramientoAttribute($value)
    {
        $this->attributes['fechaNombramiento'] = Carbon::parse($value)->format('Y-m-d');
    }
}
