<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subsection extends Model
{
    protected $table = "subsections";
    protected $primaryKey = "id";
    public $timestamps = false;
}
