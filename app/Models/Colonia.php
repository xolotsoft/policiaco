<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Colonia extends Model
{
    protected $table = "areas";
    protected $primaryKey = "ID";
    public $timestamps = false;
}
