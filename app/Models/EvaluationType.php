<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EvaluationType extends Model
{
     protected $table = "evaluationtype";
    protected $primaryKey = "id";
    public $timestamps = false;
}
