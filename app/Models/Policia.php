<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Policia extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = "policias";
    protected $primaryKey = "id";

    /**
     * Obtiene el registro de persona asociado con el aspirante.
     */
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'idPersona');
    }

    public function getFechaNacimientoAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function archivoHuella()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'huella');
    }

    public function archivoDocumentoCup()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'documentoCup');
    }
  
}
