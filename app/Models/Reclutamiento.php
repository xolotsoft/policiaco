<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reclutamiento extends Model
{
    protected $table = "reclutamiento";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function getPosgrado()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'posgrado');
    }

    public function medicos()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'serviciosMedicos');
    }

    public function grado()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'asimilamientoGradoPolicial');
    }

    public function archivoPerfilSolicitado()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'perfilSolicitado');
    }

    public function archivoDocumento()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'documento');
    }

    public function archivoElector()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'elector');
    }

    public function archivoNacimiento()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'nacimiento');
    }

    public function archivoCartilla()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'cartilla');
    }

    public function archivoPenales()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'penales');
    }

    public function archivoSecundaria()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'secundaria');
    }

    public function archivoBachillerato()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'bachillerato');
    }

    public function archivoTecnico()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'tecnico');
    }

    public function archivoLicenciatura()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'licenciatura');
    }

    public function archivoMaestria()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'maestria');
    }

    public function archivoBajaPolicial()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'bajaPolicial');
    }

    public function archivoFotoInfantil()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'fotoInfantil');
    }

    public function archivoComprobanteDomicilio()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'comprobanteDomicilio');
    }

    public function archivoMotivosIngreso()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'motivosIngreso');
    }

    public function archivoRefPersonales1()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'refPersonales1');
    }

    public function archivoRefPersonales2()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'refPersonales2');
    }

    public function archivoNacimientoHijo()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'nacimientoHijo');
    }

    public function archivoMatrimonio()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'matrimonio');
    }

    public function archivoSociedadConyugal()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'sociedadConyugal');
    }

    public function archivoManejo()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'manejo');
    }

    public function archivoManejoDorso()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'manejoDorso');
    }

    public function archivoDocumentoCurp()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'documentoCurp');
    }

    public function archivoDocumentoRfc()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'documentoRfc');
    }

    public function archivoEstadoCuenta()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'estadoCuenta');
    }

    public function archivoSolicitud()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'solicitud');
    }

    public function archivoCurriculum()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'curriculum');
    }

    public function archivoAntecedentesDisciplinarios()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'antecedentesDisciplinarios');
    }
}
