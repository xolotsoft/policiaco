<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Rango extends Model
{
    use SoftDeletes;

    protected $table = "rangos";
    protected $primaryKey = "id";

    public function categoria()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'Category');
    }

    public function scopeCostos($query)
    {
        return $query
        /*SELECT r.Category as categoria, r.Rank as rango,r.special as especial,
               if(r.special > 0 ,r.Special_Salary,r.Normal_Salary ) as salario,
               if(r.special > 0,SUM(r.Special_Salary),SUM(r.Normal_Salary)) as Total,
               if(r.special > 0,SUM(r.Special_Salary),SUM(r.Normal_Salary)) * 12 as Anual
        FROM rangos r
        INNER JOIN datosactividad  d on r.id = d.gradoPolicial
        group by categoria,rango,especial;*/
        ->select(
          DB::raw('rangos.Category as categoria'),
          DB::raw('rangos.Rank as grado'),
          DB::raw('rangos.special as especial'),
          DB::raw('if(rangos.special > 0 ,rangos.Special_Salary,rangos.Normal_Salary ) as salario'),
          DB::raw('if(rangos.special > 0,SUM(rangos.Special_Salary),SUM(rangos.Normal_Salary)) as Total'),
          DB::raw('if(rangos.special > 0,SUM(rangos.Special_Salary),SUM(rangos.Normal_Salary)) * 12 as Anual	')
          )
        ->join('datosactividad', 'rangos.id', '=', 'datosactividad.gradoPolicial')
        ->groupBy('categoria','grado','especial')
        ->get();
    }

}
