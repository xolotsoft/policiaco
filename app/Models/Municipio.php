<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $table = "provinces";
    protected $primaryKey = "ID";
    public $timestamps = false;
}
