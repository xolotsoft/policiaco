<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class Familia extends Model
{
    protected $table = "familiares";
    protected $primaryKey = "id";
    public $timestamps = false;

    /**
     * Obtiene el registro de catalogos asociado con el aspirante.
    */
    public function catalogos()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'categoria');
    }

    public function getFechaNacimientoAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaNacimientoAttribute($value)
    {
        $this->attributes['fechaNacimiento'] = Carbon::parse($value)->format('Y-m-d');
    }
}
