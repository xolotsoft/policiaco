<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Plan extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = "plan";
    protected $primaryKey = "id";

    /**
     * Obtiene el registro de persona asociado con el aspirante.
     */
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'idPersona');
    }

    public function planes()
    {
        return $this->hasOne('App\Models\EvaluationType', 'ID', 'nombre');
    }

    public function apto()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'resultado');
    }

    public function tipo()
    {
        return $this->hasOne('App\Models\Evaluacion', 'ID', 'caracteristicas');
    }

    public function resultados()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'resultado');
    }

    public function getFechaInicioAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaInicioAttribute($value)
    {
        $this->attributes['fechaInicio'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getFechaTerminoAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaTerminoAttribute($value)
    {
        $this->attributes['fechaTermino'] = Carbon::parse($value)->format('Y-m-d');
    }
}
