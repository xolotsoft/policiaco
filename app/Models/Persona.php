<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use DB;

class Persona extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = "persona";
    protected $primaryKey = "id";

    public function aspirante()
    {
        return $this->hasOne('App\Models\Aspirante', 'idPersona', 'id');
    }

    public function domicilio()
    {
        return $this->hasOne('App\Models\Domicilio', 'idPersona', 'id');
    }

    public function familiares()
    {
        return $this->hasMany('App\Models\Familia', 'idPersona', 'id');
    }

    public function reclutamiento()
    {
        return $this->hasOne('App\Models\Reclutamiento', 'idPersona', 'id');
    }

    public function seleccion()
    {
        return $this->hasOne('App\Models\Seleccion', 'idPersona', 'id');
    }

    public function formacion()
    {
        return $this->hasOne('App\Models\Formations', 'idPersona', 'id');
    }

    public function certificacion()
    {
        return $this->hasOne('App\Models\Certificacion', 'idPersona', 'id');
    }

    public function reingreso()
    {
        return $this->hasOne('App\Models\Reingresos', 'idPersona', 'id');
    }

    public function separacion()
    {
        return $this->hasOne('App\Models\Separacion', 'idPersona', 'id');
    }

    public function evaluaciones()
    {
        return $this->hasMany('App\Models\EvaluacionesPolicias', 'idPersona', 'id');
    }

    public function policia()
    {
        return $this->hasOne('App\Models\Policia', 'idPersona', 'id');
    }

    public function promociones()
    {
        return $this->hasMany('App\Models\Promocion', 'idPersona', 'id');
    }

    public function herramientas()
    {
        return $this->hasMany('App\Models\Herramienta', 'idPersona', 'id');
    }

    public function sanciones()
    {
        return $this->hasMany('App\Models\Sancion', 'idPersona', 'id');
    }

    public function adscripciones()
    {
        return $this->hasMany('App\Models\Adscripcion', 'idPersona', 'id');
    }

    public function estimulos()
    {
        return $this->hasMany('App\Models\Estimulo', 'idPersona', 'id');
    }

    public function planes()
    {
        return $this->hasMany('App\Models\Plan', 'idPersona', 'id');
    }

    public function administrativas()
    {
        return $this->hasMany('App\Models\Administracion', 'idPersona', 'id');
    }

    public function sexo()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'genero');
    }

    public function civil()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'estadoCivil');
    }

    public function nacimiento()
    {
        return $this->hasOne('App\Models\State', 'ID', 'lugarNacimiento');
    }

    public function getFechaNacimientoAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaNacimientoAttribute($value)
    {
        $this->attributes['fechaNacimiento'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function archivoHuella()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'huella');
    }

    public function archivoDocumentoCup()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'documentoCup');
    }

    public function scopeAspiranteDetalleKardex($query, $id)
    {
      /*
      SELECT p.id,p.nombre,p.aPaterno,p.aMaterno,p.curp,p.rfc,p.genero,p.estatura,
      	   p.peso,p.fechaNacimiento,p.lugarNacimiento,d.estado,d.municipio,d.colonia,d.calle,
             d.exterior,d.interior,d.cp,
             p.telefono,p.movil,p.email,p.estadoCivil,r.Category,r.Rank,a.folio
      FROM persona p
      INNER JOIN domicilio d on p.id=d.idPersona
      INNER JOIN aspirantes a on a.idPersona =p.id
      INNER JOIN rangos r on r.id = a.puesto
      WHERE a.deleted_at is NULL*/

        return $query
        ->select(
            'persona.id','persona.nombre','persona.aPaterno','persona.aMaterno','persona.curp','persona.rfc','persona.genero','persona.estatura', 'persona.peso','persona.fechaNacimiento',
            DB::raw('ln.Name AS lugarNacimiento'),
            DB::raw('states.Name AS estado'),
            DB::raw('provinces.Name AS municipio'),
            DB::raw('areas.Name AS colonia'),'domicilio.calle',
            'domicilio.exterior','domicilio.interior','domicilio.cp',
            'persona.telefono','persona.movil','persona.email','persona.estadoCivil',
            DB::raw('catalogos.nombre AS Category'),'rangos.Rank','aspirantes.folio'
        )
        ->join('domicilio', 'domicilio.idPersona', '=', 'persona.id')
        ->join('aspirantes', 'aspirantes.idPersona', '=', 'persona.id')
        ->join('rangos', 'rangos.id', '=', 'aspirantes.puesto')
        ->join('states', 'states.ID', '=', 'domicilio.estado')
        ->join('states AS ln', 'ln.ID', '=', 'persona.lugarNacimiento')
        ->join('provinces', 'provinces.ID', '=', 'domicilio.municipio')
        ->join('areas', 'areas.ID', '=', 'domicilio.colonia')
        ->join('catalogos', 'catalogos.id', '=', 'rangos.Category')
        ->whereNull('aspirantes.deleted_at')
        ->where('persona.id',$id)
        ->first();
    }

    public function scopePoliciaDetalleKardex($query, $id)
    {
        return $query
        ->select(
            'persona.id', 'persona.nombre', 'persona.aPaterno', 'persona.aMaterno', 'persona.curp', 'persona.rfc', 'persona.genero', 'persona.estatura', 'persona.peso', 'persona.fechaNacimiento',
            DB::raw('ln.Name AS lugarNacimiento'),
            DB::raw('states.Name AS estado'),
            DB::raw('provinces.Name AS municipio'),
            DB::raw('areas.Name AS colonia'), 'domicilio.calle',
            'domicilio.exterior', 'domicilio.interior', 'domicilio.cp',
            'persona.telefono', 'persona.movil', 'persona.email', 'persona.estadoCivil',
            DB::raw('IFNULL(catalogos.nombre , 0) AS Category'),
            DB::raw('IFNULL(rangos.Rank , 0) AS Rank')
        )
        ->join('domicilio', 'domicilio.idPersona', '=', 'persona.id')
        ->join('datosactividad', 'datosactividad.idPersona', '=', 'persona.id')
        ->join('policias', 'policias.idPersona', '=', 'persona.id')
        ->join('rangos', 'rangos.id', '=', 'datosactividad.gradoPolicial')
        ->join('states', 'states.ID', '=', 'domicilio.estado')
        ->join('states AS ln', 'ln.ID', '=', 'persona.lugarNacimiento')
        ->join('provinces', 'provinces.ID', '=', 'domicilio.municipio')
        ->join('areas', 'areas.ID', '=', 'domicilio.colonia')
        ->join('catalogos', 'catalogos.id', '=', 'rangos.Category')
        ->whereNull('policias.deleted_at')
        ->where('persona.id', $id)
        ->first();
    }

    public function scopeGraficadosActivos($query, $filters)
    {
        $query->groupBy('persona.id');
        $query->join('policias', 'policias.idPersona', '=', 'persona.id');
        $query->leftJoin('datosactividad', 'datosactividad.idPersona', '=', 'persona.id');
        $query->leftJoin('promociones', 'promociones.idPersona', '=', 'persona.id');
        $query->leftJoin('herramientas', 'herramientas.idPersona', '=', 'persona.id');
        $query->leftJoin('sanciones', 'sanciones.idPersona', '=', 'persona.id');
        $query->leftJoin('adscripcion', 'adscripcion.idPersona', '=', 'persona.id');
        $query->leftJoin('estimulos', 'estimulos.idPersona', '=', 'persona.id');
        $query->leftJoin('plan', 'plan.idPersona', '=', 'persona.id');
        $query->leftJoin('evaluacionespolicias', 'evaluacionespolicias.idPersona', '=', 'persona.id');
        $query->leftJoin('separacion', 'separacion.idPersona', '=', 'persona.id');

        if ($filters->edad != '') {
            $dt = Carbon::now()->modify('-'.$filters->edad.' years')->format('Y-m-d');
            $query->where('persona.fechaNacimiento', '>=', $dt);
        }
        if ($filters->edad != '') {
            $dt = Carbon::now()->modify('-'.($filters->edad-1).' years')->format('Y-m-d');
            $query->where('persona.fechaNacimiento', '<=', $dt);
        }
        if ($filters->estadoCivil != '') {
            $query->where('persona.estadoCivil', '=', $filters->estadoCivil);
        }
        if ($filters->lugarNacimiento != '') {
            $query->where('persona.lugarNacimiento', '=', $filters->lugarNacimiento);
        }

        if ($filters->cargoActividad != '') {
            $query->where('datosactividad.cargo', '=', $filters->cargoActividad);
        }
        if ($filters->gradoPolicialActividad != '') {
            $query->where('datosactividad.gradoPolicial', '=', $filters->gradoPolicialActividad);
        }
        if ($filters->puestoActividad != '') {
            $query->where('datosactividad.puesto', 'like', '%'.$filters->puestoActividad.'%');
        }
        if ($filters->areaAdscripcionActividad != '') {
            $query->where('datosactividad.areaAdscripcion', 'like', '%'.$filters->areaAdscripcionActividad.'%');
        }

        if ($filters->cargoPromocion != '') {
            $query->where('promociones.cargo', '=', $filters->cargoPromocion);
        }
        if ($filters->gradoPolicialPromocion != '') {
            $query->where('promociones.rango', '=', $filters->gradoPolicialPromocion);
        }

        if ($filters->tipoHerramienta != '') {
            $query->where('herramientas.tipo', '=', $filters->tipoHerramienta);
        }
        if ($filters->fechaRecepcionHerramienta != '') {
            $query->where('herramientas.fechaRecepcion', '=', $filters->fechaRecepcionHerramienta);
        }
        if ($filters->fechaDevolucionHerramienta != '') {
            $query->where('herramientas.fechaDevolucion', '=', $filters->fechaDevolucionHerramienta);
        }

        if ($filters->tipoSanciones != '') {
            $query->where('sanciones.tipoSanciones', '=', $filters->tipoSanciones);
        }
        if ($filters->causaSanciones != '') {
            $query->where('sanciones.causa', 'like', '%'.$filters->causaSanciones.'%');
        }
        if ($filters->fechaSanciones != '') {
            $query->where('sanciones.fechaSanciones', '=', $filters->fechaSanciones);
        }

        if ($filters->areaProcedencia != '') {
            $query->where('adscripcion.areaProcedencia', 'like', '%'.$filters->areaProcedencia.'%');
        }
        if ($filters->areaDesignacion != '') {
            $query->where('adscripcion.areaDesignacion', 'like', '%'.$filters->areaDesignacion.'%');
        }
        if ($filters->fechaFinAdscripcion != '') {
            $query->where('adscripcion.fechaFin', '=', $filters->fechaFinAdscripcion);
        }
        if ($filters->fechaInicioAdscripcion != '') {
            $query->where('adscripcion.fechaInicio', '=', $filters->fechaInicioAdscripcion);
        }

        if ($filters->nombrePlan != '') {
            $query->where('plan.nombre', '=', $filters->nombrePlan);
        }
        if ($filters->caracteristicasPlan != '') {
            $query->where('plan.caracteristicas', '=', $filters->caracteristicasPlan);
        }
        if ($filters->instanciaPlan != '') {
            $query->where('plan.instancia', '=', $filters->instanciaPlan);
        }
        if ($filters->gradoImportanciaPlan != '') {
            $query->where('plan.gradoImportancia', '=', $filters->gradoImportanciaPlan);
        }
        if ($filters->resultadoPlan != '') {
            $query->where('plan.resultado', '=', $filters->resultadoPlan);
        }

        if ($filters->examenControl != '') {
            $query->where('evaluacionespolicias.examen', '=', $filters->examenControl);
        }
        if ($filters->fechaResultadosControl != '') {
            $query->where('evaluacionespolicias.fechaResultados', '=', $filters->fechaResultadosControl);
        }
        if ($filters->resultadoControl != '') {
            $query->where('evaluacionespolicias.resultado', '=', $filters->resultadoControl);
        }

        if ($filters->separacionCombo != '') {
            $query->where('separacion.separacion', '=', $filters->separacionCombo);
        }
        if ($filters->tipoSeparacion != '') {
            $query->where('separacion.tipoSeparacion', '=', $filters->tipoSeparacion);
        }
        if ($filters->fechaSeparacion != '') {
            $query->where('separacion.fechaSeparacion', '=', $filters->fechaSeparacion);
        }
        if ($filters->motivoSeparacion != '') {
            $query->where('separacion.motivo', 'like', '%'.$filters->motivoSeparacion.'%');
        }

        return $query->get();
    }
}
