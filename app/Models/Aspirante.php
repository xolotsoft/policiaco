<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Aspirante extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = "aspirantes";
    protected $primaryKey = "id";

    /**
     * Obtiene el registro de persona asociado con el aspirante.
    */
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'idPersona');
    }

    /**
     * Obtiene el registro de convocatorias asociado con el aspirante.
    */
    public function convocatoria()
    {
        return $this->hasOne('App\Models\Convocatoria', 'id', 'idConvocatoria');
    }

    public function getFechaNacimientoAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function getFechaRegistroAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaRegistroAttribute($value)
    {
        $this->attributes['fechaRegistro'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function archivoHuella()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'huella');
    }

    public function archivoDocumentoCup()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'documentoCup');
    }

    public function scopeAprobados($query)
    {
        return $query
        ->join('persona', 'persona.id', '=', 'aspirantes.idPersona')
        ->join('formacioninicial', 'formacioninicial.idPersona', '=', 'aspirantes.idPersona')
        ->where('formacioninicial.cumplimiento', '=', 26)
        ->get();
    }

    public function rango()
    {
        return $this->hasOne('App\Models\Rango', 'id', 'puesto');
    }

    public function scopeNoAprobados($query)
    {
        return $query
        ->join('persona', 'persona.id', '=', 'aspirantes.idPersona')
        ->join('formacioninicial', 'formacioninicial.idPersona', '=', 'aspirantes.idPersona')
        ->where('formacioninicial.cumplimiento', '!=', 26)
        ->get();
    }
    public function scopeKardex($query)
    {
        return $query
        ->join('persona', 'persona.id', '=', 'aspirantes.idPersona')
        ->whereNull('aspirantes.deleted_at')
        ->get();
    }
}
