<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Adscripcion extends Model
{
    use SoftDeletes;
    protected $table = "adscripcion";
    protected $primaryKey = "id";
    protected $dates = ['deleted_at'];

    /**
     * Obtiene el registro de persona asociado con el aspirante.
     */
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'idPersona');
    }

    public function getFechaInicioAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaInicioAttribute($value)
    {
        $this->attributes['fechaInicio'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getFechaFinAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaFinAttribute($value)
    {
        $this->attributes['fechaFin'] = Carbon::parse($value)->format('Y-m-d');
    }
}
