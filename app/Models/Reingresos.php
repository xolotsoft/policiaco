<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class Reingresos extends Model
{
    protected $table = "reingreso";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function getFechaPresentacionAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaPresentacionAttribute($value)
    {
        $this->attributes['fechaPresentacion'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function archivoDocumentoCuip()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'documentoCuip');
    }

    public function archivoDocumentoCup()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'documentoCup');
    }

    public function getInstitucion()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'institucion');
    }

    public function getEstado()
    {
        return $this->hasOne('App\Models\State', 'ID', 'estado');
    }

    public function getMunucipio()
    {
        return $this->hasOne('App\Models\Provinces', 'ID', 'municipio');
    }

    public function getGrado()
    {
        return $this->hasOne('App\Models\Rango', 'ID', 'grado');
    }

    public function getSeparacion()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'separacion');
    }

    public function getTipoSeparacion()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'tipoSeparacion');
    }
}
