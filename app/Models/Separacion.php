<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Separacion extends Model
{
    use SoftDeletes;
    protected $table = "separacion";
    protected $primaryKey = "id";
    protected $dates = ['deleted_at'];

    /**
     * Obtiene el registro de persona asociado con el aspirante.
     */
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'idPersona');
    }

    public function catalogos()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'tipoSeparacion');
    }

    public function getFechaSeparacionAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaSeparacionAttribute($value)
    {
        $this->attributes['fechaSeparacion'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function archivoCertificado()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'certificado');
    }
}
