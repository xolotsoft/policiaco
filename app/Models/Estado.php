<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = "states";
    protected $primaryKey = "ID";
    public $timestamps = false;
}
