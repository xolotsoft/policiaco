<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seleccion extends Model
{
    protected $table = "seleccion";
    protected $primaryKey = "id";
    public $timestamps = false;
    
    public function resultado()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'resultadoConfianza');
    }
}
