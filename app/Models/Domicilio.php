<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domicilio extends Model
{
    protected $table = "domicilio";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function coloniaCat()
    {
        return $this->hasOne('App\Models\Colonia', 'ID', 'colonia');
    }

    public function municipioCat()
    {
        return $this->hasOne('App\Models\Municipio', 'ID', 'municipio');
    }

    public function estadoCat()
    {
        return $this->hasOne('App\Models\Estado', 'ID', 'estado');
    }
}
