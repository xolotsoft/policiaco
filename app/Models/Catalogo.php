<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catalogo extends Model
{
    public $timestamps = false;
    public function scopeGenero($query)
    {
        return $query->where('idMetacatalogo', 1)->lists('nombre', 'id');
    }

    public function scopeCivil($query)
    {
        return $query->where('idMetacatalogo', 2)->lists('nombre', 'id');
    }

    public function scopeParentesco($query)
    {
        return $query->where('idMetacatalogo', 3)->lists('nombre', 'id');
    }

    public function scopeCumplimiento($query)
    {
        return $query->where('idMetacatalogo', 4)->lists('nombre', 'id');
    }

    public function scopeApto($query)
    {
        return $query->where('idMetacatalogo', 5)->lists('nombre', 'id');
    }

    public function scopeSancion($query)
    {
        return $query->where('idMetacatalogo', 6)->lists('nombre', 'id');
    }

    public function scopePlanes($query)
    {
        return $query->where('idMetacatalogo', 7)->lists('nombre', 'id');
    }

    public function scopeCaracteristicas($query)
    {
        return $query->where('idMetacatalogo', 8)->lists('nombre', 'id');
    }

    public function scopeSeparaciones($query)
    {
        return $query->where('idMetacatalogo', 9)->lists('nombre', 'id');
    }

    public function scopeHerramientas($query)
    {
        return $query->where('idMetacatalogo', 10)->lists('nombre', 'id');
    }

    public function scopeEstimulos($query)
    {
        return $query->where('idMetacatalogo', 11)->lists('nombre', 'id');
    }

    public function scopeAdministracion($query)
    {
        return $query->where('idMetacatalogo', 12)->lists('nombre', 'id');
    }

    public function scopeServicios($query)
    {
        return $query->where('idMetacatalogo', 14)->lists('nombre', 'id');
    }

    public function scopeInstitucionesReingreso($query)
    {
        return $query->where('idMetacatalogo', 15)->lists('nombre', 'id');
    }

    public function scopePosgrado($query)
    {
        return $query->where('idMetacatalogo', 16)->lists('nombre', 'id');
    }

    public function scopeCategoriaPolicial($query)
    {
        return $query->where('idMetacatalogo', 17)->lists('nombre', 'id');
    }

    public function scopeCategoriaEscalaBasica($query)
    {
        return $query->where('idMetacatalogo', 18)->lists('nombre', 'id');
    }

    public function scopeCategoriaComisario($query)
    {
        return $query->where('idMetacatalogo', 19)->lists('nombre', 'id');
    }

    public function scopeCategoriaOficiales($query)
    {
        return $query->where('idMetacatalogo', 20)->lists('nombre', 'id');
    }

    public function scopeExamenes($query)
    {
        return $query->where('idMetacatalogo', 21)->lists('nombre', 'id');
    }

    public function scopeFormacionInicial($query)
    {
        return $query->where('idMetacatalogo', 22)->lists('nombre', 'id');
    }

    public function scopeAsimilamiento($query)
    {
        return $query->where('idMetacatalogo', 23)->lists('nombre', 'id');
    }

    public function scopeSeparacionExtraordinaria($query)
    {
        return $query->where('idMetacatalogo', 24)->lists('nombre', 'id');
    }

    public function scopeOrganosColegiados($query)
    {
        return $query->where('idMetacatalogo', 25)->lists('nombre', 'id');
    }
}
