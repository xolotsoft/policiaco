<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Formations extends Model
{
    protected $table = "formacioninicial";
    protected $primaryKey = "id";
    public $timestamps = false;

    /**
     * Obtiene el registro de persona asociado con el aspirante.
    */
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'idPersona');
    }

    public function resultado()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'cumplimiento');
    }

    public function capacitadoras()
    {
        return $this->hasOne('App\Models\Instancia', 'id', 'instancia');
    }

    public function archivoCertificado()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'certificado');
    }

    public function archivoConstancia()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'constancia');
    }

    public function getFechaInicioAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaInicioAttribute($value)
    {
        $this->attributes['fechaInicio'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getFechaFinAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaFinAttribute($value)
    {
        $this->attributes['fechaFin'] = Carbon::parse($value)->format('Y-m-d');
    }
}
