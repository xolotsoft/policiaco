<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class EvaluacionesPolicias extends Model
{
    use SoftDeletes;
    protected $table = "evaluacionespolicias";
    protected $primaryKey = "id";
    protected $dates = ['deleted_at'];

    /**
     * Obtiene el registro de persona asociado con el aspirante.
     */
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'idPersona');
    }

    public function catalogos()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'examen');
    }

    public function cumplimiento()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'resultado');
    }

    public function documentos()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'documento');
    }

    public function getFechaAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaAttribute($value)
    {
        $this->attributes['fecha'] = Carbon::parse($value)->format('Y-m-d');
    }

    

    public function getFechaResultadosAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaResultadosAttribute($value)
    {
        $this->attributes['fechaResultados'] = Carbon::parse($value)->format('Y-m-d');
    }
}
