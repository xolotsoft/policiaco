<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Sancion extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = "sanciones";
    protected $primaryKey = "id";

    /**
     * Obtiene el registro de persona asociado con el aspirante.
    */
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'idPersona');
    }

    public function catalogos()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'tipoSanciones');
    }

    public function getFechaSancionesAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaSancionesAttribute($value)
    {
        $this->attributes['fechaSanciones'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getFechaInicioAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaInicioAttribute($value)
    {
        $this->attributes['fechaInicio'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getFechaConclusionAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaConclusionAttribute($value)
    {
        $this->attributes['fechaConclusion'] = Carbon::parse($value)->format('Y-m-d');
    }
}
