<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Promocion extends Model
{
    use SoftDeletes;
    protected $dates = ['delete_at'];
    protected $table = 'promociones';
    protected $primaryKey = 'id';

    /**
     * Obtiene el registro de persona asociado con el aspirante.
    */
    public function persona()
    {
        return $this->hasone('App\Models\Persona', 'id', 'idPersona');
    }

    public function rangos()
    {
        return $this->hasone('App\Models\Rango', 'id', 'rango');
    }

    public function cargos()
    {
        return $this->hasone('App\Models\Catalogo', 'id', 'cargo');
    }

    public function getFechaPromocionAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaPromocionAttribute($value)
    {
        $this->attributes['fechaPromocion'] = Carbon::parse($value)->format('Y-m-d');
    }
}
