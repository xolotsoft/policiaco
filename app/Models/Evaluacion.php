<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evaluacion extends Model
{
    use SoftDeletes;

    protected $table = "evaluaciones";
    protected $primaryKey = "id";

    public function Type()
    {
        return $this->hasOne('App\Models\EvaluationType', 'ID' , 'EvaluationType');
    }
}
