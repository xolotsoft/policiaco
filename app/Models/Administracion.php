<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Administracion extends Model
{
    use SoftDeletes;
    protected $table = "administracion";
    protected $primaryKey = "id";
    protected $dates = ['deleted_at'];

    /**
     * Obtiene el registro de persona asociado con el aspirante.
     */
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'idPersona');
    }

    public function catalogos()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'descripcion');
    }

    public function getFechaInicioAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaInicioAttribute($value)
    {
        $this->attributes['fechaInicio'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getFechaTerminoAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaTerminoAttribute($value)
    {
        $this->attributes['fechaTermino'] = Carbon::parse($value)->format('Y-m-d');
    }
}
