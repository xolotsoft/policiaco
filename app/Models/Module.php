<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = "modules";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function sections()
    {
        return $this->hasMany('App\Models\Section');
    }
}
