<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Colegiado extends Model
{
    use SoftDeletes;
    protected $table = "colegiado";
    protected $primaryKey = "id";

    public function getFechasesionAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechasesionAttribute($value)
    {
        $this->attributes['fechasesion'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function archivoAcuerdos()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'acuerdos');
    }

    public function archivoResoluciones()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'resoluciones');
    }

    public function archivoActaSesion()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'actaSesion');
    }
}
