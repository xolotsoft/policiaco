<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table = "sections";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function subsections()
    {
        return $this->hasMany('App\Models\Subsection');
    }
}
