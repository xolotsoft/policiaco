<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Selections extends Model
{
    protected $table = "";
    protected $primaryKey = "";
    public $timestamps = false;
}
