<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class Certificacion extends Model
{
    protected $table = "certificacionnombramiento";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function puestoPolicial()
    {
        return $this->hasOne('App\Models\Rango', 'id', 'puesto');
    }

    public function inicial()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'formacionInicial');
    }

    public function categoriaPolicial()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'cargo');
    }

    public function archivoNombramiento()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'nombramiento');
    }

    public function archivoDocumentoCurso()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'documentoCurso');
    }

    public function getFechaIngresoAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaIngresoAttribute($value)
    {
        $this->attributes['fechaIngreso'] = Carbon::parse($value)->format('Y-m-d');
    }
}
