<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Instancia extends Model
{
    use SoftDeletes;

    protected $table = "instancias";
    protected $primaryKey = "id";

}
