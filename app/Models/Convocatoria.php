<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Carbon\Carbon;

class Convocatoria extends Model
{
    use SoftDeletes;

    protected $table = "convocatorias";
    protected $primaryKey = "id";

    public function archivo()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'idArchivo');
    }

    public function getFechaAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaAttribute($value)
    {
        $this->attributes['fecha'] = Carbon::parse($value)->format('Y-m-d');
    }
}
