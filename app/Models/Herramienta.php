<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Herramienta extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = "herramientas";
    protected $primaryKey = "id";

    /**
     * Obtiene el registro de persona asociado con el aspirante.
    */
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'idPersona');
    }

    /**
     * Obtiene el registro de catalogos asociado con el policia.
    */
    public function catalogos()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'tipo');
    }

    public function getFechaRecepcionAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaRecepcionAttribute($value)
    {
        $this->attributes['fechaRecepcion'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getFechaDevolucionAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaDevolucionAttribute($value)
    {
        $this->attributes['fechaDevolucion'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function archivoAvalResguardo()
    {
        return $this->hasOne('App\Models\Archivo', 'id', 'avalResguardo');
    }
}
