<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    protected $table = "call";
    protected $primaryKey = "id";
    public $timestamps = false;
}
