<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Estimulo extends Model
{
    use SoftDeletes;
    protected $table = "estimulos";
    protected $primaryKey = "id";
    protected $dates = ['deleted_at'];

    /**
     * Obtiene el registro de persona asociado con el aspirante.
     */
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'idPersona');
    }

    public function catalogos()
    {
        return $this->hasOne('App\Models\Catalogo', 'id', 'tipoRecompensa');
    }

    public function getFechaEmisionAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setFechaEmisionAttribute($value)
    {
        $this->attributes['fechaEmision'] = Carbon::parse($value)->format('Y-m-d');
    }
}
