<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use \DB;

class Mensaje extends Model
{
    use SoftDeletes;

    public function recibe()
    {
        return $this->hasOne('App\User', 'id', 'destinatario');
    }

    public function envia()
    {
        return $this->hasOne('App\User', 'id', 'remitente');
    }

    public function scopeNuevos($query)
    {
        return $query
        ->select(DB::raw('count(*) AS total'))
        ->where('destinatario', Auth::user()->id)
        ->where('estado', 0)
        ->first();
    }
}
