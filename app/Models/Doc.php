<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doc extends Model
{
    protected $table = "documentos";
    protected $primaryKey = "id";
    public $timestamps = false;
}
