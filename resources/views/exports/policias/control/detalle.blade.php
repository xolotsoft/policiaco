<table style="border-collapse:collapse;width:100%;">
    <thead>
        <tr>
            <td colspan="5">
                <table style="border-collapse:collapse;width:100%;">
                    <tr>
                       <th style="padding:0 5px 10px 5px;font-size:18px;width:33%;">S P C P</th>
                        <th style="padding:0 5px 10px 5px;text-align:center;width:33%;">{{Auth::user()->municipio->nombre}}</th>
                        <th style="padding:0 5px 10px 5px;text-align:right;width:33%;" >{{Carbon\Carbon::now()->format("d-m-Y")}}</th>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height:30px;">
            <th colspan="5" style="text-align:center;font-size:18px;color:#FFF;background-color:#405467;letter-spacing:2px;height:30px;">Evaluaciones de Control de Confianza</th>
        </tr>
        <tr>
            <th style="padding:10px 5px;border-bottom:1px solid #405467;" colspan="5" >Policia: {{ $persona->aPaterno}} {{$persona->aMaterno}} {{$persona->nombre}}</th>
        </tr>
        <tr style="border-bottom:1px solid #405467;">
            <th style="padding:5px;border-bottom:1px solid #405467;">Examen</th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Resultado</th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Fecha de presentación</th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Fecha de entrega de resultados</th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Estatus</th>
        </tr>
    </thead>
    <tbody>
        @forelse($persona->evaluaciones as $evaluacion)
        <tr>
            <td style="padding:5px;">{{$evaluacion->catalogos->nombre}}</td>
            <td style="padding:5px;">{{$evaluacion->cumplimiento->nombre}}</td>
            <td style="padding:5px;">{{$evaluacion->fecha}}</td>
            <td style="padding:5px;">{{$evaluacion->fechaResultados}}</td>
            <td style="padding:5px;text-align:center;">
            @php
                $end = Carbon\Carbon::parse($evaluacion->fechaResultados);
                $now = Carbon\Carbon::now();
                $length = $end->diffInDays($now);
            @endphp
            @if($length >= 660)
                <div style="width:20px;height:20px;background-color:red;border-radius:10px;margin:5px auto;"></div>
            @elseif($length >= 480)
                <div style="width:20px;height:20px;background-color:orange;border-radius:10px;margin:5px auto;"></div>
            @else
                <div style="width:20px;height:20px;background-color:green;border-radius:10px;margin:5px auto;"></div>
            @endif
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="5" style="text-align:center;">No hay evaluaciones registradas</td>
        </tr>
        @endforelse
    </tbody>
</table>
