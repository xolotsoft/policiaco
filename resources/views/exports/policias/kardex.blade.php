<table style="border-collapse:collapse;width:100%;">
<thead>
        <tr>
            <td colspan="4">
                <table style="border-collapse:collapse;width:100%;">
                    <tr>
                       <th style="padding:0 5px 10px 5px;font-size:18px;width:33%;">S P C P</th>
                        <th style="padding:0 5px 10px 5px;text-align:center;width:33%;">{{Auth::user()->municipio->nombre}}</th>
                        <th style="padding:0 5px 10px 5px;text-align:right;width:33%;" >{{Carbon\Carbon::now()->format("d-m-Y")}}</th>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height:30px;">
            <th colspan="4" style="text-align:center;font-size:18px;color:#FFF;background-color:#405467;letter-spacing:2px;height:30px;">Kardex</th>
        </tr>
    </thead>
    <tbody>
        @include('exports.policias.kardex.personales')
        @include('exports.policias.kardex.domicilio')
        @include('exports.policias.kardex.familiares')
        @include('exports.policias.kardex.actividad')
        @include('exports.policias.kardex.promociones')
        @include('exports.policias.kardex.herramientas')
        @include('exports.policias.kardex.sanciones')
        @include('exports.policias.kardex.adscripcion')
        @include('exports.policias.kardex.estimulos')
        @include('exports.policias.kardex.plan')
        @include('exports.policias.kardex.administrativas')
        @include('exports.policias.kardex.separacion')
        @include('exports.policias.kardex.evaluaciones')
    </tbody>
</table>
