<tr>
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Plan Individual de Carrera</th>
</tr>
<tr>
    <th style="padding:5px;">Tipo</th>
    <th style="padding:5px;">Capacitación</th>
    <th style="padding:5px;">Duración</th>
    <th style="padding:5px;">Resultado</th>
</tr>
@forelse($persona->planes as $plan)
    <tr>
        <td style="padding:5px;">{{$plan->planes->Name}}</td>
        <td style="padding:5px;">{{$plan->tipo->EvaluationName}}</td>
        <td style="padding:5px;">{{$plan->duracion}} Hrs.</td>
        <td style="padding:5px;">{{$plan->apto->nombre}}</td>
    </tr>
@empty
<tr>
    <td colspan="4" style="text-align:center;padding:5px;">No hay registros</td>
</tr>
@endforelse
