<tr>
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Estímulos</th>
</tr>
<tr>
    <th style="padding:5px;">Tipo</th>
    <th style="padding:5px;">Motivo</th>
    <th style="padding:5px;">Premio</th>
    <th style="padding:5px;">Fecha</th>
</tr>
@forelse($persona->estimulos as $estimulo)
    <tr>
        <td style="padding:5px;">{{$estimulo->catalogos->nombre}}</td>
        <td style="padding:5px;">{{$estimulo->motivo}}</td>
        <td style="padding:5px;">{{$estimulo->premio}}</td>
        <td style="padding:5px;">{{$estimulo->fechaEmision}}</td>
    </tr>
@empty
<tr>
    <td colspan="4" style="text-align:center;">No hay registros</td>
</tr>
@endforelse
