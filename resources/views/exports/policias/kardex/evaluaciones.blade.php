<tr>
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Evaluaciones de Control de Confianza</th>
</tr>
<tr>
    <th style="padding:5px;">Examen</th>
    <th style="padding:5px;">Resultado</th>
    <th style="padding:5px;">Fecha de presentación</th>
    <th style="padding:5px;">Fecha de entrega de resultados</th>
</tr>
@forelse($persona->evaluaciones as $evaluacion)
<tr>
    <td style="padding:5px;">{{$evaluacion->catalogos->nombre}}</td>
    <td style="padding:5px;">{{$evaluacion->cumplimiento->nombre}}</td>
    <td style="padding:5px;">{{$evaluacion->fecha}}</td>
    <td style="padding:5px;">{{$evaluacion->fechaResultados}}</td>
</tr>
@empty
<tr>
    <td colspan="4" style="text-align:center;">No hay evaluaciones registradas</td>
</tr>
@endforelse
