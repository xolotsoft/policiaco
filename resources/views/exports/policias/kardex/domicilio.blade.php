<tr>
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Domicilio</th>
</tr>
@if($persona->domicilio)
<tr>
    <th colspan="1" style="padding:5px;">Calle</th>
    <td colspan="3" style="padding:5px;">{{$persona->domicilio->calle}}</td>
</tr>
<tr>
    <th style="padding:5px;">No. Interior</th>
    <td style="padding:5px;">{{$persona->domicilio->interior}}</td>
    <th style="padding:5px;">No. Exterior</th>
    <td style="padding:5px;">{{$persona->domicilio->exterior}} </td>
</tr>
<tr>
    <th style="padding:5px;">Colonia</th>
    <td style="padding:5px;">{{$persona->domicilio->coloniaCat->Name}}</td>
    <th style="padding:5px;">Municipio</th>
    <td style="padding:5px;">{{$persona->domicilio->municipioCat->Name}}</td>

</tr>
<tr>
    <th style="padding:5px;">Estado</th>
    <td style="padding:5px;">{{$persona->domicilio->estadoCat->Name}} </td>
    <th style="padding:5px;">Código Postal</th>
    <td style="padding:5px;">{{$persona->domicilio->cp}}</td>
</tr>
@else
<tr>
    <th colspan="1" style="padding:5px;">Calle</th>
    <td colspan="3" style="padding:5px;"></td>
</tr>
<tr>
    <th style="padding:5px;">No. Interior</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">No. Exterior</th>
    <td style="padding:5px;"> </td>
</tr>
<tr>
    <th style="padding:5px;">Colonia</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Municipio</th>
    <td style="padding:5px;"></td>

</tr>
<tr>
    <th style="padding:5px;">Estado</th>
    <td style="padding:5px;"> </td>
    <th style="padding:5px;">Código Postal</th>
    <td style="padding:5px;"></td>
</tr>
@endif
