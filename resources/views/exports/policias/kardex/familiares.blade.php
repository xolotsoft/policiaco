<tr class="headings">
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Familiares</th>
</tr>
<tr class="headings">
    <th colspan="2" style="padding:5px;">Nombre</th>
    <th colspan="2" style="padding:5px;">Parentesco</th>
</tr>
@forelse($persona->familiares as $familiar)
    <tr class="even pointer">
        <td style="padding:5px;" colspan="2">{{$familiar->nombre}} {{$familiar->aPaterno.' '.$familiar->aMaterno}}</td>
        <td style="padding:5px;" colspan="2">{{$familiar->catalogos->nombre}}</td>
    </tr>

@empty
<tr class="headings">
    <td colspan="4" style="text-align:center;padding:5px;">No hay familiares registrados</td>
</tr>
@endforelse
