<tr>
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Datos Personales</th>
</tr>
<tr>
    <th style="padding:5px;">Nombre</th>
    <td style="padding:5px;">{{ $persona->aPaterno}} {{$persona->aMaterno}} {{$persona->nombre}}</td>
    <th style="padding:5px;">CURP</th>
    <td style="padding:5px;">{{$persona->curp}}</td>
</tr>
<tr>
    <th style="padding:5px;">RFC</th>
    <td style="padding:5px;">{{$persona->rfc}}</td>
    <th style="padding:5px;">No. Empleado</th>
    <td style="padding:5px;">{{$persona->policia->id}}</td>
</tr>
<tr>
    <th style="padding:5px;">Fecha de Nacimiento</th>
    <td style="padding:5px;">{{$persona->fechaNacimiento}}</td>
    <th style="padding:5px;">Lugar de Nacimiento</th>
    <td style="padding:5px;">{{$persona->nacimiento->Name}}</td>
</tr>
<tr>
    <th style="padding:5px;">Altura</th>
    <td style="padding:5px;">{{$persona->estatura}} Mts.</td>
    <th style="padding:5px;">Peso</th>
    <td style="padding:5px;">{{$persona->peso}} Kgs.</td>
</tr>
<tr>
    <th style="padding:5px;">Teléfono</th>
    <td style="padding:5px;">{{$persona->telefono}}</td>
    <th style="padding:5px;">Móvil</th>
    <td style="padding:5px;">{{$persona->movil}}</td>
</tr>
<tr>
    <th style="padding:5px;">Sexo</th>
    <td style="padding:5px;">{{$persona->sexo->nombre}}</td>
    <th rowspan="3" style="padding:5px;">Foto</th>
    <td rowspan="3" style="padding:5px;">{{ Html::image('img/persona.png', '', array('style' => 'width:100px;border-radius:50%;')) }} </td>
</tr>
<tr>
    <th style="padding:5px;">Correo electrónico</th>
    <td style="padding:5px;">{{$persona->email}}</td>
</tr>
<tr>
    <th style="padding:5px;">Estado civil</th>
    <td style="padding:5px;">{{$persona->civil->nombre}}</td>
</tr>
