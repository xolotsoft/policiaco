<tr>
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Promociones</th>
</tr>
<tr>
    <th style="padding:5px;">Categoría</th>
    <th style="padding:5px;">Grado</th>
    <th style="padding:5px;">Especial</th>
    <th style="padding:5px;">Fecha</th>
</tr>
@forelse($persona->promociones as $promocion)
    <tr>
        <td style="padding:5px;">{{$promocion->cargos->nombre}}</td>
        <td style="padding:5px;">{{$promocion->rangos->Rank}}</td>
        <td style="padding:5px;">{{$promocion->rangos->Special?'SI':'NO'}}</td>
        <td style="padding:5px;">{{$promocion->fechaPromocion}}</td>
    </tr>
@empty
<tr>
    <td colspan="4" style="text-align:center;padding:5px;">No hay registros</td>
</tr>
@endforelse
