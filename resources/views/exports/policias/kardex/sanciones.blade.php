<tr>
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Sanciones</th>
</tr>
<tr>
    <th style="padding:5px;">Causa</th>
    <th style="padding:5px;">Tipo</th>
    <th style="padding:5px;">Duración</th>
    <th style="padding:5px;">Fecha</th>
</tr>
@forelse($persona->sanciones as $sancion)
    <tr>
        <td style="padding:5px;">{{$sancion->causa}}</td>
        <td style="padding:5px;">{{$sancion->catalogos->nombre}}</td>
        <td style="padding:5px;">{{$sancion->duracion}}</td>
        <td style="padding:5px;">{{$sancion->fechaSanciones}}</td>
    </tr>
@empty
<tr>
    <td colspan="4" style="text-align:center;padding:5px;">No hay registros</td>
</tr>
@endforelse
