<tr>
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Cambios de Adscripción</th>
</tr>
<tr>
    <th style="padding:5px;">Tareas</th>
    <th style="padding:5px;">Unidad Administrativa</th>
    <th style="padding:5px;">Razón del Cambio</th>
    <th style="padding:5px;">Fecha Inicio</th>
</tr>
@forelse($persona->adscripciones as $adscripcion)
    <tr>
        <td style="padding:5px;">{{$adscripcion->servicioRealizar}}</td>
        <td style="padding:5px;">{{$adscripcion->areaProcedencia}}</td>
        <td style="padding:5px;">{{$adscripcion->razon}}</td>
        <td style="padding:5px;">{{$adscripcion->fechaInicio}}</td>
    </tr>
@empty
<tr>
    <td colspan="4" style="text-align:center;">No hay registros</td>
</tr>
@endforelse
