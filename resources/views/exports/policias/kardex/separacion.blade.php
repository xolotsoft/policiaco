<tr>
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Baja o Separación</th>
</tr>
@if($persona->separacion)
<tr>
    <th style="padding:5px;">Tipo de separación</th>
    <td style="padding:5px;">{{$persona->separacion->catalogos->nombre}}</td>
    <th style="padding:5px;">Fecha de separación</th>
    <td style="padding:5px;">{{$persona->separacion->fechaSeparacion}}</td>
</tr>
<tr>
    <th style="padding:5px;">Motivo</th>
    <td style="padding:5px;">{{$persona->separacion->motivo}}</td>
    <th style="padding:5px;">Aprobado por</th>
    <td style="padding:5px;">{{$persona->separacion->aprobado}}</td>
</tr>
@else
<tr>
    <th style="padding:5px;">Separación</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Tipo de separación</th>
    <td style="padding:5px;"></td>
</tr>
<tr>
    <th style="padding:5px;">Fecha de separación</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Motivo</th>
    <td style="padding:5px;"></td>
</tr>
@endif
