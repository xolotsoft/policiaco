<tr class="headings">
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Herramientas</th>
</tr>
<tr class="headings">
    <th style="padding:5px;">Descripción</th>
    <th style="padding:5px;">Tipo</th>
    <th style="padding:5px;">Cantidad</th>
    <th style="padding:5px;">Fecha Devolución</th>
</tr>
@forelse($persona->herramientas as $herramienta)
    <tr class="even pointer">
        <td style="padding:5px;">{{$herramienta->descripcion}}</td>
        <td style="padding:5px;">{{$herramienta->catalogos->nombre}}</td>
        <td style="padding:5px;">{{$herramienta->cantidad}}</td>
        <td style="padding:5px;">{{$herramienta->fechaDevolucion}}</td>
    </tr>
@empty
<tr class="headings">
    <td colspan="4" style="text-align:center;padding:5px;">No hay registros</td>
</tr>
@endforelse
