<tr>
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Actividades Administrativas</th>
</tr>
<tr>
    <th style="padding:5px;">Descripción</th>
    <th style="padding:5px;">Autoriza</th>
    <th style="padding:5px;">Fecha Inicio</th>
    <th style="padding:5px;">Fecha Termino</th>
</tr>
@forelse($persona->administrativas as $administrativa)
    <tr>
        <td style="padding:5px;">{{$administrativa->descripcion}}</td>
        <td style="padding:5px;">{{$administrativa->autorizante}}</td>
        <td style="padding:5px;">{{$administrativa->fechaInicio}}</td>
        <td style="padding:5px;">{{$administrativa->fechaTermino}}</td>
    </tr>
@empty
<tr>
    <td colspan="4" style="text-align:center;">No hay registros</td>
</tr>
@endforelse
