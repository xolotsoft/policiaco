<table style="border-collapse:collapse;width:100%;">
    <thead>
        <tr>
            <td colspan="3">
                <table style="border-collapse:collapse;width:100%;">
                    <tr>
                       <th style="padding:0 5px 10px 5px;font-size:18px;width:33%;">S P C P</th>
                        <th style="padding:0 5px 10px 5px;text-align:center;width:33%;">{{Auth::user()->municipio->nombre}}</th>
                        <th style="padding:0 5px 10px 5px;text-align:right;width:33%;" >{{Carbon\Carbon::now()->format("d-m-Y")}}</th>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height:30px;">
            <th colspan="3" style="text-align:center;font-size:18px;color:#FFF;background-color:#405467;letter-spacing:2px;height:30px;">Policias Activos</th>
        </tr>
        <tr class="headings">
            <th style="padding:5px;border-bottom:1px solid #405467;">Empleado</th>
            <th style="padding:5px;border-bottom:1px solid #405467;">RFC </th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Nombre </th>
        </tr>
    </thead>
    <tbody>
        @foreach($policias as $policia)
        <tr>
            <td style="padding:5px;">{{ $policia->id }}</td>
            <td style="padding:5px;">{{ $policia->persona->rfc }}</td>
            <td style="padding:5px;">{{ $policia->persona->aPaterno }} {{ $policia->persona->aMaterno }} {{ $policia->persona->nombre }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
