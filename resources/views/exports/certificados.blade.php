<table style="border-collapse:collapse;width:100%;">
    <thead>
        <tr>
            <td colspan="5">
                <table style="border-collapse:collapse;width:100%;">
                    <tr>
                       <th style="padding:0 5px 10px 5px;font-size:18px;width:33%;">S P C P</th>
                        <th style="padding:0 5px 10px 5px;text-align:center;width:33%;">{{Auth::user()->municipio->nombre}}</th>
                        <th style="padding:0 5px 10px 5px;text-align:right;width:33%;" >{{Carbon\Carbon::now()->format("d-m-Y")}}</th>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height:30px;">
            <th colspan="5" style="text-align:center;font-size:18px;color:#FFF;background-color:#405467;letter-spacing:2px;height:30px;">Certificados</th>
        </tr>
        <tr>
            <th style="padding:5px;border-bottom:1px solid #405467;">Grado</th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Ingreso</th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Adscripción</th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Calificación </th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Documento </th>
        </tr>
    </thead>
    <tbody>
    @foreach($certificados as $certificado)
        <tr>
            <td style="padding:5px;">{{$certificado->puestoPolicial->Rank}}</td>
            <td style="padding:5px;">{{$certificado->fechaIngreso}}</td>
            <td style="padding:5px;">{{$certificado->adscripcion}}</td>
            <td style="padding:5px;">{{$certificado->calificacion}}</td>
            <td style="padding:5px;">{{substr($certificado->archivoDocumentoCurso->name, 0, -4)}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
