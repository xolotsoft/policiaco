<table style="border-collapse:collapse;width:100%;">
    <thead>
        <tr>
            <td colspan="6">
                <table style="border-collapse:collapse;width:100%;">
                    <tr>
                       <th style="padding:0 5px 10px 5px;font-size:18px;width:33%;">S P C P</th>
                        <th style="padding:0 5px 10px 5px;text-align:center;width:33%;">{{Auth::user()->municipio->nombre}}</th>
                        <th style="padding:0 5px 10px 5px;text-align:right;width:33%;" >{{Carbon\Carbon::now()->format("d-m-Y")}}</th>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height:30px;">
            <th colspan="6" style="text-align:center;font-size:18px;color:#FFF;background-color:#405467;letter-spacing:2px;height:30px;">Costos</th>
        </tr>
        <tr >
            <th style="padding:5px;border-bottom:1px solid #405467;">Categoría </th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Grado </th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Especial </th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Salario</th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Total Mensual</th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Total Anual</th>
        </tr>
    </thead>
    <tbody >
        @foreach($costos as $costo)
        <tr >
            <td style="padding:5px;">{{$costo->categoria}}</td>
            <td style="padding:5px;">{{$costo->grado}}</td>
            <td style="padding:5px;">{{$costo->especial?'SI':'NO'}}</td>
            <td style="padding:5px;">$ {{$costo->salario}}</td>
            <td style="padding:5px;">$ {{$costo->Total}}</td>
            <td style="padding:5px;">$ {{$costo->Anual}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
