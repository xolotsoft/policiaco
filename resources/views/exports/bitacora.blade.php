<table style="border-collapse:collapse;width:100%;">
    <thead>
        <tr>
            <td colspan="5">
                <table style="border-collapse:collapse;width:100%;">
                    <tr>
                       <th style="padding:0 5px 10px 5px;font-size:18px;width:33%;">S P C P</th>
                        <th style="padding:0 5px 10px 5px;text-align:center;width:33%;">{{Auth::user()->municipio->nombre}}</th>
                        <th style="padding:0 5px 10px 5px;text-align:right;width:33%;" >{{Carbon\Carbon::now()->format("d-m-Y")}}</th>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height:30px;">
            <th colspan="5" style="text-align:center;font-size:18px;color:#FFF;background-color:#405467;letter-spacing:2px;height:30px;">Bitácora</th>
        </tr>
        <tr class="headings">
            <th style="padding:5px;border-bottom:1px solid #405467;">Usuario </th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Fecha</th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Módulo </th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Sección</th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Acción</th>
        </tr>
    </thead>
    <tbody>
        @foreach($bitacora as $val)
            <tr class="even pointer">
                <td style="padding:5px;">{{$val->user->name}}</td>
                <td style="padding:5px;">{{$val->fecha}}</td>
                <td style="padding:5px;">{{$val->modulo}}</td>
                <td style="padding:5px;">{{$val->seccion}}</td>
                <td style="padding:5px;">{{$val->accion}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
