<tr class="headings">
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Formación</th>
</tr>
@if($persona->formacion)
<tr class="headings">
    <th style="padding:5px;">Materia</th>
    <td style="padding:5px;">
        {{$persona->formacion->material}}
    </td>
    <th style="padding:5px;">Fecha de inicio</th>
    <td style="padding:5px;">
        {{$persona->formacion->fechaInicio}}
    </td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Duracion</th>
    <td style="padding:5px;">
        {{$persona->formacion->duracion}}
    </td>
    <th style="padding:5px;">Fecha de finalización</th>
    <td style="padding:5px;">
        {{$persona->formacion->fechaFin}}
    </td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Certificado</th>
    <td style="padding:5px;">
    {{$persona->formacion->certificado?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Constancia</th>
    <td style="padding:5px;">
    {{$persona->formacion->constancia?'SI':'NO'}}
    </td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Cumplimiento</th>
    <td style="padding:5px;">
        {{$persona->formacion->cumplimiento !=0 ? $persona->formacion->resultado->nombre : ''}}
    </td>
    <th style="padding:5px;">Instancia capacitadora</th>
    <td style="padding:5px;">
        {{$persona->formacion->instancia !=0 ? $persona->formacion->capacitadoras->Name : ''}}
    </td>
</tr>
<tr class="headings">
    <th colspan="1" style="padding:5px;">Observaciones</th>
    <td colspan="3" style="padding:5px;">
        {{$persona->formacion->observaciones}}
    </td>
</tr>
@else
<tr class="headings">
    <th style="padding:5px;">Materia</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Fecha de inicio</th>
    <td style="padding:5px;"></td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Duracion</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Fecha de finalización</th>
    <td style="padding:5px;"></td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Certificado</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Constancia</th>
    <td style="padding:5px;"></td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Cumplimiento</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Instancia capacitadora</th>
    <td style="padding:5px;"></td>
</tr>
<tr class="headings">
    <th colspan="1" style="padding:5px;">Observaciones</th>
    <td colspan="3" style="padding:5px;"></td>
</tr>
@endif
