<tr>
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Reingreso Labor Policial</th>
</tr>
@if($persona->reingreso)
<tr>
    <th style="padding:5px;">Institución</th>
    <td style="padding:5px;">{{$persona->reingreso->institucion != 0 ? $persona->reingreso->getInstitucion->nombre : ''}}</td>
    <th style="padding:5px;">Entidad</th>
    <td style="padding:5px;">{{$persona->reingreso->estado != 0 ? $persona->reingreso->getEstado->Name : ''}}</td>
</tr>
<tr>
    <th style="padding:5px;">Municipio</th>
    <td style="padding:5px;">{{$persona->reingreso->municipio !=0 ? $persona->reingreso->getMunucipio->Name : ''}}</td>
    <th style="padding:5px;">Grado</th>
    <td style="padding:5px;">{{$persona->reingreso->grado != 0 ? $persona->reingreso->getGrado->Rank : ''}}</td>
</tr>
<tr>
    <th style="padding:5px;">Antigüedad</th>
    <td style="padding:5px;">{{$persona->reingreso->antiguedad}}</td>
    <th style="padding:5px;">Instancia Elegida</th>
    <td style="padding:5px;">{{$persona->reingreso->nombreInstancia}}</td>
</tr>
<tr>
    <th style="padding:5px;">Instancia Evaluadora</th>
    <td style="padding:5px;">{{$persona->reingreso->instanciaEvaluadora}}</td>
    <th style="padding:5px;">Fecha presentación de exámenes</th>
    <td style="padding:5px;">{{$persona->reingreso->fechaPresentacion}}</td>
</tr>
<tr>
    <th colspan="1" style="padding:5px;">Observaciones</th>
    <td colspan="3" style="padding:5px;">{{$persona->reingreso->observaciones}}</td>
</tr>
@else
<tr>
    <th style="padding:5px;">Institución</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Entidad</th>
    <td style="padding:5px;"></td>
</tr>
<tr>
    <th style="padding:5px;">Municipio</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Grado</th>
    <td style="padding:5px;"></td>
</tr>
<tr>
    <th style="padding:5px;">Antigüedad</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Instancia Elegida</th>
    <td style="padding:5px;"></td>
</tr>
<tr>
    <th style="padding:5px;">Instancia Evaluadora</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Fecha presentación de exámenes</th>
    <td style="padding:5px;"></td>
</tr>
<tr>
    <th colspan="2" style="padding:5px;">Observaciones</th>
    <td colspan="2" style="padding:5px;"></td>
</tr>
@endif
