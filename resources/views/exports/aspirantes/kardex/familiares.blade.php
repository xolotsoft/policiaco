<tr class="headings">
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Familiares</th>
</tr>
<tr class="headings">
    <th style="padding:5px;" colspan="2" class="column-title">Nombre</th>
    <th style="padding:5px;" colspan="2" class="column-title">Parentesco</th>
</tr>
@forelse($persona->familiares as $familiar)
    <tr class="even pointer">
        <td style="padding:5px;" colspan="2">{{$familiar->nombre}} {{$familiar->aPaterno.' '.$familiar->aMaterno}}</td>
        <td style="padding:5px;" colspan="2">{{$familiar->catalogos->nombre}}</td>
    </tr>
@empty
<tr class="headings">
    <td style="padding:5px;" colspan="4" class=" " style="text-align:center;">No hay familiares registrados</td>
</tr>
@endforelse
{{--  <div style="page-break-after: always;"></div>  --}}
