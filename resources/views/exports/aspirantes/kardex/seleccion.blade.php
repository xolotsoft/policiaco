<tr>
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Selección</th>
</tr>
<tr>
    <th colspan="2" style="padding:5px;">Resultado</th>
    <th colspan="2" style="padding:5px;">Observaciones</th>
</tr>
@if($persona->seleccion)
<tr>
    <td colspan="2" style="padding:5px;">{{$persona->seleccion->resultado->nombre}}</td>
    <td colspan="2" style="padding:5px;">{{$persona->seleccion->observaciones}}</td>
</tr>
@else
<tr>
    <td colspan="2" style="padding:5px;">&nbsp;</td>
    <td colspan="2" style="padding:5px;">&nbsp;</td>
</tr>
@endif
