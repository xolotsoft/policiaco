<tr class="headings">
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Certificación y Nombramiento</th>
</tr>
@if($persona->certificacion)
<tr class="headings">
    <th style="padding:5px;">Categoría Policial</th>
    <td style="padding:5px;">
        {{$persona->certificacion->cargo !=0 ? $persona->certificacion->categoriaPolicial->nombre : ''}}
    </td>
    <th style="padding:5px;">Grado Policial</th>
    <td style="padding:5px;">
        {{$persona->certificacion->puesto !=0 ? $persona->certificacion->puestoPolicial->Rank : ''}}
    </td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Fecha de ingreso</th>
    <td style="padding:5px;">
        {{$persona->certificacion->fechaIngreso}}
    </td>
    <th style="padding:5px;">Area o unidad de adscripción</th>
    <td style="padding:5px;">
        {{$persona->certificacion->adscripcion}}
    </td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Nombre de Curso</th>
    <td style="padding:5px;">
        {{$persona->certificacion->nombreCurso}}
    </td>
    <th style="padding:5px;">Formación Inicial</th>
    <td style="padding:5px;">
        {{$persona->certificacion->formacionInicial !=0 ? $persona->certificacion->inicial->nombre : ''}}
    </td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Calificación de curso de formación inicial</th>
    <td style="padding:5px;">
        {{$persona->certificacion->calificacion}}
    </td>
    <th style="padding:5px;">Documento que avala el curso</th>
    <td style="padding:5px;">
    {{$persona->certificacion->documentoCurso?'SI':'NO'}}
    </td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Calificación de curso de formación inicial</th>
    <td style="padding:5px;">
        {{$persona->certificacion->calificacion}}
    </td>
    <th style="padding:5px;">Documento que avala el curso</th>
    <td style="padding:5px;">
    {{$persona->certificacion->documentoCurso?'SI':'NO'}}
    </td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Observaciones</th>
    <td style="padding:5px;">
        {{$persona->certificacion->observaciones}}
    </td>
    <th style="padding:5px;">Nombramiento</th>
    <td style="padding:5px;">
    {{$persona->certificacion->nombramiento?'SI':'NO'}}
    </td>
</tr>
@else
<tr class="headings">
    <th style="padding:5px;">Categoría Policial</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Grado Policial</th>
    <td style="padding:5px;"></td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Fecha de ingreso</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Area o unidad de adscripción</th>
    <td style="padding:5px;"></td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Nombre de Curso</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Formación Inicial</th>
    <td style="padding:5px;"></td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Calificación de curso de formación inicial</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Documento que avala el curso</th>
    <td style="padding:5px;"></td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Calificación de curso de formación inicial</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Documento que avala el curso</th>
    <td style="padding:5px;"></td>
</tr>
<tr class="headings">
    <th style="padding:5px;">Observaciones</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Nombramiento</th>
    <td style="padding:5px;"></td>
</tr>
@endif
