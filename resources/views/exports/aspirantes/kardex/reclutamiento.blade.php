<tr>
    <th colspan="4" style="text-align:center;font-size:16px;color:#FFF;background-color:#405467;letter-spacing:2px;height:25px;">Reclutamiento</th>
</tr>
@if($persona->reclutamiento)
<tr>
    <th style="padding:5px;">Perfil Solicitado</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->perfilSolicitado?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Documento</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->documento?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Credencial de Elector</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->elector?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Acta de Nacimiento</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->nacimiento?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Cartilla de Servicio Militar</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->cartilla?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Antecedentes No Penales</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->penales?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Secundaria</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->secundaria?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Bachillerato</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->bachillerato?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Certificado Técnico</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->tecnico?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Licencitura</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->licenciatura?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Posgrado</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->posgrado != 0 ? $persona->reclutamiento->getPosgrado->nombre : '' }}
    </td>
    <th style="padding:5px;">Certificado de Posgrado</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->maestria?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Certificado de baja Policial</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->bajaPolicial?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Fotografia</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->fotoInfantil?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Comprobante de Domicilio</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->comprobanteDomicilio?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Carta con motivos de ingreso</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->motivoIngreso?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Referencias personales 1</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->refPersonales1?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Referencias personales 2</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->refPersonales2?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Acta de nacimiento de hijos</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->nacimientoHijo?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Acta de Matrimonio</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->matrimonio?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Acta de sociedad conyugal</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->sociedadConyugal?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Licencia de manejo</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->manejo?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">RFC</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->documentoRfc?'SI':'NO'}}
    </td>
    <th style="padding:5px;">CURP</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->documentoCurp?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Estados de cuenta bancarios</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->estadoCuenta?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Solicitud de Ingreso</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->solicitud?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Currículum</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->currículum?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Asimilamiento Grado Policial</th>
    <td style="padding:5px;">
        {{$persona->reclutamiento->asimilamientoGradoPolicial != 0 ? $persona->reclutamiento->grado->nombre : ''}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Servicios médicos</th>
    <td style="padding:5px;">
       {{$persona->reclutamiento->medicos->nombre}}
    </td>
    <th style="padding:5px;">Observaciones</th>
    <td style="padding:5px;">
       {{$persona->reclutamiento->observaciones}}
    </td>
</tr>
@else
<tr>
    <th style="padding:5px;">Perfil Solicitado</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Documento</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Credencial de Elector</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Acta de Nacimiento</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Cartilla de Servicio Militar</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Antecedentes No Penales</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Secundaria</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Bachillerato</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Certificado Técnico</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Licencitura</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Posgrado</th>
    <td style="padding:5px;"></td>
    <th style="padding:5px;">Certificado de Posgrado</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Certificado de baja Policial</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Fotografia</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Comprobante de Domicilio</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Carta con motivos de ingreso</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Referencias personales 1</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Referencias personales 2</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Acta de nacimiento de hijos</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Acta de Matrimonio</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Acta de sociedad conyugal</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Licencia de manejo</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Licencia de manejo</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">CURP</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">RFC</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Servicios médicos</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Estados de cuenta bancarios</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Solicitud de Ingreso</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
</tr>
<tr>
    <th style="padding:5px;">Currículum</th>
    <td style="padding:5px;">
        {{false?'SI':'NO'}}
    </td>
    <th style="padding:5px;">Asimilamiento Grado Policial</th>
    <td style="padding:5px;"></td>
</tr>
@endif
