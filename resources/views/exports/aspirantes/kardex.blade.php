<table style="border-collapse:collapse;width:100%;">
<thead>
        <tr>
            <td colspan="4">
                <table style="border-collapse:collapse;width:100%;">
                    <tr>
                       <th style="padding:0 5px 10px 5px;font-size:18px;width:33%;">S P C P</th>
                        <th style="padding:0 5px 10px 5px;text-align:center;width:33%;">{{Auth::user()->municipio->nombre}}</th>
                        <th style="padding:0 5px 10px 5px;text-align:right;width:33%;" >{{Carbon\Carbon::now()->format("d-m-Y")}}</th>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height:30px;">
            <th colspan="4" style="text-align:center;font-size:18px;color:#FFF;background-color:#405467;letter-spacing:2px;height:30px;">Kardex</th>
        </tr>
    </thead>
    <tbody>
            @include('exports.aspirantes.kardex.personales')
            @include('exports.aspirantes.kardex.domicilio')
            @include('exports.aspirantes.kardex.familiares')
            @include('exports.aspirantes.kardex.reclutamiento')
            @include('exports.aspirantes.kardex.seleccion')
            @include('exports.aspirantes.kardex.formacion')
            @include('exports.aspirantes.kardex.certificacion')
            @include('exports.aspirantes.kardex.reingreso')
            @include('exports.aspirantes.kardex.separacion')
            @include('exports.aspirantes.kardex.evaluaciones')
    </tbody>
</table>
