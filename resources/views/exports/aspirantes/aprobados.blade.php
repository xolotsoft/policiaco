<table style="border-collapse:collapse;width:100%;">
    <thead>
        <tr>
            <td colspan="3">
                <table style="border-collapse:collapse;width:100%;">
                    <tr>
                       <th style="padding:0 5px 10px 5px;font-size:18px;width:33%;">S P C P</th>
                        <th style="padding:0 5px 10px 5px;text-align:center;width:33%;">{{Auth::user()->municipio->nombre}}</th>
                        <th style="padding:0 5px 10px 5px;text-align:right;width:33%;" >{{Carbon\Carbon::now()->format("d-m-Y")}}</th>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height:30px;">
            <th colspan="3" style="text-align:center;font-size:18px;color:#FFF;background-color:#405467;letter-spacing:2px;height:30px;">Aspirantes Aprobados</th>
        </tr>
        <tr>
            <th style="padding:5px;border-bottom:1px solid #405467;">Folio</th>
            <th style="padding:5px;border-bottom:1px solid #405467;">RFC </th>
            <th style="padding:5px;border-bottom:1px solid #405467;">Nombre </th>
        </tr>
    </thead>
    <tbody>
        @foreach($aspirantes as $aspirante)
            @if($aspirante->Total > 4)
            <tr>
                <td style="padding:5px;">{{ $aspirante->folio }}</td>
                <td style="padding:5px;">{{ $aspirante->rfc }}</td>
                <td style="padding:5px;">{{ $aspirante->aPaterno}} {{$aspirante->aMaterno}} {{$aspirante->nombre}}</td>
            </tr>
            @endif
        @endforeach
    </tbody>
</table>
