<div class="compose col-md-6 col-xs-12">
    <div class="compose-header">
        Nuevo Mensaje
        <button type="button" class="close compose-close">
            <span>×</span>
        </button>
    </div>
    <br>
    <div class="compose-body">
            <form class="form-horizontal form-label-left" id="nuevo-mensaje">
                @include('formItems.select', [
                    'name' => 'destinatario',
                    'label' => 'Destinatario',
                    'content' => $destinatarios,
                    'place' => 'Seleciona un destinatario',
                    'req'   => true
                ])
                @include('formItems.text', [
                    'name' => 'asunto',
                    'label' => 'Asunto',
                    'tooltip' => null,
                    'req'   => true
                ])
                @include('formItems.area', [
                    'name' => 'mensaje',
                    'label' => 'Mensaje',
                    'tooltip' => null,
                    'req'   => true
                ])
                <button class="btn btn-success pull-right" type="submit">Enviar <i class="fa fa-envelope-o"></i></button>
            {{ Form::close() }}
    </div>
    <div class="compose-footer"></div>
</div>