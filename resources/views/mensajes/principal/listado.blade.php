@foreach($mensajes as $mensaje)
    <a href="#" onClick="mostrarMensaje({{$mensaje->id}})">
        <div class="mail_list">
        <div class="left">
            @if($mensaje->estado == 0)
            <i class="fa fa-circle"></i>
            @else
            <i class="fa fa-circle-o"></i>
            @endif
        </div>
        <div class="right">
            <h3>{{$mensaje->envia->username}} <small>{{\Carbon\Carbon::parse($mensaje->created_at)->format('g:i A')}}</small></h3>
            <p>{{$mensaje->asunto}}</p>
        </div>
        </div>
    </a>
@endforeach
