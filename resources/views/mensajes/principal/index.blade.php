<div class="row">
    <div class="col-sm-3 mail_list_column">
    <button id="compose" class="btn btn-sm btn-success btn-block" type="button">REDACTAR</button>
    <div id="listadoMensajes">
        @include('mensajes.principal.listado')
    </div>
</div>
<div id="contenidoMensajes">
    @include('mensajes.principal.contenido')
</div>
@include('mensajes.principal.nuevo')