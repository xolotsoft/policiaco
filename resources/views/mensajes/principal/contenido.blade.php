@if(isset($mensaje))
    <div class="col-sm-9 mail_view">
        <div class="inbox-body">
            <div class="mail_heading row">
            <div class="col-md-8">
                <div class="btn-group">
                <button onClick="responder({{$mensaje->envia->id}})" class="btn btn-sm btn-primary" type="button"><i class="fa fa-reply"></i> Responder</button>
                <button onClick="eliminarMensaje({{$mensaje->id}})" class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash"><i class="fa fa-trash-o"></i></button>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <p class="date">{{\Carbon\Carbon::parse($mensaje->created_at)->format('Y-m-d g:i A')}}</p>
            </div>
            <div class="col-xs-12">
                <h4>{{$mensaje->asunto}}</h4>
            </div>
            </div>
            <div class="sender-info">
            <div class="row">
                <div class="col-md-12">
                 De: <strong>{{$mensaje->envia->name}}</strong>
                </div>
            </div>
            </div>
            <div class="view-mail">
            <p>{{$mensaje->mensaje}}</p>
            </div>
        </div>
    </div>
@else
    <div class="col-sm-9 mail_view" style="text-align:center;">
        <h2>SELECCIONA UN MENSAJE</h2>
    </div>
@endif