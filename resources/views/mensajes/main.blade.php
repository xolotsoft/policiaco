<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-server"></i> {{ ucfirst($action) }}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="contenido">
                @include('mensajes.'.$action.'.index')
            </div>
        </div>
    </div>
</div>
