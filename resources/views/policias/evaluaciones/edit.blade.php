{{Form::model($evaluacion, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'editarcontrol'])}}
    @include('formItems.hide',[
        'name' => 'id'
    ])
    @include('policias.evaluaciones.form')
    @include('formItems.submit', ['label' => 'Editar'])
{{ Form::close() }}
