<div id="listadoEvaluaciones">
    @include('policias.evaluaciones.listado')
</div>
<div id="formcontrol">
{{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true, 'id'=>'evaluaciones'])}}
    @include('policias.evaluaciones.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}
</div>
