<div class="table-responsive">
    <table class="table table-striped jambo_table">
        <thead>
            <tr class="headings">
                <th class="column-title">Tipo</th>
                <th class="column-title">Capacitación</th>
                <th class="column-title">Duracion</th>
                <th class="column-title">Obligatoria</th>
                <th class="column-title">Resultado</th>
                <th class="column-title no-link last"><span class="nobr">Acción</span></th>
            </tr>
        </thead>
        <tbody>
            @foreach($planes as $plan)
            <tr class="even pointer">
                <td class=" ">{{$plan->planes->Name}}</td>
                <td class=" ">{{$plan->tipo->EvaluationName}}</td>
                <td class=" ">{{$plan->duracion}} Hrs.</td>
                <td class=" ">@include('components.status', ['param' => $plan->obligatorio])</td>
                <td class=" ">{{$plan->apto->nombre}}</td>
                <td class=" last">
                    {{--  <a class="btn btn-round btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Mostrar"><i class="fa fa-eye" aria-hidden="true" onclick="showplan({{$plan->id}});"></i></a>  --}}
                    <a class="btn btn-round btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil" aria-hidden="true" onclick="editplan({{$plan->id}});"></i></a>
                    @if(Auth::user()->perfil_id == 1 || Auth::user()->perfil_id == 2)
                    <a class="btn btn-round btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times" aria-hidden="true" onclick="destroyplan({{$plan->id}});"></i></a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
