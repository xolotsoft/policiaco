@include('formItems.select', [
    'name' => 'nombre',
    'label' => 'Tipo',
    'content' => $nombres,
    'req'   => true,
    'place' => 'Seleciona una opción'
])
@include('formItems.select', [
    'name'    => 'caracteristicas',
    'label'   => 'Tipo de capacitación',
    'content' => $caracteristicas,
    'place'   => 'Seleciona una opción',
    'req'     => true,
    'dis'     => !$editable
])
@include('formItems.select', [
    'name' => 'instancia',
    'label' => 'Instancia capacitadora',
    'place'   => 'Selecciona una opción',
    'content' => $instancias
])
@include('formItems.select', [
    'name'    => 'gradoImportancia',
    'label'   => 'Grado de importancia',
    'content' => [
        'Alta'=>'Alta',
        'Media'=>'Media',
        'Baja'=>'Baja'
    ],
    'place'   => 'Seleciona una opción',
    'req'     => false
])
@include('formItems.text', [
    'name' => 'duracion',
    'label' => 'Duración',
    'place' => '32',
    'req'   => true,
    'tooltip' => 'Ingrese la duración'
])
@include('formItems.check', [
    'name' => 'obligatorio',
    'label' => 'Obligatorio'
])
@include('formItems.date', [
    'name' => 'fechaInicio',
    'label' => 'Fecha de inicio',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.date', [
    'name' => 'fechaTermino',
    'label' => 'Fecha de Termino',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.file', [
    'name' => 'documento',
    'label' => 'Documento',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.select', [
    'name' => 'resultado',
    'label' => 'Resultado',
    'content' => $apto,
    'req'   => true,
    'place' => 'Seleciona una opción'
])
@include('formItems.area', [
    'name' => 'comentarios',
    'label' => 'Comentarios',
    'req'   => false,
    'tooltip' => false
])
