<div id="listadoPlan">
    @include('policias.plan.listado')
</div>
<div id="formplanes">
    {{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true, 'id'=>'planIndividual'])}}
        @include('policias.plan.form')
        @include('formItems.submit', ['label' => 'Guardar'])
    {{ Form::close() }}
</div>
