{{Form::model($plan, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'editarplanes'])}}
    @include('formItems.hide',[
        'name' => 'id'
    ])
    @include('policias.plan.form')
    @include('formItems.submit', ['label' => 'Editar'])
{{ Form::close() }}
