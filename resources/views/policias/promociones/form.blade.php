@include('formItems.select', [
    'name' => 'cargo',
    'label' => 'Categoría Policial',
    'place' => 'Seleciona una opción',
    'content' => $categoriaPolicial,
    'req'     => true,
])
@include('formItems.select', [
    'name'    => 'rango',
    'label'   => 'Grado Policial',
    'content' => $puestos,
    'place'   => 'Seleciona una opción',
    'req'     => true,
    'dis'     => !$editable
])
@include('formItems.date', [
    'name' => 'fechaPromocion',
    'label' => 'Fecha de promoción',
    'place' => '',
    'req'   => true,
    'tooltip' => ''
])
@include('formItems.area', [
    'name' => 'observacionesPromociones',
    'label' => 'Observaciones',
    'req'   => false,
    'tooltip' => ''
])
