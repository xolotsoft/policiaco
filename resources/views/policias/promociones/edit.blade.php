{{Form::model($promocion, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'editarpromociones'])}}
    @include('formItems.hide',[
        'name' => 'id'
    ])
    @include('policias.promociones.form')
    @include('formItems.submit', ['label' => 'Editar'])
{{ Form::close() }}
