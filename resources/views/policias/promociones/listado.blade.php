<div class="table-responsive">
    <table class="table table-striped jambo_table">
        <thead>
            <tr class="headings">
                <th class="column-title">Categoría </th>
                <th class="column-title">Grado </th>
                <th class="column-title">Nivel de mando </th>
                <th class="column-title">Especial </th>
                <th class="column-title">Fecha </th>
                <th class="column-title no-link last"><span class="nobr">Acción</span></th>
            </tr>
        </thead>
        <tbody>
            @foreach($promociones as $promocion)
            <tr class="even pointer">
                <td class=" ">{{$promocion->nombre}}</td>
                <td class=" ">{{$promocion->Rank}}</td>
                <td class=" ">{{$promocion->CommandLevel}}</td>
                <td class=" ">@include('components.status', ['param' => $promocion->Special])</td>
                <td class=" ">{{$promocion->fechaPromocion}}</td>
                <td class=" last">
                    {{--  <a class="btn btn-round btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Mostrar"><i class="fa fa-eye" aria-hidden="true" onclick="showpromocion({{$promocion->id}});"></i></a>  --}}
                    <a class="btn btn-round btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil" aria-hidden="true" onclick="editpromocion({{$promocion->id}});"></i></a>
                    @if(Auth::user()->perfil_id == 1 || Auth::user()->perfil_id == 2)
                    <a class="btn btn-round btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times" aria-hidden="true" onclick="destroypromocion({{$promocion->id}});"></i></a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
