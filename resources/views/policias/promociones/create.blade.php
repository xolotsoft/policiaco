<div id="listadoPromociones">
    @include('policias.promociones.listado')
</div>
<div id="formpromociones">
{{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true, 'id'=>'promociones'])}}
    @include('policias.promociones.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}
</div>
