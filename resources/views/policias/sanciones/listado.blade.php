<div class="table-responsive">
    <table class="table table-striped jambo_table">
        <thead>
            <tr class="headings">
                <th class="column-title">Fecha </th>
                <th class="column-title">Causa </th>
                <th class="column-title">Observaciones </th>
                <th class="column-title">Tipo </th>
                <th class="column-title">Duración </th>
                <th class="column-title no-link last"><span class="nobr">Acción</span></th>
            </tr>
        </thead>
        <tbody>
            @foreach($sanciones as $sancion)
            <tr class="even pointer">
                <td class=" ">{{$sancion->fechaSanciones}}</td>
                <td class=" ">{{$sancion->causa}}</td>
                <td class=" ">{{$sancion->observacionesSanciones}}</td>
                <td class=" ">{{$sancion->catalogos->nombre}}</td>
                <td class=" ">{{$sancion->duracion}}</td>
                <td class=" last">
                    {{--  <a class="btn btn-round btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Mostrar"><i class="fa fa-eye" aria-hidden="true" onclick="showsancion({{$sancion->id}});"></i></a>  --}}
                    <a class="btn btn-round btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil" aria-hidden="true" onclick="editsancion({{$sancion->id}});"></i></a>
                    @if(Auth::user()->perfil_id < 3)
                    <a class="btn btn-round btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times" aria-hidden="true" onclick="destroysancion({{$sancion->id}});"></i></a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
