{{Form::model($sancion, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'editarsanciones'])}}
    @include('formItems.hide',[
        'name' => 'id'
    ])
    @include('policias.sanciones.form')
    @include('formItems.submit', ['label' => 'Editar'])
{{ Form::close() }}
