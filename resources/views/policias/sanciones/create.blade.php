<div id="listadoSanciones">
    @include('policias.sanciones.listado')
</div>
<div id="formsanciones">
{{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true, 'id'=>'sanciones'])}}
    @include('policias.sanciones.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}
</div>
