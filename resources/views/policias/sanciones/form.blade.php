@include('formItems.select', [
    'name' => 'tipoSanciones',
    'label' => 'Tipo',
    'content' => $tipos,
    'req'   => true,
    'place' => 'Selecciona una opción'
])
<div id="fechasSanciones" style="display:none;">
@include('formItems.date', [
    'name' => 'fechaInicio',
    'label' => 'Fecha de inicio',
    'place' => '',
    'req'   => true,
    'tooltip' => 'Ingrese la fecha'
])
@include('formItems.date', [
    'name' => 'fechaConclusion',
    'label' => 'Fecha de Conclusión',
    'place' => '',
    'req'   => true,
    'tooltip' => 'Ingrese la fecha'
])
</div>
@include('formItems.text', [
    'name'  => 'duracion',
    'label' => 'Duración',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.text', [
    'name' => 'causa',
    'label' => 'Causa',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.date', [
    'name' => 'fechaSanciones',
    'label' => 'Fecha',
    'place' => '',
    'tooltip' => false
])
@include('formItems.area', [
    'name' => 'observacionesSanciones',
    'label' => 'Observaciones',
    'place' => '',
    'req'   => false,
    'tooltip' => false
])
