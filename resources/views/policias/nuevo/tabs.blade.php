<ul id="myTab" class="nav nav-tabs nav-pills" role="tablist">
    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Datos Personales</a>
    </li>
    <li role="presentation" class="disabled"><a href="#tab_content2" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Domicilio</a>
    </li>
    <li role="presentation" class="disabled"><a href="#tab_content3" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Familiares</a>
    </li>
    @if($editable)
    <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab4" data-toggle="tab" aria-expanded="false">Datos de Actividad</a>
    </li>
    <li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab5" data-toggle="tab" aria-expanded="false">Promociones</a>
    </li>
    <li role="presentation" class=""><a href="#tab_content6" role="tab" id="profile-tab6" data-toggle="tab" aria-expanded="false">Herramientas</a>
    </li>
    <li role="presentation" class=""><a href="#tab_content7" role="tab" id="profile-tab7" data-toggle="tab" aria-expanded="false">Sanciones</a>
    </li>
    <li role="presentation" class=""><a href="#tab_content8" role="tab" id="profile-tab8" data-toggle="tab" aria-expanded="false">Cambios de Adscripción</a>
    </li>
    <li role="presentation" class=""><a href="#tab_content9" role="tab" id="profile-tab9" data-toggle="tab" aria-expanded="false">Estímulos</a>
    </li>
    <li role="presentation" class=""><a href="#tab_content10" role="tab" id="profile-tab10" data-toggle="tab" aria-expanded="false">Plan individual de carrera</a>
    </li>
    <li role="presentation" class=""><a href="#tab_content11" role="tab" id="profile-tab11" data-toggle="tab" aria-expanded="false">Actividades Administrativas</a>
    </li>
    <li role="presentation" class=""><a href="#tab_content12" role="tab" id="profile-tab12" data-toggle="tab" aria-expanded="false">Separación del servicio</a>
    </li>
    <li role="presentation" class=""><a href="#tab_content13" role="tab" id="profile-tab13" data-toggle="tab" aria-expanded="false">Evaluaciones de control y confianza</a>
    </li>
    @endif
</ul>