<div class="nav-tabs-responsive" role="tabpanel" data-example-id="togglable-tabs">
    @include('policias.nuevo.tabs')
    <div id="myTabContent" class="tab-content">
        <br>
        @include('policias.personales.main')
        @include('policias.domicilio.main')
        @include('policias.familiares.main')
        @include('policias.actividad.main')
        @include('policias.promociones.main')
        @include('policias.herramientas.main')
        @include('policias.sanciones.main')
        @include('policias.adscripcion.main')
        @include('policias.estimulos.main')
        @include('policias.plan.main')
        @include('policias.administracion.main')
        @include('policias.separacion.main')
        @include('policias.evaluaciones.main')
    </div>
</div>
