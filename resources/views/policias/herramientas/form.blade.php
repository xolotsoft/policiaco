@include('formItems.text', [
    'name'  => 'descripcion',
    'label' => 'Descripción',
    'place' => '',
    'req'   => true,
    'tooltip' => ''
])
@include('formItems.select', [
    'name' => 'tipo',
    'label' => 'Tipo',
    'content' => $tipo,
    'req'   => true,
    'place' => 'Seleciona una opción'
])
@include('formItems.text', [
    'name' => 'cantidad',
    'label' => 'Cantidad',
    'place' => '',
    'req'   => true,
    'tooltip' => ''
])
@include('formItems.date', [
    'name' => 'fechaRecepcion',
    'label' => 'Fecha de recepción',
    'place' => '',
    'tooltip' => ''
])
@include('formItems.date', [
    'name' => 'fechaDevolucion',
    'label' => 'Fecha devolución',
    'place' => '',
    'tooltip' => ''
])
@include('formItems.text', [
    'name' => 'areaResguardo',
    'label' => 'Área de Resguardo',
    'place' => '',
    'req'   => false,
    'tooltip' => ''
])
@include('formItems.area', [
    'name' => 'observacionesHerramientas',
    'label' => 'Observaciones',
    'req'   => false,
    'tooltip' => ''
])