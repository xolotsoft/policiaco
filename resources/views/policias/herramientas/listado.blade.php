<div class="table-responsive">
    <table class="table table-striped jambo_table">
        <thead>
            <tr class="headings">
                <th class="column-title">Descripción </th>
                <th class="column-title">Tipo </th>
                <th class="column-title">Cantidad </th>
                <th class="column-title">Fecha </th>
                <th class="column-title no-link last"><span class="nobr">Acción</span></th>
            </tr>
        </thead>
        <tbody>
            @foreach($herramientas as $herramienta)
            <tr class="even pointer">
                <td class=" ">{{$herramienta->descripcion}}</td>
                <td class=" ">{{$herramienta->catalogos->nombre}}</td>
                <td class=" ">{{$herramienta->cantidad}}</td>
                <td class=" ">{{$herramienta->fechaRecepcion}}</td>
                <td class=" last">
                    {{--  <a class="btn btn-round btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Mostrar"><i class="fa fa-eye" aria-hidden="true" onclick="showherramienta({{$herramienta->id}});"></i></a>  --}}
                    <a class="btn btn-round btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil" aria-hidden="true" onclick="editherramienta({{$herramienta->id}});"></i></a>
                    @if(Auth::user()->perfil_id == 1 || Auth::user()->perfil_id == 2)
                    <a class="btn btn-round btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times" aria-hidden="true" onclick="destroyherramienta({{$herramienta->id}});"></i></a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>