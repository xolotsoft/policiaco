{{Form::model($herramienta, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'editarherramientas'])}}
    @include('formItems.hide',[
        'name' => 'id'
    ])
    @include('policias.herramientas.form')
    @include('formItems.submit', ['label' => 'Editar'])
{{ Form::close() }}
