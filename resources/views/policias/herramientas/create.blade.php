<div id="listadoHerramientas">
    @include('policias.herramientas.listado')
</div>
<div id="formherramientas">
    {{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true, 'id'=>'herramientas'])}}
        @include('policias.herramientas.form')
        @include('formItems.submit', ['label' => 'Guardar'])
    {{ Form::close() }}
</div>
