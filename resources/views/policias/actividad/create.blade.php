{{Form::model($actividad,['class' => 'form-horizontal form-label-left', 'files' => true, 'id'=>'datosActividad'])}}
    @include('policias.actividad.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}