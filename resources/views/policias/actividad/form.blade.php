@include('formItems.select', [
    'name' => 'cargo',
    'label' => 'Categoría Policial',
    'place' => 'Seleciona una opción',
    'content' => $categoriaPolicial,
])
@include('formItems.select', [
    'name'    => 'gradoPolicial',
    'label'   => 'Grado Policial',
    'content' => $rangos,
    'place'   => 'Seleciona una opción',
    'req'     => false,
    'dis' => !$editable
])
@include('formItems.date', [
    'name'  => 'fechaNombramiento',
    'label' => 'Fecha de nombramiento',
    'place' => '',
    'req'   => false
])
@include('formItems.text', [
    'name'    => 'puesto',
    'label'   => 'Puesto',
    'place'   => '',
    'tooltip' => ''
])
@include('formItems.text', [
    'name'    => 'funciones',
    'label'   => 'Funciones',
    'place'   => '',
    'tooltip' => ''
])
@include('formItems.text', [
    'name'    => 'areaAdscripcion',
    'label'   => 'Area de adscripción',
    'place'   => '',
    'tooltip' => ''
])
@include('formItems.text', [
    'name'    => 'numeroEmpleado',
    'label'   => 'Número de empleado',
    'place'   => '',
    'tooltip' => ''
])
@include('formItems.text', [
    'name'    => 'cuip',
    'label'   => 'CUIP',
    'place'   => '',
    'tooltip' => ''
])
@include('formItems.text', [
    'name'    => 'cup',
    'label'   => 'CUP',
    'place'   => '',
    'tooltip' => ''
])
@include('formItems.check', [
    'name' => 'respondiente',
    'label' => 'Primer Respondiente'
])
@include('formItems.file', [
    'name'  => 'certificadoNombramiento',
    'label' => 'Certificado de nombramiento',
    'req'   => false,
    'val' => isset($actividad->certificadoNombramiento)?$actividad->archivoCertificadoNombramiento:null
])
@include('formItems.file', [
    'name' => 'constanciaGrado',
    'label' => 'Constancia de grado',
    'req'   => false,
    'val' => isset($actividad->constanciaGrado)?$actividad->archivoConstanciaGrado:null

])
@include('formItems.file', [
    'name' => 'fotografiaUniforme',
    'label' => 'Fotografía con uniforme',
    'req'   => false,
    'val' => isset($actividad->fotografiaUniforme)?$actividad->archivoFotografiaUniforme:null

])
@include('formItems.file', [
    'name' => 'credencialPorteArma',
    'label' => 'Credencial de porte de arma',
    'req'   => false,
    'val' => isset($actividad->credencialPorteArma)?$actividad->archivoCredencialPorteArma:null

])
@include('formItems.area', [
    'name' => 'observacionesActividad',
    'label' => 'Observaciones',
    'req'   => false,
    'tooltip' => ''
])
<!-- @include('formItems.area', [
    'name' => 'observacionesPromocion',
    'label' => 'Observaciones de promoción',
    'req'   => false,
    'tooltip' => ''
]) -->
