<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                @if($action == 'organoColegiado')
                    <h2><i class="fa fa-server"></i> Órganos Colegiados</h2>
                @else
                    <h2><i class="fa fa-server"></i> {{ titulo($action) }}</h2>
                @endif
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        @if($action !== 'organoColegiado')
                            {{ HTML::image('img/persona.png', '', array('class' => 'fotografia')) }}
                        @else
                            <button id="btnAction" class="btn btn-success">Nuevo</button>
                        @endif
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="contenido">
                @include('policias.'.$action.'.index')
            </div>
        </div>
    </div>
</div>
