<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
    {{Form::model($policia, ['class' => 'form_wizard wizard_horizontal', 'files' => true, 'id' => 'wizard'])}}
            @include('policias.personales.pasos')
            @include('policias.personales.paso-1')
            @include('policias.personales.paso-2')
            @include('policias.personales.paso-3')
    {{ Form::close() }}
</div>
