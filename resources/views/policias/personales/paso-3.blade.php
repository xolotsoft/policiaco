<div id="step-3">
    <div class="form-horizontal form-label-left">
        @include('formItems.text', [
          'name' => 'telefono',
          'label' => 'Teléfono',
          'place' => '5521582312',
          'max' =>10,
          'req' => true,
          'tooltip' => 'Introduzca 10 digitos de su numero telefonico fijo'
        ])
        @include('formItems.text', [
          'name' => 'movil',
          'label' => 'Movil',
          'place' => '7181234567',
          'max' =>10,
          'tooltip' => 'Los teléfonos del Estado de México tienen 10 dígitos formados de la suma de su clave local de 2 o 3 dígitos y después el número local formado por 7 u 8 dígitos.  En total 10 dígitos.
                        Por ejemplo 7181234567'
        ])
        @include('formItems.text', [
          'name' => 'email',
          'label' => 'eMail',
          'place' => 'info@gmail.com',
          'tooltip' => 'Introducir su correo electronico por ejemplo abcdr@jrmks.com o abcdr@jrmks.com.mx segun sea el caso'
        ])
        @include('formItems.file', [
          'name' => 'huella',
          'label' => 'Huellas dactilares',
          'req' => true,
          'tooltip' => 'Seleccione el archivo correspondiente por ejemplo imagen.jpg,imagen.png,imagen.bmp,imagen.pdf',
          'val' => isset($policia->huella)?$policia->archivoHuella:null
        ])
        @include('formItems.file', [
            'name' => 'elector',
            'label' => 'Credencial de elector',
            'req'   => true,
            'val' => isset($reclutamiento->elector)?$reclutamiento->archivoElector:null
        ])
        @include('formItems.file', [
            'name' => 'comprobanteDomicilio',
            'label' => 'Comprobante de domicilio',
            'req'   => true,
            'val' => isset($reclutamiento->comprobanteDomicilio)?$reclutamiento->archivoComprobanteDomicilio:null
        ])
        @include('formItems.file', [
            'name' => 'fotoInfantil',
            'label' => 'Fotografía',
            'req'   => true,
            'val' => isset($reclutamiento->fotoInfantil)?$reclutamiento->archivoFotoInfantil:null
        ])
        @include('formItems.area', [
          'name' => 'observaciones',
          'label' => 'Observaciones',
          'place' => 'Introduzca cualquier observación sobresaliente '
        ])
        @include('formItems.submit', [
          'label' => 'Guardar'
        ])
    </div>
</div>
