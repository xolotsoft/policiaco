<div class="conteiner">
    <div class ='form-horizontal form-label-left'>
        <div class="form-group ">
            {{ Form::label('sesion', 'Sesión', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                    <p class="control-label col-xs-3">
                        {{$colegiado->sesion}}
                    </p>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('fechasesion', 'Fecha de sesión', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-3">
                    {{$colegiado->fechasesion}}
                </p>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('temas', 'Temas', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-3">
                    {{$colegiado->temas}}
                </p>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('acuerdos', 'Acuerdos', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-6">
                    <span>{{$colegiado->archivoAcuerdos->name}} </span>
                    <a href="{{ url('archivos/ver/'.$colegiado->acuerdos) }}" target="_blank">[ ver ]</a>
                    <a href="{{ url('archivos/descargar/'.$colegiado->acuerdos) }}" target="_blank">[ descargar ]</a>
                </p>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('resoluciones', 'Resoluciones', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-6">
                    <span>{{$colegiado->archivoResoluciones->name}} </span>
                    <a href="{{ url('archivos/ver/'.$colegiado->resoluciones) }}" target="_blank">[ ver ]</a>
                    <a href="{{ url('archivos/descargar/'.$colegiado->resoluciones) }}" target="_blank">[ descargar ]</a>
                </p>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('actaSesion', 'Acta de sesión', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-6">
                    <span>{{$colegiado->archivoActaSesion->name}} </span>
                    <a href="{{ url('archivos/ver/'.$colegiado->actaSesion) }}" target="_blank">[ ver ]</a>
                    <a href="{{ url('archivos/descargar/'.$colegiado->actaSesion) }}" target="_blank">[ descargar ]</a>
                </p>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('observaciones', 'Observaciones', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-3">
                    {{$colegiado->observaciones}}
                </p>
            </div>
        </div>
    </div>
</div>
