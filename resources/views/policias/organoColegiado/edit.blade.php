{{Form::model($colegiado, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'editar'])}}
    @include('policias.organoColegiado.form')
    @include('formItems.submit', ['label' => 'Editar'])
{{ Form::close() }}
