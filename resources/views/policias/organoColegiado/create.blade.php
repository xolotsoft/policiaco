{{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true, 'id'=>'colegiado'])}}
    @include('policias.organoColegiado.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}
