<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Sesión</th>
                <th class="column-title">Fecha </th>
                <th class="column-title no-link last"><span class="nobr">Acción</span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($organos as $organo)
            <tr class="even pointer">
                <td class=" ">{{$organo->sesion}}</td>
                <td class=" ">{{$organo->fechasesion}}</td>
                @include('components.actions', ['param' => $organo])
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
