@include('formItems.hide',[
    'name' => 'editable',
    'val' => $editable
])
@include('formItems.select', [
    'name'    => 'comision',
    'label'   => 'Comisión del Servicio Profesional de Carrera Policial Honor y Justicia',
    'content' => $comisiones,
    'req'     => true,
    'place'   => 'Selecciona una opción',
    'add'     => true
])
@include('formItems.text', [
    'name'  => 'integrante',
    'label' => 'Integrante/Puesto',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.text', [
    'name'  => 'sesion',
    'label' => 'Sesión',
    'place' => '',
    'req'   => true,
    'tooltip' => 'Ingresar la sesión'
])
@include('formItems.date', [
    'name' => 'fechasesion',
    'label' => 'Fecha de sesión',
    'place' => '',
    'req'   => true,
    'tooltip' => 'Ingresar la fecha de sesión'
])
@include('formItems.area', [
    'name' => 'temas',
    'label' => 'Temas',
    'place' => '',
    'req'   => true,
    'tooltip' => 'Ingresa los temas de la sesión'
])
@include('formItems.file', [
    'name' => 'acuerdos',
    'label' => 'Acuerdos',
    'req'   => true,
    'tooltip' => false,
    'val' => isset($colegiado->acuerdos)?$colegiado->archivoAcuerdos:null
])
@include('formItems.file', [
    'name' => 'resoluciones',
    'label' => 'Resoluciones',
    'req'   => true,
    'tooltip' => false,
    'val' => isset($colegiado->resoluciones)?$colegiado->archivoResoluciones:null
])
@include('formItems.file', [
    'name' => 'actaSesion',
    'label' => 'Acta de la sesión',
    'req'   => true,
    'tooltip' => false,
    'val' => isset($colegiado->actaSesion)?$colegiado->archivoActaSesion:null
])
@include('formItems.area', [
    'name' => 'observaciones',
    'label' => 'Observaciones',
    'req'   => false,
    'tooltip' => ''
])
