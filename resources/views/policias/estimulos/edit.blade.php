{{Form::model($estimulo, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'editarestimulos'])}}
    @include('formItems.hide',[
        'name' => 'id'
    ])
    @include('policias.estimulos.form')
    @include('formItems.submit', ['label' => 'Editar'])
{{ Form::close() }}
