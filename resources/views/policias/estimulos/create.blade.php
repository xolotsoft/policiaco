<div id="listadoEstimulos">
    @include('policias.estimulos.listado')
</div>
<div id="formestimulos">
{{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true, 'id'=>'estimulos'])}}
    @include('policias.estimulos.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}
</div>
