@include('formItems.select', [
    'name' => 'tipoRecompensa',
    'label' => 'Tipo',
    'content' => $tipos,
    'req'   => true,
    'place' => 'Selecciona una opción'
])
@include('formItems.text', [
    'name' => 'motivo',
    'label' => 'Motivo',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.text', [
    'name' => 'premio',
    'label' => 'Premio',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.date', [
    'name'  => 'fechaEmision',
    'label' => 'Fecha',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.area', [
    'name' => 'observacionesEstimulos',
    'label' => 'Observaciones',
    'place' => '',
    'req'   => false,
    'tooltip' => false
])