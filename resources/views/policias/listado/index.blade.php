<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Empleado</th>
                <th class="column-title">RFC </th>
                <th class="column-title">Nombre </th>
                <th class="column-title no-link last"><span class="nobr">Acción</span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($policias as $policia)
            <tr class="even pointer">
                <td class=" ">{{ $policia->id }}</td>
                <td class=" ">{{ $policia->persona->rfc }}</td>
                <td class=" ">{{ $policia->persona->aPaterno }} {{ $policia->persona->aMaterno }} {{ $policia->persona->nombre }}</td>
                <td class=" last">
                    @if(Auth::user()->perfil_id == 1)
                    <a class="btn btn-round btn-danger btn-xs pull-right" title="Eliminar" onclick="destroy({{$policia->id}});">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a>
                    @endif
                    <a class="btn btn-round btn-warning btn-xs pull-right" title="Editar" onclick="edit({{$policia->idPersona}});">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>