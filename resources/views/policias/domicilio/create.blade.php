{{Form::model($domicilio, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id'=>'domicilio'])}}
    @include('policias.domicilio.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}