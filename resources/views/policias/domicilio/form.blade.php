@include('formItems.hide',[
    'name' => 'editable',
    'val' => $editable
])
@include('formItems.text', [
    'name'  => 'calle',
    'label' => 'Calle',
    'place' => 'Siempre viva',
    'req'   => true,
    'tooltip' => 'Ingresar nombre de calle donde vive actualmente'
])
@include('formItems.text', [
    'name' => 'exterior',
    'label' => 'Número Exterior',
    'place' => '59',
    'req'   => true,
    'tooltip' => 'Ingresar numero exterior donde habita'
])
@include('formItems.text', [
    'name' => 'interior',
    'label' => 'Número Interior',
    'place' => '2',
    'tooltip' => 'Ingresar numero interior donde habita'
])
@include('formItems.select', [
    'name' => 'pais',
    'label' => 'País',
    'content' => $paises,
    'req'   => true,
    'place' => 'Seleciona una opción'
])
@include('formItems.select', [
    'name' => 'estado',
    'label' => 'Estado',
    'content' => $estados,
    'place' => 'Seleciona una opción',
    'req'   => true
])
@include('formItems.select', [
    'name' => 'municipio',
    'label' => 'Municipio',
    'content' => [],
    'dis' => true,
    'req'   => true,
    'place' => 'Seleciona una opción'
])
@include('formItems.select', [
    'name' => 'colonia',
    'label' => 'Colonia',
    'content' => [],
    'dis' => true,
    'req'   => true,
    'place' => 'Seleciona una opción'
])
@include('formItems.text', [
    'name' => 'cp',
    'label' => 'Código Postal',
    'place' => '05000',
    'req'   => true,
    'tooltip' => 'Ingrese el codigo postal donde habita'
])
