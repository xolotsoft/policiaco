{{Form::model($separaciones, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id'=>'separacionform'])}}
    @include('policias.separacion.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}
