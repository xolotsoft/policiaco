@include('formItems.hide',[
    'name' => 'editable',
    'val' => $editable
])
@include('formItems.select', [
    'name' => 'separacion',
    'label' => 'Separación',
    'content' => ['1'=>'Ordinaria','2'=>'Extraordinaria'],
    'req'   => true,
    'place' => 'Seleciona una opción'
])
@include('formItems.select', [
    'name' => 'tipoSeparacion',
    'label' => 'Tipo de separación',
    'content' => $separacion,
    'req'   => true,
    'dis' => true,
    'place' => 'Seleciona una opción'
])
@include('formItems.date', [
    'name' => 'fechaSeparacion',
    'label' => 'Fecha de separación',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.text', [
    'name' => 'motivo',
    'label' => 'Motivo',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.file', [
    'name' => 'certificado',
    'label' => 'Documento',
    'req'   => true,
    'tooltip' => false,
    'val' => isset($separaciones->certificado)?$separaciones->archivoCertificado:null
])
@include('formItems.text', [
    'name' => 'aprobado',
    'label' => 'Aprobado por',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
