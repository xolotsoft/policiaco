<div class="table-responsive">
    <table class="table table-striped jambo_table">
        <thead>
            <tr class="headings">
                <!-- <th class="column-title">Tipo</th> -->
                <th class="column-title">Fecha inicio</th>
                <th class="column-title">Fecha fin</th>
                <th class="column-title">Descripción</th>
                <th class="column-title no-link last"><span class="nobr">Acción</span></th>
            </tr>
        </thead>
        <tbody>
            @foreach($administraciones as $administracion)
            <tr class="even pointer">
                <!-- <td class=" "></td> -->
                <td class=" ">{{$administracion->fechaInicio}}</td>
                <td class=" ">{{$administracion->fechaTermino}}</td>
                <td class=" ">{{$administracion->descripcion}}</td>
                <td class=" last">
                    {{--  <a class="btn btn-round btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Mostrar"><i class="fa fa-eye" aria-hidden="true" onclick="showadministra({{$administracion->id}});"></i></a>  --}}
                    <a class="btn btn-round btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil" aria-hidden="true" onclick="editadministrativa({{$administracion->id}});"></i></a>
                    @if(Auth::user()->perfil_id == 1 || Auth::user()->perfil_id == 2)
                    <a class="btn btn-round btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times" aria-hidden="true" onclick="destroyadministrativa({{$administracion->id}});"></i></a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
