<div id="listadoAdministracion">
    @include('policias.administracion.listado')
</div>
<div id="formadministrativa">
{{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true, 'id'=>'administracion'])}}
    @include('policias.administracion.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}
</div>
