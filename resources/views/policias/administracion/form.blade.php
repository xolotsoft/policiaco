@include('formItems.text', [
    'name' => 'descripcion',
    'label' => 'Descripción',
    'req'   => true,
    'place' => ''
    
])
@include('formItems.date', [
    'name' => 'fechaInicio',
    'label' => 'Fecha de inicio',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.date', [
    'name' => 'fechaTermino',
    'label' => 'Fecha de Termino',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.file', [
    'name' => 'documento',
    'label' => 'Documento',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.check', [
    'name' => 'asignado',
    'label' => 'Asignado'
])
@include('formItems.check', [
    'name' => 'realizado',
    'label' => 'Realizado'
])
@include('formItems.check', [
    'name' => 'comprobado',
    'label' => 'Comprobado'
])
@include('formItems.check', [
    'name' => 'validado',
    'label' => 'Validado'
])
@include('formItems.text', [
    'name' => 'autorizante',
    'label' => 'Quien autoriza',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.text', [
    'name' => 'requerimientos',
    'label' => 'Requerimientos',
    'req'   => false,
    'place' => ''
    
])
@include('formItems.area', [
    'name' => 'observaciones',
    'label' => 'Observaciones',
    'req'   => false,
    'tooltip' => false
])
