{{Form::model($administracion, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'editaradministrativa'])}}
    @include('formItems.hide',[
        'name' => 'id'
    ])
    @include('policias.administracion.form')
    @include('formItems.submit', ['label' => 'Editar'])
{{ Form::close() }}
