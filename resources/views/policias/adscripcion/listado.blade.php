<div class="table-responsive">
    <table class="table table-striped jambo_table">
        <thead>
            <tr class="headings">
                <th class="column-title">Tareas</th>
                <th class="column-title">Fecha inicio</th>
                <th class="column-title">Unidad Administrativa</th>
                <th class="column-title">Razón del cambio</th>
                <th class="column-title no-link last"><span class="nobr">Acción</span></th>
            </tr>
        </thead>
        <tbody>
            @foreach($adscripciones as $adscripcion)
            <tr class="even pointer">
                <td class=" ">{{$adscripcion->servicioRealizar}}</td>
                <td class=" ">{{$adscripcion->fechaInicio}}</td>
                <td class=" ">{{$adscripcion->areaProcedencia}}</td>
                <td class=" ">{{$adscripcion->razon}}</td>
                <td class=" last">
                    {{--  <a class="btn btn-round btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Mostrar"><i class="fa fa-eye" aria-hidden="true" onclick="showadscripcion({{$adscripcion->id}});"></i></a>  --}}
                    <a class="btn btn-round btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil" aria-hidden="true" onclick="editadscripcion({{$adscripcion->id}});"></i></a>
                    @if(Auth::user()->perfil_id == 1 || Auth::user()->perfil_id == 2)
                    <a class="btn btn-round btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times" aria-hidden="true" onclick="destroyadscripcion({{$adscripcion->id}});"></i></a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
