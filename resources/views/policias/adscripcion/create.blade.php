<div id="listadoAdscripcion">
    @include('policias.adscripcion.listado')
</div>
<div id="formadscripcion">
{{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true, 'id'=>'adscripcion'])}}
    @include('policias.adscripcion.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}
</div>
