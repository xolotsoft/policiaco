@include('formItems.text', [
    'name'  => 'areaProcedencia',
    'label' => 'Área de procedencia',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.text', [
    'name'  => 'areaDesignacion',
    'label' => 'Área de designación',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.text', [
    'name'  => 'servicioRealizaba',
    'label' => 'Servicio que realizaba',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.text', [
    'name'  => 'servicioRealizar',
    'label' => 'Servicios que va a realizar',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.date', [
    'name'  => 'fechaFin',
    'label' => 'Fecha de fin de funciones que realizaba',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.date', [
    'name'  => 'fechaInicio',
    'label' => 'Fecha de inicio de funciones  que va a realizar',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.text', [
    'name'  => 'razon',
    'label' => 'Razón del cambio',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.area', [
    'name' => 'observacionesAdscripcion',
    'label' => 'Observaciones',
    'place' => '',
    'req'   => false,
    'tooltip' => false
])
