<div id="listadoFamiliares">
    @include('policias.familiares.listado')
</div>
<div id="formFamiliares">
    {{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'familiares'])}}
        @include('policias.familiares.form')
        @include('formItems.submit', ['label' => 'Guardar'])
    {{ Form::close() }}
</div>
