@include('reportes.graficados.filtros.personales')
@include('reportes.graficados.filtros.actividad')
@include('reportes.graficados.filtros.promociones')
@include('reportes.graficados.filtros.herramientas')
@include('reportes.graficados.filtros.sanciones')
@include('reportes.graficados.filtros.adscripcion')
@include('reportes.graficados.filtros.estimulos')
@include('reportes.graficados.filtros.plan')
@include('reportes.graficados.filtros.controles')
@include('reportes.graficados.filtros.separacion')
<div class="col-md-6 col-md-offset-3 col-xs-12">
    <canvas id="myChart" width="400" height="400"></canvas>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
