<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-server"></i> {!! titulo($action) !!}</h2>
                <ul class="nav navbar-right panel_toolbox">
                    @if($action == 'graficadosActivos' || $action == 'graficadosInactivos')
                    <li id="filtrar">
                        <span class="fa-stack fa-lg" style="color:green;">
                            <i class="fa fa-square fa-stack-2x"></i>
                            <i class="fa fa-filter fa-stack-1x fa-inverse"></i>
                        </span>
                    </li>
                    <li id="contraerFiltros">
                        <span class="fa-stack fa-lg" style="color:orange;">
                            <i class="fa fa-square fa-stack-2x"></i>
                            <i class="fa fa-chevron-up fa-stack-1x fa-inverse"></i>
                        </span>
                    </li>
                    @else
                    @if($action == 'aspirantesKardex' || $action == 'aspirantesControlDeConfianza' || $action == 'policiasKardex' || $action == 'policiasControlDeConfianza')
                    <li id="regresar">
                        <span class="fa-stack fa-lg" style="color:orange;">
                            <i class="fa fa-square fa-stack-2x"></i>
                            <i class="fa fa-arrow-left fa-stack-1x fa-inverse"></i>
                        </span>
                    </li>
                    @endif
                    <li id="exportPDF" data-section="{{ $action }}">
                        <span class="fa-stack fa-lg" style="color:red;">
                            <i class="fa fa-square fa-stack-2x"></i>
                            <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span>
                    </li>
                    @endif
                <!--   Habiliar si es necesario descargar en excel
                 <li id="exportEXCEL" data-section="{{ $action }}">
                        <span class="fa-stack fa-lg" style="color:green;">
                            <i class="fa fa-square fa-stack-2x"></i>
                            <i class="fa fa-file-excel-o fa-stack-1x fa-inverse"></i>
                        </span>
                    </li>
                !-->
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="contenido">
                @include('reportes.'.$action)
            </div>
        </div>
    </div>
</div>
