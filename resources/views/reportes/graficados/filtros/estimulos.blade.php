<div class="col-md-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Estímulos</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content form-horizontal form-label-left" style="display: block;">
            @include('formItems.select', [
                'name' => 'tipoRecompensa',
                'label' => 'Tipo',
                'content' => $estimulos,
                'req'   => false,
                'place' => 'Selecciona una opción',
                'w' => 9
            ])
            @include('formItems.text', [
                'name' => 'motivoEstimulo',
                'label' => 'Motivo',
                'place' => '',
                'req'   => false,
                'tooltip' => false,
                'w' => 9
            ])
            @include('formItems.text', [
                'name' => 'premioEstimulo',
                'label' => 'Premio',
                'place' => '',
                'req'   => false,
                'tooltip' => false,
                'w' => 9
            ])
        </div>
    </div>
</div>
