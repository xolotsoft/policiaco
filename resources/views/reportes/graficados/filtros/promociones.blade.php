<div class="col-md-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Promociones</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content form-horizontal form-label-left" style="display: block;">
            @include('formItems.select', [
                'name' => 'cargoPromocion',
                'label' => 'Categoría Policial',
                'place' => 'Seleciona una opción',
                'content' => $categoriaPolicial,
                'w' => 9
            ])
            @include('formItems.select', [
                'name'    => 'gradoPolicialPromocion',
                'label'   => 'Grado Policial',
                'content' => $rangos,
                'place'   => 'Seleciona una opción',
                'req'     => false,
                'dis' => true,
                'w' => 9
            ])
        </div>
    </div>
</div>
