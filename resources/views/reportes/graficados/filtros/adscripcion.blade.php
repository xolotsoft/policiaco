<div class="col-md-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Cambios de Adscripción</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content form-horizontal form-label-left" style="display: block;">
            @include('formItems.text', [
                'name'  => 'areaProcedencia',
                'label' => 'Área de procedencia',
                'place' => '',
                'req'   => false,
                'tooltip' => false,
                'w' => 9
            ])
            @include('formItems.text', [
                'name'  => 'areaDesignacion',
                'label' => 'Área de designación',
                'place' => '',
                'req'   => false,
                'tooltip' => false,
                'w' => 9
            ])
            @include('formItems.date', [
                'name'  => 'fechaFinAdscripcion',
                'label' => 'Fecha de fin de funciones que realizaba',
                'req'   => false,
                'tooltip' => false,
                'w' => 9
            ])
            @include('formItems.date', [
                'name'  => 'fechaInicioAdscripcion',
                'label' => 'Fecha de inicio de funciones  que va a realizar',
                'req'   => false,
                'tooltip' => false,
                'w' => 9
            ])
        </div>
    </div>
</div>
