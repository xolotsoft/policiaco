<div class="col-md-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Controles de Confianza</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content form-horizontal form-label-left" style="display: block;">
            @include('formItems.select', [
                'name' => 'examenControl',
                'label' => 'Examen',
                'content' => $examenes,
                'req'   => false,
                'place' => 'Selecciona una opción',
                'w' => 9
            ])
            @include('formItems.date', [
                'name' => 'fechaResultadosControl',
                'label' => 'Fecha de entrega de resultados del examen',
                'place' => '',
                'req'   => false,
                'tooltip' => false,
                'w' => 9
            ])
            @include('formItems.select', [
                'name'  => 'resultadoControl',
                'label' => 'Resultado de examen',
                'content' => $resultados,
                'place' => 'Selecciona una opción',
                'req'   => false,
                'tooltip' => false,
                'w' => 9
            ])
        </div>
    </div>
</div>
