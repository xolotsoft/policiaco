<div class="col-md-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Separación del Servicio</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content form-horizontal form-label-left" style="display: block;">
            @include('formItems.select', [
                'name' => 'separacionCombo',
                'label' => 'Separación',
                'content' => ['1'=>'Ordinaria','2'=>'Extraordinaria'],
                'req'   => false,
                'place' => 'Seleciona una opción',
                'w' => 9
            ])
            @include('formItems.select', [
                'name' => 'tipoSeparacion',
                'label' => 'Tipo de separación',
                'content' => $separacion,
                'req'   => false,
                'dis' => true,
                'place' => 'Seleciona una opción',
                'w' => 9
            ])
            @include('formItems.date', [
                'name' => 'fechaSeparacion',
                'label' => 'Fecha de separación',
                'req'   => false,
                'tooltip' => false,
                'w' => 9
            ])
            @include('formItems.text', [
                'name' => 'motivoSeparacion',
                'label' => 'Motivo',
                'place' => '',
                'req'   => false,
                'tooltip' => false,
                'w' => 9
            ])
        </div>
    </div>
</div>
