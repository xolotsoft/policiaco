<div class="col-md-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Datos de Actividad</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content form-horizontal form-label-left" style="display: block;">
            @include('formItems.select', [
                'name' => 'cargoActividad',
                'label' => 'Categoría Policial',
                'place' => 'Seleciona una opción',
                'content' => $categoriaPolicial,
                'w' => 9
            ])
            @include('formItems.select', [
                'name'    => 'gradoPolicialActividad',
                'label'   => 'Grado Policial',
                'content' => $rangos,
                'place'   => 'Seleciona una opción',
                'req'     => false,
                'dis' => true,
                'w' => 9
            ])
            @include('formItems.text', [
                'name'    => 'puestoActividad',
                'label'   => 'Puesto',
                'place'   => '',
                'tooltip' => false,
                'w' => 9
            ])
            @include('formItems.text', [
                'name'    => 'areaAdscripcionActividad',
                'label'   => 'Area de adscripción',
                'place'   => '',
                'tooltip' => false,
                'w' => 9
            ])
        </div>
    </div>
</div>
