<div class="col-md-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Datos personales</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content form-horizontal form-label-left" style="display: block;">
            @include('formItems.text', [
                'name' => 'edad',
                'label' => 'Edad',
                'place' => '',
                'tooltip' => false,
                'w'=>9
            ])
            @include('formItems.select', [
                'name' => 'estadoCivil',
                'label' => 'Estado Civil',
                'content' => $civil,
                'place' => 'Seleciona una opción',
                'req' => false,
                'tooltip' => false,
                'w'=>9
            ])
            @include('formItems.select', [
                'name' => 'lugarNacimiento',
                'label' => 'Lugar de Nacimiento',
                'content' => $estados,
                'place' => 'Seleciona una opción',
                'req' => false,
                'tooltip' => false,
                'w'=>9
            ])
        </div>
    </div>
</div>
