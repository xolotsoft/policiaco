<div class="col-md-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Plan individual de carrera</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content form-horizontal form-label-left" style="display: block;">
            @include('formItems.select', [
                'name' => 'nombrePlan',
                'label' => 'Tipo',
                'content' => $nombres,
                'req'   => false,
                'place' => 'Seleciona una opción',
                'w' => 9
            ])
            @include('formItems.select', [
                'name'    => 'caracteristicasPlan',
                'label'   => 'Tipo de capacitación',
                'content' => $caracteristicas,
                'place'   => 'Seleciona una opción',
                'req'     => false,
                'dis'     => false,
                'w' => 9
            ])
            @include('formItems.select', [
                'name' => 'instanciaPlan',
                'label' => 'Instancia capacitadora',
                'place'   => 'Selecciona una opción',
                'content' => $instancias,
                'w' => 9
            ])
            @include('formItems.select', [
                'name'    => 'gradoImportanciaPlan',
                'label'   => 'Grado de importancia',
                'content' => [
                    'Alta'=>'Alta',
                    'Media'=>'Media',
                    'Baja'=>'Baja'
                ],
                'place'   => 'Seleciona una opción',
                'req'     => false,
                'w' => 9
            ])
            @include('formItems.select', [
                'name' => 'resultadoPlan',
                'label' => 'Resultado',
                'content' => $apto,
                'req'   => false,
                'place' => 'Seleciona una opción',
                'w' => 9
            ])
        </div>
    </div>
</div>
