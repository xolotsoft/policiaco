<div class="col-md-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Sanciones</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content form-horizontal form-label-left" style="display: block;">
            @include('formItems.select', [
                'name' => 'tipoSanciones',
                'label' => 'Tipo',
                'content' => $tipos,
                'req'   => false,
                'place' => false,
                'w' => 9
            ])
            @include('formItems.text', [
                'name' => 'causaSanciones',
                'label' => 'Causa',
                'place' => '',
                'req'   => false,
                'tooltip' => false,
                'w' => 9
            ])
            @include('formItems.date', [
                'name' => 'fechaSanciones',
                'label' => 'Fecha',
                'place' => '',
                'tooltip' => false,
                'w' => 9
            ])
        </div>
    </div>
</div>
