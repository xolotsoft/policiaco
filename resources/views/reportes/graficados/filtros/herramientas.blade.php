<div class="col-md-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Herramientas</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content form-horizontal form-label-left" style="display: block;">
            @include('formItems.select', [
                'name' => 'tipoHerramienta',
                'label' => 'Tipo',
                'content' => $tipo,
                'req'   => false,
                'place' => 'Seleciona una opción',
                'w' => 9
            ])
            @include('formItems.date', [
                'name' => 'fechaRecepcionHerramienta',
                'label' => 'Fecha de recepción',
                'place' => '',
                'tooltip' => '',
                'w' => 9
            ])
            @include('formItems.date', [
                'name' => 'fechaDevolucionHerramienta',
                'label' => 'Fecha devolución',
                'place' => '',
                'tooltip' => '',
                'w' => 9
            ])
        </div>
    </div>
</div>
