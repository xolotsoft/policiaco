<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Folio</th>
                <th class="column-title">RFC </th>
                <th class="column-title">Nombre </th>
            </tr>
        </thead>
        <tbody>
            @foreach($aspirantes as $aspirante)
                @if($aspirante->Total > 4)
                    <tr class="even pointer">
                        <td class=" ">{{$aspirante->folio}}</td>
                        <td class=" ">{{$aspirante->rfc}}</td>
                        <td class=" ">{{ $aspirante->aPaterno}} {{$aspirante->aMaterno}} {{$aspirante->nombre}}</td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>
