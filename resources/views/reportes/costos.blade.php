<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Categoría </th>
                <th class="column-title">Grado </th>
                <th class="column-title">Especial </th>
                <th class="column-title">Salario</th>
                <th class="column-title">Total Mensual</th>
                <th class="column-title">Total Anual</th>
            </tr>
        </thead>
        <tbody >
            @foreach($costos as $costo)
            <tr>
                <td class=" ">{{$costo->categoria}}</td>
                <td class=" ">{{$costo->grado}}</td>
                <td class=" ">{{$costo->especial?'SI':'NO'}}</td>
                <td class=" a-right ">$ {{$costo->salario}}</td>
                <td class=" a-right ">$ {{$costo->Total}}</td>
                <td class=" a-right ">$ {{$costo->Anual}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
