<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Folio</th>
                <th class="column-title">RFC </th>
                <th class="column-title">Nombre </th>
            </tr>
        </thead>
        <tbody>
            @foreach($aspirantes as $aspirante)
            <tr class="even pointer">
                <td class=" ">{{$aspirante->folio}}</td>
                <td class=" ">{{$aspirante->persona->rfc}}</td>
                <td class=" ">{{ $aspirante->persona->aPaterno}} {{$aspirante->persona->aMaterno}} {{$aspirante->persona->nombre}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
