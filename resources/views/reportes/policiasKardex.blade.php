<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Empleado</th>
                <th class="column-title">RFC </th>
                <th class="column-title">Nombre </th>
                <th class="column-title">Ver </th>
            </tr>
        </thead>
        <tbody>
            @foreach($policias as $policia)
            <tr class="even pointer">
                <td class=" ">{{ $policia->id }}</td>
                <td class=" ">{{ $policia->persona->rfc }}</td>
                <td class=" ">{{ $policia->persona->aPaterno }} {{ $policia->persona->aMaterno }} {{ $policia->persona->nombre }}</td>
                <td>
                  <a class="btn btn-round btn-info btn-xs" title="Mostrar" onclick="show({{$policia->idPersona}});">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
