<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Baja o Separación</th>
</tr>
@if($persona->separacion)
<tr class="headings">
    <th class="column-title">Tipo de separación</th>
    <td class=" ">{{$persona->separacion->catalogos->nombre}}</td>
    <th class="column-title">Fecha de separación</th>
    <td class=" ">{{$persona->separacion->fechaSeparacion}}</td>
</tr>
<tr class="headings">
    <th class="column-title">Motivo</th>
    <td class=" ">{{$persona->separacion->motivo}}</td>
    <th class="column-title">Aprobado por</th>
    <td class=" ">{{$persona->separacion->aprobado}}</td>
</tr>
@else
<tr class="headings">
    <th class="column-title">Separación</th>
    <td class=" "></td>
    <th class="column-title">Tipo de separación</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th class="column-title">Fecha de separación</th>
    <td class=" "></td>
    <th class="column-title">Motivo</th>
    <td class=" "></td>
</tr>
@endif
