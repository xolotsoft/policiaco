<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Reingreso Labor Policial</th>
</tr>
@if($persona->reingreso)
<tr class="headings">
    <th class="column-title">Institución</th>
    <td class=" ">{{$persona->reingreso->institucion != 0 ? $persona->reingreso->getInstitucion->nombre : ''}}</td>
    <th class="column-title">Entidad</th>
    <td class=" ">{{$persona->reingreso->estado != 0 ? $persona->reingreso->getEstado->Name : ''}}</td>
</tr>
<tr class="headings">
    <th class="column-title">Municipio</th>
    <td class=" ">{{$persona->reingreso->municipio !=0 ? $persona->reingreso->getMunucipio->Name : ''}}</td>
    <th class="column-title">Grado</th>
    <td class=" ">{{$persona->reingreso->grado != 0 ? $persona->reingreso->getGrado->Rank : ''}}</td>
</tr>
<tr class="headings">
    <th class="column-title">Antigüedad</th>
    <td class=" ">{{$persona->reingreso->antiguedad}}</td>
    <th class="column-title">Instancia Elegida</th>
    <td class=" ">{{$persona->reingreso->nombreInstancia}}</td>
</tr>
<tr class="headings">
    <th class="column-title">Instancia Evaluadora</th>
    <td class=" ">{{$persona->reingreso->instanciaEvaluadora}}</td>
    <th class="column-title">Fecha presentación de exámenes</th>
    <td class=" ">{{$persona->reingreso->fechaPresentacion}}</td>
</tr>
<tr class="headings">
    <th colspan="1" class="column-title">Observaciones</th>
    <td colspan="3" class=" ">{{$persona->reingreso->observaciones}}</td>
</tr>
@else
<tr class="headings">
    <th class="column-title">Institución</th>
    <td class=" "></td>
    <th class="column-title">Entidad</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th class="column-title">Municipio</th>
    <td class=" "></td>
    <th class="column-title">Grado</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th class="column-title">Antigüedad</th>
    <td class=" "></td>
    <th class="column-title">Instancia Elegida</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th class="column-title">Instancia Evaluadora</th>
    <td class=" "></td>
    <th class="column-title">Fecha presentación de exámenes</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th colspan="2" class="column-title">Observaciones</th>
    <td colspan="2" class=" "></td>
</tr>
@endif
