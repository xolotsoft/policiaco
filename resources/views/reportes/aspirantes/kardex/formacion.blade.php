<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Formación</th>
</tr>
@if($persona->formacion)
<tr class="headings">
    <th class="column-title">Materia</th>
    <td class=" ">
        {{$persona->formacion->material}}
    </td>
    <th class="column-title">Fecha de inicio</th>
    <td class=" ">
        {{$persona->formacion->fechaInicio}}
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Duracion</th>
    <td class=" ">
        {{$persona->formacion->duracion}}
    </td>
    <th class="column-title">Fecha de finalización</th>
    <td class=" ">
        {{$persona->formacion->fechaFin}}
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Certificado</th>
    <td class=" ">
    @include('components.status', ['param' => $persona->formacion->certificado])
    </td>
    <th class="column-title">Constancia</th>
    <td class=" ">
    @include('components.status', ['param' =>$persona->formacion->constancia])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Cumplimiento</th>
    <td class=" ">
        {{$persona->formacion->cumplimiento !=0 ? $persona->formacion->resultado->nombre : ''}}
    </td>
    <th class="column-title">Instancia capacitadora</th>
    <td class=" ">
        {{$persona->formacion->instancia !=0 ? $persona->formacion->capacitadoras->Name : ''}}
    </td>
</tr>
<tr class="headings">
    <th colspan="1" class="column-title">Observaciones</th>
    <td colspan="3" class=" ">
        {{$persona->formacion->observaciones}}
    </td>
</tr>
@else
<tr class="headings">
    <th class="column-title">Materia</th>
    <td class=" "></td>
    <th class="column-title">Fecha de inicio</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th class="column-title">Duracion</th>
    <td class=" "></td>
    <th class="column-title">Fecha de finalización</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th class="column-title">Certificado</th>
    <td class=" "></td>
    <th class="column-title">Constancia</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th class="column-title">Cumplimiento</th>
    <td class=" "></td>
    <th class="column-title">Instancia capacitadora</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th colspan="1" class="column-title">Observaciones</th>
    <td colspan="3" class=" "></td>
</tr>
@endif
