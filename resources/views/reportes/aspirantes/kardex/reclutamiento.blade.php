<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Reclutamiento</th>
</tr>
@if($persona->reclutamiento)
<tr class="headings">
    <th class="column-title">Perfil Solicitado</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->perfilSolicitado])
    </td>
    <th class="column-title">Documento</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->documento])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Credencial de Elector</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->elector])
    </td>
    <th class="column-title">Acta de Nacimiento</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->nacimiento])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Cartilla de Servicio Militar</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->cartilla])
    </td>
    <th class="column-title">Antecedentes No Penales</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->penales])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Secundaria</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->secundaria])
    </td>
    <th class="column-title">Bachillerato</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->bachillerato])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Certificado Técnico</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->tecnico])
    </td>
    <th class="column-title">Licencitura</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->licenciatura])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Posgrado</th>
    <td class=" ">
        {{$persona->reclutamiento->posgrado != 0 ? $persona->reclutamiento->getPosgrado->nombre : '' }}
    </td>
    <th class="column-title">Certificado de Posgrado</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->maestria])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Certificado de baja Policial</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->bajaPolicial])
    </td>
    <th class="column-title">Fotografia</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->fotoInfantil])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Comprobante de Domicilio</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->comprobanteDomicilio])
    </td>
    <th class="column-title">Carta con motivos de ingreso</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->motivoIngreso])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Referencias personales 1</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->refPersonales1])
    </td>
    <th class="column-title">Referencias personales 2</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->refPersonales2])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Acta de nacimiento de hijos</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->nacimientoHijo])
    </td>
    <th class="column-title">Acta de Matrimonio</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->matrimonio])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Acta de sociedad conyugal</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->sociedadConyugal])
    </td>
    <th class="column-title">Licencia de manejo</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->manejo])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">RFC</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->documentoRfc])
    </td>
    <th class="column-title">CURP</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->documentoCurp])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Estados de cuenta bancarios</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->estadoCuenta])
    </td>
    <th class="column-title">Solicitud de Ingreso</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->solicitud])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Currículum</th>
    <td class=" ">
        @include('components.status', ['param' => $persona->reclutamiento->currículum])
    </td>
    <th class="column-title">Asimilamiento Grado Policial</th>
    <td class=" ">
        {{$persona->reclutamiento->asimilamientoGradoPolicial != 0 ? $persona->reclutamiento->grado->nombre : ''}}
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Servicios médicos</th>
    <td class=" ">
       {{$persona->reclutamiento->medicos->nombre}}
    </td>
    <th class="column-title">Observaciones</th>
    <td class=" ">
       {{$persona->reclutamiento->observaciones}}
    </td>
</tr>
@else
<tr class="headings">
    <th class="column-title">Perfil Solicitado</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">Documento</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Credencial de Elector</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">Acta de Nacimiento</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Cartilla de Servicio Militar</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">Antecedentes No Penales</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Secundaria</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">Bachillerato</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Certificado Técnico</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">Licencitura</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Posgrado</th>
    <td class=" "></td>
    <th class="column-title">Certificado de Posgrado</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Certificado de baja Policial</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">Fotografia</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Comprobante de Domicilio</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">Carta con motivos de ingreso</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Referencias personales 1</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">Referencias personales 2</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Acta de nacimiento de hijos</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">Acta de Matrimonio</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Acta de sociedad conyugal</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">Licencia de manejo</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Licencia de manejo</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">CURP</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">RFC</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">Servicios médicos</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Estados de cuenta bancarios</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">Solicitud de Ingreso</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Currículum</th>
    <td class=" ">
        @include('components.status', ['param' => false])
    </td>
    <th class="column-title">Asimilamiento Grado Policial</th>
    <td class=" "></td>
</tr>
@endif
