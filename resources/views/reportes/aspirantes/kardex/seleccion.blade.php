<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Selección</th>
</tr>
<tr class="headings">
    <th colspan="2" class="column-title">Resultado</th>
    <th colspan="2" class="column-title">Observaciones</th>
</tr>
@if($persona->seleccion)
<tr class="headings">
    <td colspan="2">{{$persona->seleccion->resultado->nombre}}</td>
    <td colspan="2">{{$persona->seleccion->observaciones}}</td>
</tr>
@else
<tr class="headings">
    <td colspan="2">&nbsp;</td>
    <td colspan="2">&nbsp;</td>
</tr>
@endif
