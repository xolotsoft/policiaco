<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Certificación y Nombramiento</th>
</tr>
@if($persona->certificacion)
<tr class="headings">
    <th class="column-title">Categoría Policial</th>
    <td class=" ">
        {{$persona->certificacion->cargo !=0 ? $persona->certificacion->categoriaPolicial->nombre : ''}}
    </td>
    <th class="column-title">Grado Policial</th>
    <td class=" ">
        {{$persona->certificacion->puesto !=0 ? $persona->certificacion->puestoPolicial->Rank : ''}}
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Fecha de ingreso</th>
    <td class=" ">
        {{$persona->certificacion->fechaIngreso}}
    </td>
    <th class="column-title">Area o unidad de adscripción</th>
    <td class=" ">
        {{$persona->certificacion->adscripcion}}
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Nombre de Curso</th>
    <td class=" ">
        {{$persona->certificacion->nombreCurso}}
    </td>
    <th class="column-title">Formación Inicial</th>
    <td class=" ">
        {{$persona->certificacion->formacionInicial !=0 ? $persona->certificacion->inicial->nombre : ''}}
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Calificación de curso de formación inicial</th>
    <td class=" ">
        {{$persona->certificacion->calificacion}}
    </td>
    <th class="column-title">Documento que avala el curso</th>
    <td class=" ">
    @include('components.status', ['param' => $persona->certificacion->documentoCurso])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Calificación de curso de formación inicial</th>
    <td class=" ">
        {{$persona->certificacion->calificacion}}
    </td>
    <th class="column-title">Documento que avala el curso</th>
    <td class=" ">
    @include('components.status', ['param' => $persona->certificacion->documentoCurso])
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Observaciones</th>
    <td class=" ">
        {{$persona->certificacion->observaciones}}
    </td>
    <th class="column-title">Nombramiento</th>
    <td class=" ">
    @include('components.status', ['param' => $persona->certificacion->nombramiento])
    </td>
</tr>
@else
<tr class="headings">
    <th class="column-title">Categoría Policial</th>
    <td class=" "></td>
    <th class="column-title">Grado Policial</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th class="column-title">Fecha de ingreso</th>
    <td class=" "></td>
    <th class="column-title">Area o unidad de adscripción</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th class="column-title">Nombre de Curso</th>
    <td class=" "></td>
    <th class="column-title">Formación Inicial</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th class="column-title">Calificación de curso de formación inicial</th>
    <td class=" "></td>
    <th class="column-title">Documento que avala el curso</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th class="column-title">Calificación de curso de formación inicial</th>
    <td class=" "></td>
    <th class="column-title">Documento que avala el curso</th>
    <td class=" "></td>
</tr>
<tr class="headings">
    <th class="column-title">Observaciones</th>
    <td class=" "></td>
    <th class="column-title">Nombramiento</th>
    <td class=" "></td>
</tr>
@endif
