<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <tbody>
            @include('reportes.policias.kardex.personales')
            @include('reportes.policias.kardex.domicilio')
            @include('reportes.policias.kardex.familiares')
            @include('reportes.policias.kardex.actividad')
            @include('reportes.policias.kardex.promociones')
            @include('reportes.policias.kardex.herramientas')
            @include('reportes.policias.kardex.sanciones')
            @include('reportes.policias.kardex.adscripcion')
            @include('reportes.policias.kardex.estimulos')
            @include('reportes.policias.kardex.plan')
            @include('reportes.policias.kardex.administrativas')
            @include('reportes.policias.kardex.separacion')
            @include('reportes.policias.kardex.evaluaciones')
        </tbody>
    </table>
</div>
