<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Usuario </th>
                <th class="column-title">Fecha</th>
                <th class="column-title">Módulo </th>
                <th class="column-title">Sección</th>
                <th class="column-title">Acción</th>
                <th class="column-title no-link last"><span class="nobr">Detalles</span></th>
            </tr>
        </thead>
        <tbody>
            @foreach($bitacora as $val)
                <tr class="even pointer">
                    <td class=" ">{{$val->user->name}}</td>
                    <td class=" ">{{$val->fecha}}</td>
                    <td class=" ">{{$val->modulo}}</td>
                    <td class=" ">{{$val->seccion}}</td>
                    <td class=" ">{{$val->accion}}</td>
                    <td class="last">
                        <a class="btn btn-round btn-info btn-xs" title="Mostrar" onclick="show('{{$val->query}}');">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="modal fade" id="modal-id">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Detalle Bitácora</h4>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
