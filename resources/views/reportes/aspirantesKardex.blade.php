<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Folio</th>
                <th class="column-title">CURP </th>
                <th class="column-title">Nombre </th>
                <th class="column-title">Ver </th>
            </tr>
        </thead>
        <tbody>
            @foreach($aspirantes as $aspirante)
            <tr class="even pointer">
                <td class=" ">{{$aspirante->folio}}</td>
                <td class=" ">{{$aspirante->persona->curp}}</td>
                <td class=" ">{{ $aspirante->persona->aPaterno}} {{$aspirante->persona->aMaterno}} {{$aspirante->persona->nombre}}</td>
                <td>
                  <a class="btn btn-round btn-info btn-xs" title="Mostrar" onclick="show({{$aspirante->idPersona}});">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </a>
                </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
