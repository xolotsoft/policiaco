<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Examen</th>
                <th class="column-title">Resultado</th>
                <th class="column-title">Fecha de presentación</th>
                <th class="column-title">Fecha de entrega de resultados</th>
                <th class="column-title">Estatus</th>
            </tr>
        </thead>
        <tbody>
            @forelse($persona->evaluaciones as $evaluacion)
            <tr>
                <td>{{$evaluacion->catalogos->nombre}}</td>
                <td>{{$evaluacion->cumplimiento->nombre}}</td>
                <td>{{$evaluacion->fecha}}</td>
                <td>{{$evaluacion->fechaResultados}}</td>
                <td>
                @php
                    $end = Carbon\Carbon::parse($evaluacion->fechaResultados);
                    $now = Carbon\Carbon::now();
                    $length = $end->diffInDays($now);
                @endphp
                @if($length >= 660)
                    <a class="btn btn-danger btn-xs">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a>
                @elseif($length >= 480)
                    <a class="btn btn-warning btn-xs">
                        <i class="fa fa-warning" aria-hidden="true"></i>
                    </a>
                @else
                    <a class="btn btn-success btn-xs">
                        <i class="fa fa-check" aria-hidden="true"></i>
                    </a>
                @endif
                </td>
            </tr>
            @empty
            <tr class="headings">
                <td colspan="4" style="text-align:center;">No hay evaluaciones registradas</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>
