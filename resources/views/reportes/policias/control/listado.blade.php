<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Folio</th>
                <th class="column-title">CURP </th>
                <th class="column-title">Nombre </th>
                <th class="column-title">Ver </th>
            </tr>
        </thead>
        <tbody>
            @foreach($policias as $policia)
            <tr class="even pointer">
                <td>{{$policia->id}}</td>
                <td>{{$policia->persona->curp}}</td>
                <td>{{ $policia->persona->aPaterno}} {{$policia->persona->aMaterno}} {{$policia->persona->nombre}}</td>
                <td>
                  <a class="btn btn-round btn-info btn-xs" title="Mostrar" onclick="show({{$policia->idPersona}});">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </a>
                </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
