<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Sanciones</th>
</tr>
<tr class="headings">
    <th class="column-title">Causa</th>
    <th class="column-title">Tipo</th>
    <th class="column-title">Duración</th>
    <th class="column-title">Fecha</th>
</tr>
@forelse($persona->sanciones as $sancion)
    <tr class="even pointer">
        <td>{{$sancion->causa}}</td>
        <td>{{$sancion->catalogos->nombre}}</td>
        <td>{{$sancion->duracion}}</td>
        <td>{{$sancion->fechaSanciones}}</td>
    </tr>
@empty
<tr class="headings">
    <td colspan="4" style="text-align:center;">No hay registros</td>
</tr>
@endforelse
