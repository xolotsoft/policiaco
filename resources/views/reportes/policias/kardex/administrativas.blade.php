<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Actividades Administrativas</th>
</tr>
<tr class="headings">
    <th class="column-title">Descripción</th>
    <th class="column-title">Autoriza</th>
    <th class="column-title">Fecha Inicio</th>
    <th class="column-title">Fecha Termino</th>
</tr>
@forelse($persona->administrativas as $administrativa)
    <tr class="even pointer">
        <td>{{$administrativa->descripcion}}</td>
        <td>{{$administrativa->autorizante}}</td>
        <td>{{$administrativa->fechaInicio}}</td>
        <td>{{$administrativa->fechaTermino}}</td>
    </tr>
@empty
<tr class="headings">
    <td colspan="4" style="text-align:center;">No hay registros</td>
</tr>
@endforelse
