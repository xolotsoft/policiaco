<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Cambios de Adscripción</th>
</tr>
<tr class="headings">
    <th class="column-title">Tareas</th>
    <th class="column-title">Unidad Administrativa</th>
    <th class="column-title">Razón del Cambio</th>
    <th class="column-title">Fecha Inicio</th>
</tr>
@forelse($persona->adscripciones as $adscripcion)
    <tr class="even pointer">
        <td>{{$adscripcion->servicioRealizar}}</td>
        <td>{{$adscripcion->areaProcedencia}}</td>
        <td>{{$adscripcion->razon}}</td>
        <td>{{$adscripcion->fechaInicio}}</td>
    </tr>
@empty
<tr class="headings">
    <td colspan="4" style="text-align:center;">No hay registros</td>
</tr>
@endforelse
