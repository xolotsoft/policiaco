<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Herramientas</th>
</tr>
<tr class="headings">
    <th class="column-title">Descripción</th>
    <th class="column-title">Tipo</th>
    <th class="column-title">Cantidad</th>
    <th class="column-title">Fecha Devolución</th>
</tr>
@forelse($persona->herramientas as $herramienta)
    <tr class="even pointer">
        <td>{{$herramienta->descripcion}}</td>
        <td>{{$herramienta->catalogos->nombre}}</td>
        <td>{{$herramienta->cantidad}}</td>
        <td>{{$herramienta->fechaDevolucion}}</td>
    </tr>
@empty
<tr class="headings">
    <td colspan="4" style="text-align:center;">No hay registros</td>
</tr>
@endforelse
