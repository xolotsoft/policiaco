<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Plan Individual de Carrera</th>
</tr>
<tr class="headings">
    <th class="column-title">Tipo</th>
    <th class="column-title">Capacitación</th>
    <th class="column-title">Duración</th>
    <th class="column-title">Resultado</th>
</tr>
@forelse($persona->planes as $plan)
    <tr class="even pointer">
        <td>{{$plan->planes->Name}}</td>
        <td>{{$plan->tipo->EvaluationName}}</td>
        <td>{{$plan->duracion}} Hrs.</td>
        <td>{{$plan->apto->nombre}}</td>
    </tr>
@empty
<tr class="headings">
    <td colspan="4" style="text-align:center;">No hay registros</td>
</tr>
@endforelse
