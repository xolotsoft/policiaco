<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Datos Personales</th>
</tr>
<tr class="headings">
    <th class="column-title">Nombre</th>
    <td class=" ">{{ $persona->aPaterno}} {{$persona->aMaterno}} {{$persona->nombre}}</td>
    <th class="column-title">CURP</th>
    <td class=" ">{{$persona->curp}}</td>
</tr>
<tr class="headings">
    <th class="column-title">RFC</th>
    <td class=" ">{{$persona->rfc}}</td>
    <th class="column-title">No. Empleado</th>
    <td class=" ">{{$persona->policia->id}}</td>
</tr>
<tr class="headings">
    <th class="column-title">Fecha de Nacimiento</th>
    <td class=" ">{{$persona->fechaNacimiento}}</td>
    <th class="column-title">Lugar de Nacimiento</th>
    <td class=" ">{{$persona->nacimiento->Name}}</td>
</tr>
<tr class="headings">
    <th class="column-title">Altura</th>
    <td class=" ">{{$persona->estatura}} Mts.</td>
    <th class="column-title">Peso</th>
    <td class=" ">{{$persona->peso}} Kgs.</td>
</tr>
<tr class="headings">
    <th class="column-title">Teléfono</th>
    <td class=" ">{{$persona->telefono}}</td>
    <th class="column-title">Móvil</th>
    <td class=" ">{{$persona->movil}}</td>
</tr>
<tr class="headings">
    <th class="column-title">Sexo</th>
    <td class=" ">{{$persona->sexo->nombre}}</td>
    <th rowspan="3" class="column-title">Foto</th>
    <td rowspan="3" class=" ">
        @if($persona->reclutamiento)
            <img src="data:{{$persona->reclutamiento->archivoFotoInfantil->type}};base64,{{$persona->reclutamiento->archivoFotoInfantil->data}}" style="width:100px;height:100px;border-radius:50%;" />
        @else
            {{ Html::image('img/persona.png', '', array('style' => 'width:100px;height:100px;border-radius:50%;')) }}
        @endif
    </td>
</tr>
<tr class="headings">
    <th class="column-title">Correo electrónico</th>
    <td class=" ">{{$persona->email}}</td>
</tr>
<tr class="headings">
    <th class="column-title">Estado civil</th>
    <td class=" ">{{$persona->civil->nombre}}</td>
</tr>
