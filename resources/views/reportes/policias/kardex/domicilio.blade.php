<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Domicilio</th>
</tr>
@if($persona->domicilio)
<tr class="headings">
    <th colspan="1" class="column-title">Calle</th>
    <td colspan="3" class=" ">{{$persona->domicilio->calle}}</td>
</tr>
<tr class="headings">
    <th class="column-title">No. Interior</th>
    <td class=" ">{{$persona->domicilio->interior}}</td>
    <th class="column-title">No. Exterior</th>
    <td class=" ">{{$persona->domicilio->exterior}} </td>
</tr>
<tr class="headings">
    <th class="column-title">Colonia</th>
    <td class=" ">{{$persona->domicilio->coloniaCat->Name}}</td>
    <th class="column-title">Municipio</th>
    <td class=" ">{{$persona->domicilio->municipioCat->Name}}</td>

</tr>
<tr class="headings">
    <th class="column-title">Estado</th>
    <td class=" ">{{$persona->domicilio->estadoCat->Name}} </td>
    <th class="column-title">Código Postal</th>
    <td class=" ">{{$persona->domicilio->cp}}</td>
</tr>
@else
<tr class="headings">
    <th colspan="1" class="column-title">Calle</th>
    <td colspan="3" class=" "></td>
</tr>
<tr class="headings">
    <th class="column-title">No. Interior</th>
    <td class=" "></td>
    <th class="column-title">No. Exterior</th>
    <td class=" "> </td>
</tr>
<tr class="headings">
    <th class="column-title">Colonia</th>
    <td class=" "></td>
    <th class="column-title">Municipio</th>
    <td class=" "></td>

</tr>
<tr class="headings">
    <th class="column-title">Estado</th>
    <td class=" "> </td>
    <th class="column-title">Código Postal</th>
    <td class=" "></td>
</tr>
@endif
