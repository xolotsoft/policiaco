<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Evaluaciones de Control de Confianza</th>
</tr>
<tr class="headings">
    <th class="column-title">Examen</th>
    <th class="column-title">Resultado</th>
    <th class="column-title">Fecha de presentación</th>
    <th class="column-title">Fecha de entrega de resultados</th>
</tr>
@forelse($persona->evaluaciones as $evaluacion)
<tr class="headings">
    <td>{{$evaluacion->catalogos->nombre}}</td>
    <td>{{$evaluacion->cumplimiento->nombre}}</td>
    <td>{{$evaluacion->fecha}}</td>
    <td>{{$evaluacion->fechaResultados}}</td>
</tr>
@empty
<tr class="headings">
    <td colspan="4" style="text-align:center;">No hay evaluaciones registradas</td>
</tr>
@endforelse
