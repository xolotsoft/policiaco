<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Familiares</th>
</tr>
<tr class="headings">
    <th colspan="2" class="column-title">Nombre</th>
    <th colspan="2" class="column-title">Parentesco</th>
</tr>
@forelse($persona->familiares as $familiar)
    <tr class="even pointer">
        <td colspan="2">{{$familiar->nombre}} {{$familiar->aPaterno.' '.$familiar->aMaterno}}</td>
        <td colspan="2">{{$familiar->catalogos->nombre}}</td>
    </tr>

@empty
<tr class="headings">
    <td colspan="4" class=" " style="text-align:center;">No hay familiares registrados</td>
</tr>
@endforelse
