<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Promociones</th>
</tr>
<tr class="headings">
    <th class="column-title">Categoría</th>
    <th class="column-title">Grado</th>
    <th class="column-title">Especial</th>
    <th class="column-title">Fecha</th>
</tr>
@forelse($persona->promociones as $promocion)
    <tr class="even pointer">
        <td>{{$promocion->cargos->nombre}}</td>
        <td>{{$promocion->rangos->Rank}}</td>
        <td>@include('components.status', ['param' => $promocion->rangos->Special])</td>
        <td>{{$promocion->fechaPromocion}}</td>
    </tr>
@empty
<tr class="headings">
    <td colspan="4" style="text-align:center;">No hay registros</td>
</tr>
@endforelse
