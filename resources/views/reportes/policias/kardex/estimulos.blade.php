<tr class="headings">
    <th colspan="4" class="column-title" style="text-align:center;font-size:14px;color:#FFF;background-color:#405467;letter-spacing:2px;">Estímulos</th>
</tr>
<tr class="headings">
    <th class="column-title">Tipo</th>
    <th class="column-title">Motivo</th>
    <th class="column-title">Premio</th>
    <th class="column-title">Fecha</th>
</tr>
@forelse($persona->estimulos as $estimulo)
    <tr class="even pointer">
        <td>{{$estimulo->catalogos->nombre}}</td>
        <td>{{$estimulo->motivo}}</td>
        <td>{{$estimulo->premio}}</td>
        <td>{{$estimulo->fechaEmision}}</td>
    </tr>
@empty
<tr class="headings">
    <td colspan="4" style="text-align:center;">No hay registros</td>
</tr>
@endforelse
