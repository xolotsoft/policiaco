<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <tbody>
            @include('reportes.aspirantes.kardex.personales')
            @include('reportes.aspirantes.kardex.domicilio')
            @include('reportes.aspirantes.kardex.familiares')
            @include('reportes.aspirantes.kardex.reclutamiento')
            @include('reportes.aspirantes.kardex.seleccion')
            @include('reportes.aspirantes.kardex.formacion')
            @include('reportes.aspirantes.kardex.certificacion')
            @include('reportes.aspirantes.kardex.reingreso')
            @include('reportes.aspirantes.kardex.separacion')
            @include('reportes.aspirantes.kardex.evaluaciones')
        </tbody>
    </table>
</div>
