
<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Grado</th>
                <th class="column-title">Ingreso</th>
                <th class="column-title">Adscripción</th>
                <th class="column-title">Calificación </th>
                <th class="column-title">Documento </th>
            </tr>
        </thead>
        <tbody>
        @foreach($certificados as $certificado)
            <tr>
                <td class="">{{$certificado->puestoPolicial->Rank}}</td>
                <td class="">{{$certificado->fechaIngreso}}</td>
                <td class="">{{$certificado->adscripcion}}</td>
                <td class="">{{$certificado->calificacion}}</td>
                <td class="">{{substr($certificado->archivoDocumentoCurso->name, 0, -4)}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
