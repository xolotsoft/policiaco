<!DOCTYPE html>
<html lang="es-Mx">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {!! Html::favicon('img/favicon.png') !!}
        <title>SCP | Bienvenido</title>
        {!! Html::style('vendors/bootstrap/dist/css/bootstrap.min.css') !!}
        {!! Html::style('vendors/font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('vendors/nprogress/nprogress.css') !!}
        {!! Html::style('vendors/animate.css/animate.min.css') !!}
        {!! Html::style('build/css/custom.min.css') !!}
        {!! Html::style('vendors/toastr/toastr.min.css') !!}
        {!! Html::script('vendors/jquery/dist/jquery.min.js'); !!}
        {!! Html::script('vendors/toastr/toastr.min.js'); !!}
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body class="login">
        @if(Session::has('error'))
            <script>
                toastr.error('{!!Session::get('error')!!}');
            </script>
        @endif
        <div>
            <a class="hiddenanchor" id="signup"></a>
            <a class="hiddenanchor" id="signin"></a>
            <div class="login_wrapper">
                <div class="animate form login_form">
                    <section class="login_content">
                        {{ Form::open(array('url' => 'login')) }}
                            <h1 style="font-size:42px;font-weight:bolder;">S P C P</h1>
                            <br>
                            <div>
                                <input type="text" class="form-control" placeholder="Usuario" required="" name="username"/>
                            </div>
                            <div>
                                <input type="password" class="form-control" placeholder="Contraseña" required="" name="password"/>
                            </div>
                            <div>
                            <input id="municipio" type="hidden" name="municipio" value=""/>
                                <select name="municipio" id="" class="form-control" placeholder="Contraseña" required="">
                                    <option value="">Selecciona un Municipio</option>
                                    @foreach($municipios as $municipio)
                                        <option value="{{$municipio->id}}">{{$municipio->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div>
                                <button type="submit" class="btn btn-default btn-success btn-block submit" style="letter-spacing:3px;">INGRESAR</button>
                            </div>
                            <div class="clearfix"></div>
                            <div class="separator">
                                <p class="change_link">Servicio Profesional de Carrera Policial</p>
                                <div class="clearfix"></div>
                                <br />
                                <div>
                                    <h1></h1>
                                    <p>©2017 All Rights Reserved.</p>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </section>
                </div>
            </div>
        </div>
        <script>
            // var municipios = '{{$municipios}}';
            // console.log(municipios);
            var full = window.location.host
            var parts = full.split('.')
            switch (parts[0]) {
                case 'localhost':
                    $('#municipio').val('2');
                    break;
                case 'tonala':
                    $('#municipio').val('4');
                    break;
                case 'tapachula':
                    $('#municipio').val('5');
                    break;
                case 'motozintla':
                    $('#municipio').val('6');
                    break;
                case 'lasmargaritas':
                    $('#municipio').val('7');
                    break;
                default:
                    $('#municipio').val('2');
                    break;
            }
        </script>
    </body>
</html>
