<div class="page-title">
    <div class="title_left">
        <br><br>
        <h3>
            @if($controller == 'Catalogos')
                Catálogos
            @else
                {{ $controller }}
            @endif
        </h3>
    </div>
    <div class="title_right">
        {{ Html::image('img/logos/'.Auth::user()->municipio_id.'.png', '', array('class' => 'logo-municipio')) }}
    </div>
</div>
