<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <strong>{{Auth::user()->perfil->nombre}}</strong> - {!! Auth::user()->name!!}
                    <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out pull-right"></i> Salir</a></li>
                    </ul>
                </li>
                <li role="presentation" class="">
                    <a href="{{ url('mensajes/principal') }}" class="info-number" aria-expanded="false">
                        <i class="fa fa-envelope-o"></i>
                        @if(isset($countm))
                            @if($countm > 0)
                                <span class="badge bg-green" id="countm">{{$countm}}</span>
                            @endif
                        @endif
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
