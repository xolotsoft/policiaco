<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="#" class="site_title">
                <span class="spcpt">SPCP</span>
                {{ HTML::image('img/escudos/'.Auth::user()->municipio_id.'.png', '', array('class' => 'escudo-municipio')) }}
                <span>Municipio de <br>{{Auth::user()->municipio->nombre}}</span>
            </a>
        </div>
        <div class="clearfix"></div>
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                @foreach($modules as $module)
                    @if($module->perfil >= Auth::user()->perfil_id)
                    <li><a><i class="fa fa-{{ $module->fa }}"></i> {{ $module->nombre}} <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                        @foreach($module->sections as $section)
                            @if($section->activo == 1)
                                @if($section->perfil >= Auth::user()->perfil_id)
                                    @if($section->subs == 0)
                                        <li>
                                            <a href="{{ url($section->controlador.'/'.$section->metodo) }}">{{ $section->nombre }}</a>
                                        </li>
                                    @else
                                        <li>
                                            <a>{{ $section->nombre }} <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                            @foreach($section->subsections as $subsection)
                                                <li>
                                                    <a href="{{ url($subsection->controlador.'/'.strtolower($section->nombre).'/'.$subsection->metodo) }}">{{ $subsection->nombre }}</a>
                                                </li>
                                            @endforeach
                                            </ul>
                                        </li>
                                    @endif
                                @endif
                            @endif
                        @endforeach
                        </ul>
                    </li>
                    @endif
                @endforeach
                </ul>
            </div>
        </div>
        {{--  <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>  --}}
    </div>
</div>
