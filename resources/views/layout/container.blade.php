@extends('layout.app', ['title' => $controller])
@section('container')
<div class="container body">
    <div class="main_container">
        @include('layout.nav', ['modules' => $modules])
        @include('layout.top', ['countm' => $countm])
        @yield('page')
        @include('layout.footer')
    </div>
</div>
@endsection
@section('script')
    @yield('script')
@endsection