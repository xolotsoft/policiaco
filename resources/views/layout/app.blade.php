<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {!! Html::favicon('img/favicon.png') !!}
        <title>SCP | {{ $title }}</title>
        @include('layout.styles')
    </head>
    <body class="nav-md">
        @yield('container')
        @include('layout.scripts')
        @yield('script')
    </body>
</html>