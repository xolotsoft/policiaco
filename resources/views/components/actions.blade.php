<td class=" last">
    <a class="btn btn-round btn-info btn-xs" title="Mostrar" onclick="show({{$param->id}});">
        <i class="fa fa-eye" aria-hidden="true"></i>
    </a>
    <a class="btn btn-round btn-warning btn-xs" title="Editar" onclick="edit({{$param->id}});">
        <i class="fa fa-pencil" aria-hidden="true"></i>
    </a>
    @if(Auth::user()->perfil_id == 1)
    <a class="btn btn-round btn-danger btn-xs" title="Eliminar" onclick="destroy({{$param->id}});">
        <i class="fa fa-times" aria-hidden="true"></i>
    </a>
    @endif
</td>
