<a class="btn btn-{{$param?'success':'danger'}} btn-xs">
    <i class="fa fa-{{$param?'check':'times'}}" aria-hidden="true"></i>
</a>