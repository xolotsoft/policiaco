@extends('layout.container', ['title' => $controller, 'modules' => $modules, 'countm' => isset($countm)?$countm:null])
@section('page')
<div class="right_col" role="main">
    <div class="">
        @include('layout.titulo')
            <div class="clearfix"></div>
        @include(strtolower($controller).'.main')
    </div>
</div>
@endsection
@section('script')
    {!! Html::script('js/'.strtolower($controller).'/'.$action.'.js?'.rand(100000,999999)) !!}
@endsection
