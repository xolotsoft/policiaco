@include('formItems.select', [
    'name' => 'Category',
    'label' => 'Categoría',
    'place' => 'Seleciona una opción',
    'content' => $categoriaPolicial
])
@include('formItems.text', [
    'name' => 'Rank',
    'label' => 'Grado'
])
@include('formItems.text', [
    'name' => 'CommandLevel',
    'label' => 'Nivel de Mando'
])
@include('formItems.check', [
    'name' => 'Special',
    'label' => 'Especial'
])
@include('formItems.text', [
    'name' => 'Normal_Salary',
    'label' => 'Salario Normal'
])
@include('formItems.text', [
    'name' => 'Special_Salary',
    'label' => 'Salario Especial'
])
@include('formItems.check', [
    'name' => 'OnlyMexican',
    'label' => 'Solo Mexicano'
])
@include('formItems.text', [
    'name' => 'HeightMan',
    'label' => 'Estatura Hombre'
])
@include('formItems.text', [
    'name' => 'HeightWoman',
    'label' => 'Estatura Mujer'
])
@include('formItems.text', [
    'name' => 'MinimumSchooling',
    'label' => 'Escolaridad Minima'
])
@include('formItems.text', [
    'name' => 'Age',
    'label' => 'Edad'
])
@include('formItems.text', [
    'name' => 'Old',
    'label' => 'Antiguedad'
])
@include('formItems.text', [
    'name' => 'Courses_ContinuedFormation',
    'label' => 'Cursos de Formación'
])
@include('formItems.text', [
    'name' => 'Courses_SpecializedFormation',
    'label' => 'Cursos de Especialización'
])
@include('formItems.text', [
    'name' => 'Active_ContinuedFormation',
    'label' => 'Formación Activa'
])
@include('formItems.text', [
    'name' => 'Active_SpecializedFormation',
    'label' => 'Especialización Activa'
])
@include('formItems.text', [
    'name' => 'ApproveCertificate',
    'label' => 'Certificado de Aprobación'
])
@include('formItems.text', [
    'name' => 'ApproveCertificateFileName',
    'label' => 'Archivo de Certificade de Aprobación'
])
