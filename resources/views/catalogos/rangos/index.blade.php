<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Categoria</th>
                <th class="column-title">Grado</th>
                <th class="column-title">Nivel de Mando</th>
                <th class="column-title">Salario Normal</th>
                <th class="column-title">Especial</th>
                <th class="column-title">Solo Mexicano</th>
                <th class="column-title no-link last"><span class="nobr">Acción</span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($rangos as $rango)
            <tr class="even pointer">
                <td class="text-center">{{$rango->categoria->nombre}}</td>
                <td class="text-center">{{$rango->Rank}}</td>
                <td class="text-center">{{$rango->CommandLevel}}</td>
                <td class="text-left">${{number_format($rango->Normal_Salary,2)}}</td>
                <td class="text-center">
                    @include('components.status', ['param' => $rango->Special])
                </td>
                <td class="text-center">
                    @include('components.status', ['param' => $rango->OnlyMexican])
                </td>
                @include('components.actions', ['param' => $rango])
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
