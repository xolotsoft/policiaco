<div class="conteiner">
    <div class ='form-horizontal form-label-left'>
        @include('formItems.show',['label' => 'Categoria', 'value' => $rango->categoria->nombre])
        @include('formItems.show',['label' => 'Grado', 'value' => $rango->Rank])
        @include('formItems.show',['label' => 'Nivel de Mando', 'value' => $rango->CommandLevel])
        @include('formItems.showchk',['label' => 'Especial', 'value' => $rango->Special])
        @include('formItems.showNumForm',['label' => 'Salario Normal', 'value' => $rango->Normal_Salary])
        @include('formItems.showNumForm',['label' => 'Salario Especial', 'value' => $rango->Special_Salary])
        @include('formItems.showchk',['label' => 'Solo Mexicano', 'value' => $rango->OnlyMexican])
        @include('formItems.showParam',['label' => 'Estatura Hombre', 'value' => $rango->HeightMan, 'unidad' => 'm'])
        @include('formItems.showParam',['label' => 'Estatura Mujer', 'value' => $rango->HeightWoman, 'unidad' => 'm'])
        @include('formItems.show',['label' => 'Escolaridad Minima', 'value' => $rango->MinimumSchooling])
        @include('formItems.showParam',['label' => 'Edad', 'value' => $rango->Age, 'unidad' => 'años'])
        @include('formItems.show',['label' => 'Antiguedad', 'value' => $rango->Old])
        @include('formItems.show',['label' => 'Cursos de Formación', 'value' => $rango->Courses_ContinuedFormation])
        @include('formItems.show',['label' => 'Cursos de Especialización', 'value' => $rango->Courses_SpecializedFormation])
        @include('formItems.show',['label' => 'Formación Activa', 'value' => $rango->Active_ContinuedFormation])
        @include('formItems.show',['label' => 'Especialización Activa', 'value' => $rango->Active_SpecializedFormation])
        @include('formItems.show',['label' => 'Certificado de Aprovación', 'value' => $rango->ApproveCertificate])
        @include('formItems.show',['label' => 'Archivo de Certificade de Aprovación', 'value' => $rango->ApproveCertificateFileName])
    </div>
</div>
