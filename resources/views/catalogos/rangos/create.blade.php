<div class="conteiner">
    {{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true])}}
        @include('catalogos.rangos.form')
        @include('formItems.submit', ['label' => 'Guardar'])
    {{ Form::close() }}
</div>