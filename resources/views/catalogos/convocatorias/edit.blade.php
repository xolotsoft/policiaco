<div class="conteiner">
    {{Form::model($convocatoria, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'editar'])}}
        @include('catalogos.convocatorias.form')
        @include('formItems.submit', ['label' => 'Editar'])
    {{ Form::close() }}
</div>