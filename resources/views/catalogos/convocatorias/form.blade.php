@include('formItems.text', [
    'name'  => 'nombre',
    'label' => 'Nombre',
    'place' => 'Convocatoria X',
    'req'   => true,
    'tooltip' => 'Escribe el nombre de la convocatoria'
])
@include('formItems.text', [
    'name'  => 'puestos',
    'label' => 'Puestos a concursar',
    'place' => '10',
    'req'   => true,
    'max'    => '2',
    'tooltip' => 'Ingresa la cantidad total de puestos a concursar para esta convocatoria'
])
@include('formItems.date', [
    'name'  => 'fecha',
    'label' => 'Fecha',
    'req'   => true
])
<div class="form-group">
    {{ Form::label('edadMinima', 'Edad Minima', ['class' => 'control-label col-xs-3'])}}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {{Form::selectRange('edadMinima', 18, 30, null, ['id'=>'edadMinima'])}}
    </div>
</div>
<div class="form-group">
    {{ Form::label('edadMaxima', 'Edad Maxima', ['class' => 'control-label col-xs-3'])}}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {{Form::selectRange('edadMaxima', 30, 40, null,['id'=>'edadMaxima'])}}
    </div>
</div>
@include('formItems.file', [
    'name' => 'File',
    'label' => 'Documento',
    'req'   => true
])
@include('formItems.area', [
    'name'  => 'descripcion',
    'label' => 'Descripción',
    'max'   => '255',
    'req'   => true
])
