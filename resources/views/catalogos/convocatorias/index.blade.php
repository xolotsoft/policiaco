<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Convocatoria</th>
                <th class="column-title">Puesto </th>
                <th class="column-title">Fecha </th>
                <th class="column-title">Edad Minima </th>
                <th class="column-title">Edad Maxima </th>
                <th class="column-title no-link last"><span class="nobr">Acción</span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($convocatorias as $convocatoria)
            <tr class="even pointer">
                <td class=" ">{{$convocatoria->nombre}}</td>
                <td class=" ">{{$convocatoria->puestos}}</td>
                <td class=" ">{{$convocatoria->fecha}}</td>
                <td class=" ">{{$convocatoria->edadMinima}}</td>
                <td class=" ">{{$convocatoria->edadMaxima}}</td>
                @include('components.actions', ['param' => $convocatoria])
            </tr>
            @endforeach
        </tbody>
    </table>
</div>