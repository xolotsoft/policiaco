<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<form class="form-horizontal form-label-left">
	    <div class="form-group">
	        {{ Form::label('fecha', 'Fecha de Registro', ['class' => 'control-label col-xs-3'])}}
	        <div class="col-md-6 col-sm-6 col-xs-12">
	            {{Form::text('fecha', '', ['class'=>'form-control col-xs-7'])}}
	        </div>
	    </div>
	    <div class="form-group">
	        {{ Form::label('convocatoria', 'Número de convocatoria', ['class' => 'control-label col-xs-3'])}}
	        <div class="col-md-6 col-sm-6 col-xs-12">
	            {{Form::text('convocatoria', '', ['class'=>'form-control col-xs-7'])}} 
	        </div>
	    </div>
	    <div class="form-group">
	        {{ Form::label('detalles', 'Detalles de convocatoria', ['class' => 'control-label col-xs-3'])}}
	        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Detalles de convocatoria</label>
	        <div class="col-md-6 col-sm-6 col-xs-12">
	            {{Form::text('detalles', '', ['class'=>'form-control col-xs-7'])}}
	        </div>
	    </div>
	    <div class="form-group">
	        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nombre del puesto</label>
	        <div class="col-md-6 col-sm-6 col-xs-12">
	            <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="middle-name">
	        </div>
	    </div>
	    <div class="form-group">
	        <label class="control-label col-md-3 col-sm-3 col-xs-12">No. de Folio <span class="required">*</span>
	        </label>
	        <div class="col-md-6 col-sm-6 col-xs-12">
	            <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text">
	        </div>
	    </div>
	</form>
</div>