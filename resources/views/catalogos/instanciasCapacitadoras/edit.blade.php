<div class="conteiner">
    {{Form::model($instancia, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'editar'])}}
        @include('catalogos.instanciasCapacitadoras.form')
        @include('formItems.submit', ['label' => 'Editar'])
    {{ Form::close() }}
</div>
