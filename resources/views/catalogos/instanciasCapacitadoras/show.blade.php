<div class="conteiner">
    <div class ='form-horizontal form-label-left'>
        <div class="form-group">
            {{ Form::label('Name', 'Descripción', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-3">
                    {{$instancia->Name}}
                </p>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('Observations', 'Observaciones', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-3">
                    {{$instancia->Observations}}
                </p>
            </div>
        </div>
    </div>
</div>