<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Descripción</th>
                <th class="column-title">Observaciones</th>
                <th class="column-title no-link last"><span class="nobr">Acción</span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($instancias as $instancia)
            <tr class="even pointer">
                <td class="text-center">{{$instancia->Name}}</td>
                <td class="text-center">{{$instancia->Observations}}</td>
                 @include('components.actions', ['param' => $instancia])
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
