@include('formItems.text', [
    'name' => 'Name',
    'label' => 'Descripción',
    'req' => true
])
@include('formItems.area', [
    'name' => 'Observations',
    'label' => 'Observaciones',
    'req' => false
])
