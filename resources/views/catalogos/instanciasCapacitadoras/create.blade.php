<div class="conteiner">
    {{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true])}}
        @include('catalogos.instanciasCapacitadoras.form')
        @include('formItems.submit', ['label' => 'Guardar'])
    {{ Form::close() }}
</div>
