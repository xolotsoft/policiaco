<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Tipo</th>
                <th class="column-title">Nombre</th>
                <th class="column-title">Duración</th>
                <th class="column-title">Categoria</th>
                <th class="column-title">Especializacón</th>
                <th class="column-title">Alta Dirección</th>
                <th class="column-title">Obligatorio</th>
                <th class="column-title no-link last"><span class="nobr">Acción</span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($evaluaciones as $evaluacion)
            <tr class="even pointer">
                <td class="text-center">{{$evaluacion->Type->Name}}</td>
                <td class="text-center">{{$evaluacion->EvaluationName}}</td>
                <td class="text-center">{{$evaluacion->Duration}}</td>
                <td class="text-center">{{$evaluacion->ImportanceDegree}}</td>
                <td class="text-center">{{$evaluacion->Specialization}}</td>
                <td class="text-center">{{$evaluacion->Direction}}</td>
                <td class="text-center">
                    @include('components.status', ['param' => $evaluacion->Compulsory])
                </td>
                @include('components.actions', ['param' => $evaluacion])
            </tr>
            @endforeach
        </tbody>
    </table>
</div>