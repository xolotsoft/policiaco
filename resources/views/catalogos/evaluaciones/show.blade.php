<div class="conteiner">
    <div class ='form-horizontal form-label-left'>
        @include('formItems.show', ['label' => 'Tipo de evaluación', 'value' => $evaluacion->Type->Name])
        @include('formItems.show', ['label' => 'Nombre', 'value' => $evaluacion->EvaluationName])
        @include('formItems.show', ['label' => 'Duración', 'value' => $evaluacion->Duration])
        @include('formItems.show', ['label' => 'Categoría', 'value' => $evaluacion->ImportanceDegree])
        @include('formItems.show', ['label' => 'Especialización', 'value' => $evaluacion->Specialization])
        @include('formItems.show', ['label' => 'Alta Dirección', 'value' => $evaluacion->Direction])
        @include('formItems.showchk', ['label' => 'Obligatorio', 'value' => $evaluacion->Compulsory])
        @include('formItems.show', ['label' => 'Comentarios', 'value' => $evaluacion->Comments])
    </div>
</div>