@include('formItems.select', [
    'name' => 'EvaluationType',
    'label' => 'Tipo de evaluación',
    'content' => $tipos
])
@include('formItems.text', [
    'name' => 'EvaluationName',
    'label' => 'Nombre'
])
@include('formItems.text', [
    'name' => 'Duration',
    'label' => 'Duración'
])
@include('formItems.select', [
    'name' => 'ImportanceDegree',
    'label' => 'Categoría',
    'content' => ['Actualizacion' => 'Actualizacion']
])
@include('formItems.text', [
    'name' => 'Specialization',
    'label' => 'Especialización'
])
@include('formItems.text', [
    'name' => 'Direction',
    'label' => 'Alta Dirección'
])
@include('formItems.check', [
    'name' => 'Compulsory',
    'label' => 'Obligatorio'
])
@include('formItems.area', [
    'name' => 'Comments',
    'label' => 'Comentarios'
])
@include('formItems.submit', ['label' => 'Guardar'])
