<div class="form-group">
    <div class="input-group">
        <span class="input-group-btn">
            <button type="submit" class="btn btn-success pull-right" id="submit">
                {{$label}} <span class="fa fa-save"></span>
            </button>
        </span>
    </div>
</div>