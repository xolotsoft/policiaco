<div class="form-group">
    {{ Form::label('', $label, ['class' => 'control-label col-xs-3'])}}
    <div class="col-md-6 col-sm-6 col-xs-12">
        <p class="control-label col-xs-3">
            @include('components.status', ['param' => $value])
        </p>
    </div>
</div>