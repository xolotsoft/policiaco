<?php
if(isset($add)){
    $content['NEW'] = 'Otro';
}
?>
<div class="form-group">
    {!! (isset($req) && $req === true)?'<span class="fa fa-asterisk text-danger"></span>':''!!}
    {{ isset($label)?Form::label($name, $label, ['class' => 'control-label col-xs-3']):''}}
    <div class="col-md-{{isset($w)?$w:6}} col-sm-{{isset($w)?$w:6}} col-xs-12">
        {{Form::select($name, $content, null, [
            'placeholder' => isset($place)?$place:null,
            'class'=> isset($add)?"add":null,
            'disabled' => isset($dis)?$dis:false,
            'data-tooltip-content'=>"#tooltip-".$name
        ])}}
    </div>
</div>
@if (isset($tooltip) && $tooltip !== false) {
@include('formItems.tooltip')
@endif
