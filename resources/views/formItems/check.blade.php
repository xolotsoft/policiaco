<div class="form-group">
    {{ Form::label($name, $label, ['class' => 'control-label col-xs-3'])}}
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="checkbox">
            <label>
                {{Form::checkbox($name, '1', null, ['class'=>'flat'])}}
            </label>
        </div>
    </div>
</div>