<div class="form-group">
    {!! (isset($req) && $req === true)?'<span class="fa fa-asterisk text-danger"></span>':''!!}
    {{ isset($label)?Form::label($name, $label, ['class' => 'control-label col-xs-3']):''}}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {{ Form::file($name, [
            'class'=>'form-control',
            'disabled'=>isset($dis)?$dis:false,
            'data-tooltip-content'=>"#tooltip-".$name
        ]) }}
        @if(isset($val))
            <span>{{$val->name}} </span>
            <a href="{{ url('archivos/ver/'.$val->id) }}" target="_blank">[ ver ]</a>
            <a href="{{ url('archivos/descargar/'.$val->id) }}" target="_blank">[ descargar ]</a>
        @endif
    </div>
</div>
