<div class="form-group">
    {!! (isset($req) && $req === true)?'<span class="fa fa-asterisk text-danger"></span>':''!!}
    {{ isset($label)?Form::label($name, $label, ['class' => 'control-label col-xs-3']):''}}
    <div class="col-md-{{isset($w)?$w:6}} col-sm-{{isset($w)?$w:6}} col-xs-12">
        {{ Form::text($name, isset($val) ? $val: null, [
            'class'                => 'form-control tooltipo',
            'maxlength'            => isset($max)?$max: '',
            'placeholder'          => isset($place)?$place: '',
            'disabled'             => isset($dis)?$dis: false,
            'data-tooltip-content' => "#tooltip-".$name
        ]) }}
    </div>
</div>
@if (isset($tooltip) && $tooltip !== false)
    @include('formItems.tooltip')
@endif
