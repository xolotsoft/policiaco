<div class="form-group">
    {!! (isset($req) && $req === true)?'<span class="fa fa-asterisk text-danger"></span>':''!!}
    {{ isset($label)?Form::label($name, $label, ['class' => 'control-label col-xs-3']):''}}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {{Form::textarea($name, null, [
            'class'=>'form-control tooltipo',
            'disabled'=>isset($dis)?$dis:false,
            'placeholder'=>isset($place)?$place:'',
            'data-tooltip-content'=>"#tooltip-".$name
        ])}}
    </div>
</div>
@if (isset($tooltip) && $tooltip !== false)
    @include('formItems.tooltip')
@endif