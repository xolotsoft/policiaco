<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Nombre</th>
                <th class="column-title">Logo </th>
                <th class="column-title">Escudo </th>
                <th class="column-title">Activo </th>
                <th class="column-title no-link last"><span class="nobr">Acción</span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($municipios as $municipio)
            <tr class="even pointer">
                <td class=" ">{{$municipio->nombre}}</td>
                <td class=" ">{{ HTML::image('img/logos/'.$municipio->id.'.png', '', array('class' => 'img-listado-logo')) }}</td>
                <td class=" ">{{ HTML::image('img/escudos/'.$municipio->id.'.png', '', array('class' => 'img-listado-escudo')) }}</td>
                <td class="text-center">
                    @include('components.status', ['param' => $municipio->estado])
                </td>
                @include('components.actions', ['param' => $municipio])
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
