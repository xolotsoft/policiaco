<div class="conteiner">
    <div class ='form-horizontal form-label-left'>
        <div class="form-group ">
            {{ Form::label('Name', 'Nombre de la Convocatoria', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                    <p class="control-label col-xs-3">
                        {{$convocatoria->nombre}}
                    </p>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('puesto', 'Puestos a Concursar', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-3">
                    {{$convocatoria->puestos}}
                </p>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('fecha', 'Fecha', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-3">
                    {{$convocatoria->fecha}}
                </p>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('edadMinima', 'Edad Minima', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-3">
                    {{$convocatoria->edadMinima}}
                </p>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('edadMaxima', 'Edad Maxima', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-3">
                    {{$convocatoria->edadMaxima}}
                </p>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('documento', 'Documento', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-12">
                    <a target="_blank" data-toggle="modal" href='#modal-id'>
                        <img src="data:{{$convocatoria->archivo->type}};base64,{{$convocatoria->archivo->data}}" class="img-responsive" />
                    </a>
                </p>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('descripcion', 'Descripción', ['class' => 'control-label col-xs-3'])}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="control-label col-xs-3">
                    {{$convocatoria->descripcion}}
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-id">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Documento de convocatoria {{$convocatoria->nombre}}</h4>
            </div>
            <div class="modal-body">
                <img src="data:{{$convocatoria->archivo->type}};base64,{{$convocatoria->archivo->data}}" class="img-responsive" />
            </div>
            <div class="modal-footer">
                <a type="button" class="btn btn-primary" href="data:{{$convocatoria->archivo->type}};base64,{{$convocatoria->archivo->data}}" download="{{$convocatoria->archivo->name}}">Descargar</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>