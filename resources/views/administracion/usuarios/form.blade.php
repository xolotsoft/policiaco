@include('formItems.text', [
    'name'  => 'name',
    'label' => 'Nombre',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.text', [
    'name'  => 'username',
    'label' => 'Usuario',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.text', [
    'name'  => 'password',
    'label' => 'Contraseña',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.text', [
    'name'  => 'password',
    'label' => 'Verificar contraseña',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.select', [
    'name' => 'perfil_id',
    'label' => 'Perfil',
    'content' => $perfiles
])
@include('formItems.select', [
    'name' => 'municipio_id',
    'label' => 'Municipio',
    'content' => $municipios
])
@include('formItems.date', [
    'name'  => 'expired_at',
    'label' => 'Fecha de Expiración',
    'req'   => true
])
@include('formItems.check', [
    'name' => 'status',
    'label' => 'Activo'
])
