<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Usuario</th>
                <th class="column-title">Municipio </th>
                <th class="column-title">Perfil </th>
                <th class="column-title">Expira </th>
                <th class="column-title">Activo </th>
                <th class="column-title no-link last"><span class="nobr">Acción</span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($usuarios as $usuario)
            <tr class="even pointer">
                <td class=" ">{{$usuario->username}}</td>
                <td class=" ">{{$usuario->municipio->nombre}}</td>
                <td class=" ">{{$usuario->perfil->nombre}}</td>
                <td class=" ">{{$usuario->expired_at}}</td>
                <td class="text-center">
                    @include('components.status', ['param' => $usuario->status])
                </td>
                @include('components.actions', ['param' => $usuario])
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
