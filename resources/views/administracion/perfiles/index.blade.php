<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Usuario</th>
                <th class="column-title">Municipio </th>
                <th class="column-title">Perfil </th>
                <th class="column-title">Expira </th>
                <th class="column-title">Activo </th>
                <th class="column-title no-link last"><span class="nobr">Acción</span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($perfiles as $perfil)
            <tr class="even pointer">
                <td class=" ">{{$perfil->username}}</td>
                <td class=" ">{{$perfil->municipio->nombre}}</td>
                <td class=" ">{{$perfil->perfil->nombre}}</td>
                <td class=" ">{{$perfil->expired_at}}</td>
                <td class="text-center">
                    @include('components.status', ['param' => $evaluacion->status])
                </td>
                @include('components.actions', ['param' => $perfil])
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
