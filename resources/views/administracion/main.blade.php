<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-pencil-square-o"></i> {{ ucfirst($action) }}</h2>
                <div class="clearfix"></div>
            </div>
            <div id="tabla">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_content" id="contenido">
                        @include('administracion.'.$action.'.index')
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
