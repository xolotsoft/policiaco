<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Folio</th>
                <th class="column-title">RFC </th>
                <th class="column-title">Nombre </th>
                <th class="column-title">Porcentaje </th>
                <th class="column-title no-link last"><span class="nobr">Acción</span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($aspirantes as $aspirante)
            <tr class="even pointer">
                <td class=" ">{{$aspirante->folio}}</td>
                <td class=" ">{{$aspirante->persona->rfc}}</td>
                <td class=" ">{{ $aspirante->persona->aPaterno}} {{$aspirante->persona->aMaterno}} {{$aspirante->persona->nombre}}</td>
                <td class=" ">{{$aspirante->etapa}} %</td>
                <td class=" last">
                @if(Auth::user()->perfil_id == 1)
                <a class="btn btn-round btn-danger btn-xs pull-right" title="Eliminar" onclick="destroy({{$aspirante->id}});">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
                @endif
                <a class="btn btn-round btn-warning btn-xs pull-right" title="Editar" onclick="edit({{$aspirante->idPersona}});">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
            </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
