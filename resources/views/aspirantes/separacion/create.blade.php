{{Form::model($separaciones, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'separacion'])}}
    @include('aspirantes.separacion.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}
