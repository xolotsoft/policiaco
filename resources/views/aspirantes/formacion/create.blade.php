{{Form::model($formacion, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'formacion'])}}
    @include('aspirantes.formacion.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}
