@include('formItems.text', [
    'name' => 'material',
    'label' => 'Materia',
    'tooltip' => 'Introduzca Materia',
    'place' => 'Proteccion civil'
])
@include('formItems.date', [
    'name' => 'fechaInicio',
    'label' => 'Fecha de inicio',
    'tooltip' => 'Introduzca fecha inical de formacion DD-MM-YYYY',
    'place' => 'DD-MM-YYYY'
])
@include('formItems.date', [
    'name' => 'fechaFin',
    'label' => 'Fecha de finalización',
    'tooltip' => 'Introduzca fecha final de formacion DD-MM-YYYY',
    'place' => 'DD-MM-YYYY'
])
@include('formItems.text', [
    'name' => 'duracion',
    'label' => 'Duración',
    'tooltip' => 'Introduzca la duracion de la formacion en horas',
    'place' => '4'
])
@include('formItems.file', [
    'name' => 'certificado',
    'label' => 'Certificado',
    'val' => isset($formacion->certificado)?$formacion->archivoCertificado:null
])
@include('formItems.file', [
    'name' => 'constancia',
    'label' => 'Constancia',
    'val' => isset($formacion->constancia)?$formacion->archivoConstancia:null
])
@include('formItems.select', [
    'name' => 'cumplimiento',
    'label' => 'Cumplimiento',
    'place'   => 'Selecciona una opción',
    'content' => $cumplimiento
])
@include('formItems.select', [
    'name' => 'instancia',
    'label' => 'Instancia capacitadora',
    'place'   => 'Selecciona una opción',
    'content' => $instancias
])
@include('formItems.area', [
    'name' => 'observaciones',
    'label' => 'Observaciones',
    'tooltip' => 'Introduzca observaciones de formacion'
])
