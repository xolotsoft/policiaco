<div id="step-3">
    <div class="form-horizontal form-label-left">
        @include('formItems.date', [
          'name' => 'fechaNacimiento',
          'label' => 'Fecha de Nacimiento',
          'req' => true,
          'tooltip' => 'Seleccione fecha de nacimiento ',
          'place' =>'DD-MM-YYYY'
        ])
        @include('formItems.select', [
          'name' => 'lugarNacimiento',
          'label' => 'Lugar de Nacimiento',
          'content' => $estados,
          'place' => 'Seleciona una opción',
          'req' => true,
          'tooltip' => 'Seleccione Entidad Federativa de nacimiento'
        ])
        @include('formItems.select', [
          'name' => 'genero',
          'label' => 'Sexo',
          'content' => $genero,
          'place' => 'Seleciona una opción',
          'req' => true,
          'tooltip' => 'Seleccione genero'
        ])
        @include('formItems.select', [
          'name' => 'estadoCivil',
          'label' => 'Estado Civil',
          'content' => $civil,
          'place' => 'Seleciona una opción',
          'req' => true,
          'tooltip' => 'Seleccione estado civil'
        ])
        @include('formItems.text', [
          'name' => 'estatura',
          'label' => 'Estatura (m)',
          'place' => '1.70',
          'req' => true,
          'tooltip' => 'Introducir estatura en metros por ejemplo genero femenino 1.60 o genero masculino 1.70'
        ])
        @include('formItems.text', [
          'name' => 'peso',
          'label' => 'Peso (kg)',
          'place' => '80.5',
          'req' => true,
          'tooltip' => 'Introducir peso corporal en kilogramos por ejemplo genero femenino 55 o genero masculino 80.5 '
        ])
    </div>
</div>
