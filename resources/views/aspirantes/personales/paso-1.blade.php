<div id="step-1">
    <div class="form-horizontal form-label-left">
        @include('formItems.hide',[
            'name' => 'editable',
            'val' => $editable
        ])
        @include('formItems.text', [
            'name'  => 'curp',
            'label' => 'CURP',
            'place' => 'AAAA750931HDFZRS05',
            'req'   => true,
            'max' =>18,
            'tooltip' => 'Clave Única de Registro de Población (CURP) es un código alfanumérico único de identidad de 18 caracteres'
        ])
        @include('formItems.text', [
            'name'  => 'rfc',
            'label' => 'RFC',
            'place' => 'AAAA750931B01',
            'req'   => true,
            'max' =>13,
            'tooltip' => 'El Registro Federal de Contribuyentes (RFC) es una clave alfanumérica que se compone de 13 caracteres. Los dos primeros, generalmente corresponden al apellido paterno, el tercero a la inicial del apellido materno y el cuarto al primer nombre'
        ])
        @include('formItems.text', [
            'name'  => 'nombre',
            'label' => 'Nombre',
            'place' => 'Pablo',
            'req'   => true,
            'tooltip' => 'Introducir solo el nombre '
        ])
        @include('formItems.text', [
            'name'  => 'aPaterno',
            'label' => 'Apellido Paterno',
            'place' => 'Perez',
            'req'   => true,
            'tooltip' => 'Introducir solo apellido paterno '
        ])
        @include('formItems.text', [
            'name'  => 'aMaterno',
            'label' => 'Apellido Materno',
            'place' => 'García',
            'req'   => false,
            'dis'   => false,
            'tooltip' => 'Introducir solo apellido materno '
        ])
    </div>
</div>
