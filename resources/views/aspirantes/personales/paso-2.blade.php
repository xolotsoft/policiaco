<div id="step-2">
    <div class="form-horizontal form-label-left">
    <br/>
        @include('formItems.date', [
            'name' => 'fechaRegistro',
            'label' => 'Fecha de Registro',
            'req' => true,
            'tooltip' => 'Seleccione fecha de registro ',
            'place' =>'DD-MM-YYYY'
        ])
        @include('formItems.select', [
            'name' => 'idConvocatoria',
            'label' => 'Número de convocatoria',
            'content' => $convocatorias,
            'place' => 'Selecciona una opción',
            'req' => true,
            'tooltip' => 'Seleccione una convocatoria '
        ])
        @include('formItems.area', [
            'name' => 'detalleConvocatoria',
            'label' => 'Detalles de convocatoria',
            'dis' => true
        ])
        @include('formItems.select', [
            'name' => 'puesto',
            'label' => 'Nombre del puesto',
            'content' => $puestos,
            'place' => 'Selecciona una opción',
            'req' => true,
            'tooltip' => 'Seleccione un puesto'
        ])
        @include('formItems.text', [
            'name'  => 'folio',
            'label' => 'No. de Folio',
            'dis'   => true,
            'val'   => isset($aspirante->folio)?$aspirante->folio:$folio
        ])
    </div>
</div>
