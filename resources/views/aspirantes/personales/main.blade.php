<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
    {{Form::model($aspirante, ['class' => 'form_wizard wizard_horizontal', 'files' => true, 'id' => 'wizard'])}}
        @include('aspirantes.personales.pasos')
        @include('aspirantes.personales.paso-1')
        @include('aspirantes.personales.paso-2')
        @include('aspirantes.personales.paso-3')
        @include('aspirantes.personales.paso-4')
    {{ Form::close() }}
</div>
