<ul class="wizard_steps">
    <li>
        <a href="#step-1">
            <span class="step_no">1</span>
            <span class="step_descr">Paso 1</span>
        </a>
    </li>
    <li>
        <a href="#step-2">
            <span class="step_no">2</span>
            <span class="step_descr"> Paso 2</span>
        </a>
    </li>
    <li>
        <a href="#step-3">
            <span class="step_no">3</span>
            <span class="step_descr">Paso 3</span>
        </a>
    </li>
    <li>
        <a href="#step-4">
            <span class="step_no">4</span>
            <span class="step_descr">Paso 4</span>
        </a>
    </li>
</ul>