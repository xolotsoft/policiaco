<div class="table-responsive">
    <table class="table table-striped jambo_table datatable">
        <thead>
            <tr class="headings">
                <th class="column-title">Folio</th>
                <th class="column-title">RFC </th>
                <th class="column-title">Nombre </th>
                <th class="column-title no-link last"><span class="nobr">Acción</span>
                </th>
            </tr>
        </thead>
        <tbody>
        @foreach($aptos as $apto)
        <tr class="even pointer">
            <td class=" ">{{ $apto->folio }}</td>
            <td class=" ">{{ $apto->persona->rfc}}</td>
            <td class=" ">{{ $apto->persona->aPaterno }} {{ $apto->persona->aMaterno }} {{ $apto->persona->nombre }}</td>
            <td class=" last">
                <a class="btn btn-round btn-info btn-xs" title="Nombramiento" onClick="nombramiento({{$apto->id}},'{{$apto->folio}}')">
                    <i class="fa fa-arrow-up" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div class="modal fade" id="apto-id">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            {{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'nombrar'])}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Documento de Nombramiento <span id="folioNombramiento"></span></h4>
                </div>
                <div class="modal-body">
                        @include('formItems.hide', [
                            'name' => 'idAspirante'
                        ])
                        @include('formItems.file', [
                            'name' => 'nombramiento',
                            'label' => 'Nombramiento',
                            'req' => true,
                            'tooltip' => 'Ingresa el documento que avala el nombramiento'
                        ])   
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Incorporar</button>
                    <button type="button" class="btn btn-default" onClick="cerrarModal()">Cerrar</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>