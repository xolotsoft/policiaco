@include('formItems.select', [
    'name' => 'institucion',
    'label' => 'Institución',
    'content' => $instituciones,
    'req'   => true,
    'place' => 'Seleciona una opción'
])
@include('formItems.select', [
    'name' => 'estado',
    'label' => 'Entidad',
    'content' => $estados,
    'place' => 'Seleciona una opción',
    'req'   => true
])
@include('formItems.select', [
    'name' => 'municipio',
    'label' => 'Municipio',
    'content' => $municipios,
    'dis' => $editable ? false : true,
    'req'   => true,
    'place' => 'Seleciona una opción'
])
@include('formItems.select', [
    'name' => 'grado',
    'label' => 'Grado',
    'place' => 'Seleciona una opción',
    'content' => $puestos
])
@include('formItems.text', [
    'name' => 'antiguedad',
    'label' => 'Antigüedad',
    'label' => 'Antigüedad',
    'place' => '2',
    'content' => $puestos,
    'tooltip' => 'Introduzca Antigüedad'
])
@include('formItems.text', [
    'name' => 'nombreInstancia',
    'label' => 'Instancia Elegida',
    'place' => 'Instancia Elegida',
    'tooltip' => 'Instancia Elegida'
])
<div class="form-group">
    {{ Form::label('instanciaEvaluadora', 'Instancia Evaluadora', ['class' => 'control-label col-xs-3'])}}
    <div class="col-md-3 col-sm-3 col-xs-12">
        {{ Form::radio('instanciaEvaluadora', 'Estatal', true) }} Estatal
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12">
        {{ Form::radio('instanciaEvaluadora', 'Federal') }} Federal
    </div>
</div>
@include('formItems.date', [
    'name' => 'fechaPresentacion',
    'label' => 'Fecha presentación de exámenes',
    'place' => 'DD-MM-YYYY',
    'tooltip' => 'Seleccione una fecha'
])
@include('formItems.area', [
    'name' => 'observaciones',
    'label' => 'Observaciones',
    'tooltip' => 'Introduzca alguna observacion del familiar seleccionado'
])
