{{Form::model($reingresos, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'reingresos'])}}
    @include('aspirantes.reingresos.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}
