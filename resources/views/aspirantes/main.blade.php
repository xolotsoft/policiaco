<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-server"></i> {{ titulo($action) }}</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                    @if($action !== 'aptosParaNombramiento')
                        {{ HTML::image('img/persona.png', '', array('class' => 'fotografia')) }}
                    @endif
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="contenido">
                @include('aspirantes.'.$action.'.index')
            </div>
        </div>
    </div>
</div>
