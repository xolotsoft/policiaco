<div class="nav-tabs-responsive" role="tabpanel" data-example-id="togglable-tabs">
    @include('aspirantes.nuevo.tabs')
    <div id="myTabContent" class="tab-content">
        <br>
        @include('aspirantes.personales.main')
        @include('aspirantes.domicilio.main')
        @include('aspirantes.familiares.main')
        @include('aspirantes.reclutamiento.main')
        @include('aspirantes.seleccion.main')
        @include('aspirantes.formacion.main')
        @include('aspirantes.certificacion.main')
        @include('aspirantes.reingresos.main')
        @include('aspirantes.separacion.main')
        @include('aspirantes.evaluaciones.main')
    </div>
</div>
