<ul id="myTab" class="nav nav-tabs nav-pills" role="tablist">
    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Datos Personales</a>
    </li>
    <li role="presentation" class="disabled"><a href="#tab_content2" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Domicilio</a>
    </li>
    <li role="presentation" class="disabled"><a href="#tab_content3" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Familiares</a>
    </li>
    <li role="presentation" class="disabled"><a href="#tab_content4" role="tab" id="profile-tab4" data-toggle="tab" aria-expanded="false">Reclutamiento</a>
    </li>
    <li role="presentation" class="disabled"><a href="#tab_content5" role="tab" id="profile-tab5" data-toggle="tab" aria-expanded="false">Selección</a>
    </li>
    <li role="presentation" class="disabled"><a href="#tab_content6" role="tab" id="profile-tab6" data-toggle="tab" aria-expanded="false">Formación Inicial</a>
    </li>
    <li role="presentation" class="disabled"><a href="#tab_content7" role="tab" id="profile-tab7" data-toggle="tab" aria-expanded="false">Certificación y Nombramiento</a>
    </li>
    <li role="presentation" class="disabled"><a href="#tab_content8" role="tab" id="profile-tab8" data-toggle="tab" aria-expanded="false">Reingreso Labor Policial</a>
    </li>
    <li role="presentation" class="disabled"><a href="#tab_content9" role="tab" id="profile-tab9" data-toggle="tab" aria-expanded="false">Baja Separación</a>
    </li>
    <li role="presentation" class="disabled"><a href="#tab_content10" role="tab" id="profile-tab10" data-toggle="tab" aria-expanded="false">Evaluaciones de control y confianza</a>
    </li>
</ul>
