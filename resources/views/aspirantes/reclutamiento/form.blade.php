@include('formItems.hide',[
    'name' => 'editable',
    'val' => $editable
])
@include('formItems.file', [
    'name' => 'perfilSolicitado',
    'label' => 'Perfil Solicitado',
    'req'   => false,
    'val' => isset($reclutamiento->perfilSolicitado)?$reclutamiento->archivoPerfilSolicitado:null
])
@include('formItems.file', [
    'name' => 'documento',
    'label' => 'Documento',
    'req'   => false,
    'val' => isset($reclutamiento->documento)?$reclutamiento->archivoDocumento:null
])
@include('formItems.file', [
    'name' => 'elector',
    'label' => 'Credencial de elector',
    'req'   => true,
    'val' => isset($reclutamiento->elector)?$reclutamiento->archivoElector:null
])
@include('formItems.file', [
    'name' => 'nacimiento',
    'label' => 'Acta de Nacimiento',
    'req'   => true,
    'val' => isset($reclutamiento->nacimiento)?$reclutamiento->archivoNacimiento:null
])
@include('formItems.file', [
    'name' => 'cartilla',
    'label' => 'Cartilla de Servicio Militar',
    'req'   => true,
    'val' => isset($reclutamiento->cartilla)?$reclutamiento->archivoCartilla:null
])
@include('formItems.file', [
    'name' => 'penales',
    'label' => 'Antecedentes no penales (mínimo 6 meses)',
    'req'   => true,
    'val' => isset($reclutamiento->penales)?$reclutamiento->archivoPenales:null
])
@include('formItems.file', [
    'name' => 'secundaria',
    'label' => 'Secundaria',
    'req'   => true,
    'val' => isset($reclutamiento->secundaria)?$reclutamiento->archivoSecundaria:null
])
@include('formItems.file', [
    'name' => 'bachillerato',
    'label' => 'Bachillerato',
    'req'   => true,
    'val' => isset($reclutamiento->bachillerato)?$reclutamiento->archivoBachillerato:null
])
@include('formItems.file', [
    'name' => 'tecnico',
    'label' => 'Certificado Técnico',
    'req'   => false,
    'val' => isset($reclutamiento->tecnico)?$reclutamiento->archivoTecnico:null
])
@include('formItems.file', [
    'name' => 'licenciatura',
    'label' => 'Licenciatura',
    'req'   => false,
    'val' => isset($reclutamiento->licenciatura)?$reclutamiento->archivoLicenciatura:null
])
@include('formItems.select', [
    'name' => 'posgrado',
    'label' => 'Posgrado',
    'content' => $posgrados,
    'req'   => false,
    'place' => 'Seleciona una opción',
    'add' => true
])
@include('formItems.file', [
    'name' => 'maestria',
    'label' => 'Certificado de posgrado',
    'req'   => false,
    'val' => isset($reclutamiento->maestria)?$reclutamiento->archivoMaestria:null
])
@include('formItems.file', [
    'name' => 'bajaPolicial',
    'label' => 'Certificado de baja policial',
    'req'   => false,
    'val' => isset($reclutamiento->bajaPolicial)?$reclutamiento->archivoBajaPolicial:null
])
@include('formItems.file', [
    'name' => 'fotoInfantil',
    'label' => 'Fotografía',
    'req'   => true,
    'val' => isset($reclutamiento->fotoInfantil)?$reclutamiento->archivoFotoInfantil:null
])
@include('formItems.file', [
    'name' => 'comprobanteDomicilio',
    'label' => 'Comprobante de domicilio',
    'req'   => true,
    'val' => isset($reclutamiento->comprobanteDomicilio)?$reclutamiento->archivoComprobanteDomicilio:null
])
@include('formItems.file', [
    'name' => 'motivosIngreso',
    'label' => 'Carta con motivos de ingreso',
    'req'   => true,
    'val' => isset($reclutamiento->motivosIngreso)?$reclutamiento->archivoMotivosIngreso:null
])
@include('formItems.file', [
    'name' => 'refPersonales1',
    'label' => 'Referencias personales 1',
    'req'   => true,
    'val' => isset($reclutamiento->refPersonales1)?$reclutamiento->archivoRefPersonales1:null
])
@include('formItems.file', [
    'name' => 'refPersonales2',
    'label' => 'Referencias personales 2',
    'req'   => true,
    'val' => isset($reclutamiento->refPersonales2)?$reclutamiento->archivoRefPersonales2:null
])
@include('formItems.file', [
    'name' => 'nacimientoHijo',
    'label' => 'Acta de nacimiento de hijos',
    'req'   => false,
    'val' => isset($reclutamiento->nacimientoHijo)?$reclutamiento->archivoNacimientoHijo:null
])
@include('formItems.file', [
    'name' => 'matrimonio',
    'label' => 'Acta de matrimonio',
    'req'   => false,
    'val' => isset($reclutamiento->matrimonio)?$reclutamiento->archivoMatrimonio:null
])
@include('formItems.file', [
    'name' => 'sociedadConyugal',
    'label' => 'Acta de sociedad conyugal',
    'req'   => false,
    'val' => isset($reclutamiento->sociedadConyugal)?$reclutamiento->archivoSociedadConyugal:null
])
@include('formItems.file', [
    'name' => 'manejo',
    'label' => 'Licencia de manejo',
    'req'   => true,
    'val' => isset($reclutamiento->manejo)?$reclutamiento->archivoManejo:null
])
@include('formItems.file', [
    'name' => 'documentoCurp',
    'label' => 'CURP',
    'req'   => true,
    'val' => isset($reclutamiento->documentoCurp)?$reclutamiento->archivoDocumentoCurp:null
])
@include('formItems.file', [
    'name' => 'documentoRfc',
    'label' => 'RFC',
    'req'   => true,
    'val' => isset($reclutamiento->documentoRfc)?$reclutamiento->archivoDocumentoRfc:null
])
@include('formItems.select', [
    'name' => 'serviciosMedicos',
    'label' => 'Servicios médicos',
    'content' => $servicios,
    'req'   => true,
    'place' => 'Seleciona una opción',
    'add' => true
])
@include('formItems.file', [
    'name' => 'estadoCuenta',
    'label' => 'Estados de cuenta bancarios',
    'req'   => false,
    'val' => isset($reclutamiento->estadoCuenta)?$reclutamiento->archivoEstadoCuenta:null
])
@include('formItems.file', [
    'name' => 'solicitud',
    'label' => 'Solicitud de Ingreso',
    'req'   => true,
    'val' => isset($reclutamiento->solicitud)?$reclutamiento->archivoSolicitud:null
])
@include('formItems.file', [
    'name' => 'curriculum',
    'label' => 'Currículum',
    'req'   => false,
    'val' => isset($reclutamiento->curriculum)?$reclutamiento->archivoCurriculum:null
])
@include('formItems.select', [
    'name' => 'asimilamientoGradoPolicial',
    'label' => 'Asimilamiento Grado Policial',
    'content' => $asimilamiento,
    'req'   => false,
    'place' => 'Seleciona una opción',
])
@include('formItems.area', [
    'name' => 'observacionesReclutamiento',
    'label' => 'Observaciones',
    'tooltip' => 'Introduzca alguna observacion del familiar seleccionado'
])
