{{Form::model($reclutamiento, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'reclutamiento'])}}
    @include('aspirantes.reclutamiento.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}