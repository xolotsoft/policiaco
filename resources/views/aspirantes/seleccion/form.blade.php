@include('formItems.select', [
    'name'    => 'resultadoConfianza',
    'label'   => 'Resultado de control de confianza',
    'place'   => 'Selecciona una opción',
    'content' => $cat
])
@include('formItems.area', [
    'name' => 'observaciones',
    'label' => 'Observaciones',
    'tooltip' => 'Introduzca observaciones del area de seleccion'
])
