{{Form::model($seleccion, ['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'seleccion'])}}
    @include('aspirantes.seleccion.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}
