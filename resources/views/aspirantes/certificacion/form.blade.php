@include('formItems.select', [
    'name' => 'cargo',
    'label' => 'Categoría Policial',
    'place' => 'Seleciona una opción',
    'content' => $categoriaPolicial,
])
@include('formItems.select', [
    'name' => 'puesto',
    'label' => 'Grado Policial',
    'place' => 'Seleciona una opción',
    'content' => $grados,
    'dis' => !$editable
])
@include('formItems.date', [
    'name' => 'fechaIngreso',
    'label' => 'Fecha de ingreso',
    'place' => 'DD-MM-YYYY',
    'tooltip' => 'Seleccione Fecha de Ingreso '
])
@include('formItems.text', [
    'name' => 'adscripcion',
    'label' => 'Area o unidad de adscripción',
    'place' => 'Área de Contrainteligencia',
    'tooltip' => 'Introduzca un area o unidad de nombramiento y certifiacion'
])
@include('formItems.text', [
    'name' => 'nombreCurso',
    'label' => 'Nombre de Curso',
    'place' => 'Nombre de Curso',
    'tooltip' => 'Nombre de Curso'
])
@include('formItems.select', [
    'name' => 'formacionInicial',
    'label' => 'Formación Inicial',
    'place' => 'Seleciona una opción',
    'content' => $formaciones,
    'dis' => false
])
@include('formItems.text', [
    'name' => 'calificacion',
    'label' => 'Calificación de curso de formación inicial',
    'place' => '10',
    'tooltip' => 'Introduzca calificacion asignada por instructor'
])
@include('formItems.file', [
    'name' => 'documentoCurso',
    'label' => 'Documento que avala el curso',
    'val' => isset($certificacion->documentoCurso)?$certificacion->archivoDocumentoCurso:null
])
@include('formItems.file', [
    'name' => 'nombramiento',
    'label' => 'Nombramiento',
    'val' => isset($certificacion->nombramiento)?$certificacion->archivoNombramiento:null
])
@include('formItems.area', [
    'name' => 'observaciones',
    'label' => 'Observaciones',
    'tooltip' => 'Introduzca alguna observacion del familiar seleccionado'
])
