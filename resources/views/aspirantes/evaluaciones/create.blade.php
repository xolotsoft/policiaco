<div id="listadoEvaluaciones">
    @include('aspirantes.evaluaciones.listado')
</div>
<div id="formcontrol">
{{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true, 'id'=>'evaluaciones'])}}
    @include('aspirantes.evaluaciones.form')
    @include('formItems.submit', ['label' => 'Guardar'])
{{ Form::close() }}
</div>
