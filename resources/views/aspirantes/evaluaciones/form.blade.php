@include('formItems.select', [
    'name' => 'examen',
    'label' => 'Examen',
    'content' => $examenes,
    'req'   => true,
    'place' => 'Selecciona una opción'
])
@include('formItems.date', [
    'name' => 'fecha',
    'label' => 'Fecha de presentación del examen',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.date', [
    'name' => 'fechaResultados',
    'label' => 'Fecha de entrega de resultados del examen',
    'place' => '',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.select', [
    'name'  => 'resultado',
    'label' => 'Resultado de examen',
    'content' => $resultados,
    'place' => 'Selecciona una opción',
    'req'   => true,
    'tooltip' => false
])
@include('formItems.area', [
    'name' => 'observaciones',
    'label' => 'Observaciones',
    'place' => '',
    'req'   => false,
    'tooltip' => false
])
