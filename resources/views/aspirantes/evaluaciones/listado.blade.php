<div class="table-responsive">
    <table class="table table-striped jambo_table">
        <thead>
            <tr class="headings">
                <th class="column-title">Examen</th>
                <th class="column-title">Resultado</th>
                <th class="column-title">Fecha de presentación</th>
                <th class="column-title">Fecha de resultados</th>
                <th class="column-title no-link last"><span class="nobr">Acción</span></th>
            </tr>
        </thead>
        <tbody>
            @foreach($evaluaciones as $evaluacion)
            <tr class="even pointer">
                <td class=" ">{{$evaluacion->catalogos->nombre}}</td>
                <td class=" ">{{$evaluacion->cumplimiento->nombre}}</td>
                <td class=" ">{{$evaluacion->fecha}}</td>
                <td class=" ">{{$evaluacion->fechaResultados}}</td>
                <td class=" last">
                    {{--  <a class="btn btn-round btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Mostrar"><i class="fa fa-eye" aria-hidden="true" onclick="showcontrol({{$evaluacion->id}});"></i></a>  --}}
                    <a class="btn btn-round btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil" aria-hidden="true" onclick="editcontrol({{$evaluacion->id}});"></i></a>
                    @if(Auth::user()->perfil_id == 1 || Auth::user()->perfil_id == 2)
                    <a class="btn btn-round btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times" aria-hidden="true" onclick="destroycontrol({{$evaluacion->id}});"></i></a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
