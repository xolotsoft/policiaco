<div class="table-responsive">
    <table class="table table-striped jambo_table">
        <thead>
            <tr class="headings">
                <th class="column-title">Nombre </th>
                <th class="column-title">Apellidos </th>
                <th class="column-title">Parentesco </th>
                <th class="column-title no-link last"><span class="nobr">Acción</span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($familiares as $familiar)
            <tr class="even pointer">
                <td class=" ">{{$familiar->nombre}}</td>
                <td class=" ">{{$familiar->aPaterno.' '.$familiar->aMaterno}}</td>
                <td class=" ">{{$familiar->catalogos->nombre}}</td>
                <td class=" last">
                    {{--  <a class="btn btn-round btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Mostrar"><i class="fa fa-eye" aria-hidden="true" onclick="showFamiliar({{$familiar->id}});"></i></a>  --}}
                    <a class="btn btn-round btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil" aria-hidden="true" onclick="editFamiliar({{$familiar->id}});"></i></a>
                    @if(Auth::user()->perfil_id == 1 || Auth::user()->perfil_id == 2)
                    <a class="btn btn-round btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-times" aria-hidden="true" onclick="destroyFamiliar({{$familiar->id}});"></i></a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
