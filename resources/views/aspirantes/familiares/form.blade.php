@include('formItems.select', [
    'name' => 'categoria',
    'label' => 'Categoría',
    'content'=>$parentesco,
    'req'   => true,
    'place' => 'Seleciona una opción'
])
@include('formItems.text', [
    'name' => 'nombre',
    'label' => 'Nombre',
    'req'   => true,
    'place' => 'Claudia',
    'tooltip' => 'Introduzca el nombre del familiar seleccionado'
])
@include('formItems.text', [
    'name' => 'aPaterno',
    'label' => 'Apellido Paterno',
    'req'   => true,
    'place' => 'Perez',
    'tooltip' => 'Introduzca el primer apellido del familiar seleccionado'
])
@include('formItems.text', [
    'name' => 'aMaterno',
    'label' => 'Apellido Materno',
    'req'   => true,
    'place' => 'Garcia',
    'tooltip' => 'Introduzca el segundo apellido del familiar seleccionado'
])
@include('formItems.date', [
    'name' => 'fechaNacimiento',
    'label' => 'Fecha de nacimiento',
    'req'   => true,
    'place' => 'DD-MM-YYYY',
    'tooltip' => 'Introduzca fecha de nacimiento del familiar seleccionado'
])
@include('formItems.select', [
    'name' => 'genero',
    'label' => 'Sexo',
    'content' => $genero,
    'req'   => true,
    'place' => 'Seleciona una opción'
])
@include('formItems.text', [
    'name' => 'direccion',
    'label' => 'Dirección',
    'req'   => true,
    'place' => 'Calle numero,Colonia,Municipio o Delegacion y Codigo Postal',
    'tooltip' => 'Introduzca la direccion completa del familiar seleccionado'
])
@include('formItems.text', [
    'name' => 'telefono',
    'label' => 'Teléfono',
    'place' => '5521582312',
    'req'   => true,
      'tooltip' => 'Introduzca 10 digitos de su numero telefonico fijo del familiar seleccionado'
])
@include('formItems.area', [
    'name' => 'observacionesFamiliares',
    'label' => 'Observaciones',
    'tooltip' => 'Introduzca alguna observacion del familiar seleccionado'
])
