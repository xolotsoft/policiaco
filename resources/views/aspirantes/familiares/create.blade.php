<div id="listadoFamiliares">
    @include('aspirantes.familiares.listado')
</div>
<div id="formFamiliares">
    {{Form::open(['class' => 'form-horizontal form-label-left', 'files' => true, 'id' => 'familiares'])}}
        @include('aspirantes.familiares.form')
        @include('formItems.submit', ['label' => 'Guardar'])
    {{ Form::close() }}
</div>
