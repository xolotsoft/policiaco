@include('formItems.hide',[
    'name' => 'editable',
    'val' => $editable
])
@include('formItems.text', [
    'name'  => 'calle',
    'label' => 'Calle',
    'place' => 'Siempre viva',
    'req'   => true,
    'tooltip' => 'Ingresar nombre de calle donde vive actualmente'
])
@include('formItems.text', [
    'name' => 'exterior',
    'label' => 'Número Exterior',
    'place' => '59',
    'req'   => true,
    'tooltip' => 'Ingresar numero exterior donde habita'
])
@include('formItems.text', [
    'name' => 'interior',
    'label' => 'Número Interior',
    'place' => '2',
    'tooltip' => 'Ingresar numero interior donde habita'
])
@include('formItems.select', [
    'name' => 'pais',
    'label' => 'País',
    'content' => $paises,
    'req'   => true
])
@include('formItems.select', [
    'name' => 'estado',
    'label' => 'Estado',
    'content' => $estados,
    'place' => 'Seleciona una opción',
    'req'   => true
])
@include('formItems.select', [
    'name' => 'municipio',
    'label' => 'Municipio',
    'content' => $municipios,
    'dis' => $editable ? false : true,
    'req'   => true,
    'place' => 'Seleciona una opción'
])
@include('formItems.select', [
    'name' => 'colonia',
    'label' => 'Colonia',
    'content' => $colonias,
    'dis' => $editable ? false : true,
    'req'   => true,
    'place' => 'Seleciona una opción',
    'add' => true
])
@include('formItems.text', [
    'name' => 'cp',
    'label' => 'Código Postal',
    'place' => '05000',
    'req'   => true,
    'tooltip' => 'Ingrese el codigo postal donde habita'
])
@include('formItems.area', [
    'name' => 'observacionesDomicilio',
    'label' => 'Observaciones',
    'tooltip' => 'Introduzca alguna observacion del familiar seleccionado'
])
